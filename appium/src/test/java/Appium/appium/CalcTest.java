package Appium.appium;


import java.net.URL;

import org.aspectj.lang.annotation.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class CalcTest {
	
	static AppiumDriver<MobileElement> driver;
	
	
	public static void main(String[] args) {
		try {
			OpenCalc();
		} catch (Exception e) {
			System.out.println(e.getCause());
			e.printStackTrace();
		}
		
	}
	
	
	public static void OpenCalc() throws Exception
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Galaxy On8");
//		caps.setCapability("udid", "10.24.27.237:2222"); //Give Device ID of your mobile phone
		
		caps.setCapability("udid", "3300278dcb181473");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "7.0");
		caps.setCapability("appPackage", "com.sec.android.app.popupcalculator");
		caps.setCapability("appActivity", "com.sec.android.app.popupcalculator.Calculator");
		
		driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		System.out.println("application statred");			

		
		MobileElement two=driver.findElement(By.xpath("android.widget.Button[@resource-id='com.sec.android.app.popupcalculator:id/bt_02']"));		
		System.out.println("clicked on 2.");
		MobileElement plus=driver.findElement(By.xpath("com.sec.android.app.popupcalculator:id/bt_add"));
		MobileElement thr=driver.findElement(By.xpath("com.sec.android.app.popupcalculator:id/bt_03"));		
		MobileElement res=driver.findElement(By.xpath("com.sec.android.app.popupcalculator:id/bt_equal"));
		
		two.click();
		plus.click();
		thr.click();
	}
	
	
}
