package Appium.appium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class EmulatorTest {

	static AppiumDriver<MobileElement> driver;
	
	public static void main(String[] args) {
		OpenApp();
	}
	
	public static void OpenApp()
	{
		//Set the Desired Capabilities
				DesiredCapabilities caps = new DesiredCapabilities();
				caps.setCapability("deviceName", "Android SDK built for x86");
				caps.setCapability("udid", "emulator-5554"); //Give Device ID of your mobile phone
				caps.setCapability("platformName", "Android");
				caps.setCapability("platformVersion", "9");
				caps.setCapability("appPackage", "com.google.android.apps.nexuslauncher");
				caps.setCapability("appActivity", "com.google.android.apps.nexuslauncher.NexusLauncherActivity");
				caps.setCapability("noReset", "true");

				//Instantiate Appium Driver
				try {
					driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
					System.out.println("application statred");

				} catch (MalformedURLException e) {
					System.out.println(e.getMessage());
				}

	}

}
