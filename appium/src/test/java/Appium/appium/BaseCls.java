package Appium.appium;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseCls {
	static AppiumDriver<MobileElement> driver;
	
	@BeforeTest
	public void SetUp()
	{
		try {
		DesiredCapabilities caps=new DesiredCapabilities();
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
		caps.setCapability(MobileCapabilityType.VERSION, "7");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy On8");
		caps.setCapability(MobileCapabilityType.UDID, "3300278dcb181473");
		caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
//		caps.setCapability(MobileCapabilityType.APP, "ANDROID");
		caps.setCapability(MobileCapabilityType.BROWSER_NAME, "CHROME");
		
		
			URL url=new URL("http://127.0.0.1:4723/wd/hub");
			
			driver = new AndroidDriver<MobileElement>(url, caps);
			System.out.println("application statred");
			
		} catch (Exception e) {
			System.out.println("error cause : "+e.getCause());
			e.printStackTrace();
		}
		
	}
	
//	@Test
//	public void SampleTest()
//	{
//		System.out.println("Chrome opened.");
//	}
	
	@AfterTest
	public void TearDown()
	{
		driver.quit();
	}

}
