package com.si.rest.Functional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.testng.annotations.Test;
import static common.Filereader.Config; 
import common.BaseTest;
import io.restassured.response.Response;
import restutilities.BasicApiFunctions;


public class Resttest extends BaseTest {
	
	
	
	@Test
	public static void testcase1() throws IOException 
	{
	String RequestData = ""; 
	String filePath =  Config.getProperty("input1");
	System.out.println("################3"+filePath);
    try  
    {  
    RequestData = new String ( Files.readAllBytes( Paths.get(filePath) ) );
	System.out.println("################3"+RequestData);
    }  
    catch (IOException e)  
   { 
  	  e.printStackTrace(); 
   }  
        Response response = BasicApiFunctions.RequestwithoutSSL("/create", RequestData).post();		
	    BasicApiFunctions.printResponse(response);
	    BasicApiFunctions.verifyStatusCode(response);
	    BasicApiFunctions.verifyResponseTime(response);
	    BasicApiFunctions.verifyResponseValue(response);
    
	}
	

}
