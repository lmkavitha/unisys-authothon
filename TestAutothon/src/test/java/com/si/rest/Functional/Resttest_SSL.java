package com.si.rest.Functional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.testng.annotations.Test;
import static common.Filereader.Config; 
import common.BaseTest;
import io.restassured.response.Response;

import restutilities.BasicApiFunctions;
import restutilities.RestLibFunction;


public class Resttest_SSL extends BaseTest {
	
	
	
	@Test
	public static void testcase1() throws IOException 
	{

    Response response = RestLibFunction.createIdentityResponse("Ebtsinput", "ebtsFormat");
	BasicApiFunctions.printResponse(response);
    
    
	}

}
