package com.si.selenium;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.BaseTest;
import seleniumpageobjects.HomePage;
import seleniumpageobjects.LoginPage;

import common.BaseTest;




public class FTC002_VerifyInvalidUserLogin extends BaseTest{
	
	LoginPage loginpage;
	HomePage homepage;
	
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver =initializeDriver();
		
	}
	
	@Test
	public void navigateToBackOfficeApplication() throws Exception {
		
		// Create login page instance
		loginpage = new LoginPage(driver);
		
		// navigate to back office application		
		loginpage.navigateToBO();
		
		// verify user is navigated to login page '
		
		loginpage.verifyLoginPageIsDisplayed();
		
	}
	
	
	@Test
	public void verifyInvalidUserLogin() throws Exception {
				
		// enter user name in login page			
		//loginpage.enterLoginUsername(username);
		loginpage.enterLoginUsername("invalidusername");
		
		// enter password in login page
		loginpage.enterLoginPassword("invalidpassword");		
		
		// click on Login button
		loginpage.clickOnLogin();
		
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		// verify login error message displayed
		loginpage.verifyIncorrectCredntialsErrorMsg();		
	
	}
	

}
