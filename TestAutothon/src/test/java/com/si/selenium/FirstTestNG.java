package com.si.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FirstTestNG {
	
	WebDriver driver;
	
	@BeforeTest
	public void setUp() {
		
		//System.setProperty("webdriver.ie.driver", "C:\\Users\\GowdaHar\\workspace\\Automation\\resources\\drivers\\IEDriverServer.exe");
		//driver = new InternetExplorerDriver();			
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\GowdaHar\\workspace\\Automation\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();	
		
	}
	
	
	@Test
	public void login() throws InterruptedException {
				
		driver.manage().window().maximize();
		
		driver.manage().deleteAllCookies();
		
		//driver.get("http://ustr-erl2-3022:8301/stealth-identity-backofficeweb/");
		driver.get("https://inblr-nvm-015.eu.uis.unisys.com:8444/stealth-identity-backofficeweb/");

		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("login-logo")));
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.id("eidUserName")).sendKeys("superuser");
		
		driver.findElement(By.id("eidPwd")).sendKeys("Unisys12");
		
		driver.findElement(By.cssSelector("button[id=btnLogin]")).sendKeys(Keys.ENTER);		
			
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//driver.findElement(By.id("userInitials")).isDisplayed();		
		
		//driver.findElement(By.cssSelector("input[id=eidUserName]")).sendKeys("superuser");
	
	}
	
	@AfterTest
	public void tearDown() {
		
		//driver.close();
		driver.quit();
	}

}
