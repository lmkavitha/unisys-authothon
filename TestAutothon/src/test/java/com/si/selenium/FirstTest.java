package com.si.selenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FirstTest {

	public static void main(String[] args) throws InterruptedException, AWTException {
		
		//String username = "Harshith"+ new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date());
		//String password = "Unisys"+ new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date());
		
		//System.setProperty("webdriver.ie.driver", "C:\\Users\\GowdaHar\\workspace\\Automation\\resources\\drivers\\IEDriverServer.exe");
		//WebDriver driver = new InternetExplorerDriver();
		
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\GowdaHar\\workspace\\AutomationComplete\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		
		
		/*ChromeOptions option= new ChromeOptions();
		//option.addArguments("headless");
		option.addArguments("ignore-certificate-errors");
		WebDriver driver=new ChromeDriver(option);*/
		
		Thread certSelectionThread = null;
	    Runnable r = new Runnable() {
		
		
		 @Override
	     public void run() {
	      try {
	       Thread.sleep(1000 * 10);
	       Robot robot = new Robot();
	       robot.keyPress(KeyEvent.VK_ENTER);
	      } catch (AWTException e) {
	       e.printStackTrace();
	      } catch (InterruptedException e) {
	       e.printStackTrace();
	      }
	     }
	    };
	    certSelectionThread = new Thread(r);
	    certSelectionThread.start();
		
     
		
		driver.manage().window().maximize();
		
		driver.manage().deleteAllCookies();
		
		 Thread.sleep(3000);
			
			/*Robot robot = new Robot();
		      // robot.keyPress(KeyEvent.VK_ENTER);	       
		       robot.keyPress(KeyEvent.VK_ESCAPE);;*/
		
		//driver.get("http://ustr-erl2-3023:8301/stealth-identity-backofficeweb/");
		
		driver.get("https://inblr-nvm-022.eu.uis.unisys.com:8444/stealth-identity-backofficeweb/");

		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("login-logo")));
		
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		int range = 5;
		for(int i =1; i <= range;) {
		
		driver.findElement(By.id("eidUserName")).clear();
		driver.findElement(By.id("eidUserName")).sendKeys("Harshith"+ new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date()));
		
		driver.findElement(By.id("eidPwd")).clear();
		driver.findElement(By.id("eidPwd")).sendKeys("Unisys"+ new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date()));
		
		driver.findElement(By.id("btnLogin")).click();
		
		//		Thread.sleep(2000);
		
		i++;
		
		System.out.println("Count >>>"+i);
		}
		
		//driver.findElement(By.xpath("//a[@data-menu-target='confMgmtTransactionSettings']"));
		
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
		//driver.findElement(By.cssSelector("input[id=eidUserName]")).sendKeys("superuser");
		
		
		
	//	#eidUserName
		
	//	driver.close();
		driver.quit();

	}

}
