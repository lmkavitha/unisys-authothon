package restutilities;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import common.Filereader;
import common.BaseTest;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import restutilities.BasicApiFunctions;
public class RestLibFunction extends BaseTest

{
	
	
public static String projDir = BaseTest.getRootDirectory();



/*public static Response createGallery(String inputData, String format) throws IOException 
{	
	JSONObject requestParams = new JSONObject();

	Response response = ApiFunctions.setHttpRequest("/create", requestParams.toString()).post();			
	return response;
	
}*/



public static Response createIdentityResponse(String inputData, String format) throws IOException 
{	
	JSONObject requestParams = new JSONObject();
	System.out.println("inputdata : "+inputData);
	System.out.println("url"+Config.getProperty("url"));
	String filePath = Config.getProperty(inputData);
	System.out.println(filePath);
	String encodedData = CustomLib.encodeFileToBase64Binary(new File(filePath));
	requestParams.put("requestData", encodedData);
	requestParams.put("format", Config.getProperty(format));
	Response response = BasicApiFunctions.RequestwithSSL("/CreateIdentity", requestParams.toString()).post();			
	return response;
	
}
//Added by Chithra
// To update UPID and Transaction Type in the Input XML 

public  static void modifyIdentity(String IdentityValue , String filepath){
    try{
    	String filePath =  Config.getProperty(filepath);
           DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
           DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
           Document doc = docBuilder.parse(new File(filePath));
           
           
           
           
           Node nodeTransactiontype =  doc.getElementsByTagName("TransactionType").item(0);
           nodeTransactiontype.setTextContent("UPID");
          Node nodeIdentityId =  doc.getElementsByTagName("IdentityID").item(0);
          nodeIdentityId.setTextContent(IdentityValue);
        
                        
                        
                          TransformerFactory transformerFactory = TransformerFactory.newInstance();
                          Transformer transformer = transformerFactory.newTransformer();
                          DOMSource source = new DOMSource(doc);
                          StreamResult result = new StreamResult(new File(filePath));
                          transformer.transform(source, result);
                          System.out.println("File Updated succesfully" );
      }
   
      catch (Exception e) 
      {
          e.printStackTrace();
      }
    }
//Added by Chithra
//To Fetch the First name,middlename and Last name from the database before the Identity update

public static String getBeforevaluesFromDB(String Registrationnumber ,String Flag  ) throws ClassNotFoundException, SQLException {
    //Accessing driver from the JAR file 
    Class.forName("oracle.jdbc.OracleDriver");
    //System.out.println("Oracle JDBC driver loaded ok.");

    Connection con=DriverManager.getConnection("jdbc:oracle:thin:@inblr-nvm-031.eu.uis.unisys.com:1521:orcl","March18","Unisys12"); 


    Statement stmt = con.createStatement();

    ResultSet result = stmt.executeQuery("Select FIRST_NAME ,MIDDLE_NAME ,LAST_NAME from TBL_FR_PERSON WHERE PERSON_ID="+Registrationnumber);

    
    while(result.next()){
        String Name1 = result.getString("FIRST_NAME");
        String Name2 = result.getString("MIDDLE_NAME");
        String Name3 = result.getString("LAST_NAME");
        String Name4 = result.getString("GENDER");
        String Name5 = result.getString("DOB");
        
        if (Flag == "FIRST_NAME")
        {
        	
        	System.out.println("Before Update" +"FirstName = " + Name1);
             return Name1;     
        }
       
        else if(Flag == "MIDDLE_NAME") 
        
        {
        	
        	System.out.println("Before Update" + "MiddleName = " + Name2 );
            return Name2;
        }
        
        
        else if(Flag == "LAST_NAME")
        {
        	
        	System.out.println("Before Update" + "LastName = " + Name3);
        	return Name3;
        }
      
        else if(Flag == "GENDER")
        {
        	
        	System.out.println("Before Update" + "Gender = " + Name4);
        	return Name4;
        }
        	else 
        	{	
        		
        		System.out.println("Before Update" + "DOB = " + Name5);
            	return Name5;
            
    }
    }
    con.close();
	return Flag;
    
        }

public static Response updateIdentityResponse(String inputData, String format) throws IOException 
{
	JSONObject requestParams = new JSONObject();
	Properties prop = new Properties();
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        
   prop.load(classLoader.getResourceAsStream("Config.properties"));
   String filePath = classLoader.getResource(prop.getProperty(inputData)).getFile();
	String encodedData = CustomLib.encodeFileToBase64Binary(new File(filePath));
	requestParams.put("requestData", encodedData);
	requestParams.put("format", prop.getProperty(format));
	Response response = BasicApiFunctions.RequestwithoutSSL("/CreateIdentity", requestParams.toString()).post();			
	return response;
	
}
public static String getAftervaluesFromDB(String Registrationnumber , String Flag) throws ClassNotFoundException, SQLException {
    //Accessing driver from the JAR file 
    Class.forName("oracle.jdbc.OracleDriver");
    //System.out.println("Oracle JDBC driver loaded ok.");

    Connection con=DriverManager.getConnection("jdbc:oracle:thin:@inblr-nvm-031.eu.uis.unisys.com:1521:orcl","March18","Unisys12"); 


    Statement stmt = con.createStatement();

    ResultSet result = stmt.executeQuery("Select FIRST_NAME ,MIDDLE_NAME ,LAST_NAME from TBL_FR_PERSON WHERE PERSON_ID="+Registrationnumber);

    
    while(result.next()){
    	
    	
    	String Name1 = result.getString("FIRST_NAME");
        String Name2 = result.getString("MIDDLE_NAME");
        String Name3 = result.getString("LAST_NAME");
        String Name4 = result.getString("GENDER");
        String Name5 = result.getString("DOB");
        
        if (Flag == "FIRST_NAME")
        {
        	
        	System.out.println("After Update" +"FirstName = " + Name1);
             return Name1;     
        }
       
        else if(Flag == "MIDDLE_NAME") 
        
        {
        	
        	System.out.println("After Update" + "MiddleName = " + Name2 );
            return Name2;
        }
        
        
        else if(Flag == "LAST_NAME")
        {
        	
        	System.out.println("After Update" + "LastName = " + Name3);
        	return Name3;
        }
      
        else if(Flag == "GENDER")
        {
        	
        	System.out.println("After Update" + "Gender = " + Name4);
        	return Name4;
        }
        	else 
        	{	
        		
        		System.out.println("After Update" + "DOB = " + Name5);
            	return Name5;
            
    }
       
    }
    
  
    con.close();
	return Flag;
	
        }

public static int dbValidation(String Registrationnumber)
{  
       int n = 0;
       try
       {  
              //step1 load the driver class  
              Class.forName("oracle.jdbc.driver.OracleDriver");  
                
              //step2 create  the connection object  
              Connection con=DriverManager.getConnection(Config.getProperty("hostname"),Config.getProperty("username"),Config.getProperty("password"));  
                
              //step3 create the statement object  
              Statement stmt=con.createStatement(); 
              
              //step4 Execute query
              ResultSet rs=stmt.executeQuery("Select * from TBL_FR_APPLICATION WHERE APPLICATION_ID="+Registrationnumber);
              //Object String;
              //int n = 0;
              while(rs.next()) 
                    n = rs.getInt("STATUS");
                    
                    if(n==6)
                    {
                           System.out.println("Registration Status is:  "+n+" which means its an IDENTITY CREATED");
                    }
                    else if (n==1)
                    {
                           System.out.println("Registration Status is:  "+n+"  which means its an APPLICATION CREATED");
                    }
                    else if (n==5)
                    {
                           System.out.println("Registration Status is:  "+n+"  which means its an Rejected application");
                    }
                    else
                           System.out.println("Registration Status is:  "+n+" which means identity is DELETED/doesnot exist");
                    
              //step5 close the connection object  
              con.close();  
                             
              }
              catch(Exception e)
              { System.out.println(e);}
       
         
              return n;
       }  


public static void verifyIdentityDeletion(int DBValidationStatus)
{  
  if (DBValidationStatus!=6||DBValidationStatus!=5||DBValidationStatus!=1)
    { 
        Reporter.log("<p> Success - Identity deleted from database" + "</p>");

     }
 else
  { 
     Reporter.log("<P> Failed - Identity not deleted from database" + "</p>");
  }  
}
}