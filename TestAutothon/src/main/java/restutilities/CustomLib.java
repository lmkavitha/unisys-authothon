package restutilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;


import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

/**
 * 
 * @author GowdaHar
 *
 */
public class CustomLib {
	
	/**
	 * Method Name: getRootDirectory 
	 * Description: Method to get Root directory
	 * 
	 * @return :rootDir
	 * @throws IOException 
	 */
	/*public static String getRootDirectory()
	{
		File path = new File("");
		String absolPath = path.getAbsolutePath();
		File dir = new File(absolPath);
		String rootDir = dir.getParent();
		//String rootDir = dir.getAbsolutePath();
		return rootDir;
	
	}
		*/
	
	/**
	 * Method Name: waitForObjectToload
	 * Description: this method waits until object to load or time seconds 
	 * @param driver : WebDriver Instance
	 * @param by : element locator
	 * @param timeSeconds : time in seconds 
	 */
	
	public static String encodeFileToBase64Binary(File file) 
	{
		String encodedfile = null;
		try 
		{
			FileInputStream fis = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fis.read(bytes);
			encodedfile = Base64.getEncoder().encodeToString(bytes);
			fis.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return encodedfile;
	} 
	
	
	public static String DecodeFileToBase64Binary(Response response) 
	{
		JsonPath jsonPathEvaluator = response.jsonPath();
		String Status = jsonPathEvaluator.get("GetIdentityResponse");
		String DecodedData = null;
		try 
		{
			byte[] byteArray = Base64.getDecoder().decode(Status.getBytes());
			  
			  System.out.println(Arrays.toString(byteArray));
			  
			 
			  // Print the decoded string 
			 
			   DecodedData = new String(byteArray);
			 
			  System.out.println("decoded d  = " + DecodedData);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return DecodedData;
	} 

}
