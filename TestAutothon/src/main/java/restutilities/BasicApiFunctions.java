package restutilities;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import static common.Filereader.Config;
import static common.Filereader.TextProp;

import common.BaseTest;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
public class BasicApiFunctions extends BaseTest
{


/*{
	
	public ApiFunctions(){
		Filereader.loadProperties();
		System.out.println("yes");
	}*/

	public static String projDir = BaseTest.getRootDirectory();
	
	


	
	
	
	
	
	
	
	
	
	

	public static String createdIdentityValue(Response response) 
	{
		JsonPath jsonPathVariale = response.jsonPath();			
		
		return jsonPathVariale.getString("CreateIdentityResponse").toString();
	}
	
	
	
	
	
	
	 public static void setCertificates() {
	    	
	    	// configure certificates to Rest assured base call    	
			RestAssured.config().getSSLConfig().with().keyStore(Config.getProperty("clientWindows"), Config.getProperty("certificatePassword"));
			RestAssured.config().getSSLConfig().with().trustStore(Config.getProperty("serverWindows"), Config.getProperty("certificatePassword"));
			
			// set the certificates with password to Rest assured base call 
			RestAssured.keyStore(Config.getProperty("clientWindows"), Config.getProperty("certificatePassword"));
			RestAssured.trustStore(Config.getProperty("serverWindows"), Config.getProperty("certificatePassword"));
		}
	  
	    
	public static RequestSpecification RequestwithoutSSL(String basepath, String requestParams)
	{
		//setCertificates();
		RestAssured.baseURI= Config.getProperty("baseURI1");
		RestAssured.basePath=basepath;
		RequestSpecification request = RestAssured.given();
		
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		request.body(requestParams);
		
		return request;
	}
	public static RequestSpecification postvideoinside(String requestParams)
	{
		//setCertificates();
		RestAssured.baseURI= Config.getProperty("baseURIAutothon");
		RestAssured.basePath=Config.getProperty("postbasepathAutothon");
		RequestSpecification request = RestAssured.given();
		
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		request.body(requestParams);
		
		return request;
	}
	
	public static RequestSpecification RequestwithSSL(String basepath, String requestParams)
	{
		setCertificates();
		RestAssured.baseURI= Config.getProperty("baseURI");
		RestAssured.basePath=basepath;
		RequestSpecification request = RestAssured.given();
		
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		request.body(requestParams);
		//request.queryParams(source, firstParameterValue, parameterNameValuePairs)
		
		return request;
	}
	
	public static RequestSpecification Queryparam(String basepath)
	{
		//setCertificates();
		RestAssured.baseURI= Config.getProperty("baseURI1");
		RestAssured.basePath=basepath;
		String source;
		Integer sourcevalue=2342;
		String type;
		RequestSpecification request = RestAssured.given();
		//request.queryParams(source, sourcevalue)
		request.queryParam("source", sourcevalue);
		request.queryParam("groupid", "flight_100");
		
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		//request.body(requestParams);
		//request.queryParams(source, firstParameterValue, parameterNameValuePairs)
		
		return request;
	}
	
	public static RequestSpecification GetVideo()
	{
		//setCertificates();
		RestAssured.baseURI= Config.getProperty("baseURIAutothon");
		RestAssured.basePath=Config.getProperty("basepathAutothon");
		
		RequestSpecification request = RestAssured.given();
		
		
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		//request.body(requestParams);
		//request.queryParams(source, firstParameterValue, parameterNameValuePairs)
		
		return request;
	}
	
	public static RequestSpecification PostVideo()
	{
		RestAssured.baseURI= Config.getProperty("baseURIAutothon");
		RestAssured.basePath=Config.getProperty("postbasepathAutothon");
		RequestSpecification request = RestAssured.given();
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		JSONObject requestParams = new JSONObject();
		requestParams.put("team", "team-name");
		requestParams.put("video", "search-video-name");
		requestParams.put("upcoming-videos.sample[0]", Config.getProperty("videonames"));
		
		return request;
	}
	
	public static RequestSpecification ValidateVideo()
	{
		RestAssured.baseURI= Config.getProperty("baseURIAutothon");
		RestAssured.basePath=Config.getProperty("validatebasepathAutothon")+Config.getProperty("uploadvideo");
		RequestSpecification request = RestAssured.given();
		request.accept("application/json");
		request.header("Content-Type", "application/json");
		
		return request;
	}
	
	public static void printResponse(Response response)
	{
		Reporter.log("Response is      :" +response.body().prettyPrint());
	}
	public static void verifyStatusCode(Response response) {
		try {
			int statusCode = response.getStatusCode();
			// Reporter.log("code "+statusCode,true);
			Assert.assertEquals(statusCode, 200);
			Reporter.log("<P> success-  Status Code is" + "</p>");
			Reporter.log("<p> Response Status Code :" + response.getStatusCode(), true);
			//Reporter.log("<p> Response Status Line:" + response.getStatusLine(), true);

		} catch (Exception e) {
			Reporter.log("<p> Failed - Response Status is not valid :" + e + "</p>", true);
		}

	}
	
	public static void verifyResponseValue(Response response) 
	{
		JsonPath jsonPathEvaluator = response.jsonPath();
		Boolean success = jsonPathEvaluator.get("meta.success");
		System.out.println("$$$$$$$"+success);
		Boolean expected = true;
		
		try 
		{
			
			Assert.assertEquals(success, expected);
			
			Reporter.log("<p> Passed - gallery created",true);
				
		} 
		catch (Exception e) 
		{

				Reporter.log("<p> Failed - gallery not created" + e,true);
				
		}

	}
//Added by Angelina
	// Function is to validate all error codes
	public static void verifyErrorResponse(Response response,int ErrCode,String ErrDesc) 
	{
		JsonPath jsonPathEvaluator = response.jsonPath();
		String ResErrorDescription = jsonPathEvaluator.get("ErrorDescription");
		String ResErrorCode = jsonPathEvaluator.get("ErrorCode");

		try 
		{
				Assert.assertEquals(Integer.parseInt((ResErrorCode)), ErrCode);
				Assert.assertEquals(ResErrorDescription, TextProp.getProperty(ErrDesc));
				Reporter.log("<p> Success");
				Reporter.log("<p> Error code displayed is    :" + ResErrorCode);
				Reporter.log("<p> Error Description displayed is    :" + ResErrorDescription);
		} 
		catch (Exception e) 
		{

				Reporter.log("<p> Failed </p>" + e);
				Reporter.log("<p> Error code displayed is    :" + ResErrorCode);
				Reporter.log("<p> Error Description displayed is    :" + ResErrorDescription);
		}

}

			// Function is to validate response time
			
			    public static void verifyResponseTime(Response response) {
				try {
					long responseTime=response.getTime();
					
					Reporter.log("Response time is within 10 sec" + responseTime);
					
					
					if (responseTime<10000)
					{
						Reporter.log("Response time is within 10 sec" + "</p>");
						
						
					}
					else
					{
						Reporter.log("Response time not within 10 sec" + "</p>");
						Assert.fail();
						
						
					}
				} catch (Exception e) {
					
					Reporter.log("<p> Response time not displayed :" + e + "</p>", true);
				}

			}

			 // Added by Angelina
				// Function is to validate 1012 error code
				public static void verify1012ErrorResponse(Response response) {
					JsonPath jsonPathEvaluator = response.jsonPath();
					String ErrorDescription = jsonPathEvaluator.get("ErrorDescription");
					String ErrorCode = jsonPathEvaluator.get("ErrorCode");

					if (!ErrorDescription.isEmpty() || ErrorCode.isEmpty()) {

						try {
							Assert.assertEquals(Integer.parseInt((ErrorCode)), 1012);
							Assert.assertEquals(ErrorDescription, TextProp.getProperty("1012ErrDesc"));

							Reporter.log(
									"<p> Success - 1012 error code and description is displayed for invalid Fingerprint Image </p>",
									true);
							Reporter.log("<p> Error code displayed is    :" + ErrorCode, true);
							Reporter.log("<p> Error Description displayed is    :" + ErrorDescription, true);
						} catch (Exception e) {

							Reporter.log("<p> Failed - 1012 error is not displayed, verification unsuccesfull </p>" + e, true);
							Reporter.log("<p> Error code displayed is    :" + ErrorCode, true);
							Reporter.log("<p> Error Description displayed is    :" + ErrorDescription, true);
						}

					} else {
						Reporter.log("<p> Response is Empty </p>", true);
					}

				}
    
}

