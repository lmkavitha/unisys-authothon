package seleniumpageobjects;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import restutilities.BasicApiFunctions;
import restutilities.RestLibFunction; 
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;

import org.openqa.selenium.OutputType;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import common.Filereader;
import static common.Filereader.Config;
import static common.Filereader.TextProp;

import java.util.ArrayList;
//import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;

import common.BaseTest;


public class youpage extends BaseTest {

	public WebDriver driver;	
	public static String projDir = BaseTest.getRootDirectory();
	//@FindBy(id="userInitials")
	//private WebElement loggedUserInitials;
	String baseUrl = "https://www.youtube.com";
	// Home page constructor
	ITestResult result;
	public static File srcFile;
	public static Date currentDay = Calendar.getInstance().getTime();
	public String screenshots = null;
	public youpage(WebDriver driver) {
		this.driver=driver;
		//PageFactory.initElements(driver, HomePage.class);
	}
	
	// ------------------------------ HomePage Locators -------------------------------------- //
	By searchId		= 		By.xpath(OR.getProperty("YoutubeSearch")); 
	By textsearch	=       By.xpath(OR.getProperty("searchtext"));	
    By videotab     =       By.xpath(OR.getProperty("videos"));
    By clickSearch  =       By.xpath(OR.getProperty("searchclick"));
    By Videolink    =       By.linkText(OR.getProperty("response"));
    By vidSetting   =       By.xpath(OR.getProperty("setting"));
    By listvid      =       By.id(OR.getProperty("idlist"));
//---------------------------------------------------------------------------------------

	public youpage YoutubetestValidation() throws Exception
	{
		 driver.manage().window().maximize();
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.get(baseUrl);
		 Thread.sleep(100);
		 driver.findElement(searchId).sendKeys("step-inforum");
		 Thread.sleep(100);
		 driver.findElement(clickSearch).click();
		 Thread.sleep(1000);
		 driver.findElement(textsearch).click();
		 Thread.sleep(1000);
		 driver.findElement(videotab).click();
		 System.out.println("vidoes tab selected");
		 Thread.sleep(300);
		 WebElement videolink= driver.findElement(Videolink);
		 ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", videolink);
		 takeSnapShot(driver,"D:\\sysgen\\unisys-authothon\\TestAutothon\\ScreenShot\\test.png");
		 videolink.click();
		 System.out.println("Selected video is open now");
		 WebElement settingbun = driver.findElement(vidSetting);
		 settingbun.click();
		 settingbun.sendKeys(Keys.DOWN);
		 settingbun.sendKeys(Keys.DOWN);
		 settingbun.sendKeys(Keys.DOWN);
		 settingbun.sendKeys(Keys.RIGHT);
		 settingbun.sendKeys(Keys.UP);
		 settingbun.sendKeys(Keys.UP);
		 settingbun.sendKeys(Keys.UP);
		 settingbun.sendKeys(Keys.ENTER);
		 Thread.sleep(300);
		 System.out.println("Video quality changed to 360p");
		 List<WebElement> nextvid =  driver.findElements(listvid);
		 long totalvid = nextvid.size();
		 System.out.println("List of videos  "+totalvid);
		//Create instance of JSONObject
		 ArrayList<String> videoName = new ArrayList<String>();
		 JSONObject jObj = new JSONObject();
		 String data = "Video";
		 int num = 1;
		 for(WebElement e : nextvid) {
			 
			 videoName.add(e.getText());
			 jObj.put(data+num,e.getText());
			 num++;
			  System.out.println(e.getText()); 
		 
		 }
		 Map map = new LinkedHashMap(1);

		 JSONArray jArr = new JSONArray();
		 map = new LinkedHashMap(1);
		 map.put("phone type", "home");
		 map.put("number", "1234567890");
		 
		 //jArr.add(map);
	
		 
		//Writing test data to the JSONWrite.json JSON file
		 PrintWriter pWriter = new PrintWriter("ScreenShot/JSONWrite.json");
		// pWriter.write(jObj.toJSONString());
		 
		 
		 
		 
		
		return new youpage(driver);
		
	}
	
	public static void APIGetCall() throws IOException 
	{
	
	Response video1 = BasicApiFunctions.GetVideo().get();
	String responstext = video1.getBody().asString();
	BasicApiFunctions.printResponse(video1);
	Config.setProperty("videoname", responstext);
	//ObjectRepositories.setProperty()
	File file = new File(projDir+"\\src\\main\\resources\\configuration\\ObjectRepositories.properties");
	FileOutputStream fos =  new  FileOutputStream(file);
	//Config.store(fos, "videoname");
	//File file1 = new File(projDir+"\\src\\main\\resources\\configuration\\ObjectRepositories.properties");
	//FileOutputStream fos1 =  new  FileOutputStream(file1);
	//ObjectRepositories.store(fos, "response");
	}
	
	public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{

        //Convert web driver object to TakeScreenshot

        TakesScreenshot scrShot =((TakesScreenshot)webdriver);

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File(fileWithPath);

                //Copy file at destination

                FileUtils.copyFile(SrcFile, DestFile);

    }
}
