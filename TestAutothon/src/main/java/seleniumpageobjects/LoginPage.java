package seleniumpageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import common.BaseTest;
import guifunctions.UIFunctions;
import common.Filereader.*;
import static common.Filereader.Config;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;


public class LoginPage extends BaseTest{
	
	public static String projDir = BaseTest.getRootDirectory();
	
	public WebDriver driver;
	public String baseUrl = Config.getProperty("backOfficeUrl");	
	//public String baseUrl = "https://ustr-erl2-3445.na.uis.unisys.com:8444/stealth-identity-backofficeweb/";
	// login page constructor
	public LoginPage(WebDriver driver) {

		this.driver = driver;
		
		BaseTest.loadProperties();
			}
	
	
	// ---------------------------- LoginPage Locators -------------------------------------- //
			By userNameFiled			=		By.id("username");
			By loginLogoStealth			= 		By.cssSelector(OR.getProperty("logoSTEALTH_CSS")); 
			By loginUsername			=		By.id(OR.getProperty("userName_ID"));
												//By.cssSelector("input[id='eidUserName']");
			By loginPassword			=		By.id(OR.getProperty("password_ID"));
			By loginButton				=		By.id(OR.getProperty("loginBtn_ID"));	
			By IncorrectCredentialsMsg  =		By.id(OR.getProperty("loginStatusMsg_ID"));
		
	//---------------------------------------------------------------------------------------
	//								LoginPage Functions
	//---------------------------------------------------------------------------------------
			
	
	
	/**
	 * This is to navigate to Stealth Back Office application
	 * @param driver
	 * @return
	 * @throws Exception
	 */
	public LoginPage navigateToBO() throws Exception {
		
		
		
		Thread certSelectionThread = null;
	    Runnable r = new Runnable() {
		
		// script to handle select a certificate popup
		 @Override
	     public void run() {
	      try {
	       Thread.sleep(1000 * 10);
	       Robot robot = new Robot();
	       robot.keyPress(KeyEvent.VK_ENTER);
	      } catch (AWTException e) {
	       e.printStackTrace();
	      } catch (InterruptedException e) {
	       e.printStackTrace();
	      }
	     }
	    };
	    certSelectionThread = new Thread(r);
	    certSelectionThread.start();
		
	    // Navigate to back office application
	 	driver.get(baseUrl);
		System.out.println(driver);
		//JavascriptExecutor js = (JavascriptExecutor) driver;      
        //js.executeScript("document.body.style.zoom='100%'");
		
		//wait for the login page load
		BaseTest.waitForObjectToload(driver, loginLogoStealth, 30);		
		
		return new LoginPage(driver);
	}
	
	/**
	 * This function is to verify the login page assertions
	 * @return
	 */
	public LoginPage verifyLoginPageIsDisplayed() {
		
		// Assert user is navigated to Login page		
		String pageTitleExpected = TextProp.getProperty("pageTitle");
		Assert.assertEquals(driver.getTitle(), pageTitleExpected);
		
		Assert.assertTrue(driver.findElement(loginLogoStealth).isDisplayed());
		Reporter.log("<p> Login page logo is displayed ");		
		
		return new LoginPage(driver);
	}
	
	/**
	 * This function enters user name in Login page
	 * @param username
	 * @return LoginPage object
	 */
	public LoginPage enterLoginUsername(String username) {		
		// enter user name in login name text box
			UIFunctions.typeTextboxValue(driver, loginUsername, Config.getProperty(username));			
		return new LoginPage(driver);
	}
	
	/**
	 * This function enters password in password box
	 * @param password
	 * @return LoginPage object
	 */
	public LoginPage enterLoginPassword(String password) {		
		// enter password in password text box
		UIFunctions.typeTextboxValue(driver, loginPassword,Config.getProperty(password));		
		return new LoginPage(driver);
	}
	
	/**
	 * This method clicks on Login button
	 * @return
	 * @throws Exception
	 */
	public LoginPage clickOnLogin() throws Exception {		
		// click on login button
		UIFunctions.clickElement(driver, loginButton, "Login Button");				
		
		// wait for element to load
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	//	CustomLib.waitForObjectToload(driver, by, timeSeconds);
		//Thread.sleep(5000);	
		//JavascriptExecutor js = new JavascriptExecutor();					
		return new LoginPage(driver);
	}
	
	public LoginPage userLogin(String username, String password) {
		
		// enter user name in login name text box
			UIFunctions.typeTextboxValue(driver, loginUsername, username);
		
		// enter password in password text box
			UIFunctions.typeTextboxValue(driver, loginPassword, password);
			
		// click on login button
			UIFunctions.clickElement(driver, loginButton, "Login Button");
		
		return new LoginPage(driver);
		
	}
	
	
	/**
	 * This method verifies the invalid credentials error message
	 * @return
	 */
	public LoginPage verifyIncorrectCredntialsErrorMsg() {
		
		BaseTest.waitForObjectToload(driver, IncorrectCredentialsMsg, 10);		
		
		String ActualErrorMsg = BaseTest.getElementText(driver, IncorrectCredentialsMsg);
		Assert.assertEquals(ActualErrorMsg, TextProp.getProperty("incorrectCredentialsMsg"));
		
		return new LoginPage(driver);		
	}
	
	
	public LoginPage enterUserName(String user) {
		
		driver.findElement(userNameFiled).sendKeys("superuser");
		
		return new LoginPage(driver);
	}
	

	
	
	
}
