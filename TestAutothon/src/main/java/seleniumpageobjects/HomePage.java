package seleniumpageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import common.Filereader;
import static common.Filereader.Config;
import static common.Filereader.TextProp;
import common.BaseTest;


public class HomePage extends BaseTest {
	
	public WebDriver driver;	
	public static String projDir = BaseTest.getRootDirectory();
	//@FindBy(id="userInitials")
	//private WebElement loggedUserInitials;
	
	// Home page constructor
	public HomePage(WebDriver driver) {
		this.driver=driver;
		//PageFactory.initElements(driver, HomePage.class);
	}
	
	// ------------------------------ HomePage Locators -------------------------------------- //
				By loggedInUserInitials		= 		By.id(OR.getProperty("loggedInUserInitials_ID")); 
				By home2					=		By.cssSelector("");
				By home3					=		By.id("");
				By home4					=		By.id("");		
			
	//---------------------------------------------------------------------------------------
	//								HomePage Functions
	//---------------------------------------------------------------------------------------
			
	
	
	/**
	 * This function is to verify user is Home page
	 * @return
	 * @throws Exception
	 */
	public HomePage verifyUserIsInHomePage() throws Exception { 
		
		// Assert user is navigated to Home page
		String pageTitleExpected = TextProp.getProperty("pageTitle");
		Assert.assertEquals(driver.getTitle(), pageTitleExpected);
		
		//Assert.assertTrue(CustomLib.isElementPresent(driver, loggedInUserInitials));
		Assert.assertTrue(BaseTest.isElementPresent(driver, loggedInUserInitials));
		
	 return new HomePage(driver);
	}
}
