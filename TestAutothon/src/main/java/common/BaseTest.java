package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Listeners;
	@Listeners(common.TestListener.class)
public  class BaseTest extends Filereader {

	public static WebDriver driver;

	public  Properties prop;
	public  Properties Textprop;
	
	/**
	 * Method Name: getRootDirectory 
	 * Description: Method to get Root directory
	 * 
	 * @return :projDir
	 * @throws IOException 
	 */

public static  String getRootDirectory(){
	

	   String projDir = System.getProperty("user.dir");
	 return projDir;

	  
}	


/**
 * Method Name: waitForObjectToload
 * Description: this method waits until object to load or time seconds 
 * @param driver : WebDriver Instance
 * @param by : element locator
 * @param timeSeconds : time in seconds 
 */
public static void waitForObjectToload(WebDriver driver,By by,int timeSeconds) {
	
	WebDriverWait wait = new WebDriverWait(driver, timeSeconds);
	wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	
}
	public WebDriver initializeDriver() throws IOException
{
		
	
String browserName=Config.getProperty("browser");
System.out.println(browserName);

if(browserName.equals("chrome"))
{
	
	System.out.println(driver);
	 System.setProperty("webdriver.chrome.driver", ".//Driver//chromedriver.exe");
	driver= new ChromeDriver();
		//execute in chrome driver
	
}
else if (browserName.equals("firefox"))
{
	 driver= new FirefoxDriver();
	//firefox code
}
else if (browserName.equals("IE"))
{
//	IE code
}
System.out.println(browserName);

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


return driver;
}

/**
 * Method Name: isElementPresent 
 * Description: this function is to verify the element is present or visible
 * 
 * @param by  :	element locator
 * @param driver: WebDriver instance
 * @return  true: if element is present , 
 * 		    false: if element is not present
 */
public static boolean isElementPresent(WebDriver driver,By by) throws Exception {
	try {
		driver.findElement(by).isDisplayed();
		return true;
	} catch (NoSuchElementException e) {
		return false;
	}
}

/**
 * Method Name:  getElementText
 * Description: This method is to get text of the located element
 * @param driver : WebDriver instance
 * @param by : locator
 * @return : element text
 */
public static String getElementText(WebDriver driver,By by) {
	
	String text = driver.findElement(by).getText();
	
	return text;
}

/**
 * 
 * @param file
 * @return
 */


	//To add Wait for object,is elementpresent,getelementtext
	


}
