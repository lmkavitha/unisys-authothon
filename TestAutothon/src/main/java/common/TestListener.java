package common;

import common.Filereader;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import common.BaseTest;


/**
 * @author GowdaHar
 *
 */
public class TestListener implements ITestListener {
	
	
	public String projDir = BaseTest.getRootDirectory();
	public static File srcFile;
	public static Date currentDay = Calendar.getInstance().getTime();
	public String screenshots = null;
	
	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}
	/*public void onTestFailure(ITestResult result) {		
		
		printTestResult(result);		
	
	}*/
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	/*// This method will be used to execute in 
	public void printTestResult(ITestResult result) {
		
	
		try {	
			// take screenshot
	   	 File scrFile = ((TakesScreenshot)BaseTest.driver).getScreenshotAs(OutputType.FILE);
	   	 	// append screenshot with name in dest location
	     File screenshotName = new File(".//ScreenShot//"+result.getName()+result.getStartMillis()+".png");
	     //Now add screenshot to results by copying the file
	     FileHandler.copy(scrFile, screenshotName);
	     //FileUtils.
	     // append screenshot to reports
	     Reporter.log("<a href=\"" + screenshotName + "\"> "
	    		+ "<p><img src=\"" + screenshotName  + "\" alt=\"\""+" width='100'/></p></a><br />"); 
	       
	      
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		*/
		 
	}


