package common;

import java.io.FileInputStream;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.BeforeTest;



public  class Filereader {


		
		public static Properties Config;
		public static Properties OR;
		public static Properties TextProp;
		public static String projDir = BaseTest.getRootDirectory();
		
			
		/**
		 * Method Name: loadProperties
		 * Description: this method locates the input java.property file and loads its 
		 * 				content to property object created
		 * @return 
		 * 
		 */
		
		@BeforeTest
		public static void loadProperties() {
			
			Config = new Properties();
			OR = new Properties();
			TextProp = new Properties();
				
			try {
				
				System.out.println(projDir);
				System.out.println(BaseTest.getRootDirectory());
					// load properties file from Config file path
					InputStream input1 = new FileInputStream(projDir+"\\src\\main\\resources\\configuration\\"+"Config.properties");

					Config.load(input1);
					
					// load properties file from ObjectRepo file path
					InputStream input2 = new FileInputStream(projDir+"\\src\\main\\resources\\configuration\\"+"ObjectRepositories.properties");
					OR.load(input2);
					
					
					// load properties file from TextProp file path
					InputStream input3 = new FileInputStream(projDir+"\\src\\main\\resources\\configuration\\"+"Text.properties");
					TextProp.load(input3);
					
					
				} catch (FileNotFoundException e1) {				
					e1.printStackTrace();
				} catch (IOException e) {			
					e.printStackTrace();
				}
		

			
				
				
			
		}
		

	}

	
	
	
	
	
		 
