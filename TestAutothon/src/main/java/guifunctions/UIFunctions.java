package guifunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;

import common.BaseTest;

public class UIFunctions extends BaseTest {
	
	public static String projDir = BaseTest.getRootDirectory();
	/**
	 * Method Name: clickElement
	 * Description: This method clicks on specified web element
	 * 
	 * @param driver : WebDriver
	 * @param by	 : WebElement Locator
	 * @param elementName : Name of the element to be clicked
	 */
	public static void clickElement(WebDriver driver,By by, String elementName) {
		
		try {			
			// Click on WebElement
			//driver.findElement(by).click();
			//driver.findElement(by).sendKeys(Keys.SPACE);				
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", driver.findElement(by));
				
			// report
				Reporter.log("<p>Successfully clicked on element: "+elementName);
				
		} catch (NoSuchElementException e) {			
			Reporter.log("<p> Element to be clicked is not present "+e);			
		}		
	}
	
	/**
	 * Method Name: typeTextboxValue
	 * Description: This method enter value to specified Web element-textbox 
	 * 
	 * @param driver: WebDriver
	 * @param by	: WebElement Locator
	 * @param value	: Value to be entered in the text box
	 */
	public static void typeTextboxValue(WebDriver driver, By by, String value) {
		
		try {
			
			// clear and enter the textbox value
		//	driver.findElement(by).clear();
			driver.findElement(by).sendKeys(value);
			//Reporter.log("<p> Successfully entered textbox value: "+ value);
			
		} catch (NoSuchElementException e) {
			Reporter.log("<p> Element/Textbox to enter data is not present "+e);
		}
	}
	
	/**
	 * Method Name: selectDropDownValue
	 * Description: This method selects the drop down by visible value
	 * 
	 * @param ele : WebElements
	 * @param value : drop down value to be selected
	 */
	public static void selectDropDownValue(WebElement ele, String value) {
		
			try {
				
				//Dropdown from which value to be selected
				Select dropDown = new Select(ele);
				
				// selecting dropdown by value
				dropDown.selectByVisibleText(value);
				
			} catch (NoSuchElementException e) {
				Reporter.log("<p> Element to select dropdown is not present "+e);
				e.printStackTrace();
			}			
		}
	
	/**
	 * Method Name: mouseHoverElement
	 * Description: This function is used to hover on the element which is not visible
	 * 
	 * @param driver : WebDriver
	 * @param ele	 : WebElement
	 * @throws Exception
	 */
	public static void mouseHoverElement(WebDriver driver, WebElement ele) throws Exception{
		
		// mouse hover on the specified element
		Actions builder = new Actions(driver);
		builder.moveToElement(ele).build().perform();
		
	}
	
	
	/**
	 * Method Name: isElementPresent
	 * Description: 
	 * 
	 * @param driver : WebDriver
	 * @param by	 : WebElement locator
	 * @return		 : True if element is present or else False
	 */
	public static boolean isElementPresent(WebDriver driver, By by) {
		
				try {
					
					//Verify element is present
					driver.findElement(by).isDisplayed();
					
					return true;
					
				} catch (NoSuchElementException e) {
					// TODO Auto-generated catch block
					Reporter.log("<p> No such element present "+e);
					return false;
				}
				
				
		
		
		
	}
	
}
