package com.unisys.eportal.manager;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;

import net.sourceforge.htmlunit.corejs.javascript.tools.shell.Global;

import org.testng.Assert;

import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableHyperlink;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
public class ReportManager {

	public static String vDatetype1 = "MM_dd_yyyy_HH_mm";
	public static String vDatetype2 = "MM_dd_yyyy";
	public static String vDatetype3 = "dd";
	public static String vDatetype4 = "HH:mm";
	public static String vDatetype5 = "HH:mm:ss";
	public static String vDatetype6 = "MM_dd_yyyy_HH_mm_a";
	public static String vDatetype7 = "E_dd_MMM_yyyy_HH_mm_a_z";
	public static String vDatetype8 = "E_MMM_dd_yyyy_HH_mm_a_z";
	public static String vDatetype9 = "MM/dd/yyyy" + " " + vDatetype5;
	private static String finalResult = "pass";
	private static boolean finalResultFlag = true;
	public static String outputNotePadResultFile;
	public static String outputHTMLFile;
	public static String SummaryHTMLFile;
	public static String outPutNotepadStepsFile;
	public static String outPutNotepadLogFile;
	public static String outputExcelFile;
	public static int stepCount ;
	public static int stepCountSummary ;
	protected static int LogCount = 1;

	public static int totalstepsgrand;

	public static long startTime;
	public static long endTime;
	public static long totalTime;


	/**
	 * @param FileName
	 * @param vDateForReports
	 */
	public ReportManager(String FileName, String vDateForReports) {

		try {
			createExcelResultTemplate(FileName, vDateForReports);
			GlobalVariables.htmlFolderName =GlobalVariables.applicationFolder + "\\Reports\\HTML_Executed_On_" + vDateForReports + "\\";
			GlobalVariables.htmlFolderName2 =GlobalVariables.applicationFolder + "\\Reports\\HTML_Executed_On_" + vDateForReports + "\\";
			File folder = new File(GlobalVariables.htmlFolderName);
			File folder2 = new File(GlobalVariables.htmlFolderName2);
			if (!folder.exists()) 
			{
				folder.mkdir();
			}
			
			
			if (!folder2.exists()) 
			{
				folder2.mkdir();
				
			}
			
			
//			Called in Main Function	
//			createHTMLResultTemplate(FileName, vDateForReports);
			
			createNotepadResultTemplate(FileName, vDateForReports);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @param fileName
	 * @param vGetDateForHTML
	 * @throws IOException
	 */
/*	public void createHTMLResultTemplate(String fileName, String vGetDateForHTML)
			throws IOException {

		File folder = new File(System.getProperty("user.dir") + "\\Reports\\");
		if (!folder.exists()) {
			folder.mkdir();
		}

		outputHTMLFile = System.getProperty("user.dir") + "\\Reports\\"
				+ fileName.toUpperCase() + "_" + vGetDateForHTML + ".html";

		File fhtml = new File(outputHTMLFile);
		BufferedWriter html = new BufferedWriter(new FileWriter(fhtml));
		if (fhtml.exists()) {
			fhtml.delete();
		}
		// HTML REPORT CODE
		html.write("<!DOCTYPE html><html><body>"
				+ "<FONT COLOR=\"#000000\" size=\"5\">"
				+ "<dd><pre>******************************************************<br/>"
				+ "Test Script Name	: " + "<em>" + "<b>"
				+ fileName.toUpperCase() + "</b>" + "</em><br/>"
				+ "Test Executed By 	: "
				+ (System.getProperty("user.name")).toUpperCase() + "<br/>"
				+ "Browser Used Is 	: " + "<br/>" + "Test Start Time 	: "
				+ Calendar.getInstance().getTime().toString() + "<br/>"
				+ "Test End Time 		: " + "<br/>" + "Script Execution Time 	: "
				+ "<br/>" + "Script Execution Status : " + "<br/>"
				+ "******************************************************"
				+ "</FONT></pre></dd></body></html>");
		// #000000 #800000
		html.write("<!DOCTYPE html><html><body>"
				+ "<table border=\"1\" width=\"100%\">" +

				"<tr  bgcolor=#FFDEAD>" + "<FONT COLOR=\"0000CC\" size=\"4\">"
				+ "<th width=\"5%\">Step</th>"
				+ "<th width=\"12%\">Execution Time-Date</th>"
				+ "<th width=\"35%\">Step Description</th>"
				+ "<th width=\"38%\">Actual Result</th>"
				+ "<th width=\"10%\">Status</th>" + "</FONT></tr>"
				+ "</table></body></html>");
		html.close();

	}*/
	
	public void createHTMLSummaryTemplate(String fileName, String vGetDateForHTML)
			throws IOException {
		
		//Html File Name in the Reports Folder
		SummaryHTMLFile = GlobalVariables.htmlFolderName2 + fileName + ".html";
		File ffhtml = new File(SummaryHTMLFile);
		BufferedWriter html = new BufferedWriter(new FileWriter(ffhtml));

		// HTML REPORT CODE
		html.write("<!DOCTYPE html><html><body>"
				+ "<FONT COLOR=\"#000000\" size=\"5\">"
				+ "<dd><pre>******************************************************<br/>"
				+ "Batch Script Name	: " + "ePortal_Manager_Automation" + "<b>"
				+ fileName + "</b>" + "</em><br/>"  //fileName.toUpperCase()
				+ "Test Executed By 	: "
				+ (System.getProperty("user.name")).toUpperCase() + "<br/>"
				+ "Browser Used Is 	: " + "<br/>" + "Test Start Time 	: "
				+ Calendar.getInstance().getTime().toString() + "<br/>"
				+ "Test End Time 		: " + "<br/>" + "Script Execution Time 	: "
				+ "<br/>" + "Script Execution Status : " + "<br/>"
				+ "******************************************************"
				+ "</FONT></pre></dd></body></html>");
		// #000000 #800000
		
		html.write("<!DOCTYPE html><html><body>"
				+ "<table border=\"1\" width=\"100%\">" +

				"<tr  bgcolor=#FFDEAD>" + "<FONT COLOR=\"0000CC\" size=\"4\">"
				+ "<th width=\"5%\">TC No.</th>"
				+ "<th width=\"12%\">Execution Time-Date</th>"
				+ "<th width=\"35%\">Test Case Executed</th>"
				//+ "<th width=\"38%\">Actual Result</th>"
				+ "<th width=\"20%\">Status</th>" + "</FONT></tr>"
				+ "</table></body></html>");
		html.close();

	}
	
	
	
	
	
	
	public void createHTMLResultTemplate(String fileName, String vGetDateForHTML)
			throws IOException {
		
		//Html File Name in the Reports Folder
		outputHTMLFile = GlobalVariables.htmlFolderName + fileName + ".html";
		File fhtml = new File(outputHTMLFile);
		BufferedWriter html = new BufferedWriter(new FileWriter(fhtml));

		// HTML REPORT CODE
		html.write("<!DOCTYPE html><html><body>"
				+ "<FONT COLOR=\"#000000\" size=\"5\">"
				+ "<dd><pre>******************************************************<br/>"
				+ "Test Script Name	: " + "<em>" + "<b>"
				+ fileName + "</b>" + "</em><br/>"  //fileName.toUpperCase()
				+ "Test Executed By 	: "
				+ (System.getProperty("user.name")).toUpperCase() + "<br/>"
				+ "Browser Used Is 	: " + "<br/>" + "Test Start Time 	: "
				+ Calendar.getInstance().getTime().toString() + "<br/>"
				+ "Test End Time 		: " + "<br/>" + "Script Execution Time 	: "
				+ "<br/>" + "Script Execution Status : " + "<br/>"
				+ "******************************************************"
				+ "</FONT></pre></dd></body></html>");
		// #000000 #800000
		
		html.write("<!DOCTYPE html><html><body>"
				+ "<table border=\"1\" width=\"100%\">" +

				"<tr  bgcolor=#FFDEAD>" + "<FONT COLOR=\"0000CC\" size=\"4\">"
				+ "<th width=\"5%\">TC No.</th>"
				+ "<th width=\"12%\">Execution Time-Date</th>"
				+ "<th width=\"35%\">Expected Result</th>"
				+ "<th width=\"38%\">Actual Result</th>"
				+ "<th width=\"10%\">Status</th>" + "</FONT></tr>"
				+ "</table></body></html>");
		html.close();

	}


	/**
	 * @param fileName
	 * @param vGetDateForNotepad
	 * @throws IOException
	 */
	public void createNotepadResultTemplate(String fileName,
			String vGetDateForNotepad) throws IOException {

		File folder = new File(GlobalVariables.projectLocation + "\\Reports\\");
			folder.mkdir();
	

		outputNotePadResultFile =GlobalVariables.applicationFolder+ "\\Reports\\" + fileName.toUpperCase() + "_"+ vGetDateForNotepad + ".txt";

		outPutNotepadStepsFile =GlobalVariables.applicationFolder+ "\\Reports\\" + fileName.toUpperCase() + ".txt";
		outPutNotepadLogFile =GlobalVariables.applicationFolder + "\\Reports\\"+ fileName.toUpperCase() + "_" + "Execution LOG".toUpperCase()	+ "_" + vGetDateForNotepad + ".txt";

		File f = new File(outputNotePadResultFile);
		File f2 = new File(outPutNotepadStepsFile);
		File f3 = new File(outPutNotepadLogFile);

		BufferedWriter wr = new BufferedWriter(new FileWriter(f));

		BufferedWriter bw1 = new BufferedWriter(new FileWriter(f2));
		BufferedWriter bw2 = new BufferedWriter(new FileWriter(f3));

		if (f.exists()) {
			f.delete();
		}

		if (f2.exists()) {
			f2.delete();
		}
		if (f3.exists()) {
			f3.delete();
		}

		f.createNewFile();
		f2.createNewFile();
		f3.createNewFile();

		bw1.write("\t\t" + fileName + " - " + "STEPS");
		bw1.newLine();
		bw1.newLine();

		wr.write("\t\t\t"
				+ "*******************************************************");
		wr.newLine();
		wr.write("\t\t\t" + "Test Script Name" + "\t" + ": "
				+ fileName.toUpperCase());
		wr.newLine();
		wr.write("\t\t\t" + "Test Executed By" + "\t" + ": "
				+ (System.getProperty("user.name")).toUpperCase());
		wr.newLine();
		wr.write("\t\t\t" + "Browser Used Is" + "\t\t" + ": ");
		wr.newLine();
		wr.write("\t\t\t" + "Test Start Time" + "\t\t" + ": "
				+ Calendar.getInstance().getTime().toString());
		wr.newLine();
		wr.write("\t\t\t" + "Test End Time: ");
		wr.newLine();
		wr.write("\t\t\t" + "Script Execution Time: ");
		wr.newLine();
		wr.write("\t\t\t" + "Script Execution Status: ");
		wr.newLine();
		wr.write("\t\t\t"
				+ "*******************************************************");
		wr.newLine();
		wr.newLine();

		wr.close();

		bw1.close();
		bw2.close();
		// stepCount = 1;

	}


	/**
	 * @param FileName
	 * @param vDateForExcel
	 * @throws Exception
	 */
	public void createExcelResultTemplate(String FileName, String vDateForExcel)
			throws Exception {
		totalstepsgrand = 0;
		File folder = new File(System.getProperty("user.dir") + "/Reports/");
		if (!folder.exists()) {
			folder.mkdir();

		}
		outputExcelFile =GlobalVariables.applicationFolder + "/Reports/"
				+ FileName.toUpperCase() + "_" + vDateForExcel + ".xls";
		WritableWorkbook workbook = Workbook.createWorkbook(new File(
				outputExcelFile));
		WritableSheet sheet1 = workbook.createSheet("Test_Summary", 0);
		WritableSheet sheet2 = workbook.createSheet("Test_Cases", 1);
		WritableFont fontType = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.GOLD);
		WritableCellFormat fontFormat = new WritableCellFormat(fontType);
		fontFormat.setAlignment(Alignment.CENTRE);
		fontFormat.setBackground(Colour.DARK_RED2);
		fontFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		WritableFont fontType1 = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_RED2);
		WritableCellFormat fontFormat1 = new WritableCellFormat(fontType1);
		fontFormat1.setAlignment(Alignment.LEFT);
		fontFormat1.setBackground(Colour.TAN);
		fontFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
		Label label1 = new Label(0, 0, "Selenium Automation Test Results",
				fontFormat);
		Label label15 = new Label(0, 2, "Application Name: ", fontFormat1);
		Label label17 = new Label(0, 3, "Test Executed By: ", fontFormat1);
		Label label18 = new Label(0, 4, "Test Executed In: ", fontFormat1);
		Label label2 = new Label(0, 5, "Test Execution Date: ", fontFormat1);
		Label label3 = new Label(0, 6, "Test Execution Start: ", fontFormat1);
		Label label4 = new Label(0, 7, "Test Execution End: ", fontFormat1);
		Label label19 = new Label(0, 8, "Total Execution Time: ", fontFormat1);
		Label label5 = new Label(0, 9, "No of Test Cases: ", fontFormat1);
		Label label13 = new Label(0, 10, "Total Number of Steps: ", fontFormat1);
		Label label16 = new Label(2, 2, FileName, fontFormat1);
		Label label01 = new Label(2, 3,
				(System.getProperty("user.name")).toUpperCase(), fontFormat1);
		Label label02 = new Label(2, 4, "", fontFormat1);
		Label label6 = new Label(2, 5, currentDate(), fontFormat1);
		startTime = currentTimeInMillis();
		Label label7 = new Label(2, 6, formatIntoHHMMSS(startTime), fontFormat1);
		Label label8 = new Label(2, 7, "", fontFormat1);
		Label label9 = new Label(2, 8, " ", fontFormat1);
		Label label14 = new Label(2, 9, " ", fontFormat1);
		Label label03 = new Label(2, 10, " ", fontFormat1);
		Label label11 = new Label(0, 12, "Test case Name", fontFormat);
		Label label12 = new Label(3, 12, "Status", fontFormat);
		Label label10 = new Label(4, 12, "No of Steps", fontFormat);
		Label label21 = new Label(0, 0, "Step", fontFormat);	
		Label label22 = new Label(1, 0, "Expected Result", fontFormat);
		Label label23 = new Label(2, 0, "Actual Result", fontFormat);
		Label label24 = new Label(3, 0, "Status", fontFormat);
		sheet1.addCell(label01);
		sheet1.addCell(label02);
		sheet1.addCell(label03);
		sheet1.addCell(label1);
		sheet1.addCell(label2);
		sheet1.addCell(label3);
		sheet1.addCell(label4);
		sheet1.addCell(label5);
		sheet1.addCell(label6);
		sheet1.addCell(label7);
		sheet1.addCell(label8);
		sheet1.addCell(label9);
		sheet1.addCell(label10);
		sheet1.addCell(label11);
		sheet1.addCell(label12);
		sheet1.addCell(label13);
		sheet1.addCell(label14);
		sheet1.addCell(label15);
		sheet1.addCell(label16);
		sheet1.addCell(label17);
		sheet1.addCell(label18);
		sheet1.addCell(label19);
		sheet1.mergeCells(0, 0, 4, 0);
		sheet1.mergeCells(0, 1, 4, 1);
		sheet1.mergeCells(0, 2, 1, 2);
		sheet1.mergeCells(0, 3, 1, 3);
		sheet1.mergeCells(0, 4, 1, 4);
		sheet1.mergeCells(0, 5, 1, 5);
		sheet1.mergeCells(0, 6, 1, 6);
		sheet1.mergeCells(0, 7, 1, 7);
		sheet1.mergeCells(0, 8, 1, 8);
		sheet1.mergeCells(0, 9, 1, 9);
		sheet1.mergeCells(0, 10, 1, 10);
		sheet1.mergeCells(0, 11, 4, 11);
		sheet1.mergeCells(0, 12, 2, 12);
		sheet1.setColumnView(2, 25);
		sheet2.setColumnView(0, 10);
		sheet2.setColumnView(1, 55);
		sheet2.setColumnView(2, 55);
		sheet2.setColumnView(3, 10);
		sheet2.addCell(label21);
		sheet2.addCell(label22);
		sheet2.addCell(label23);
		sheet2.addCell(label24);
		sheet2.mergeCells(0, 1, 3, 1);
		sheet2.getSettings().setVerticalFreeze(1);
		sheet1.getSettings().setDefaultColumnWidth(15);
		workbook.write();
		workbook.close();
	}

	/**
	 * @param FileName
	 * @param vDateForReports
	 * @throws Exception
	 */
	public void createReportTemplate(String FileName, String vDateForReports)
			throws Exception {

		createExcelResultTemplate(FileName, vDateForReports);
		createHTMLResultTemplate(FileName, vDateForReports);
		createNotepadResultTemplate(FileName, vDateForReports);

	}

	protected static int getTestSummaryRowCount() throws Exception {
		Workbook workbook = Workbook.getWorkbook(new File(outputExcelFile));
		Sheet sheet1 = workbook.getSheet(0);
		return (sheet1.getRows());
	}

	protected static int getTestCasesRowCount() throws Exception {
		Workbook workbook = Workbook.getWorkbook(new File(outputExcelFile));
		Sheet sheet1 = workbook.getSheet(1);
		return (sheet1.getRows());
	}

	/**
	 * @param TestCaseName
	 * @param BrowserName
	 * @throws Exception
	 */
	public void writeTestCaseNameToExcel(String TestCaseName, String BrowserName)
			throws Exception {
System.out.println("Ravi : " + outputExcelFile);

		int summaryRownumber = getTestSummaryRowCount();
		int testRownumber = getTestCasesRowCount();
		Workbook workbook = Workbook.getWorkbook(new File(outputExcelFile));
		WritableWorkbook copy = Workbook.createWorkbook(new File(
				outputExcelFile), workbook);
		WritableSheet sheet1 = copy.getSheet(0);
		WritableSheet sheet2 = copy.getSheet(1);
		WritableFont fontType = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.SINGLE, Colour.DARK_RED2);
		WritableCellFormat fontFormat = new WritableCellFormat(fontType);
		fontFormat.setAlignment(Alignment.LEFT);
		fontFormat.setBackground(Colour.TAN);
		fontFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		WritableFont fontType1 = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_RED2);
		WritableCellFormat fontFormat1 = new WritableCellFormat(fontType1);
		fontFormat1.setAlignment(Alignment.LEFT);
		fontFormat1.setBackground(Colour.TAN);
		fontFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
		Label browserName = new Label(2, 4, BrowserName, fontFormat1);
		Label testCaseName = new Label(0, summaryRownumber, TestCaseName,
				fontFormat);
		Label Status = new Label(3, summaryRownumber, " ", fontFormat1);
		Label StepCounts = new Label(4, summaryRownumber, " ", fontFormat1);
		Number testCaseNumber = new Number(2, 9, summaryRownumber - 12,
				fontFormat1);
		sheet1.addCell(testCaseNumber);
		sheet1.addCell(browserName);
		sheet1.addCell(testCaseName);
		sheet1.addCell(Status);
		sheet1.addCell(StepCounts);
		sheet1.mergeCells(0, summaryRownumber, 2, summaryRownumber);
		WritableHyperlink hl = new WritableHyperlink(0, summaryRownumber,
				TestCaseName, sheet2, 0, testRownumber);
		sheet1.addHyperlink(hl);
		Label testCaseName1 = new Label(0, summaryRownumber, TestCaseName,
				fontFormat);
		sheet1.addCell(testCaseName1);
		Label TestName = new Label(0, testRownumber, "Test Case Name: "
				+ TestCaseName, fontFormat1);
		sheet2.mergeCells(0, testRownumber, 3, testRownumber);
		sheet2.addCell(TestName);
		copy.write();
		copy.close();
		workbook.close();
	}

	/**
	 * @param TestCaseName
	 * @param BrowserName
	 * @throws IOException
	 */
	public void createReports(String TestCaseName, String BrowserName)
			throws IOException {

		if (BrowserName.contains("ie")) {
			BrowserName = "INTERNET EXPLORER";
		} else if (BrowserName.contains("firefox")) {
			BrowserName = "FIREFOX";
		} else if (BrowserName.contains("chrome")) {
			BrowserName = "CHROME";
		} else {
			BrowserName = "HTMLUNIT DRIVER";
		}

		// createNotepad(TestCaseName,BrowserName);
		// createNotepadHTML(fileName, vGetDateForNotepad)
		try {
			writeTestCaseNameToExcel(TestCaseName, BrowserName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param value
	 * @param value1
	 * @param Result
	 * @throws IOException
	 */

	
	public void SummaryupdateReports(updateValue value, String value1, String Result)	throws IOException {
	
		
		   //srk	
			
			String newHTMLtext = null;

			File HTMLFile = new File(SummaryHTMLFile);

			BufferedReader HTMLFilereader = new BufferedReader(new FileReader(HTMLFile));

			String line1 = "", oldtext1 = "";
			while ((line1 = HTMLFilereader.readLine()) != null) 
			{
				oldtext1 += line1 + "\r\n";
				// System.out.println("Older HTML text is - " + oldtext1);
			}
			HTMLFilereader.close();

			switch (value) {
			case bName: {
				// try {
				// ReportManager.writeTestCaseNameToExcel(value1,Result);
				// } catch (Exception e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				value1 = "chrome";
				System.out.println("script is running ");
				
				String old_HTMLValue = "Browser Used Is 	: " + "<br/>";
				String new_HTMLValue = "Browser Used Is 	: " + value1 + "<br/>";

				// To replace a line in a file
				
				newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			
				FileWriter HTMLFilewriter = new FileWriter(SummaryHTMLFile);

				
				HTMLFilewriter.write(newHTMLtext);

				
				HTMLFilewriter.close();
				
				old_HTMLValue = null;
				new_HTMLValue = null;
				break;
			}
			case tEndTime: {

				String old_NotepadValue = "\t\t\t" + "Test End Time: ";
				String new_NotepadValue = "\t\t\t" + "Test End Time" + "\t\t"+ ": " + Calendar.getInstance().getTime();

				String old_HTMLValue = "Test End Time 		: " + "<br/>";
				String new_HTMLValue = "Test End Time 		: " + Calendar.getInstance().getTime().toString() + "<br/>";

				// To replace a line in a file
				
				newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			
				FileWriter HTMLFilewriter = new FileWriter(SummaryHTMLFile);

				
				HTMLFilewriter.write(newHTMLtext);

				
				HTMLFilewriter.close();
				old_NotepadValue = null;
				new_NotepadValue = null;
				old_HTMLValue = null;
				new_HTMLValue = null;

				break;
			}

			case execTime: {
				String old_NotepadValue = "\t\t\t" + "Script Execution Time: ";
				String new_NotepadValue = "\t\t\t" + "Script Execution Time" + "\t" + ": " + value1;

				String old_HTMLValue = "Script Execution Time 	: " + "<br/>";
				String new_HTMLValue = "Script Execution Time 	: " + value1 + "<br/>";

				// To replace a line in a file
				
				newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

				FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
				FileWriter HTMLFilewriter = new FileWriter(SummaryHTMLFile);

			
				HTMLFilewriter.write(newHTMLtext);

				NotePadFilewriter.close();
				HTMLFilewriter.close();

				old_NotepadValue = null;
				new_NotepadValue = null;
				old_HTMLValue = null;
				new_HTMLValue = null;

				break;
			}

			case execStatus: {

				String old_NotepadValue = "\t\t\t" + "Script Execution Status: ";
				String new_NotepadValue = "\t\t\t" + "Script Execution Status	: "+ Result;

				String old_HTMLValue = "Script Execution Status : " + "<br/>";
				String new_HTMLValue = null;
				
				if (Result.equalsIgnoreCase("1")) 
				{
					Result = "Pass";
					new_HTMLValue = "Script Execution Status : "+ "<FONT COLOR=\"#006400\"><b>" + Result.toUpperCase()+ "</b></FONT>" + "<br/>";
				} 
				else 
				{
					Result = "Fail";
					new_HTMLValue = "Script Execution Status : "+ "<FONT COLOR=\"#B22222\"><b>" + Result.toUpperCase()+ "</b></FONT>" + "<br/>";

				}

				// To replace a line in a file
				
				newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

				FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
				FileWriter HTMLFilewriter = new FileWriter(SummaryHTMLFile);

			
				HTMLFilewriter.write(newHTMLtext);
				NotePadFilewriter.close();
				HTMLFilewriter.close();
				old_NotepadValue = null;
				new_NotepadValue = null;
				old_HTMLValue = null;
				new_HTMLValue = null;
				break;

				
				
				
				
			}
			}
					
		}
	
	
	
	
	
	public void updateReports(updateValue value, String value1, String Result)	throws IOException {

		// totalSteps = stepCount;
		// totalstepsgrand = totalstepsgrand + totalSteps;
       //srk
		
	   //srk	
		String newNotepadText = null;
		String newHTMLtext = null;

		File NotePadFile = new File(outputNotePadResultFile);
		File HTMLFile = new File(outputHTMLFile);

		BufferedReader NotePadFilereader = new BufferedReader(new FileReader(NotePadFile));
		BufferedReader HTMLFilereader = new BufferedReader(new FileReader(HTMLFile));

		String line = "", oldtext = "";
		while ((line = NotePadFilereader.readLine()) != null) 
		{
			oldtext += line + "\r\n";
			// System.out.println("Older HTML text is - " + oldtext);
		}
		NotePadFilereader.close();

		String line1 = "", oldtext1 = "";
		while ((line1 = HTMLFilereader.readLine()) != null) 
		{
			oldtext1 += line1 + "\r\n";
			// System.out.println("Older HTML text is - " + oldtext1);
		}
		HTMLFilereader.close();

		switch (value) {
		case bName: {
			// try {
			// ReportManager.writeTestCaseNameToExcel(value1,Result);
			// } catch (Exception e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			value1 = "Chrome";
			System.out.println("In srk ");
			String old_NotepadValue = "\t\t\t" + "Browser Used Is" + "\t\t"+ ": ";
			String new_NotepadValue = "\t\t\t" + "Browser Used Is" + "\t\t"+ ": " + value1;

			String old_HTMLValue = "Browser Used Is 	: " + "<br/>";
			String new_HTMLValue = "Browser Used Is 	: " + value1 + "<br/>";

			// To replace a line in a file
			newNotepadText = oldtext.replaceAll(old_NotepadValue,new_NotepadValue);
			newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
			FileWriter HTMLFilewriter = new FileWriter(outputHTMLFile);

			NotePadFilewriter.write(newNotepadText);
			HTMLFilewriter.write(newHTMLtext);

			NotePadFilewriter.close();
			HTMLFilewriter.close();
			old_NotepadValue = null;
			new_NotepadValue = null;
			old_HTMLValue = null;
			new_HTMLValue = null;
			break;
		}
		case tEndTime: {

			String old_NotepadValue = "\t\t\t" + "Test End Time: ";
			String new_NotepadValue = "\t\t\t" + "Test End Time" + "\t\t"+ ": " + Calendar.getInstance().getTime();

			String old_HTMLValue = "Test End Time 		: " + "<br/>";
			String new_HTMLValue = "Test End Time 		: " + Calendar.getInstance().getTime().toString() + "<br/>";

			// To replace a line in a file
			newNotepadText = oldtext.replaceAll(old_NotepadValue,new_NotepadValue);
			newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
			FileWriter HTMLFilewriter = new FileWriter(outputHTMLFile);

			NotePadFilewriter.write(newNotepadText);
			HTMLFilewriter.write(newHTMLtext);

			NotePadFilewriter.close();
			HTMLFilewriter.close();
			old_NotepadValue = null;
			new_NotepadValue = null;
			old_HTMLValue = null;
			new_HTMLValue = null;

			break;
		}

		case execTime: {
			String old_NotepadValue = "\t\t\t" + "Script Execution Time: ";
			String new_NotepadValue = "\t\t\t" + "Script Execution Time" + "\t" + ": " + value1;

			String old_HTMLValue = "Script Execution Time 	: " + "<br/>";
			String new_HTMLValue = "Script Execution Time 	: " + value1 + "<br/>";

			// To replace a line in a file
			newNotepadText = oldtext.replaceAll(old_NotepadValue,new_NotepadValue);
			newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
			FileWriter HTMLFilewriter = new FileWriter(outputHTMLFile);

			NotePadFilewriter.write(newNotepadText);
			HTMLFilewriter.write(newHTMLtext);

			NotePadFilewriter.close();
			HTMLFilewriter.close();

			old_NotepadValue = null;
			new_NotepadValue = null;
			old_HTMLValue = null;
			new_HTMLValue = null;

			break;
		}

		case execStatus: {

			String old_NotepadValue = "\t\t\t" + "Script Execution Status: ";
			String new_NotepadValue = "\t\t\t" + "Script Execution Status	: "+ Result;

			String old_HTMLValue = "Script Execution Status : " + "<br/>";
			String new_HTMLValue = null;
			
			if (Result.equalsIgnoreCase("1")) 
			{
				Result = "Pass";
				new_HTMLValue = "Script Execution Status : "+ "<FONT COLOR=\"#006400\"><b>" + Result.toUpperCase()+ "</b></FONT>" + "<br/>";
			} 
			else 
			{
				Result = "Fail";
				new_HTMLValue = "Script Execution Status : "+ "<FONT COLOR=\"#B22222\"><b>" + Result.toUpperCase()+ "</b></FONT>" + "<br/>";

			}

			// To replace a line in a file
			newNotepadText = oldtext.replaceAll(old_NotepadValue,new_NotepadValue);
			newHTMLtext = oldtext1.replaceAll(old_HTMLValue, new_HTMLValue);

			FileWriter NotePadFilewriter = new FileWriter(outputNotePadResultFile);
			FileWriter HTMLFilewriter = new FileWriter(outputHTMLFile);

			NotePadFilewriter.write(newNotepadText);
			HTMLFilewriter.write(newHTMLtext);
			NotePadFilewriter.close();
			HTMLFilewriter.close();
			old_NotepadValue = null;
			new_NotepadValue = null;
			old_HTMLValue = null;
			new_HTMLValue = null;
			break;

		}

		}

	}

	/**
	 * @param status
	 * @param expected
	 * @param actual
	 */
	
	public static void writeToSummary(String status, String TestCase) {
		try {

			boolean append = true;

			FileWriter fout1 = new FileWriter(SummaryHTMLFile, append);
			BufferedWriter fbw1 = new BufferedWriter(fout1);

			String gstatus = null;
			if (status.equalsIgnoreCase("pass")) {
				gstatus = "<FONT COLOR=\"#006400\"><b>" + status.toUpperCase()
						+ "</b></FONT>";
			} else {
				gstatus = "<FONT COLOR=\"#B22222\"><b>" + status.toUpperCase()
						+ "</b></FONT>";
			}
		

			fbw1.write("<!DOCTYPE html><html><body>"
					+ "<table border=\"1\" width=\"100%\">" +

					"<td align=\"center\" width=\"5%\">" + stepCountSummary + "</td>"
					+ "<td align=\"center\" width=\"12%\">"
					+ getDateFormat(vDatetype9) + "</td>"
					+ "<td width=\"35%\">" +"<a href=" + TestCase + ".html>" + TestCase + "</a>"+"</td>"
					//+ "<td width=\"35%\">" +"<a href= TestCase"+TestCase  "</td>"
					//+ "<td width=\"38%\">" + actual + "</td>"
					+ "<td align=\"center\" width=\"20%\">" + gstatus + "</td>"
					+ "</font></table></body></html>");

			fbw1.close();

			
			//stepCountSummary++;

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	
	
	protected static void writeToHTML(String status, String expected,
			String actual) {
		try {

			boolean append = true;

			FileWriter fout1 = new FileWriter(outputHTMLFile, append);
			BufferedWriter fbw1 = new BufferedWriter(fout1);

			String gstatus = null;
			if (status.equalsIgnoreCase("pass")) {
				gstatus = "<FONT COLOR=\"#006400\"><b>" + status.toUpperCase()
						+ "</b></FONT>";
			} else {
				gstatus = "<FONT COLOR=\"#B22222\"><b>" + status.toUpperCase()
						+ "</b></FONT>";
			}

			fbw1.write("<!DOCTYPE html><html><body>"
					+ "<table border=\"1\" width=\"100%\">" +

					"<td align=\"center\" width=\"5%\">" + stepCount + "</td>"
					+ "<td align=\"center\" width=\"12%\">"
					+ getDateFormat(vDatetype9) + "</td>"
					+ "<td width=\"35%\">" + expected + "</td>"
					+ "<td width=\"38%\">" + actual + "</td>"
					+ "<td align=\"center\" width=\"10%\">" + gstatus + "</td>"
					+ "</font></table></body></html>");

			fbw1.close();

			//stepCount++;

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	/**
	 * @param status
	 * @param expected
	 * @param actual
	 */
	protected static void writeToNotepad(String status, String expected,
			String actual) {
		try {

			boolean append = true;
			FileWriter fout = new FileWriter(outputNotePadResultFile, append);
			FileWriter fout2 = new FileWriter(outPutNotepadStepsFile, append);
			BufferedWriter fbw = new BufferedWriter(fout);
			BufferedWriter fbw2 = new BufferedWriter(fout2);

			fbw2.write(expected + " --> ");

			fbw.write("Step ");
			fbw.write(stepCount + "");
			fbw.write("		");
			// fbw.write("(" + getDateTimeForNotepad() + ")");
			fbw.write(getDateFormat(vDatetype9));
			fbw.write("		");
			fbw.write(status.toUpperCase());
			fbw.newLine();
			fbw.write("----------------------------------------------------");
			fbw.newLine();
			fbw.write("Step Description : " + expected);
			fbw.newLine();
			fbw.write("Actual Result	 : " + actual);
			fbw.newLine();
			fbw.newLine();
			fbw.newLine();

			fbw.close();
			fbw2.close();
			//stepCount++;

		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	/**
	 * @param Status
	 * @param Expected
	 * @param Actual
	 * @throws Exception
	 */
	protected static void WriteToExcel(String Status, String Expected,
			String Actual) throws Exception {
		int usedRow = getTestCasesRowCount();
		Workbook workbook = Workbook.getWorkbook(new File(outputExcelFile));
		WritableWorkbook copy = Workbook.createWorkbook(new File(
				outputExcelFile), workbook);
		WritableSheet sheet1 = copy.getSheet(1);
		WritableFont fontType = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.NO_BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.BLACK);
		WritableCellFormat format = new WritableCellFormat(fontType);
		format.setAlignment(Alignment.CENTRE);
		format.setVerticalAlignment(VerticalAlignment.CENTRE);
		format.setBorder(Border.ALL, BorderLineStyle.THIN);
		format.setWrap(true);
		WritableFont passedFont = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.GREEN);
		WritableCellFormat passedFormat = new WritableCellFormat(passedFont);
		passedFormat.setAlignment(Alignment.CENTRE);
		passedFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		passedFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
		passedFormat.setWrap(true);
		WritableFont failedfont = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.RED);
		WritableCellFormat failedformat = new WritableCellFormat(failedfont);
		failedformat.setAlignment(Alignment.CENTRE);
		failedformat.setVerticalAlignment(VerticalAlignment.CENTRE);
		failedformat.setBorder(Border.ALL, BorderLineStyle.THIN);
		failedformat.setWrap(true);
		WritableFont warningfont = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.ORANGE);
		WritableCellFormat warningformat = new WritableCellFormat(warningfont);
		warningformat.setAlignment(Alignment.CENTRE);
		warningformat.setVerticalAlignment(VerticalAlignment.CENTRE);
		warningformat.setBorder(Border.ALL, BorderLineStyle.THIN);
		warningformat.setWrap(true);
		WritableCellFormat format1 = new WritableCellFormat(fontType);
		format1.setAlignment(Alignment.LEFT);
		format1.setBorder(Border.ALL, BorderLineStyle.THIN);
		format1.setVerticalAlignment(VerticalAlignment.TOP);
		format1.setWrap(true);
		Number StepNumber = new Number(0, usedRow, stepCount, format);
		sheet1.addCell(StepNumber);
		Label ExpectedResult = new Label(1, usedRow, Expected, format1);
		sheet1.addCell(ExpectedResult);
		Label ActualResult = new Label(2, usedRow, Actual, format1);
		sheet1.addCell(ActualResult);
		if ((Status.toLowerCase()).contains("fail"))
		{
			if(finalResultFlag)
			{
				finalResultFlag = false;
				finalResult = "FAIL";
			}
			Label status = new Label(3, usedRow, Status.toUpperCase(),failedformat);
			sheet1.addCell(status);
		} else if ((Status.toLowerCase()).contains("warning")) {
			Label status = new Label(3, usedRow, "WARN", warningformat);
			sheet1.addCell(status);
		} else {
			Label status = new Label(3, usedRow, Status.toUpperCase(),
					passedFormat);
			sheet1.addCell(status);
		}
		copy.write();
		copy.close();
		workbook.close();
		//stepCount++;
	}

	/**
	 * @param testResult
	 * @throws Exception
	 */
	//Chetan
	public int getFinalResult()
	{
		if(finalResult.equalsIgnoreCase("pass"))
				return 1;
		else
				return 0;

	}
	public void updatExcelTestResult(int testResult) throws Exception {
		int summaryRownumber = getTestSummaryRowCount();
		int testRownumber = getTestCasesRowCount();
		Workbook workbook = Workbook.getWorkbook(new File(outputExcelFile));
		WritableWorkbook copy = Workbook.createWorkbook(new File(
				outputExcelFile), workbook);
		WritableSheet sheet1 = copy.getSheet(0);
		WritableSheet sheet2 = copy.getSheet(1);
		if (testResult == 1) {
			WritableFont fontType1 = new WritableFont(
					WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
					false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_GREEN);
			WritableCellFormat fontFormat1 = new WritableCellFormat(fontType1);
			fontFormat1.setAlignment(Alignment.CENTRE);
			fontFormat1.setBackground(Colour.TAN);
			fontFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
			Label status = new Label(3, summaryRownumber - 1, "Pass",
					fontFormat1);
			Number StepCounts = new Number(4, summaryRownumber - 1, stepCount,
					fontFormat1);
			sheet1.addCell(status);
			sheet1.addCell(StepCounts);
		} else {
			WritableFont fontType1 = new WritableFont(
					WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
					false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_RED);
			WritableCellFormat fontFormat1 = new WritableCellFormat(fontType1);
			fontFormat1.setAlignment(Alignment.CENTRE);
			fontFormat1.setBackground(Colour.TAN);
			fontFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
			WritableFont fontType2 = new WritableFont(
					WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
					false, UnderlineStyle.SINGLE, Colour.DARK_RED);
			WritableCellFormat fontFormat2 = new WritableCellFormat(fontType2);
			fontFormat2.setAlignment(Alignment.CENTRE);
			fontFormat2.setBackground(Colour.TAN);
			fontFormat2.setBorder(Border.ALL, BorderLineStyle.THIN);
			Number StepCounts = new Number(4, summaryRownumber - 1, stepCount,
					fontFormat1);
			WritableHyperlink hl = new WritableHyperlink(3,
					summaryRownumber - 1, "Fail", sheet2, 0, testRownumber - 1);
			sheet1.addHyperlink(hl);
			Label status = new Label(3, summaryRownumber - 1, "Fail",
					fontFormat2);
			sheet1.addCell(status);
			sheet1.addCell(StepCounts);
		}
		WritableFont fontType1 = new WritableFont(
				WritableFont.createFont("calibri"), 11, WritableFont.BOLD,
				false, UnderlineStyle.NO_UNDERLINE, Colour.DARK_RED2);
		WritableCellFormat fontFormat1 = new WritableCellFormat(fontType1);
		fontFormat1.setAlignment(Alignment.LEFT);
		fontFormat1.setBackground(Colour.TAN);
		fontFormat1.setBorder(Border.ALL, BorderLineStyle.THIN);
		Number label14 = new Number(2, 10, (totalstepsgrand), fontFormat1);
		sheet1.addCell(label14);
		endTime = currentTimeInMillis();
		Label label8 = new Label(2, 7, formatIntoHHMMSS(endTime), fontFormat1);
		sheet1.addCell(label8);
		totalTime = endTime - startTime;
		Label label9 = new Label(2, 8, textExecutionTime(endTime - startTime),
				fontFormat1);
		sheet1.addCell(label9);
		sheet2.mergeCells(0, testRownumber, 3, testRownumber);
		copy.write();
		copy.close();
		workbook.close();
	}

	/**
	 * @param Status
	 * @param Expected
	 * @param Actual
	 */
	
public static void ReportSummaryEvent(TestStatus Status, String TestCase) {
		
		switch (Status) {
		case PASS: {
			try {
				
				writeToSummary("PASS", TestCase);
			
				//Assert.assertTrue(true, Actual);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCountSummary++;
			break;
		}
		case FAIL: {
			try {
				
				writeToSummary("PASS", TestCase);
			
				//writeToNotepad("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
				//writeToHTML("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
				//WriteToExcel("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
		//	Assert.fail();
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCountSummary++;
			break;
		}
		case WARNING: {
			try {
				
				writeToSummary("PASS", TestCase);
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCountSummary++;
			break;
		}

		}
	}
	
	
	
	public static void ReportEvent(TestStatus Status, String Expected,String Actual) {
		
		switch (Status) {
		case PASS: {
			try {
				writeToNotepad("PASS", Expected, Actual);
				writeToHTML("PASS", Expected, Actual);
				WriteToExcel("PASS", Expected, Actual);
				Assert.assertTrue(true, Actual);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCount++;
			break;
		}
		case FAIL: {
			try {
				writeToNotepad("FAIL", Expected, Actual.toUpperCase());
				writeToHTML("FAIL", Expected, Actual.toUpperCase());
				WriteToExcel("FAIL", Expected, Actual.toUpperCase());
				failurepopUp(Expected, Actual);
				//writeToNotepad("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
				//writeToHTML("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
				//WriteToExcel("FAIL", "",
						//"Exiting Script Execution".toUpperCase());
		//	Assert.fail();
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCount++;
			break;
		}
		case WARNING: {
			try {
				writeToNotepad("WARNING", Expected, Actual);
				writeToHTML("WARNING", Expected, Actual);
				WriteToExcel("WARNING", Expected, Actual);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			stepCount++;
			break;
		}

		}
	}
	
	
	/**
	 * @param Expected
	 * @param Actual
	 */
	protected static void failurepopUp(String Expected, String Actual) {

		JFrame jframe = new JFrame();
		String sTitle = "Automation Revolution";
		jframe.setTitle(sTitle.toUpperCase());
		jframe.setSize(600, 150);

		java.awt.Toolkit kit = jframe.getToolkit();
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		Insets in = kit.getScreenInsets(gs[0].getDefaultConfiguration());

		Dimension d = kit.getScreenSize();
		int max_width = (d.width - in.left - in.right);
		int max_height = (d.height - in.top - in.bottom);

		jframe.setLocation((int) (max_width - jframe.getWidth()) / 2,
				(int) (max_height - jframe.getHeight()) / 2);

		Container pane = jframe.getContentPane();
		pane.setLayout(new GridLayout(4, 1));

		JLabel L0 = new JLabel(" Test Execution Failure Notification");
		L0.setBackground(Color.YELLOW);

		JLabel L1 = new JLabel(" STEP NO: " + stepCount);
		L1.setForeground(Color.RED);

		JLabel L2 = new JLabel(" Description: " + Expected);
		JLabel L3 = new JLabel(" Actual Result: " + Actual.toUpperCase());
		L3.setForeground(Color.RED);

		pane.add(L0);
		pane.add(L1);
		pane.add(L2);
		pane.add(L3);
		jframe.setVisible(true);
		jframe.setAlwaysOnTop(true);

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		jframe.dispose();
	}

	/**
	 * @param methodName
	 * @throws IOException
	 */
	public static void writeLog(String methodName) throws IOException {
		boolean append = true;
		FileWriter fout = new FileWriter(outPutNotepadLogFile, append);
		BufferedWriter fbw = new BufferedWriter(fout);
		fbw.write("Step ");
		fbw.write(LogCount + "" + "\t");
		fbw.write(getDateFormat(vDatetype9) + "\t");
		fbw.write("Inside method " + methodName);
		fbw.newLine();
		fbw.close();
		LogCount++;
		// TODO Auto-generated method stub

	}

	/**
	 * @param Result
	 * @return
	 */
	public String getFinalStatus(int Result) {
		// TODO Auto-generated method stub
		System.out.println(Result+"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
		String TestStatus = null;
		if (Result == 1) 
		{
			TestStatus = "PASS";
		} 
		else if (Result == 2) 
		{
			TestStatus = "FAIL";
		}
		else 
		{
			TestStatus = "WARNING";
		}
		return TestStatus;
	}

	public static String getDateFormat(String vDateFormat) {
		
		Date vDate = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat(vDateFormat);
		return sdf.format(vDate);

	}
	protected static String currentTime() {

 		Date date = Calendar.getInstance().getTime();
 		SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:ss a");
 		return (dateformat.format(date));

 	}
	protected static String currentDate() {

 		Date date = Calendar.getInstance().getTime();
 		SimpleDateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
 		return (dateformat.format(date));

 	}
	protected static long currentTimeInMillis() {

 		long currentTime = Calendar.getInstance().getTimeInMillis();
		return currentTime;
 	}
	protected static String formatIntoHHMMSS(long milliSeconds) {
		 
 		DateFormat f1 = new SimpleDateFormat("hh:mm:ss a");
		return(f1.format(milliSeconds));
 	}
	protected static String textExecutionTime(long diffSeconds) {
		 
 	    diffSeconds = diffSeconds / 1000;
        long hours = diffSeconds / 3600;
        long remainder = diffSeconds % 3600;
        long minutes = remainder / 60;
        long seconds = remainder % 60;

        return ((hours < 10 ? "0" : "") + hours + ":"
                    + (minutes < 10 ? "0" : "") + minutes + ":"
                    + (seconds < 10 ? "0" : "") + seconds);
 	}	
}
