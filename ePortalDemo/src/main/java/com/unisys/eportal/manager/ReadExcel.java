package com.unisys.eportal.manager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadExcel {

	private String inputFile;
	private String sheetName;

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public HashMap<Integer, ArrayList> read() throws IOException  {
		File inputWorkbook = new File(inputFile);
		Workbook w;
		HashMap<Integer, ArrayList> sheetInfo = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			// Get the first sheet
			Sheet sheet = w.getSheet(sheetName);
			// Loop over first 10 column and lines
			sheetInfo = new HashMap<Integer, ArrayList>();
			ArrayList<String> columnInfo = null;
			for (int i = 0; i < sheet.getRows(); i++) {
				columnInfo = new ArrayList();
				for (int j = 0; j < sheet.getColumns(); j++) {
					Cell cell = sheet.getCell(j, i);
//					System.out.println("I got a label "
//							+ cell.getContents());
//					CellType type = cell.getType();
//					if (cell.getType() == CellType.LABEL) {
//						System.out.println("I got a label "
//								+ cell.getContents());
//					}
//
//					if (cell.getType() == CellType.NUMBER) {
//						System.out.println("I got a number "
//								+ cell.getContents());
//					}
					columnInfo.add(cell.getContents());
					
				}
				sheetInfo.put(new Integer(i), columnInfo);
			}
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return sheetInfo;
	}
//
//	public static void main(String[] args) throws IOException {
//		ReadExcel test = new ReadExcel();
//		test.setInputFile("c:/temp/lars.xls");
//		test.read();
//	}

	/**
	 * @return the sheetName
	 */
	public String getSheetName() {
		return sheetName;
	}

	/**
	 * @param sheetName the sheetName to set
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
	
	public int getRowCount() throws IOException
	{
		File inputWorkbook = new File(inputFile);
		Workbook w;
		HashMap<Integer, ArrayList> sheetInfo = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			// Get the first sheet
			Sheet sheet = w.getSheet(sheetName);
			// Loop over first 10 column and lines
			return sheet.getRows();
			} 
			catch (BiffException e) 
			{
					e.printStackTrace();
					return 0;
			}
	}
	
}