Feature: Deployed App Property
 
Scenario: Successful gather properties on an deployed application
	Given User is on ePortal Manager Home Page
	When User Navigate to Web Cluster Page
	Then Navigate to Managed Deployed Application
	And Fetch the deployed application properties successfully