Feature: Bring Cluster online
 
Scenario: Successful create a cluster and bring it online
    Given User is on ePortal Manager Home Page to create a cluster and bring it online
    When User Navigate to web cluster section to create new cluster and bring it online
    Then User successfully created a cluster and brought it online