#Author: mohammed.kankudti@unisys.com
Feature: Deployed Application Operation

  Scenario: Validate Remove Deployed Application Serial Sync Method
    Given User is on ePortal home Page
    When User checks for Deployed Application
    Then Validate Deployed Application Removal Serial Sync Method
