package com.unisys.eportal.BackendDescription;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.unisys.eportal.manager.Common_Methods;
import static com.unisys.eportal.manager.LoginPageData.*;
import com.unisys.eportal.manager.Initialize;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.updateValue;
import com.unisys.eportal.manager.Initialize.TestStatus;

public class BackendDescription {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;
	public String Chrome;
	
	//added By Ravi Kumar
	//Date- 10th Jan 2019
	//TCD- CPE-1151
	//Description- Modify the descripition of backend server.

@Given("^New User is on ePortal Manager Home Page$")
public void I_want_to_open_eportal_Manager_to_Modify_Backend_Description() throws Throwable {
	System.out.println("Opening Browser");
	System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	
	
//code for reporting start	
     System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
	 GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
	 RM = GlobalVariables.RM;
	 GlobalVariables.PartitionName = "ePortal_Backend_Description"; 
	 GlobalVariables.SummaryName = "ePortal_Summary"; 
	 GlobalVariables.RM.createHTMLSummaryTemplate(GlobalVariables.SummaryName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 RM.updateReports(updateValue.bName, Chrome, "");
	 RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);	
//Code for Opening the browser
	CM = new Common_Methods(this.driver);
	 CM.openBrowser(ePortalUrl);
	System.out.println("Browser Launched successfully");
  
}


@When("^User Navigates to modify Backend Description$")
public void User_Navigate_to_Backend_server_description() throws Throwable {
	 Thread.sleep(10000);
	CM.ModifyDescription();
	 Thread.sleep(10000);
	 System.out.println("Test case steps executed successfully");
}
	 
@Then("^Successfully modify backend description$")
public void Modify_Backend_description_successful() throws Throwable {
    
//code for reporting  end	  
		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "",String.valueOf(RM.getFinalResult()));
		//ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		//Excel Reporting
		ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());	
		driver.close();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + Browseclose+"'", "Verified - '"+ Browseclose + "' - Successfull");
		System.out.println("Browser closed successfully");
//code for repoting end	
}
}



