package com.unisys.eportal.EstablishClient;

import static com.unisys.eportal.manager.LoginPageData.ePortalUrl;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.unisys.eportal.manager.Common_Methods;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//CPE-1096 97
public class EstablishClient {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;
	public String Chrome;

	@Given("^User is on ePortal Manager Home Page$")
	public void Create_a_New_WebCLuster() throws Throwable {

		System.out.println("Opening Browser");
		System.setProperty("webdriver.chrome.driver", "C:\\Old D drive\\eportal\\New folder\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;

		//Execution of repoting 

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;
		GlobalVariables.PartitionName = "ePortal_ClientConnection";
		GlobalVariables.DesiredAppstate ="Online";
		GlobalVariables.DeppAppName = "webapplication1";
		GlobalVariables.ClusterName = "TEST";
		GlobalVariables.DepAppUserCode = "EPORTAL";
		GlobalVariables.DepAppPassword="EPORTAL";	
		GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
		RM.updateReports(updateValue.bName, Chrome, "");
		RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);	


		//code for login to manager website
		CM = new Common_Methods(this.driver);
		CM.openBrowser(ePortalUrl);
		//Runtime.getRuntime().exec("C:\\Old D drive\\eportal\\autoit-v3.3.14.0\\install\\credential.exe");
		System.out.println("Now in Home Page");

	}


	@When("^User Navigate to web cluster property page and fetch Virtual Ip$")
	public void User_Navigate_to_web_cluster_property_page_and_fetch_Virtual_Ip() throws Throwable {
		CM.SelectWebclusterPropertyPage(GlobalVariables.ClusterName);
		Thread.sleep(4000);

	}

	@Then("^Access the application with IPaddress and application name$")
	public void Access_the_application_with_IPaddress_and_application_name() throws Throwable {

		Thread.sleep(4000);
		CM.launchWebApplication();	 
		//Execution of repoting 

		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "",String.valueOf(RM.getFinalResult()));

		//Excel Reporting
		//ReportManager.ReportEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());

		//Execution of repoting
		driver.close();


	}

}
