package com.unisys.eportal.ClearAtOnce;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.Properties;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.*;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.unisys.eportal.manager.Common_Methods;
import static com.unisys.eportal.manager.LoginPageData.*;
import com.unisys.eportal.manager.Initialize;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.updateValue;
import com.unisys.eportal.manager.Initialize.TestStatus;
//import com.unisys.eportal.steps.Pmassignment.ManagerLogin;


public class ClearAtOnce {
	
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;
	
	public String Chrome;
	public static String edgeDriver = "D:\\Automation\\Automation_v.2.0\\EdgeDriver\\MicrosoftWebDriver.exe";
	public static String IEDriver = "D:\\Automation\\Automation_v.2.0\\iedriver\\IEDriverServer_Win32_3.9.0\\IEDriverServer.exe";

	@Given("^User is on ePortal Manager Home Page1$")
	public void I_want_to_open_eportal_Manager() throws Throwable {	
	System.out.println("Opening Browser");
	System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	
	
//code for reporting 
	
     System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
	 GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
	 RM = GlobalVariables.RM;
	 GlobalVariables.PartitionName = "ePortal_Clear_At_Once"; 
	 GlobalVariables.SummaryName = "ePortal_Summary"; 
	 GlobalVariables.RM.createHTMLSummaryTemplate(GlobalVariables.SummaryName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 RM.updateReports(updateValue.bName, Chrome, "");
	 RM.SummaryupdateReports(updateValue.bName, Chrome, "");
	 RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);
	 
//Code for Launching application
	CM = new Common_Methods(this.driver);
	CM.openBrowser(ePortalUrl);
	//Thread.sleep(1000);
	// Running Script using IE browser
	//.CM.openIEBrowserLaunhApp(ePortalUrl);
	
	//Runtime.getRuntime().exec("C:\\Users\\Kumarra1\\Downloads\\autoit-v3\\check1.exe");
	Thread.sleep(1000);
	System.out.println("Now in Home Page");
  
}

	// code for deploying application.
	
		@When("^User Navigates to Manager Partion$")
		public void User_Navigate_to_Manage_Partion_Alerts() throws Throwable {
		 Thread.sleep(10000);
		 CM.ClearAtOnce(); 
		 Thread.sleep(10000);

	}

		@Then("^Clear alerts for Manager partion$")
		public void clear_alert_for_Manager_Partion() throws Throwable {
		
		System.out.println("All alerts are cleared at once");

		 
	//Execution of repoting 
		
			RM.updateReports(updateValue.tEndTime, "", "");
			//RM.SummaryupdateReports(updateValue.tEndTime, "", "");
			RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
			
			 ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
				
			RM.updatExcelTestResult(RM.getFinalResult());
			
			
			driver.close();
			
	//Execution of repoting 	


	}

}

