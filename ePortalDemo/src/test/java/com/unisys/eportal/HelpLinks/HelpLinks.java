package com.unisys.eportal.HelpLinks;

import static com.unisys.eportal.manager.LoginPageData.WEBCLUSTER;
import static com.unisys.eportal.manager.LoginPageData.WEBCLUSTERMSG;
import static com.unisys.eportal.manager.LoginPageData.ePortalUrl;

import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.unisys.eportal.manager.Common_Methods;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

//Verify Button Works Properly CP-1174
public class HelpLinks {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;

	public String Chrome;

	@Given("^User is on ePortal home Page$")
	public void Open_WebPartition() throws Throwable {
		System.out.println("Opening Browser");
		System.setProperty("webdriver.chrome.driver",
				"C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

//code for reporting 

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(),
				ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;
		GlobalVariables.RM.createHTMLResultTemplate("VERIFY BUTTON WORKS PROPERLY",
				ReportManager.getDateFormat(ReportManager.vDatetype2));
		RM.updateReports(updateValue.bName, Chrome, "");
		RM.writeTestCaseNameToExcel("VERIFY BUTTON WORKS PROPERLY", Chrome);

//Code for reporting
		CM = new Common_Methods(this.driver);
		CM.openBrowser(ePortalUrl);
		// Runtime.getRuntime().exec("C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\autoit\\cr.exe");
		System.out.println("Now in Home Page");

	}

	@When("^User goes to help links$")
	public void User_goes_to_help_links() throws Throwable {
		Thread.sleep(1000 * 2);
		CM.CheckButtonWork();
	}

	@Then("^Verify the help links$")
	public void Verify_the_help_links() throws Throwable {

		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,
				CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis() - vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "", String.valueOf(RM.getFinalResult()));

//Excel Reporting
		ReportManager.ReportSummaryEvent(TestStatus.PASS, GlobalVariables.PartitionName);
		// RM.writeToSummary(String.valueOf(RM.getFinalResult()),GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());
		driver.close();
	}

}
