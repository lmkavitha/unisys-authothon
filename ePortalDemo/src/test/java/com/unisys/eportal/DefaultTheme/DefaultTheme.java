package com.unisys.eportal.DefaultTheme;
import static com.unisys.eportal.manager.LoginPageData.ePortalUrl;

import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.unisys.eportal.manager.Common_Methods;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class DefaultTheme {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;
	public String Chrome;
	
	

@Given("^User is on ePortal Manager Home Page to update default theme$")
public void I_want_to_open_eportal_Manager_update_Default_Theme() throws Throwable {
	System.out.println("Opening Browser");
	System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	
	
//code for reporting start	
     System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
	 GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
	 RM = GlobalVariables.RM;
	 GlobalVariables.PartitionName = "ePortal_Default_Theme"; 
	 GlobalVariables.SummaryName = "ePortal_Summary"; 
	 GlobalVariables.RM.createHTMLSummaryTemplate(GlobalVariables.SummaryName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 RM.updateReports(updateValue.bName, Chrome, "");
	// RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);	
//Code for Opening the browser
	CM = new Common_Methods(this.driver);
	 CM.openBrowser(ePortalUrl);
	// Runtime.getRuntime().exec("C:\\Users\\Kumarra1\\Downloads\\autoit-v3\\check1.exe");
  
}


@When("^User Navigate to Preferences page$")
public void User_Navigate_to_preferences_page() throws Throwable {
	 Thread.sleep(10000);
	 CM.DefaultTheme();
	 Thread.sleep(10000);
}
	 
@Then("^User selects the default theme$")
public void Change_Default_theme_and_verify() throws Throwable {
    // Express the Regexp above with the code you wish you had
    System.out.println("Perfomance statistics displays having various parameters reading");
    
//code for reporting  end	  
		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "",String.valueOf(RM.getFinalResult()));
		//ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);


		//Excel Reporting
		//RM.updatExcelTestResult(RM.getFinalResult());	
		driver.close();
//code for repoting end		
	
}
}
