package come.unisys.eportal.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(

		glue = "com.unisys.eportal.AddPartitionToCluster", features = "classpath:cucumber/AddPartitionToCluster.feature")

public class AddPartitionToClusterRunner {

}
