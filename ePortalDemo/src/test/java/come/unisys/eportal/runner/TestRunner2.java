package come.unisys.eportal.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.steps.emailnotification",
        features = "classpath:cucumber/emailnotification.feature"
)
public class TestRunner2 {

}