package come.unisys.eportal.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
//added By Ravi Kumar
	//Date- 7th Jan 2019
	//TCD- CPE-1161
	// Description- Verify right frame links are linked to appropriate destination page.
@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.RightFrameLink",
        features = "classpath:cucumber/RightFrameLink.feature"
)

public class RightFrameLinkRunner {

}
