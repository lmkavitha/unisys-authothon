
package come.unisys.eportal.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.IISLogFetc",
        features = "classpath:cucumber/IISFunc.feature"
)
public class IISFuncRunner {

}