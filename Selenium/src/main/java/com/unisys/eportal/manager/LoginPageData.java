package com.unisys.eportal.manager;

public class LoginPageData {
	/*
	 * public static final String LOGINBUTTON = "loginbutton";
	 * 
	 * public static final String ID_PASSWORD = "_58_password"; public static final
	 * String ID_LOGIN = "_58_login"; //public static final String URL =
	 * "https://portal-qa.ito.unisys.com/web/vp-multisys/login";
	 */
	// srk
	public static final String URL = "http://portaladmin:Kodiak4Me@10.62.177.170/WebSite/Administration/TasksStartup.aspx";
	public static final String URLN = "http://portaladmin:Kodiak4Me@10.62.177.170/WebSite/Administration/NewPMWizard.aspx";
	// public static final String URL = "https://www.gmail.com";
	public static final String ePortalUrl = "http://portaladmin:Kodiak4Me@10.62.177.170/WebSite/Framework/Default.aspx";
	public static final String ePortalUrlLINK = "ePortal Manager URL";
	// public static final String ePortalUrl =
	// "http://192.62.177.170/WebSite/Administration/TasksStartup.aspx";
	public static final String ePortalUrlN = "http://portaladmin:Kodiak4Me@10.62.177.170/WebSite/Administration/NewPMWizard.aspx";
	// public static final String Jpet_Button = "AssignPMtoGroupLink_LinkText";
	public static final String PMBUTTON = "//*[@id='AssignPMtoGroupLink_LinkText']";
	public static final String PMLINK = "Assign PM in WebPartition page";
	public static final String PMLOCATION = "//*[@id='AssignPMtoGroupLink_LinkText']";
	public static final String PMPARTITION = "//iframe[@title='Assign Web Partition Wizard Frame']";
	public static final String NEXTLOCATION = "//*[@id='wizard1_NextButton']";

	public static final String NEXTBUTTON = "//*[@id='wizard1_NextButton']";
	public static final String NEXTLINK = "Next Button";

	public static final String CANCELBUTTON = "//*[@id='wizard1_CancelButton']";

	public static final String CLUSTERLINK = "//*[@id='BringGroupOnlineLink_LinkText']";
	public static final String CLUSTERCLICK = "Bring Cluster online in common task Page";
	public static final String CHECKBOX = "//*[@id='GroupsGrid_ctl02_chkSelection']";
	public static final String CHECKBOXCLICK = "Checkbox";
	public static final String REMOVEBUTTON = "//*[@id='ButtonRemove']";
	public static final String REMOVEBUTTONCLICK = "Remove Button";
	public static final String YESBUTTON = "//*[@id='ConfirmPanel1_YesButton']";
	public static final String YESBUTTONCLICK = "Yes Button";

	public static final String BACK = "//*[@id='WindowControlPanel1_BackButton1_Button']";

	public static final String EVENTLINK = "//*[@id='EmailSetupLink_LinkText']";
	public static final String EVENTLINKCLICK = "Setup Events & Alerts Notifications in common task page";
	public static final String ENABLECHECKBOX = "//*[@id='EmailNotificationUpdatePanel']/label[2]";
	public static final String ENABLECHECKBOXCLICK = "Enabled checkbox";
	public static final String RECIPIENT = "//*[@id='RcptTextBox']";
	public static final String RECIPIENTCLICK = "Recipients email address";
	public static final String OK = "//*[@id='ButtonOK']";
	public static final String OKCLICK = "OK Button";
	public static final String ALERTENABLE = "//*[@id='LogForwardingConfigurationUpdatePanel']/span[2]/label";
	public static final String ERROR = "//*[@id='EmailDropDown_Security']";
	public static final String ERRORCLICK = "Security option";

	// Event Fetching
	public static final String EVENT = ".//*[@id='EventsText_LinkText']";
	public static final String EVENTNO = ".//*[@id='RecordCountLabel1_LabelCtl']";
	public static final String EVENTLNK = "Event link";
	public static final String RANGE = "Fetch Range";
	public static final String TIMERAISE = ".//*[@id='EventsGrid_ctl03_TimeRaised']";
	public static final String DISTIME = "Display Time";
	public static final String SORTTIME = ".//*[@id='EventsGrid']/tbody/tr[1]/th[5]/a";
	public static final String SORTEVENT = ".//*[@id='SearchOptionsLink']";
	public static final String EVENTDROP = ".//*[@id='CategoryDropDown']";
	public static final String EVENTFILTER = ".//*[@id='ButtonSearch']";
	public static final String SORTEVENTLNK = "choose filter option";
	public static final String SECURITYEVENTLNK = "Select Security option";
	public static final String APPEVENTLNK = "Select Application option";
	public static final String SYSEVENTLNK = "Select System option";
	public static final String MNGENTLNK = "Select Management option";
	public static final String EPORTALENTLNK = "Select ClearPath ePortal option";

	// Application Event
	public static final String SORT = ".//*[@id='SearchOptionsLink']";
	public static final String CATEGORY = ".//*[@id='CategoryDropDown']";
	public static final String FILTER = ".//*[@id='ButtonSearch']";
	public static final String Application = "Application";
	public static final String Security = "Security";
	public static final String system = "System";
	public static final String Management = "Management";
	public static final String eportal = "ClearPath ePortal";
	public static final String srk = "naam to suna hi hoga";
	public static final String Infromation = "Information";
	// Application Log
	public static final String DEPAPP = ".//*[@id='DeployedApplicationsText_LinkText']";
	public static final String DEPAPPLNK = "Deployed Application link";
	public static final String CHAPP = ".//*[@id='DeployedApplicationsGrid_ctl02_Name']";
	public static final String CHAPPLNK = "Select the application";
	public static final String GTRLOG = ".//*[@id='GatherAppLogsLink_LinkText']";
	public static final String GTRLNK = "Gather Application Log";
	public static final String GTRLOG1 = ".//*[@id='GatherDiagnosticsLinkButton']";
	public static final String GTRLOGMSG = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String SLTAPPDLT = ".//*[@id='DiagsGrid_ctl02_chkSelection']";
	public static final String APPDLT = ".//*[@id='ButtonDelete']";
	public static final String APPDLTLNK = " Remove Button";
	public static final String APPDLTYES = ".//*[@id='ConfirmPanel1_YesButton']";
	public static final String APPDLTMSG = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String APPRFR = ".//*[@id='WindowControlPanel1_RefreshButton1_Button']";
	public static final String APPMAX = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String APPPRO = "html/body/h2";
	public static final String APPCH = ".//*[@id='DeployedApplicationsGrid_ctl02_chkSelection']";
	public static final String APPUNDPY = ".//*[@id='ButtonRemove']";
	public static final String APPUNDPYYES = ".//*[@id='ConfirmPanel1_YesButton']";
	public static final String APPUNDPYLNK = "Undeploy button";
	public static final String APPUNDPYMSG = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String APPNONE = ".//*[@id='UpdatePanel1']/h3";
	public static final String Error = "Error";
	public static final String DisableLink = "Disabled Download link";
	public static final String SuccessAudit = "SuccessAudit";
	public static final String FailureAudit = "FailureAudit";
	public static final String WARNING = "Warning";
	public static final String Hardware = "Hardware";
	public static final String APPUNDPYMsg = "Remove Button";
	// IIS Log

	public static final String IISLOG = ".//*[@id='GroupsText_LinkText']";
	public static final String IISLOGLNK = "WebCluster";
	public static final String CHCLU = ".//*[@id='GroupsGrid_ctl02_Name']";
	public static final String CHCLULNK = "WC33 Cluster";
	public static final String IISGTHR = ".//*[@id='GatherIISLogsLink_LinkText']";
	public static final String IISGTHRLNK = "Gather IIS Log";
	public static final String IISGTHRBTN = ".//*[@id='GatherDiagnosticsLinkButton']";
	public static final String IISGTHRRFR = ".//*[@id='WindowControlPanel1_RefreshButton1_Button']";
	public static final String IISMAX = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String IISGT = ".//*[@id='GatherDiagnosticsLinkButton']";
	public static final String IISGTMSG = ".//*[@id='StatusPanel1_Table1']/tbody/tr/td[2]";
	public static final String IISGTCHK = ".//*[@id='DiagsGrid_ctl02_chkSelection']";
	public static final String IISGTRMV = ".//*[@id='ButtonDelete']";
	public static final String IISGTYES = ".//*[@id='ConfirmPanel1_YesButton']";
	public static final String IISGTRMVMSG = ".//*[@id='StatusPanel1_StatusLink']";

	// Cluster

	public static final String CLSOFF = ".//*[@id='TakeOfflineNowLink_LinkText']";
	public static final String CLSOFFYES = ".//*[@id='ConfirmPanel1_YesButton']";
	public static final String CLSOFFYESLNK = "Yes Button";
	public static final String CLSOFFSTS = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String WEBCLUSTER = "//*[@id=\"GroupsText_LinkText\"]";
	public static final String WEBCLUSTERMSG = "Web Clusters";
	public static final String CLUSTERCREATELINK = ".//*[@id='CreateGroupLink_LinkText']";
	public static final String CLUSTERCREATELINKMSG = "Create a Web Cluster...";
	public static final String CLUSTERNXTBTN = "//*[@id='wizard1_NextButton']";
	public static final String CLUSTERNXTBTNMSG = "Next Button";
	public static final String CLUSTERNAMETXT = "//*[@id=\"NameTextBox\"]";
	public static final String CLUSTERNAMETXTMSG = "Cluster Name";
	public static final String CLUSTERNAMENXTBTN = "//*[@id=\"wizard1_NextButton\"]";
	public static final String CLUSTERNAMENXTBTNMSG = "Next Button";
	public static final String CLUSTERIPADDRTXT = "//*[@id=\"GroupIPTextBox\"]";
	public static final String CLUSTERIPADDRTXTMSG = "IP Address";
	public static final String CLUSTERSUBNETMASKTXT = "//*[@id=\"SubnetMaskTextBox\"]";
	public static final String CLUSTERSUBNETMASKTXTMSG = "Subnet Mask";
	public static final String CLUSTERGATEWAYIPTXT = "//*[@id=\"GatewayIPTextBox\"]";
	public static final String CLUSTERGATEWAYIPTXTMSG = "Gateway IP";
	public static final String CLUSTERNETINFONXTBTN = "//*[@id=\"wizard1_NextButton\"]";
	public static final String CLUSTERNETINFONXTBTNMSG = "Next Button";
	public static final String CLUSTERDESPNXTBTN = "//*[@id=\"wizard1_NextButton\"]";
	public static final String CLUSTERDESPNXTBTNMSG = "Next Button";
	public static final String CLUSTERFINISHBTN = "//*[@id=\"wizard1_NextButton\"]";
	public static final String CLUSTERFINISHBTNMSG = "Finish Button";
	public static final String CLUSTERCREATEDMSG = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String BRINGCLUSTERONLINELINK = ".//*[@id=\"BringOnlineLink_LinkText\"]";
	public static final String BRINGCLUSTERONLINELINKMSG = "Bring Cluster Online";
	public static final String BRINGONLINEYESBUTTON = "//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String BRINGONLINEYESBUTTONMSG = "Yes Button";
	public static final String CLUSTERCOMMINGONLINE = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String CLUSTERCOMMINGONLINEMSG = "is coming online";
	public static final String CHECKCLUSTERSTATUSONLINE = "//*[@id=\"GroupDetails_ctl01_DesiredApplicationStateValue\"]";

	// added by Ravi Kumar
	// Manage partion
	public static final String MNGPRT = ".//*[@id=\"ControllerText_LinkText\"]";
	public static final String ALERTVIEW = ".//*[@id=\"ViewAlertsLink_LinkText\"]";
	public static final String CLRALL = ".//*[@id=\"ClearAllLinkButton\"]";
	public static final String YESBUN = ".//*[@id=\"ConfirmPanelControl_YesButton\"]";
	public static final String MNGLINK = "Select Manager Partion link";
	public static final String AlretLink = " Select View Alerts Link";
	public static final String CLEARLINK = "Select Clear All Link";
	public static final String POPUPWIN = ".//*[@id=\"ConfirmPanelControl_Table1\"]/tbody/tr[1]/td[2]";
	public static final String YESLINK = "Select yes Button link";
	public static final String VIEWEVENT = ".//*[@id=\"ViewEventLogsLink_LinkText\"]";
	public static final String MNGFILT = ".//*[@id=\"SearchOptionsLink\"]";
	public static final String VEIWEVENTLINK = "Select View Event logs";
	public static final String FILTLINK = "Select Show filtering options link";
	public static final String CATDRPDWN = ".//*[@id=\"CategoryDropDown\"]";
	public static final String SEVDRPDWN = ".//*[@id=\"SeverityDropDown\"]";
	public static final String FILTERBUN = ".//*[@id=\"ButtonSearch\"]";
	public static final String FILTBUNLINK = "Select filter button link";
	public static final String DOWNLOADBUN = ".//*[@id=\"DownloadLinkButton\"]";
	public static final String DOWNLOADLINK = "Select Download log file link";
	public static final String NONEEVENT = ".//*[@id=\"Form1\"]/h3"; // for event filtering
	public static final String NONEEVENT1 = ".//*[@id=\"UpdatePanel1\"]/h3"; // for alerts filtering
	public static final String ALL = "All";
	public static final String EMPTYLIST = "No events for this filter";
	public static final String FULLLIST = "Events log is present for this filter";
	public static final String EMPTYLIST1 = "No alerts for this filter combination";
	public static final String FULLLIST1 = "Alerts log is present for this filter";
	public static final String TestSteps = "All steps passed";
	// Web partition
	public static final String WBPARTION = ".//*[@id=\"DevicesText_LinkText\"]";
	public static final String ADDWBPRT = ".//*[@id=\"AssignPMLink_LinkText\"]";
	public static final String NXTBUN = ".//*[@id=\"wizard1_NextButton\"]";
	public static final String SELECTPM = ".//*[@id=\"UnassignedDeviceList\"]";
	public static final String PMVALUE = "PM-1";
	public static final String FINISHBUN = ".//*[@id=\"wizard1_NextButton\"]";
	public static final String PERSTAT = ".//*[@id=\"ViewPerformanceLink_LinkText\"]";
	public static final String USERNAME = "portaladmin";
	public static final String PASSWD = "Kodiak4Me";
	public static final String ClearStatus = ".//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String RESTART = ".//*[@id=\"RestartLink_LinkText\"]";
	public static final String RestartLink = "Restart this manager partion Link";
	public static final String RestartPopup = ".//*[@id=\"ConfirmPanel1_Table1\"]/tbody/tr[1]/td[2]/p";
	public static final String RestartYES = ".//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String REFRESH = ".//*[@id=\"RefreshText_LinkText\"]";
	public static final String TASKLINK = ".//*[@id=\"TasksLink_LinkText\"]";
	public static final String Restartsuccess = "Partion manager restarted";
	public static final String WebDisabled = "Link is disabled as no Web partion is available";
	public static final String WebpartionName = ".//*[@id=\"DevicesGrid_ctl02_Name\"]";
	public static final String PerformaceST = ".//*[@id=\"ViewPerformanceLink_LinkText\"]";
	public static final String PerfrmSTLink = "/html/body/h2";
	public static final String Browseclose = "Browser Closed";
	public static final String MNGSYSSOFT = ".//*[@id=\"ManageSoftwareLink_LinkText\"]";
	public static final String MNGSYSTEMSOFTLINK = "Manage System Software";
	public static final String SYNMSG = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String SYNLINK = ".//*[@id=\"DeployLink_LinkText\"]";
	public static final String SYNLINKTEXT = "Synchronize System Software link";
	public static final String SIGNOFF = ".//*[@id=\"SignOffText_LinkText\"]";
	public static final String LOGOFF = "LogOff link";
	public static final String Alertpopup = "sign off alert";
	public static final String WPLINK = ".//*[@id='DevicesText_LinkText']";

	public static final String PM1MODIFY = ".//*[@id='DeviceDetails_ctl01_edit']";
	public static final String PM1DESP = ".//*[@id='DeviceDetails_ctl01_DescriptionValue']";
	public static final String FRONTENDIP = ".//*[@id='DeviceDetails_ctl01_IPAddressValue']";
	public static final String PM1OK = ".//*[@id='DeviceDetails_ctl01_update']";
	public static final String PM1DESPVALUE = ".//*[@id='DeviceDetails_ctl01_DescriptionValue']";
	public static final String PMSTATUS = ".//*[@id='ViewStatusLink_LinkText']";
	public static final String PMCURRENTSTATE = ".//*[@id='DeviceDetails_ctl01_StateValue']";
	public static final String PMCURRENTSTATEPARTITION = "//*[@id='DevicesGrid_ctl03_State']";
	public static final String WPLINKMSG = "Web Partitions";
	public static final String PM1LINKMSG = "PM-1";
	public static final String PM1MODIFYMSG = "Modify";
	public static final String PM1DESPMSG = "Description";
	public static final String FRONTENDIPMSG = "Front End IP";
	public static final String PM1OKMSG = "OK";
	public static final String PM1DESPVALUEMSG = "Description Value";
	public static final String PMSTATUSMSG = "Status";
	public static final String PMCURRENTSTATEMSG = "Current Status";
	public static final String PMCURRENTSTATEPARTITIONMSG = "Current Partition Status";

	// Preference
	public static final String PREFERENCE = ".//*[@id=\"PreferencesText_LinkText\"]";
	public static final String PreferenceLink = "Preference Link";
	public static final String THEME = ".//*[@id=\"ThemesList\"]";
	public static final String Default = "Default";
	public static final String Classic = "Classic";
	public static final String ApplyBun = ".//*[@id=\"ButtonApply\"]";
	public static final String ApplyLink = "Apply button";
	public static final String ResethomePage = ".//*[@id=\"ResetHomepageLink\"]";
	public static final String taskStartup = ".//*[@id=\"CurrentHomepage\"]";
	public static final String defaultHomePage = "Reset value to default homepage";

	// Add Web Partition To Cluster

	public static final String ADDWPTOCLUSTERLINK = "//*[@id=\"AssignPMLink_LinkText\"]";
	public static final String ADDWPNXTBUTTON = "//*[@id=\"wizard1_NextButton\"]";
	public static final String ADDWPTOCLUSTERSUCCESSMSG = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String ADDMEMBERTOCLUSTERLINK = "//*[@id=\"AddMembersLink_LinkText\"]";
	public static final String ASSIGNWebPTOCLUSTER = "//*[@id=\"ButtonAssign\"]";
	public static final String ASSIGNWebPTOCLUSTERYESBUTTON = "//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String CLUSTERDROPDOWNLIST = "//*[@id=\"GroupDropDownList\"]";
	public static final String PARTITIONDROPDOWNLIST = "//*[@id=\"UnassignedDeviceList\"]";
	public static final String SELECTALLWPCHECKBOX = "//*[@id=\"DevicesGrid_ctl01_chkAll_Button\"]";
	public static final String STATUSMESSAGE = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String CLUSTERNAME = ".//*[@id=\"GroupsGrid_ctl03_Name\"]";
	// Right frame links
	public static final String BACKENDSERVER = ".//*[@id=\"ConfigBackEndServerLink_LinkText\"]";
	public static final String BACKENDSERVERLINK = "Define a new backend server";
	public static final String BACKENDPAGE = "./html/body/table/tbody/tr[1]/td";
	public static final String NEWCLUSTER = ".//*[@id=\"CreateGroupLink_LinkText\"]";
	public static final String NEWCLUSTERLINK = "Create a Web Cluster";
	public static final String CLUSTERPAGE = "/html/body/table/tbody/tr[1]/td";
	public static final String DEPLOYCLUSTER = ".//*[@id=\"DeployAppsLink_LinkText\"]";
	public static final String DEPLOYSERVERLINK = "Deploy Applications to Web Cluster";
	public static final String STAGEDAPP = ".//*[@id=\"ManageAppsLink_LinkText\"]";
	public static final String WEBPARTCLSUTER = ".//*[@id=\"AssignPMtoGroupLink_LinkText\"]";
	public static final String WEBPARTCLUSTERLINK = "Assign a Web Partion to a Web Cluster";
	public static final String ASSIGNCLUST = "/html/body/table/tbody/tr[1]/td";
	public static final String BRINGCLUSTONLINE = ".//*[@id=\"BringGroupOnlineLink_LinkText\"]";
	public static final String CLUSTERONLINELINK = "Bring Web Cluster online";
	public static final String MONITORCLUST = "//*[@id=\"CreateGroupLink_LinkText\"]";
	public static final String SSLONMNGR = ".//*[@id=\"ConfigSSLControllerLink_LinkText\"]";
	public static final String SSLMNGLINK = "Configure SSL on Manager Partion";
	public static final String MANAGECERT = "//*[@id=\"InstallSSCertLink_LinkText\"]";
	public static final String SSLONWEBCLUST = ".//*[@id=\"ConfigSSLAppLink_LinkText\"]";
	public static final String SSLWEBLINK = "Configure SSL on Web Cluster";

	public static final String MANAGELICENSE = ".//*[@id=\"ActivateLicenseLink_LinkText\"]";
	public static final String MNGLICENSELINK = "Manage Licenses";
	public static final String ManageLicense = "//*[@id=\"AddNewLicenseLink_LinkText\"]";
	public static final String EMAILNOTIFY = "//*[@id=\"EmailSetupLink_LinkText\"]";
	public static final String EMAILLINK = "Setup Event and Alert Notifications";
	public static final String CONFIGNOTIFIC = ".//*[@id=\"Form1\"]/h2";
	public static final String GOCOMMONTASKS = ".//*[@id=\"TasksLink_LinkText\"]";
	public static final String COMMONTASKLINK = "Go to Common Task Page";
	public static final String STARTUPTASK = ".//*[@id=\"StartupTasksLink_LinkText\"]";
	public static final String BACKBUN = "//*[@id=\"WindowControlPanel1_BackButton1_Button\"]";
	public static final String BACKBUNLINK = "Back button";
	public static final String ERRORXPATH = ".//*[@id=\"AlertsGrid_ctl03_Severity\"]";
	public static final String DESCRIPTION = ".//*[@id=\"AlertDetails_ctl01_DescriptionValue\"]";
	public static final String ClusterBackBun = ".//*[@id=\"wizard1_BackButton\"]";
	public static final String CANCELBUN = ".//*[@id=\"wizard1_CancelButton\"]";
	public static final String CANCELLINK = "Cancel Button";
	public static final String BACKUPSYSTEM = ".//*[@id=\"BackupLink_LinkText\"]";
	public static final String BACKUPCURRENT = ".//*[@id=\"BackupLink_LinkText\"]";
	public static final String BACKUPNAME = ".//*[@id=\"NameTextBox\"]";
	public static final String REFRESHXPATH = ".//*[@id=\"WindowControlPanel1_RefreshButton1_Button\"]";
	public static final String refreshdata = "Refresh Page";
	public static final String backupsystemLink = "Backup the system Link";
	public static final String BackupCurrentLink = "Backup the current configuration link";
	public static final String Namedata = "Dummy_Name";
	public static final String PrimaryBackEndServerLinkMsg="Primary BackEndSever Value";

	// Backend server
	public static final String BACKENDSERVERleft = ".//*[@id=\"HostsText_LinkText\"]";
	public static final String BACKENDLINK = "Back-End Servers Link";
	public static final String BACKENDHOST = ".//*[@id=\"HostsGrid_ctl02_Name\"]";
	public static final String HostNameLink = "TR32 host Link";
	public static final String MODIFYDES = ".//*[@id=\"HostDetails_ctl01_edit\"]";
	public static final String ModifyLink = "Modify Button";
	public static final String DESCRIPTEDIT = ".//*[@id=\"HostDetails_ctl01_DescriptionValue\"]";
	public static final String SampleText = "Sample Description- this is Primary Backend Server.";
	public static final String DESOKBUN = ".//*[@id=\"HostDetails_ctl01_update\"]";
	public static final String okbunlink = "OK button Link";

	// New user creation
	public static final String MNGUSERS = ".//*[@id=\"ChangePasswordLink_LinkText\"]";
	public static final String CREATEUSER = ".//*[@id=\"CreateUserLink_LinkText\"]";
	public static final String USERNAME1 = ".//*[@id=\"UserNameTextBox\"]";
	public static final String FULLNAME = ".//*[@id=\"FullNameTextBox\"]";
	public static final String USERDESCRIPTION = ".//*[@id=\"DescriptionTextBox\"]";
	public static final String USERPASSWD = ".//*[@id=\"NewPasswordTextBox\"]";
	public static final String CONFIRMPASSWD = ".//*[@id=\"ConfirmPasswordTextBox\"]";
	public static final String ADDUSER = ".//*[@id=\"ButtonApply\"]";
	public static final String VALIDATEPASSWD = ".//*[@id=\"RequiredValidatorNewPassword\"]";
	public static final String WITHOUTUSER = ".//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String MISMATCHPASSWD = "//*[@id=\"CompareValidatorPasswords\"]";
	public static final String USER1 = "TestUser";
	public static final String PASSWD1 = "Asd123";
	public static final String PASSWD2 = "Asd124";

	public static final String ADDWPTOCLUSTERLINKMSG = "Add Web Partition To Cluster Link";
	public static final String ADDWPNXTBUTTONMSG = "Next";
	public static final String ADDWPTOCLUSTERSUCCESSMSG1 = "Add WP To Cluster Success Message ";
	public static final String ADDMEMBERTOCLUSTERLINKMSG = "Add Member To Cluster";
	public static final String ASSIGNWPTOCLUSTERMSG = "Assign Web Partition To Cluster";
	public static final String ASSIGNWPTOCLUSTERYESBUTTONMSG = "Yes";
	public static final String CLUSTERDROPDOWNLISTMSG = "Cluster Drop Down List";
	public static final String PARTITIONDROPDOWNLISTMSG = "Partition Drop Down List";
	public static final String SELECTALLWPCHECKBOXMSG = "Select All";
	public static final String STATUSMESSAGE1 = "Status Message";

	// Replication Check
	public static final String MANAGEPARTITIONLINK = "//*[@id=\"ControllerText_LinkText\"]";
	public static final String ENABLEREPLICATIONLINK = "//*[@id=\"EnableReplicationLink_LinkText\"]";
	public static final String REPLICATIONSTATEVALUE = "//*[@id=\"ControllerDetails_ctl01_ReplicationStateValue\"]";
	public static final String REPLICATIONHEALTHVALUE = "//*[@id=\"ControllerDetails_ctl01_ReplicationHealthValue\"]";
	public static final String ENABLEREPLICATIONYESBUTTON = "//*[@id=\"ConfirmPanel1_YesButton\"]";

	public static final String MANAGEPARTITIONLINKMSG = "Manager Partition";
	public static final String ENABLEREPLICATIONLINKMSG = "Enable Replication";
	public static final String REPLICATIONSTATEVALUEMSG = "Replication State Value";
	public static final String REPLICATIONHEALTHVALUEMSG = "Replication Health Value";
	public static final String ENABLEREPLICATIONYESBUTTONMSG = "Enable Replication, Yes Button";

	// Deployed Application
	public static final String DEPLOYEDAPPLINK = "//*[@id=\"DeployedApplicationsText_LinkText\"]";
	public static final String DEPLOYEDAPPLINKMSG = "Deployed Application Left Navigation Pane";
	public static final String DEPLOYEDAPPEXISTS = "//*[@id=\"H3Heading\"]";
	public static final String DEPLOYEDAPPEXISTSMSG = "Deployed Applications (None)";

	public static final String DEPLOYEDAPPMSG = "Deployed Application";
	public static final String DEPLOYEDAPPPROJECTNAMEVALUE = "//*[@id=\"ApplicationDetails_ctl01_ProjectNameValue\"]";
	public static final String DEPLOYEDAPPNAMEVALUE = "//*[@id=\"ApplicationDetails_ctl01_DeployedNameValue\"]";
	public static final String DEPLOYEDAPPIDEVERSIONVALUE = "//*[@id=\"ApplicationDetails_ctl01_IDEVersionValue\"]";
	public static final String DEPLOYEDAPPPRODUCTVERSIONVALUE = "//*[@id=\"ApplicationDetails_ctl01_ProjectVersionValue\"]";
	public static final String DEPLOYEDAPPPROJECTNAMEVALUEMSG = "Project Name";
	public static final String DEPLOYEDAPPNAMEVALUEMSG = "Deployed Application Name";
	public static final String DEPLOYEDAPPIDEVERSIONVALUEMSG = "Deployed Application IDE Version";
	public static final String DEPLOYEDAPPPRODUCTVERSIONVALUEMSG = "Deployed Application Version";
	public static final String ASSIGNWebPTOCLUSTERMsg = "Assign Web Partitions to cluster";
	public static final String SELECTDEPLOYEDAPP = "//*[@id=\"DeployedApplicationsGrid_ctl02_chkSelection\"]";
	public static final String SELECTDEPLOYEDAPPMSG = "Select Deployed Application - check box";
	public static final String UNDEPLOYBTN = "//*[@id=\"ButtonRemove\"]";
	public static final String UNDEPLOYBTNMSG = "Undeploy Button";
	public static final String SYNCMETHODOFFILINE = "//*[@id=\"ConfirmPanel1_fieldset\"]/label[1]";
	public static final String SYNCMETHODOFFILINEMSG = "Sync Method Offline";
	public static final String SYNCMETHODONLINE = "//*[@id=\"ConfirmPanel1_fieldset\"]/label[2]";
	public static final String SYNCMETHODONLINEMSG = "Sync Method Online";
	public static final String SYNCMETHODSERIAL = "//*[@id=\"ConfirmPanel1_fieldset\"]/label[3]";
	public static final String SYNCMETHODSERIALMSG = "Sync Method Serial";
	public static final String SYNCMETHODPHASED = "//*[@id=\"ConfirmPanel1_fieldset\"]/label[4]";
	public static final String SYNCMETHODPHASEDMSG = "Sync Method Phased";
	public static final String CONFIRMYESBTN = "//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String CONFIRMYESBTNMSG = "Yes";
	public static final String CONFIRMATION = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String CONFIRMATIONMSG = "The chosen Deployed Application(s) have been removed successfully";

	// stop one member
	public static final String CONFIRMOFFLINE = ".//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String STATUSMESSAGE11 = ".//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String DEVICESTATUS = ".//*[@id=\"DeviceDetails_ctl01_StateValue\"]";
	public static final String PMALERTSTATUS = ".//*[@id=\"DeviceDetails_ctl01_StatusPanelDevice_StatusLink\"]";
	public static final String TAKEOFFLINE = ".//*[@id=\"TakeOfflineNowLink_LinkText\"]";
	public static final String VIEWSTATUS = ".//*[@id=\"ViewStatusLink_LinkText\"]";
	public static final String SYNCHRONIZESTATE = ".//*[@id=\"SyncWithGroupLink_LinkText\"]";
	public static final String NEWWEBPAGESTATUS = ".//*[@id=\"EPortalGeneratedContent1\"]/div/span[3]";
	public static final String PM1XPATH = ".//*[@id=\"DevicesGrid_ctl02_Name\"]";
	public static final String DEPLOYEDAPP1 = ".//*[@id=\"DeployedApplicationsGrid_ctl02_Name\"]";
	public static final String NewPage = "http://10.62.177.137/";
	public static final String DEPLOYEDAPP = ".//*[@id=\"DeployedApplicationsText_LinkText\"]";
	public static final String syncyesbunxpath = ".//*[@id=\"ConfirmPanel1_YesButton\"]";

	// Rebuild PM
	public static final String REBUILD = ".//*[@id=\"RebuildLink_LinkText\"]";
	public static final String PM1LINK = ".//*[@id='DevicesGrid_ctl02_Name']";
	public static final String PM2XPATH = ".//*[@id=\"DevicesGrid_ctl03_Name\"]";
	public static final String PM3XPATH = ".//*[@id=\"DevicesGrid_ctl04_Name\"]";
	public static final String PM4XPATH = ".//*[@id=\"DevicesGrid_ctl05_Name\"]";

	// Remover Cluster
	public static final String CLUSTERCHECKBOX = ".//*[@id=\"GroupsGrid_ctl03_chkSelection\"]";

	// For Operations
	public static final String WebclusterName = "//*[text()='";
	public static final String OpTypepath = "//*[text()='";
	public static final String VIPAddress = "//*[@id='GroupDetails_ctl01_VirtualIPAddressValue']";
	public static final String AddNewMemberstoCluster = "//*[@id='AddMembersLink_LinkText']";
	public static final String DesiredWebclusterStatus = "//*[@id='GroupDetails_ctl01_DesiredApplicationStateValue']";
	public static final String ASSIGNWPTOCLUSTER = "//*[@id=\"ButtonAssign\"]";
	public static final String ASSIGNWPTOCLUSTERYESBUTTON = "//*[@id=\"ConfirmPanel1_YesButton\"]";
	public static final String BringCLusterOnline = "//*[@id='BringOnlineLink_LinkText']";
	public static final String HeadingMessage = "//*[@id='H3Heading']";
	public static final String CheckBoxLink = "//input[@type='checkbox']";
	public static final String WebPartitionLink = ".//*[@id='DevicesText_LinkText']";
	public static final String ShutdownWebcluster = ".//*[@id='ShutdownLink_LinkText']";
	public static final String NumberofwebpartitionsLink = ".//table[@id='DevicesGrid']/tbody/tr/td[1]";
	public static final String DepAppUserCodeLink =".//*[@id='usercode']";
  	public static final String DepAppPasswdLink =".//*[@id='password']";
	public static final String DepAppSubmitLink = ".//*[@id='EPortalSubmitButton1']";
	public static final String DeviceGridPartialLink = "//*[@id='DevicesGrid_ctl";
	public static final String WebClusterRestartLink = "//*[@id='RestartLink_LinkText']";
	public static final String WebClusterManageMembers="ManageMembersLink_LinkText";
	public static final String WebClusterMembers ="GroupDetails_ctl01_MembersValue";
	public static final String WebclusterOnlineRemoveErrorMessageLink ="StatusPanel1_StatusLink";
	// public static final String WebClusterDNSNameLink
	// =".//*[@id='DNSNameTextBox']"; need to delete
	// for Message
	public static final String WebclusterNameMsg = "WebCluster Name '";
	public static final String OpTypepathMsg = "OperationType";
	public static final String VIPAddressMsg = "Virtual IpAddress ";
	public static final String AddNewMemberstoClusterMsg = "Adding New Members to cluster";
	public static final String DesiredWebclusterStatusMsg = "Desired WebCluster Status";
	public static final String ASSIGNWPTOCLUSTERMsg = "Assgin webpartitions to Cluster";
	public static final String ASSIGNWPTOCLUSTERYESBUTTONMsg = "Assgin webpartitions to Cluster Yes Button";
	public static final String BringCLusterOnlineMsg = "Bring Cluster Online";
	public static final String IISGTYESMsg = "Click YES Button";
	public static final String HeadingMessageMsg = "Heading Message";
	public static final String CheckBoxLinkMsg = "/CheckBox";
	public static final String WebPartitionLinkMsg = "WebPartition status";
	public static final String ShutdownWebclusterMsg = "Shutdown Webcluster";
	public static final String NumberofwebpartitionsLinkMsg = "Number of web partitions ";
	public static final String DepAppUserCodeLinkMsg = "App User Code/Name ";
	public static final String DepAppPasswdLinkMsg = "App User password";
	public static final String DepAppSubmitLinkMsg = "Submit Depp Apps";
	public static final String ASSIGNWebPTOCLUSTERYESBUTTONMsg = "Click on Yes Button";
	public static final String WebClusterRestartLinkMsg = "Restart Webcluster";
	public static final String WebClusterDNSNameLinkMsg = "WebCluster DNSName";
	public static final String WebClusterManageMembersMsg="ManageMembers";
	public static final String WebClusterMembersMsg="Number of Webcluster Members";

	// Backend Server
	public static final String BackEndServersLink = ".//*[@id='HostsText_LinkText']";
	public static final String DefineNewBackEndServerLink = ".//*[@id='CreateHostLink_LinkText']";
	public static final String NextButtonLink = ".//*[@id='wizard1_NextButton']";
	public static final String CancelButtonLink = ".//*[@id='wizard1_NextButton']";
	public static final String BackEndServerNameLink = ".//*[@id='NameTextBox']";
	public static final String BackEndServerIpFourthOctcetLink = ".//*[@id='HostIP4TextBox']";
	public static final String BackEndServerNametoSelectLink = ".//*[text()='";
	public static final String BackEndServerRemoveLink = ".//*[@id='ButtonRemove']";
	public static final String BackEndServerStatusLink = ".//*[@id='StatusPanel1_StatusLink']";
	public static final String BackEndServerModifyLink = ".//*[@id='HostDetails_ctl01_edit']";
	public static final String BackEndServerModifyOKLink = ".//*[@id='HostDetails_ctl01_update']";
	public static final String BackEndServerIpAddressLink = ".//*[@id='HostDetails_ctl01_IPAddressValue']";
	public static final String BackEndServerthirdOctetmodifciationLink = ".//*[@id='HostDetails_ctl01_IPHostSegment3Value']";
	public static final String BackEndServerFourthOctetmodifciationLink = ".//*[@id='HostDetails_ctl01_IPHostSegment4Value']";
	public static final String BackEndServerNamemodifciationLink = ".//*[@id='HostDetails_ctl01_NameValue']";
	public static final String BackEndServerAlphaNumericNameVerificatioLink = ".//*[@id='HostDetails_ctl01_HostNameValidator']";
	public static final String BackEndServerInvalidIpAddressStatusLink = ".//*[@id='HostDetails_ctl01_IPRange']";
	public static final String WebClusterCommonNameLink = ".//*[@id='txtCN']";
	public static final String WebClusterManageCertificatesLink = ".//*[@id='ManageCertificateLink_LinkText']";
	public static final String WebClusterInstallSelfSignedCerificateLink = ".//*[@id='InstallSSCertLink_LinkText']";
	public static final String BackEndServerDNSNameLink =".//*[@id='DNSNameTextBox']";
  	public static final String WebClusterDNSNameLink="GroupDNSNameTextBox";//".//*[@id='GroupDNSNameTextBox']"
	public static final String ManageSystemSoftwareLink = "ManageSoftwareLink_LinkText"; // .//*[@id='ManageSoftwareLink_LinkText']
	public static final String SyncSystemSoftwareLink = "DeployLink_LinkText"; // .//*[@id='DeployLink_LinkText']
	public static final String SynchSystemsotwareStatusLink = "StatusPanel1_StatusLink"; // .//*[@id='StatusPanel1_StatusLink']

	public static final String BackEndServersLinkMsg = "Navigate to BackEnd Servers page";
	public static final String DefineNewBackEndServerLinkMsg = "Click on Define new backend server";
	public static final String NextButtonLinkMsg = "Next Button";
	public static final String CancelButtonLinkMsg = "Finish Creating dummy back end server";
	public static final String BackEndServerNameLinkMsg = "BackEndServer Name";
	public static final String BackEndServerDNSNameLinkMsg ="Back EndServer DNS Name";
	public static final String BackEndServerIpFourthOctcetLinkMsg = "BackEnd server Ips's 4th octet";
	public static final String BackEndServerNametoSelectLinkMsg = "BackEndserver Name";
	public static final String BackEndServerRemoveLinkMsg = "BackEnd Server Remove";
	public static final String BackEndServerStatusLinkMsg = "BackEndServer Status";
	public static final String BackEndServerModifyLinkMsg = "Modify BackEnd Server";
	public static final String BackEndServerModifyOKLinkMsg = "Ok Modify BackEnd Server";
	public static final String BackEndServerIpAddressLinkMsg = "BackEnd Server IpAddress";
	public static final String BackEndServerthirdOctetmodifciationLinkMsg = "BackEnd Server Ip third octet";
	public static final String BackEndServerFourthOctetmodifciationLinkMsg = "BackEnd Server Ip fourth octet";
	public static final String BackEndServerNamemodifciationLinkMsg = "Modify BackEndServer Name";
	public static final String BackEndServerAlphaNumericNameVerificatioLinkMsg = "Invalid";
	public static final String BackEndServerInvalidIpAddressStatusLinkMsg = "Invalid";
	public static final String WebClusterCommonNameLinkMsg = "CommonName";
	public static final String WebClusterManageCertificatesLinkMsg = "Manage Cerificates";
	public static final String WebClusterInstallSelfSignedCerificateLinkMsg = "Install Self-signed certificate";
	public static final String ManageSystemSoftwareLinkMsg = "Manage SystemSoftware";
	public static final String SynchSystemsotwareStatusLinkMsg = "All system software is synchronized on this Web Cluster";
	public static final String SyncSystemSoftwareLinkMsg = "Synchronize System Software";
	// classictheme
	public static final String PreferencesLink = ".//*[@id='PreferencesText_LinkText']";
	public static final String PreferencesClassic = "//select[@id='ThemesList']/option[contains(text(),'Classic')]";
	public static final String PreferencesDefault = "//select[@id='ThemesList']/option[contains(text(),'Default')]";
	public static final String PreferencesApplyThemeLink = ".//*[@id='ButtonApply']";
	public static final String PreferencesLinkMsg = "Preferences";
	public static final String PreferencesClassicMsg = "Classic Theme";
	public static final String PreferencesDefaultMsg = "Default Theme";
	public static final String PreferencesApplyThemeLinkMsg = "Apply Theme";

	public static final String MANAGECERTIFICATELINK = "//*[@id=\"ManageCertificateLink_LinkText\"]";
	public static final String MANAGECERTIFICATELINKMSG = "Manage Certificate Link";
	public static final String INSTALLSELFSIGNEDCERTIFICATELINK = "//*[@id=\"InstallSSCertLink_LinkText\"]";
	public static final String INSTALLSELFSIGNEDCERTIFICATEMSG = "Install Self-Signed Certificate";
	public static final String COMMONNAMETXT = "//*[@id=\"txtCN\"]";
	public static final String COMMONNAMEMSG = "Common Name";
	public static final String ORGANIZATIONTXT = "//*[@id=\"txtO\"]";
	public static final String ORGANIZATIONMSG = "Organozation";
	public static final String LOCALITYTXT = "//*[@id=\"txtL\"]";
	public static final String LOCALITYMSG = "Locality";
	public static final String STATETXT = "//*[@id=\"txtS\"]";
	public static final String STATEMSG = "State";
	public static final String COUNTRYTXT = "//*[@id=\"txtC\"]";
	public static final String COUNTRYMSG = "Country";
	public static final String OKBUTTON = "//*[@id=\"ButtonOK\"]";
	public static final String OKBUTTONMSG = "Ok";
	public static final String TYPEVALUE = "//*[@id=\"SSLgrid_ctl02_typeCtl\"]";
	public static final String COMMONNAMEVALUE = "//*[@id=\"SSLgrid_ctl02_commonNameCtl\"]";
	public static final String ORGANIZATIONVALUE = "//*[@id=\"SSLgrid_ctl02_organizationCtl\"]";
	public static final String LOCALITYVALUE = "//*[@id=\"SSLgrid_ctl02_localityCtl\"]";
	public static final String STATEVALUE = "//*[@id=\"SSLgrid_ctl02_stateCtl\"]";
	public static final String COUNTRYVALUE = "//*[@id=\"SSLgrid_ctl02_countryCtl\"]";
	public static final String EXPIRATIONDATEVALUE = "//*[@id=\"SSLgrid_ctl02_expDateCtl\"]";
	public static final String ISSUERVALUE = "//*[@id=\"SSLgrid_ctl02_issuerCtl\"]";
	public static final String REFRESHBUTTON = "//*[@id=\"WindowControlPanel1_RefreshButton1_Button\"]";
	public static final String REFRESHBUTTONMSG = "Refresh Button";
	public static final String BACKBUTTON = "//*[@id=\"WindowControlPanel1_BackButton1_Button\"]";
	public static final String BACKBUTTONMSG = "Back Button";
	public static final String STARTUPMESSAGE = "//*[@id=\"Form1\"]/h1";
	public static final String STARTUPMESSAGETXT = "Getting Started with ClearPath ePortal";
	public static final String STARTUPTASKMSG = "Start up task";
	public static final String STARTUP = "//*[@id=\"StartupTaskText_LinkText\"]";
	public static final String CANCELCLUSTERCREATIONBTN = "//*[@id=\"wizard1_CancelButton\"]";
	public static final String CANCELCLUSTERCREATIONBTNMSG = "Cancel button";
	public static final String CREATECLUSTERSTARTUP = "//*[@id=\"CreateGroupLink_LinkText\"]";
	public static final String CREATECLUSTERSTARTUPMSG = "Create Cluster Link";

	// added by Ravi Kumar
	// Manage partion
	public static final String CLEARBUTTON = "//*[@id=\"ClearButton\"]";
	public static final String CLEARBUTTONMSG = "Clear Button";
	public static final String ALERT = "//*[@id=\"StatusPanel1_StatusLink\"]";
	public static final String ALERTMSG = "1 alert cleared";

	// Help Links
	public static final String BUTTONHELPLINK = "//*[@id=\"ButtonHelp_LinkText\"]";
	public static final String BUTTONHELP = "//*[@id=\"wizard1_HelpButton\"]";
	public static final String BUTTONHELPMSG = "Help Button";
	public static final String BUTTONHELPLINKMSG = "Help Link";
	public static final String HELPWINDOWSTARTUPTASK = "/html/body/h1";
	public static final String HELPWINDOWSTARTUPTASKMSG = "Help Window Startup Tasks";
//SA
	//Duplicate WebCluster Name
	
		public static final String NEXT = "//*[@id='wizard1_NextButton']";
		public static final String CLUSTERCREATE = "//*[@id='CreateGroupLink_LinkText']";
//		public static final String CLUSTERCREATELINK = "Web Clusters";
		public static final String WEBCLUSTERFRAME = "//iframe[@title='New Web Cluster Wizard Frame']";
		public static final String WEBCLUSTERFRAMELink = "New Web Cluster Wizard Frame";
		public static final String NAMETEXTBOX = "//*[@id='NameTextBox']";
		public static final String WEBCLUSTER4 = "TEST";
		
		
		
		/// NEW WEBCLUSTER NAMED TEST
		public static final String WEBCLUSTERTEST = "TEST";
		public static final String EportalWebsite = "Eportal Manager Website";
		public static final String IPTEXTBOX = ".//*[@id='GroupIPTextBox']";
		public static final String IPADDRESS = "10.62.177.138";
		public static final String SUBNETTEXTBOX = ".//*[@id='SubnetMaskTextBox']";
		public static final String SUBNET = "255.255.255.192";
		public static final String GATEIPTEXTBOX = ".//*[@id='GatewayIPTextBox']";
		public static final String GATEIP = "10.62.177.190";
		public static final String Firstcluster = ".//*[@id='GroupsGrid_ctl02_Name']";
		public static final String Firstclusterlink = "First Cluster";
		public static final String ModifyFirstcluster = ".//*[@id='GroupDetails_ctl01_edit']";
		public static final String ModifyFirstclusterlink = "Modify First Cluster";
		public static final String BringOnlineLink = ".//*[@id='BringOnlineLink_LinkText']";
		public static final String BRINGONlINE = "Bring Online";
		
		public static final String ViewStatusLink = ".//*[@id='ViewStatusLink_LinkText']";
		public static final String ViewStatus = "View Status";
		public static final String TakeOffline = ".//*[@id='TakeOfflineNowLink_LinkText']";
		public static final String TakeOfflinelink = "Take offline";
		public static final String TakeOffline1 = "//*[@id=\"TakeOfflineWaitLink_LinkText\"]";
		public static final String AddMembers = ".//*[@id='AddMembersLink_LinkText']";
		public static final String AddMembersLink = "AddMembersLink";
		public static final String SelectMember = ".//*[@id='DevicesGrid_ctl02_chkSelection']";
		public static final String SelectMemberLink = "SelectMember";
		public static final String AssignPM = " .//*[@id='ButtonAssign']";
		public static final String AssignPMLink = "AssignPM";
		public static final String ManageMembers = ".//*[@id='ManageMembersLink_LinkText']";
		public static final String ManageMembersLink = "ManageMembersLink";
		public static final String STATUS = "//*[@id='StatusPanel1_StatusLink']";
		public static final String STATUSLink = "TheWebClusterwassuccessfullycreated";
		public static final String VIPTEXTBOX = "//*[@id='GroupDetails_ctl01_VirtualIPAddressValue']";
		public static final String VIPADDRESS = "192.62.177.138";
		public static final String ModifyOK = ".//*[@id='GroupDetails_ctl01_update']";
		public static final String XVIPADDRESS = "62.177.138";
		public static final String IPValidate = "//*[@id='GroupDetails_ctl01_VirtualIPAddressValidator']";
		public static final String IPValidIP = "Invalid IP Address";
		public static final String NONUSABLEIP = "INVALID IP ADDRESS";
		public static final String USABLEIP = "VALID IP ADDRESS";
		public static final String GIPTEXTBOX = "//*[@id='GroupDetails_ctl01_GatewayIPAddressValue']";
		public static final String XGIPADDRESS = "62.177.138";
		public static final String CANCELBUTTONLink = "Click on CancelButton']";
		public static final String WC=".//*[@id='GroupsText_LinkText']";
		public static final String SelectCluster= ".//*[@id='GroupsGrid_ctl02_chkSelection']";
		public static final String SelectClusterClick= "Select test Cluster"	;
		public static final String WP="//*[@id=\"DevicesText_LinkText\"]";
		//
		//backend
//		public static final String BackEndServersLink = ".//*[@id='HostsText_LinkText']";
//		public static final String DefineNewBackEndServerLink = ".//*[@id='CreateHostLink_LinkText']";
//		public static final String NextButtonLink = ".//*[@id='wizard1_NextButton']";
//	  	public static final String CancelButtonLink = ".//*[@id='wizard1_NextButton']";
//		public static final String BackEndServersLinkMsg = "Navigate to BackEnd Servers page";
//	  	public static final String DefineNewBackEndServerLinkMsg = "Click on Define new backend server";
//	  	public static final String NextButtonLinkMsg = "Next Button";
//	  	public static final String CancelButtonLinkMsg = "Finish Creating dummy back end server";
//	  	public static final String BackEndServerNameLinkMsg ="BackEndServer Name";
//	  	public static final String BackEndServerIpFourthOctcetLinkMsg ="BackEnd server Ips's 4th octet";
//	  	public static final String BackEndServerNametoSelectLinkMsg ="BackEndserver Name";
//	  	public static final String BackEndServerRemoveLinkMsg ="BackEnd Server Remove";
//	  	public static final String BackEndServerStatusLinkMsg ="BackEndServer Status";
//	  	public static final String BackEndServerModifyLinkMsg="Modify BackEnd Server";
//	  	public static final String BackEndServerModifyOKLinkMsg = "Ok Modify BackEnd Server";
//	  	public static final String BackEndServerIpAddressLinkMsg ="BackEnd Server IpAddress";
//	  	public static final String BackEndServerthirdOctetmodifciationLinkMsg="BackEnd Server Ip third octet";
//	  	public static final String BackEndServerFourthOctetmodifciationLinkMsg="BackEnd Server Ip fourth octet";
//	  	public static final String BackEndServerNamemodifciationLinkMsg ="Modify BackEndServer Name";
//	  	public static final String BackEndServerAlphaNumericNameVerificatioLinkMsg="Invalid";
//	  	public static final String BackEndServerInvalidIpAddressStatusLinkMsg = "Invalid";
//	  	public static final String BackEndServerStatusLink =".//*[@id='StatusPanel1_StatusLink']";
//	  	public static final String BackEndServerNameLink =".//*[@id='NameTextBox']";
//	  	public static final String BackEndServerIpFourthOctcetLink =".//*[@id='HostIP4TextBox']";
//	  	public static final String BackEndServerNametoSelectLink =".//*[text()='";
	  	//Client connection
//	  	public static final String DesiredWebclusterStatus ="//*[@id='GroupDetails_ctl01_DesiredApplicationStateValue']"; 
//	  	public static final String VIPAddress = "//*[@id='GroupDetails_ctl01_VirtualIPAddressValue']"; 
//	  	public static final String DepAppUserCodeLink =".//*[@id='inputField_0']";
//	  	public static final String DepAppPasswdLink =".//*[@id='inputField_1']";
//	  	public static final String DepAppSubmitLink =".//*[@id='EPortalSubmitButton1']" ;
//	  	public static final String DepAppUserCodeLinkMsg ="App User Code/Name ";
//	  	public static final String DepAppPasswdLinkMsg ="App User password";
//	  	public static final String DepAppSubmitLinkMsg ="Submit Depp Apps"; 
//	  	public static final String WebclusterName ="//*[text()='"; 
}
