/*--------------------------------------------------------------------------------
COMMON FUNCTIONS  LIBRARY - (USER DEFINED FUNCTIONS)

--------------------------------------------------------------------------------*/
package com.unisys.eportal.manager;

//import static com.unisys.vp.testautomation.selenium.pages.data.LoginPageData.NEXTLINK;
//import static com.unisys.vp.testautomation.selenium.pages.data.LoginPageData.PMLINK;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import com.unisys.eportal.manager.App_Networx;
//import util.ReportManager;
//import util.Initialize.TestStatus;
import static com.unisys.eportal.manager.LoginPageData.*;
import com.google.common.base.Function;
//import com.thoughtworks.selenium.Selenium;
//import com.unisys.vp.reports.TestStep;
//import library.SeleniumBaseClass;
//import com.unisys.vp.testautomation.selenium.pages.pom.LoginPage;
import static com.unisys.eportal.manager.LoginPageData.PMLOCATION;

//srk- excuting options

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

//import com.unisys.eportal.ebank.ePortalBatchRun;

//import config.ActionKeywords;
//import config.Constants;

//import Utility.ExcelUtils;

//public class DriverScript {

//srk

/* ***************************************************************************************
 PURPOSE: Common Actions performed on Web Application
 RETURN VALUE: None
 INPUT(s): Defined in individual Subclass Methods
 AUTHOR: Sanjeev
 *************************************************************************************** */
public class Common_Methods extends App_Networx {

	private static int Index = 0;
	private static WebDriver driver;
	//private Selenium selenium;
	@SuppressWarnings("unused")
	private String mainpage;
	public WebDriver popup;

	public enum Sel {
		id, name
	};

	private Actions DriverActions;
	@SuppressWarnings("unused")
	private Actions KeyboardActions;
	@SuppressWarnings("unused")
	private Actions KeyboardMouseActions;
	private Keyboard keyboard;
	private Mouse mouse;
	private ReportManager RM = null;
	public static int stepCount = 1;
	private long startTransactionTime;
	private long endTransactionTime;
	private int startTransactionStep;
	private int endTransactionStep;
	public static Properties OR;
	// public static ActionKeywords actionKeywords;
	public static String sActionKeyword;
	public static String sPageObject;
	public static Method method[];
	// public static int run;
	public static int iTestStep;
	public static int iTestLastStep;
	public static String sTestCaseID;
	public static String sRunMode;
	public static String sSLNO;
	public static String sSLNOO;
	public static String sTestRunner;
	// public static int Num;
	// private static ePortalBatchRun RK = null;

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Time computation RETURN VALUE: HHMMSS AUTHOR: Sanjeev
	 * Ranjan Kumar
	 ***************************************************************************************/

	public static String formatIntoHHMMSS(long diffSeconds) {
		// Convert Milli Seconds into Seconds
		diffSeconds = diffSeconds / 1000;

		long hours = diffSeconds / 3600;
		long remainder = diffSeconds % 3600;
		long minutes = remainder / 60;
		long seconds = remainder % 60;

		return ((hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":"
				+ (seconds < 10 ? "0" : "") + seconds);

	}

	// srk

	public enum SeleniumIdentifiers

	{
		className, cssSelector, id, linkText, name, partialLinkText, tagName, xpath, byIndex, byValue, byVisibleText
	}

	public Common_Methods(WebDriver driver) {
		Common_Methods.driver = driver;
		// this.selenium = new WebDriverBackedSelenium(driver,"");
		this.DriverActions = new Actions(driver);
		this.KeyboardActions = new Actions(keyboard);
		this.KeyboardMouseActions = new Actions(keyboard, mouse);
		// selenium.windowMaximize();
		mainpage = driver.getWindowHandle();
		// driver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
	}

	/*
	 * *****************************************************************************
	 * **********
	 * 
	 * @author Sanjeev Ranjan Kumar
	 * 
	 * @param TestObject The Test Object Value Read From The Data Table (XLS)
	 * 
	 * @return Is an Array returned as sTemp
	 * 
	 * @throws IOException
	 ***************************************************************************************/

	static String[] splitTestObject(String TestObject) throws IOException {
		// writeLog(new Exception().getStackTrace()[0].getMethodName().toString());
		System.out.println("Action performed  ---" + Thread.currentThread().getStackTrace()[2].getMethodName()
				+ "  : Inside a method ---" + Thread.currentThread().getStackTrace()[3].getMethodName());

		System.out.println("TestObject is " + TestObject);
		String[] sTemp = (TestObject.split(",", 3));
		System.out.println("Element name " + sTemp[0].toString());
		if (sTemp[0].contains("_")) {
			String[] sTemp1 = sTemp[0].split("_", 2);
			sTemp[0] = new String(sTemp1[1].trim());
			// System.out.println("sTemp[0] is " + sTemp[0]);
		}
		sTemp[1] = new String(sTemp[1].trim());
		// System.out.println("sTemp[1] is " + sTemp[1]);
		sTemp[2] = new String(sTemp[2].trim());
		// System.out.println("sTemp[2] is " + sTemp[2]);
		System.out.println("Element Property name " + sTemp[1].toString());
		System.out.println("Element Property value " + sTemp[2].toString());
		return sTemp;

	}

	// srk-Reading excel

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To provide user access control over Runner Classes RETURN
	 * VALUE: None INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	public static String[] execute_classCase() throws Exception {
		// System.out.println("Inside execute testcases method");
		ExcelUtils.setExcelFile(Constants.Path_TestData);
		int iTotalTestCases = ExcelUtils.getRowCount(Constants.Sheet_TestCases);
		int x = 0;

		for (int iTestcase = 1; iTestcase <= iTotalTestCases; iTestcase++) {
			// System.out.println("iterating repedetly");

			sTestCaseID = ExcelUtils.getCellData(iTestcase, Constants.Col_TestCaseID, Constants.Sheet_TestCases);
			sTestRunner = ExcelUtils.getCellData(iTestcase, Constants.Col_Runnerfile, Constants.Sheet_TestCases);
			sRunMode = ExcelUtils.getCellData(iTestcase, Constants.Col_RunMode, Constants.Sheet_TestCases);
			sSLNO = ExcelUtils.getCellData(iTestcase, Constants.Col_SLNO, Constants.Sheet_TestCases);
			// System.out.println("value of iTestcase"+ iTestcase);
			// This is the condition statement on RunMode value

			if (sRunMode.equals("Yes")) {
				// System.out.println("inside yes");

				Constants.Num[x] = "sTestRunner";

				x++;
				iTestcase++;
			}

			else {
				Constants.Num[x] = "false";
				x++;
				// iTestcase++;
			}

		}
		// System.out.println("Coming out from execute test");
		return Constants.Num;
	}

	// srk

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To read cucumber runner class from excel file RETURN
	 * VALUE: None INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	public static List<String> read_class() throws Exception {

		System.out.println("Inside execute testcases method");

		ArrayList<String> list = new ArrayList<String>();
		list.add("come.unisys.eportal.runner.AppLogRunner");
		list.add("come.unisys.eportal.runner.AppMaxRunner");
		list.add("come.unisys.eportal.runner.AppLogDeleteRunner");
		list.add("come.unisys.eportal.runner.AppProRunner");
		list.add("come.unisys.eportal.runner.AppProRunner");
		list.add("come.unisys.eportal.runner.DeployedAppRunner");

		list.add("come.unisys.eportal.runner.IISFuncRunner");
		list.add("come.unisys.eportal.runner.IISLogRunner");
		list.add("come.unisys.eportal.runner.IISLogMaxRunner");
		list.add("come.unisys.eportal.runner.IISLogDeleteRunner");

		list.add("come.unisys.eportal.runner.EventRunner");

		list.add("come.unisys.eportal.runner.ManagementEventRunner");
		list.add("come.unisys.eportal.runner.SystemEventRunner");
		list.add("come.unisys.eportal.runner.ePortalRunner");
		list.add("come.unisys.eportal.runner.EventSortRunner");
		list.add("come.unisys.eportal.runner.ApplicationEventRunner");

		list.add("come.unisys.eportal.runner.TestRunner");
		list.add("come.unisys.eportal.runner.TestRunner1");
		list.add("come.unisys.eportal.runner.TestRunner2");
		list.add("come.unisys.eportal.runner.TestRunner3");
		list.add("come.unisys.eportal.runner.SecurityEventRunner");
		list.add("come.unisys.eportal.runner.clearAtOnce");
		list.add("come.unisys.eportal.runner.ViewAlertsDataRunner");
		list.add("come.unisys.eportal.runner.ViewApplicationEventRunner");
		list.add("come.unisys.eportal.runner.ViewSecuritylogsRunner");
		list.add("come.unisys.eportal.runner.WCOfflineRunner");
		list.add("come.unisys.eportal.runner.WebClusterOnlineRunner");
		list.add("come.unisys.eportal.runner.WizardbuttonsRunner");
		list.add("come.unisys.eportal.runner.WPDescriptionRunner");
		list.add("come.unisys.eportal.runner.WPOnlineRunner");
		list.add("come.unisys.eportal.runner.WPStatusRunner");
		list.add("come.unisys.eportal.runner.InorrectPasswdRunner");
		list.add("come.unisys.eportal.runner.SystemEventRunner");
		list.add("come.unisys.eportal.runner.SystemlogsRunner");
		list.add("come.unisys.eportal.runner.RestartManagerRunner");
		list.add("come.unisys.eportal.runner.RightFrameLinkRunner");
		list.add("come.unisys.eportal.runner.RefreshRunner");

		// list.add("B");
		// list.add("C");

		// ExcelUtils.setExcelFile(Constants.Path_TestData);
		// int iTotalTestCases = ExcelUtils.getRowCount(Constants.Sheet_TestCases);
		// int x=0;
		// List<String> list = new ArrayList<String>();

		// for(int iTestcase=1;iTestcase<=iTotalTestCases;iTestcase++){
		// System.out.println("iterating repedetly");

		// sTestCaseID = ExcelUtils.getCellData(iTestcase, Constants.Col_TestCaseID,
		// Constants.Sheet_TestCases);
		/*
		 * sTestRunner = ExcelUtils.getCellData(iTestcase, Constants.Col_Runnerfile,
		 * Constants.Sheet_TestCases); sRunMode = ExcelUtils.getCellData(iTestcase,
		 * Constants.Col_RunMode,Constants.Sheet_TestCases); sSLNO =
		 * ExcelUtils.getCellData(iTestcase,
		 * Constants.Col_SLNO,Constants.Sheet_TestCases);
		 * System.out.println("value of iTestcase"+ iTestcase); //This is the condition
		 * statement on RunMode value
		 * 
		 * if (sRunMode.equals("Yes")){ // System.out.println("inside yes");
		 * 
		 * //Constants.Num[x]="sTestRunner"; list.add(sTestRunner);
		 * 
		 * //x++; //iTestcase++; }
		 * 
		 * else{ Constants.Num[x]="false"; x++; //iTestcase++; }
		 * 
		 * 
		 * } System.out.println("Coming out from execute classreading from excel");
		 * //return Constants.Num;
		 */
		System.out.println("Coming out from execute classreading from List");
		return list;
	}

	// Secure -srk
	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To check for attribute RETURN VALUE: None INPUT(s):
	 * Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public boolean checkAttributeExist(String testObject, String attributeName) throws IOException {
		boolean flag = false;
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		// flag= checkElementExist(sTemp[1], sTemp[2]);
		WebElement we = checkElementExplicitWait(sTemp[1], sTemp[2]);

		try {
			String value = we.getAttribute(attributeName);
			System.out.println("Value of an attribute " + value);
			if (value != null) {
				System.out.println("attribute does not exist" + value);
				flag = true;
			}
		} catch (Exception e) {

		}

		return flag;
	}

	/*
	 * static String checkPartitionRunningStatus(WebDriver driver ,String
	 * TestObject) throws IOException { System.out.println("Method Name is --" + new
	 * Exception().getStackTrace()[0].getMethodName().toString());
	 * 
	 * String[] sTemp = splitTestObject(TestObject); WebElement we =
	 * checkElement(sTemp[1], sTemp[2]); try { String partitionIconStatus =
	 * driver.findElement(By.xpath("//div[" +sTemp[2]+
	 * "]\\div[1]")).getAttribute("class"); String partitionHealthStatus =
	 * driver.findElement(By.xpath("//div[" +sTemp[2]+
	 * "]\\div[2]")).getAttribute("class"); return partitionIconStatus + ":" +
	 * partitionHealthStatus; } catch(Exception ex) {
	 * System.out.println("exception while finding ele properties");
	 * checkException(ex, "exception while finding ele properties"); return null; }
	 * 
	 * }
	 */

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To return WebElements with explicit wait to Main class
	 * RETURN VALUE: None INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan
	 * Kumar
	 ***************************************************************************************/

	static WebElement checkElementExplicitWait(String Identifiers, String Locators) {
		// System.out.println("Inside Method checkElement");
		SeleniumIdentifiers Identifier = SeleniumIdentifiers.valueOf(Identifiers);
		WebElement Element = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);

		switch (Identifier) {
		case className: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case cssSelector: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case id: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case linkText: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.linkText(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case name: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case partialLinkText: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.partialLinkText(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case tagName: {
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.tagName(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case xpath: {
			// System.out.println("inside xpath");
			try {
				Element = wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Locators))));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		default:
			break;

		}
		// System.out.println("Exiting Method checkElement");
		return Element;

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To return WebElement to Main class RETURN VALUE: None
	 * INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	static WebElement checkElement(String Identifiers, String Locators) {
		System.out.println("Inside Method checkElement");
		FluentWait<WebDriver> wait = null;
		wait = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS).pollingEvery(30, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);

		SeleniumIdentifiers Identifier = SeleniumIdentifiers.valueOf(Identifiers);
		WebElement Element = null;
		System.out.println("Value of Identifier" + Identifier);
		switch (Identifier) {
		case className: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(Locators))));
				Element = driver.findElement(By.className(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case cssSelector: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector(Locators))));
				Element = driver.findElement(By.cssSelector(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case id: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id(Locators))));

				Element = driver.findElement(By.id(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case linkText: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.linkText(Locators))));
				Element = driver.findElement(By.linkText(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case name: {

			clickButton(Locators);
			/*
			 * try {
			 * 
			 * System.out.println("Locators value is "+Locators);
			 * 
			 * System.out.println("Hello Name method");
			 * wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name(
			 * Locators)))); Element = driver.findElement(By.name(Locators));
			 * 
			 * //driver.findElement(By.name("Destroy")).click();
			 * System.out.println("Element name is"+ Element); } catch (Exception e) {
			 * checkException(e, "Check Element Displayed / Enabled"); }
			 */
			break;
		}
		case partialLinkText: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.partialLinkText(Locators))));
				Element = driver.findElement(By.partialLinkText(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case tagName: {
			try {
				wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.tagName(Locators))));
				Element = driver.findElement(By.tagName(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case xpath: {
			System.out.println("inside xpath");
			try {
				// wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(Locators))));
				Element = driver.findElement(By.xpath(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		default:
			break;

		}
		// System.out.println("Exiting Method checkElement");
		return Element;

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To return WebElements to Main class RETURN VALUE: None
	 * INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	static List<WebElement> checkElements(String Identifiers, String Locators) {
		// System.out.println("Inside Method checkElement");
		Wait<WebDriver> wait = new FluentWait(driver).withTimeout(240, TimeUnit.SECONDS)
				.pollingEvery(60, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
		SeleniumIdentifiers Identifier = SeleniumIdentifiers.valueOf(Identifiers);
		List<WebElement> Element = null;

		switch (Identifier) {
		case className: {
			try {
				Element = driver.findElements(By.className(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case cssSelector: {
			try {
				Element = driver.findElements(By.cssSelector(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case id: {
			try {
				Element = driver.findElements(By.id(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case linkText: {
			try {
				Element = driver.findElements(By.linkText(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case name: {
			try {
				Element = driver.findElements(By.name(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case partialLinkText: {
			try {
				Element = driver.findElements(By.partialLinkText(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case tagName: {
			try {
				Element = driver.findElements(By.tagName(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		case xpath: {
			System.out.println("inside xpath");
			try {
				Element = driver.findElements(By.xpath(Locators));
			} catch (Exception e) {
				checkException(e, "Check Element Displayed / Enabled");
			}
			break;
		}
		default:
			break;

		}
		// System.out.println("Exiting Method checkElement");
		return Element;

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To check Web Element existence RETURN VALUE: None
	 * INPUT(s): Identifier and Locator AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	static boolean checkElementExist(String Identifiers, String Locators) {
		// System.out.println("Inside Method checkElement");

		SeleniumIdentifiers Identifier = SeleniumIdentifiers.valueOf(Identifiers);
		WebElement Element = null;
		boolean flag = true;
		switch (Identifier) {
		case className: {
			try {
				Element = driver.findElement(By.className(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case cssSelector: {
			try {
				Element = driver.findElement(By.cssSelector(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case id: {
			try {
				Element = driver.findElement(By.id(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case linkText: {
			try {
				Element = driver.findElement(By.linkText(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case name: {
			try {
				Element = driver.findElement(By.name(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case partialLinkText: {
			try {
				Element = driver.findElement(By.partialLinkText(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case tagName: {
			try {
				Element = driver.findElement(By.tagName(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		case xpath: {
			System.out.println("inside xpath");
			try {
				Element = driver.findElement(By.xpath(Locators));
			} catch (Exception e) {
				flag = false;
			}
			break;
		}
		default:
			flag = false;
			break;

		}
		// System.out.println("Exiting Method checkElement");
		if (flag == true && Element.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Launch Browser and Open application RETURN VALUE: None
	 * INPUT(s): Driver and Application URL AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public static void openBrowser(String URL) throws IOException {
		try {
			System.out.println("Website URL with credentials is->" + URL);
			driver.navigate().to(URL);// launch Browser with the URL
			if(driver.getCurrentUrl().contains(URL))
			{
				System.out.println("Current URL " + driver.getCurrentUrl().toString());
				System.out.println("Launched succefully " + URL);
			}

			// Report to Notepad, HTMl and Excel
			ReportManager.ReportEvent(TestStatus.PASS, "Launch The URL - '" + URL + "'",
					"Launched The URL - '" + driver.getCurrentUrl() + "' - Successfull".toUpperCase());

		} catch (Exception e) {

			checkException(e, "Launch the URL '" + ePortalUrlLINK + "'");
			ReportManager.ReportEvent(TestStatus.WARNING, "Launch The URL -" + ePortalUrlLINK,
					"Clicked On - '" + ePortalUrlLINK + " - UnSuccessfull");

		}

		URL = null;

	}

	public void compareOutput(String TestObject, String Expected) throws IOException {
		String Actual = null;
		String[] sTemp = splitTestObject(TestObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);
		Actual = we.getText();

		if (Actual.equalsIgnoreCase(Expected)) {
			ReportManager.ReportEvent(TestStatus.PASS, "Expected Value Is - " + Expected,
					"Actual  Value Is - " + Actual + " - Successfull".toUpperCase());

		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Expected Value Is - " + Expected,
					"Actual  Value Is -  " + Actual + " - UnSuccessfull".toUpperCase());

		}
	}

	public void comparestring(String Actual, String Expected) throws IOException {

		if (Actual.equalsIgnoreCase(Expected)) {
			ReportManager.ReportEvent(TestStatus.PASS, "Expected Value Is - " + Expected,
					"Actual  Value Is - " + Actual + " - Successfull".toUpperCase());

		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Expected Value Is - " + Expected,
					"Actual  Value Is -  " + Actual + " - UnSuccessfull".toUpperCase());

		}
	}

	public void reportError(String actual, String expected) throws IOException {
		ReportManager.ReportEvent(TestStatus.FAILED, actual, expected);

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Enter Values in Edit Field RETURN VALUE: None INPUT(s):
	 * Driver, Xpath of object and data to enter in the edit box AUTHOR: Sanjeev
	 * Ranjan Kumar
	 ***************************************************************************************/
	public static void editAField(String TestObject, String Value) throws IOException {

		String[] sTemp = splitTestObject(TestObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);
		System.out.println("--Value : " + Value);

		try {

			we.clear();
			we.sendKeys(Value);
			ReportManager.ReportEvent(TestStatus.PASS, "Enter Value As - " + Value,
					"Entered Value Is - " + Value + " - Successfull".toUpperCase());

		} catch (Exception e) {

			checkException(e, "Entered Value As '" + Value + "'");
			ReportManager.ReportEvent(TestStatus.WARNING, "Enter Value As - " + Value,
					"Entered Value Is - " + Value + " - UnSuccessfull".toUpperCase());

		}

		sTemp = null;
		Value = null;

	}

	public static void editAFieldUsingActions(String TestObject, String value) throws IOException {

		String[] sTemp = splitTestObject(TestObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);
		System.out.println("--Value : " + value);
		Actions act = new Actions(driver);

		try {

			we.clear();
			act.sendKeys(we, value).perform();
			// act.sendKeys(Keys.ENTER).perform();;
			ReportManager.ReportEvent(TestStatus.PASS, "Enter Value As - " + value,
					"Entered Value Is - " + value + " - Successfull".toUpperCase());
		} catch (Exception e) {

			checkException(e, "Entered Value As '" + value + "'");
			ReportManager.ReportEvent(TestStatus.WARNING, "Enter Value As - " + value,
					"Entered Value Is - " + value + " - UnSuccessfull".toUpperCase());
		}

		sTemp = null;
		value = null;

	}

	// adding new function to get the webelement for dynamic x-path Aravind
	public static List<WebElement> getWebElements(String testObject) throws IOException {
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		List<WebElement> we = checkElements(sTemp[1], sTemp[2]);

		ReportManager.ReportEvent(TestStatus.PASS, "Verify Elements Exist -  '" + sTemp[0].toUpperCase() + "'",
				"Verified - Element Exist with a count '" + we.size() + "'");

		return we;

	}

	// adding new function to get the webelement for dynamic x-path Aravind
	public static WebElement getWebElement(String testObject) throws IOException {
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);
		ReportManager.ReportEvent(TestStatus.PASS, "Verify Element Exist - '" + sTemp[0].toUpperCase() + "'",
				"Verified - Element Exist '" + sTemp[0] + "' - Successfull".toUpperCase());

		return we;

	}
	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform Frame Switching RETURN VALUE: None AUTHOR:
	 * Sanjeev
	 ***************************************************************************************/

	public static void Switch_frame() throws InterruptedException {
		int count = driver.findElements(By.tagName("frame")).size();

		// Navigating to Content frame

		System.out.println("count=" + count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// Thread.sleep(5000);

		// Accessing PMLocation & performing click

		WebElement PM = driver.findElement(By.xpath(PMLOCATION));
		PM.click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PMLINK + "'",
				"Clicked On - '" + PMLINK + "' - Successfull");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		WebElement PM_frame = driver.findElement(By.xpath(PMPARTITION));
		driver.switchTo().frame(PM_frame);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		// Navigation for PM Assignment

		driver.findElement(By.xpath(NEXTLOCATION)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK + "'",
				"Clicked On - '" + NEXTLINK + "' - Successfull");
		Thread.sleep(5000);
		driver.findElement(By.xpath(NEXTLOCATION)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK + "'",
				"Clicked On - '" + NEXTLINK + "' - Successfull");
		Thread.sleep(5000);
		driver.findElement(By.xpath(NEXTLOCATION)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
		driver.findElement(By.xpath(NEXTLOCATION)).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		// driver.findElement(By.id("wizard1_CancelButton")).click();
		driver.findElement(By.xpath(CANCELBUTTON)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(5000);
	}

	public static void AppLog() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK + "'",
				"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHAPPLNK + "'",
				"Clicked On - '" + CHAPPLNK + "' - Successfull");
		driver.findElement(By.xpath(GTRLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + GTRLNK + "'",
				"Clicked On - '" + GTRLNK + "' - Successfull");
		driver.findElement(By.xpath(GTRLOG1)).click();
		String msg = driver.findElement(By.xpath(GTRLOGMSG)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
				"Verified message - '" + msg + "' - Successfull");

	}

	public static void AppLogDelete() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK + "'",
				"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHAPPLNK + "'",
				"Clicked On - '" + CHAPPLNK + "' - Successfull");
		driver.findElement(By.xpath(GTRLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + GTRLNK + "'",
				"Clicked On - '" + GTRLNK + "' - Successfull");
		try {

			driver.findElement(By.xpath(SLTAPPDLT)).click();
			driver.findElement(By.xpath(APPDLT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + APPDLTLNK + "'",
					"Clicked On - '" + APPDLTLNK + "' - Successfull");
			driver.findElement(By.xpath(APPDLTYES)).click();
			String msg = driver.findElement(By.xpath(APPDLTMSG)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");
		} catch (Exception e) {
			String msg = "generate Application log first";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");

		}

	}

	public static void AppLogMax() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK + "'",
				"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHAPPLNK + "'",
				"Clicked On - '" + CHAPPLNK + "' - Successfull");
		driver.findElement(By.xpath(GTRLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + GTRLNK + "'",
				"Clicked On - '" + GTRLNK + "' - Successfull");

		try {
			WebElement Msg = driver.findElement(By.xpath(APPMAX));
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			if (Msg.isDisplayed()) {
				String msg = driver.findElement(By.xpath(APPMAX)).getText();
				ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
						"Verified message - '" + msg + "' - Successfull");
			}
		} catch (Exception e) {

			String msg = "Max no. of application log is not reached";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");
		}

	}

	public static void DeployedAppPro() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK + "'",
				"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		try {
			driver.findElement(By.xpath(CHAPP)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHAPPLNK + "'",
					"Clicked On - '" + CHAPPLNK + "' - Successfull");
			String msg = driver.findElement(By.xpath(APPPRO)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");
		} catch (Exception e) {
			String msg = "Deploy an applciation first";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");

		}

	}

	public static void UndeployedApp() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPAPP)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK + "'",
				"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		System.out.println("In MethodUndeployedApp " );
		try {
			WebElement appelement = driver.findElement(By.xpath(HeadingMessage));
	    	if(appelement.getText().contains("None")){
	    		String msg="Deployed Applications (None)";
	    		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg+"'", "Verified message - '"+ msg    + "' - Successfull"); 
	    	}
	    	else {
	    		//check number of Applications to be undeployed
	    		Thread.sleep(1000);
	    		List<WebElement> els = driver.findElements(By.xpath(CheckBoxLink));
	    		//Removing 0th element as it is checkALL checkboxes 
	    		els.remove(0);
	    		int CheckedBoxcount =0;
	    		for ( WebElement el : els ) {
	    		    if ( !el.isSelected() ) {
	    		        el.click();
	    		        CheckedBoxcount++;
	    		        if(CheckedBoxcount == Integer.parseInt(GlobalVariables.NumberofApllicationsToBeRemoved)){
	    		        	System.out.println("No of Applications selected  "+CheckedBoxcount);
	    		        	ReportManager.ReportEvent(TestStatus.PASS, "Click On number of Apps- '" + GlobalVariables.NumberofApllicationsToBeRemoved+"'", "Clicked On number of Apps - '"+ CheckedBoxcount + "' - Successfull");
	    		        	break;
	    		        }
	    		    }
	    		}
	    		 driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	    		    driver.findElement(By.xpath(APPUNDPY)).click();
		    		Thread.sleep(2000);
		    		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + APPUNDPYMsg+"'", "Clicked On - '"+ APPUNDPYMsg + "' - Successfull");
		    		SelectSynchronizationMethod(GlobalVariables.SynchornizationMethod);
		    		Thread.sleep(3000);
					driver.findElement(By.xpath(APPUNDPYYES)).click();
		    		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + APPUNDPYLNK+"'", "Clicked On - '"+ APPUNDPYLNK + "' - Successfull");
		    		String msg=driver.findElement(By.xpath(APPUNDPYMSG)).getText();
		    		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg+"'", "Verified message - '"+ msg    + "' - Successfull"); 
	    	}
			
		} catch (Exception e) {
			String msg = "Deploy an applciation first";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");

		}

	}

	public static void Event_Sort() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
	}

	public static void Event() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String msg = driver.findElement(By.xpath(EVENTNO)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
				"Verified message - '" + msg + "' - Successfull");

	}

	public static void Event_Security() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(EVENTDROP)));
		dropdown.selectByValue(Security);
		driver.findElement(By.xpath(EVENTFILTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SECURITYEVENTLNK + "'",
				"Clicked On - '" + SECURITYEVENTLNK + "' - Successfull");

	}

	public static void Event_Application() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(EVENTDROP)));
		dropdown.selectByValue(Application);
		driver.findElement(By.xpath(EVENTFILTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + APPEVENTLNK + "'",
				"Clicked On - '" + APPEVENTLNK + "' - Successfull");

	}

	public static void Event_System() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(EVENTDROP)));
		dropdown.selectByValue(system);
		driver.findElement(By.xpath(EVENTFILTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SYSEVENTLNK + "'",
				"Clicked On - '" + SYSEVENTLNK + "' - Successfull");

	}

	public static void Event_Management() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(EVENTDROP)));
		dropdown.selectByValue(Management);
		driver.findElement(By.xpath(EVENTFILTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGENTLNK + "'",
				"Clicked On - '" + MNGENTLNK + "' - Successfull");

	}

	public static void Event_ePortal() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(EVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLNK + "'",
				"Clicked On - '" + EVENTLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SORTEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SORTEVENTLNK + "'",
				"Clicked On - '" + SORTEVENTLNK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(EVENTDROP)));
		dropdown.selectByValue(eportal);
		driver.findElement(By.xpath(EVENTFILTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EPORTALENTLNK + "'",
				"Clicked On - '" + EPORTALENTLNK + "' - Successfull");

	}

	public static void IISLogFunc() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(IISLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISLOGLNK + "'",
				"Clicked On - '" + IISLOGLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHCLU)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHCLULNK + "'",
				"Clicked On - '" + CHCLULNK + "' - Successfull");
		Boolean msg = driver.findElement(By.xpath(IISGTHR)).isDisplayed();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
				"Verified message - '" + msg + "' - Successfull");

	}

	public static void IISLog() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(IISLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISLOGLNK + "'",
				"Clicked On - '" + IISLOGLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHCLU)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHCLULNK + "'",
				"Clicked On - '" + CHCLULNK + "' - Successfull");
		driver.findElement(By.xpath(IISGTHR)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISGTHRLNK + "'",
				"Clicked On - '" + IISGTHRLNK + "' - Successfull");
		driver.findElement(By.xpath(IISGT)).click();
		String msg = driver.findElement(By.xpath(IISGTMSG)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
				"Verified message - '" + msg + "' - Successfull");

	}

	public static void IISLogDelete() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(IISLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISLOGLNK + "'",
				"Clicked On - '" + IISLOGLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHCLU)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHCLULNK + "'",
				"Clicked On - '" + CHCLULNK + "' - Successfull");
		driver.findElement(By.xpath(IISGTHR)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISGTHRLNK + "'",
				"Clicked On - '" + IISGTHRLNK + "' - Successfull");
		driver.findElement(By.xpath(IISGTCHK)).click();
		driver.findElement(By.xpath(IISGTRMV)).click();
		driver.findElement(By.xpath(IISGTYES)).click();
		String msg = driver.findElement(By.xpath(IISGTRMVMSG)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
				"Verified message - '" + msg + "' - Successfull");

	}

	public static void IISMax() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(IISLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISLOGLNK + "'",
				"Clicked On - '" + IISLOGLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CHCLU)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHCLULNK + "'",
				"Clicked On - '" + CHCLULNK + "' - Successfull");
		driver.findElement(By.xpath(IISGTHR)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISGTHRLNK + "'",
				"Clicked On - '" + IISGTHRLNK + "' - Successfull");

		try {
			String msg = driver.findElement(By.xpath(IISMAX)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");

		}

		catch (Exception e) {
			String msg = "Max No of IIS Lof not created";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");

		}

	}

	public static void WCOffline() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(IISLOG)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + IISLOGLNK + "'",
				"Clicked On - '" + IISLOGLNK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		try {
			driver.findElement(By.xpath(CHCLU)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHCLULNK + "'",
					"Clicked On - '" + CHCLULNK + "' - Successfull");
			driver.findElement(By.xpath(CLSOFF)).click();
			driver.findElement(By.xpath(CLSOFFYES)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLSOFFYESLNK + "'",
					"Clicked On - '" + CLSOFFYESLNK + "' - Successfull");
			String msg = driver.findElement(By.xpath(CLSOFFSTS)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");
		} catch (Exception e) {
			String msg = "Deploy an applciation first";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Successfull");
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform Cluster Status Change RETURN VALUE: None
	 * AUTHOR: Sanjeev
	 ***************************************************************************************/

	public static void Cluster_Status() throws InterruptedException {
		int count = driver.findElements(By.tagName("frame")).size();
		System.out.println("count=" + count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);

		// Accessing Cluster Link

		driver.findElement(By.xpath(CLUSTERLINK)).click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERCLICK + "'",
				"Clicked On - '" + CLUSTERCLICK + "' - Successfull");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		// Accessing CheckBox

		driver.findElement(By.xpath(CHECKBOX)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CHECKBOXCLICK + "'",
				"Clicked On - '" + CHECKBOXCLICK + "' - Successfull");
		Thread.sleep(5000);

		// Accessing Remove Button

		driver.findElement(By.xpath(REMOVEBUTTON)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + REMOVEBUTTONCLICK + "'",
				"Clicked On - '" + REMOVEBUTTONCLICK + "' - Successfull");
		Thread.sleep(5000);

		// Seeking remove confirmation

		Thread.sleep(5000);
		driver.findElement(By.xpath(YESBUTTON));
		driver.findElement(By.xpath(BACK)).click();
		;
		// driver.findElement(By.xpath(YESBUTTON)).click();
		// driver.findElement(By.xpath("//*[@id='ConfirmPanel1_YesButton']")).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK + "'",
				"Clicked On - '" + YESBUTTONCLICK + "' - Successfull");
		// Thread.sleep(5000);

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform email notification RETURN VALUE: None AUTHOR:
	 * Sanjeev
	 ***************************************************************************************/

	public static void email() throws InterruptedException {
		int count = driver.findElements(By.tagName("frame")).size();
		System.out.println("count=" + count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);

		// Accessing events & Alert Link

		driver.findElement(By.xpath(EVENTLINK)).click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLINKCLICK + "'",
				"Clicked On - '" + EVENTLINKCLICK + "' - Successfull");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		// Accessing enable checkbox

		driver.findElement(By.xpath(ENABLECHECKBOX)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ENABLECHECKBOXCLICK + "'",
				"Clicked On - '" + ENABLECHECKBOXCLICK + "' - Successfull");
		Thread.sleep(5000);

		// providing email address

		driver.findElement(By.xpath(RECIPIENT)).click();
		driver.findElement(By.xpath(RECIPIENT)).clear();
		Thread.sleep(1000);
		driver.findElement(By.xpath(RECIPIENT)).sendKeys("ravi.kumar3@in.unisys.com");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + RECIPIENTCLICK + "'",
				"Provided - '" + RECIPIENTCLICK + "' - Successfull");
		Thread.sleep(5000);

		// accessing OK Button

		driver.findElement(By.xpath(OK)).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + OKCLICK + "'",
				"Provided - '" + OKCLICK + "' - Successfull");
		Thread.sleep(5000);

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform alert/event notification RETURN VALUE: None
	 * AUTHOR: Sanjeev
	 ***************************************************************************************/

	public static void event() throws InterruptedException {
		int count = driver.findElements(By.tagName("frame")).size();
		System.out.println("count=" + count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);

		// Accessing events & Alert Link

		driver.findElement(By.xpath(EVENTLINK)).click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + EVENTLINKCLICK + "'",
				"Clicked On - '" + EVENTLINKCLICK + "' - Successfull");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

		// Accessing enable checkbox

		driver.findElement(By.xpath(ENABLECHECKBOX)).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ENABLECHECKBOXCLICK + "'",
				"Clicked On - '" + ENABLECHECKBOXCLICK + "' - Successfull");
		Thread.sleep(5000);

		// providing categories of messages

		driver.findElement(By.xpath(ERROR)).click();
		Select dropdown = new Select(driver.findElement(By.id("EmailDropDown_Security")));
		dropdown.selectByValue("1");
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ERRORCLICK + "'",
				"Provided - '" + ERRORCLICK + "' - Successfull");
		Thread.sleep(5000);

		// accessing OK Button

		driver.findElement(By.xpath(OK)).click();

		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + OKCLICK + "'",
				"Provided - '" + OKCLICK + "' - Successfull");
		Thread.sleep(5000);

	}

	public void clickOnElementUsingJS(WebElement element, String name) {
		try {
//		WebDriverWait wait = new WebDriverWait(driver,30);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
			jse.executeScript("arguments[0].click();", element);
			// testResult.addTestSteps(new TestStep("Identify Element : " + name + " and
			// Click", "PASSED"));
		} catch (org.openqa.selenium.TimeoutException e) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
			jse.executeScript("arguments[0].click();", element);
			// testResult.addTestSteps(new TestStep("Identify Element : " + name + " and
			// Click", "PASSED"));
		} catch (org.openqa.selenium.WebDriverException e) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
			jse.executeScript("arguments[0].click();", element);
			// testResult.addTestSteps(new TestStep("Identify Element : " + name + " and
			// Click", "PASSED"));
		}
	}

//srk

	public void findElementByXPathAndClick(String path, String UIName) {
		System.out.println(path);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));
		element.click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		// ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + path+"'",
		// "Clicked On - '"+ path + "' - Successfull");
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + UIName + "'",
				"Clicked On - '" + UIName + "' - Successfull");
		// ReportManager.ReportEvent(TestStatus.PASS, "Click " + testObject + " - '" +
		// sTemp[0].toUpperCase() + "'", "Click " + testObject + " - '"+ sTemp[0] + "' -
		// Successfull".toUpperCase());

	}

	// srk

	public static void clickElement(String testObject) throws IOException {

		System.out.println("inside Click Elment Method" + testObject);

		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}
		Actions act = null;
		act = new Actions(driver);
		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		try {
			we.click();
			// we.sendKeys(Keys.ENTER);
			act.moveToElement(we).click().perform();
			waitTime(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + sTemp[0] + "'",
					"Clicked On - '" + sTemp[0] + "' - Successfull");
			// ReportManager.ReportEvent(TestStatus.PASS, "Click " + testObject + " - '" +
			// sTemp[0].toUpperCase() + "'", "Click " + testObject + " - '"+ sTemp[0] + "' -
			// Successfull".toUpperCase());
		} catch (Exception e) {

			checkException(e, "Click On - '" + sTemp[0] + "'");
			ReportManager.ReportEvent(TestStatus.WARNING, "Click On - '" + sTemp[0] + "'",
					"Clicked On - '" + sTemp[0] + "' - UnSuccessfull");
			// ReportManager.ReportEvent(TestStatus.WARNING, "Click " + testObject + " - '"
			// + sTemp[0].toUpperCase() + "'", "Click " + testObject + " - '"+ sTemp[0] + "'
			// - Not Successfull".toUpperCase());

		}

		sTemp = null;
		testObject = null;

	}

	public static void clickElementExplicitWait(String testObject) throws IOException {
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElementExplicitWait(sTemp[1], sTemp[2]);

		try {
			we.click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + sTemp[0] + "'",
					"Clicked On - '" + sTemp[0] + "' - Successfull");
			// ReportManager.ReportEvent(TestStatus.PASS, "Click " + testObject + " - '" +
			// sTemp[0].toUpperCase() + "'", "Click " + testObject + " - '"+ sTemp[0] + "' -
			// Successfull".toUpperCase());

		} catch (Exception e) {

			checkException(e, "Click On - '" + sTemp[0] + "'");
			ReportManager.ReportEvent(TestStatus.WARNING, "Click On - '" + sTemp[0] + "'",
					"Clicked On - '" + sTemp[0] + "' - UnSuccessfull");
			// ReportManager.ReportEvent(TestStatus.WARNING, "Click " + testObject + " - '"
			// + sTemp[0].toUpperCase() + "'", "Click " + testObject + " - '"+ sTemp[0] + "'
			// - Not Successfull".toUpperCase());

		}

		sTemp = null;
		testObject = null;
	}

	// 8988u98
	public static boolean checkElementDisplayed(String testObject) throws IOException {

		boolean flag = false;
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		if (we != null) {
			flag = true;
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + sTemp[0] + "' Object Present On the Screen",
					"Verfied - '" + sTemp[0] + "' Object Present On the Screen  - Successfull".toUpperCase());
		} else {
			flag = false;
			ReportManager.ReportEvent(TestStatus.WARNING, "Verify - '" + sTemp[0] + "' Object Present On the Screen",
					"Verfied - '" + sTemp[0] + "' Object is not Present On the Screen- UnSuccessfull".toUpperCase());
//		Assert.fail("Exiting Script Execution");
		}

		return flag;

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform Check action RETURN VALUE: None INPUT(s):
	 * Driver and Xpath of object AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void checkBox(String TestObject, String status) throws IOException {

		String[] sTemp = splitTestObject(TestObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);
		if (status.equalsIgnoreCase("chk")) {
			try {
				if (!we.isSelected()) {
					we.click();
					ReportManager.ReportEvent(TestStatus.PASS, "Check The Checkbox '" + sTemp[0] + "'",
							"Checked The checkbox '" + sTemp[0] + "'" + "' - Successfull".toUpperCase());
				} else {
					ReportManager.ReportEvent(TestStatus.WARNING, "Check The Checkbox '" + sTemp[0] + "'",
							"Checkbox '" + sTemp[0] + "' has already been checked");
				}
			} catch (Exception e) {
				checkException(e, "Check The Checkbox '" + sTemp[0] + "'");
			}
		} else {
			try {
				if (we.isSelected()) {
					we.click();
					ReportManager.ReportEvent(TestStatus.PASS, "Uncheck The Checkbox '" + sTemp[0] + "'",
							"Unchecked The Checkbox '" + sTemp[0] + "'" + "' - Successfull".toUpperCase());
				} else {
					ReportManager.ReportEvent(TestStatus.WARNING, "Uncheck The Checkbox '" + sTemp[0] + "'",
							"Checkbox '" + sTemp[0] + "' has already been unchecked");
				}
			} catch (Exception e) {
				checkException(e, "Uncheck the checkbox '" + sTemp[0] + "'");
			}
		}

		sTemp = null;
		status = null;
		Runtime.getRuntime().gc();

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform radio button select RETURN VALUE: None
	 * INPUT(s): Driver and Xpath of object AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void radioButton(String TestObject) throws IOException {
		String[] sTemp = splitTestObject(TestObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		try {

			if (!we.isSelected()) {

				we.click();
				ReportManager.ReportEvent(TestStatus.PASS, "Select the Radio Button '" + sTemp[0] + "'",
						"Selected the Radio Button '" + sTemp[0] + "'");

			} else

			{

				ReportManager.ReportEvent(TestStatus.WARNING, "Select the Radio Button '" + sTemp[0] + "'",
						"Radio Button '" + sTemp[0] + "' has already been selected");

			}
		} catch (Exception e) {

			checkException(e, "Select the Radio Button '" + sTemp[0] + "'");
		}

		sTemp = null;
		Runtime.getRuntime().gc();
	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To perform List select RETURN VALUE: None INPUT(s):
	 * Driver and Xpath of object AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public static void listBoxSelect(String TestObject, String Value, String Identifiers) throws IOException {

		if (Value.startsWith("#")) {

			String[] nValue = Value.split("#");
			System.out.println("nValue[1]is - " + nValue[1]);
			Index = Integer.parseInt(nValue[1]);
			System.out.println("Index is - " + Index);

		}

		SeleniumIdentifiers Identifier = SeleniumIdentifiers.valueOf(Identifiers);
		String[] sTemp = splitTestObject(TestObject);
		Select select = new Select(checkElement(sTemp[1], sTemp[2]));

		System.out.println("");
		switch (Identifier) {
		case byIndex: {
			String sTemp1 = null;
			try {
				select.selectByIndex(Integer.parseInt(Value));
				sTemp1 = select.getOptions().get(Index).getText();
				// ReportManager.ReportEvent(TestStatus.PASS, "Select The Value - '" + sTemp1+
				// "' From The Dropdown '" + sTemp[0] + "'", "Selected The Value - '"+ sTemp1 +
				// "' From The Dropdown '" + sTemp[0] + "'"+ "' - Successfull".toUpperCase());
				ReportManager.ReportEvent(TestStatus.PASS, "Select The Value - '" + Value + "' From The Dropdown",
						"Selected The Value - '" + Value + "' From The Dropdown " + " - Successfull".toUpperCase());
			} catch (Exception e) {
				// checkException(e, "Select '" + sTemp1 + "' from dropdown '"+ sTemp[0] + "'");
				checkException(e, "Select '" + Value + "' from the dropdown");
			}
			break;
		}
		case byValue: {
			try {
				select.selectByValue(Value);
				ReportManager.ReportEvent(TestStatus.PASS, "Select The Value - '" + Value + "' From The Dropdown",
						"Selected The Value - '" + Value + "' From The Dropdown " + " - Successfull".toUpperCase());
				// ReportManager.ReportEvent(TestStatus.PASS, "Select '" + Value+ "' from the
				// dropdown '" + sTemp[0] + "'","Selected '" + Value + "' from the dropdown '"+
				// sTemp[0] + "'");
			} catch (Exception e) {
				// checkException(e, "Select '" + Value + "' from the dropdown '"+ sTemp[0] +
				// "'");
				checkException(e, "Select '" + Value + "' from the dropdown");
			}
			break;
		}
		case byVisibleText: {
			try {
				System.out.println("value:" + Value);
				select.selectByVisibleText(Value);
				ReportManager.ReportEvent(TestStatus.PASS, "Select The Value - '" + Value + "' From The Dropdown",
						"Selected The Value - '" + Value + "' From The Dropdown " + " - Successfull".toUpperCase());
				// ReportManager.ReportEvent(TestStatus.PASS, "Select '" + Value.toUpperCase()+
				// "' from dropdown List '" + sTemp[0] + "'", "Select '"+ Value.toUpperCase() +
				// "' from dropdown List '"+ sTemp[0] + "' - Successfull".toUpperCase());
			} catch (Exception e) {
				// checkException(e, "Select '" + Value + "' from dropdown '"+ sTemp[0] + "'");
				checkException(e, "Select '" + Value + "' from the dropdown");
			}
			break;
		}
		default:
			break;

		}
		// sTemp1 = null;
		sTemp = null;
		Value = null;
		Runtime.getRuntime().gc();
	}

	public void popUpWindow(String Action) {

		int x;
		if (Action.equalsIgnoreCase("accept")) {
			x = 1;
		} else {
			x = 0;
		}

		switch (x) {
		case 1: {

			try {

				String sTemp = driver.switchTo().alert().getText();
				driver.switchTo().alert().accept();
				ReportManager.ReportEvent(TestStatus.PASS, "Accept PopUp",
						"Accept PopUp with Message '" + sTemp + "' - Successfull".toUpperCase());

			} catch (Exception e) {
				ReportManager.ReportEvent(TestStatus.FAIL, "Accept PopUp", "No PopUp message present");
			}
			break;
		}
		case 0: {
			try {
				String sTemp = driver.switchTo().alert().getText();
				driver.switchTo().alert().dismiss();
				ReportManager.ReportEvent(TestStatus.PASS, "Dismiss PopUp",
						"Dismiss PopUp with Message '" + sTemp + "' - Successfull".toUpperCase());
			} catch (Exception e) {
				ReportManager.ReportEvent(TestStatus.FAIL, "Dismiss PopUp", "No PopUp message present");
			}
			break;
		}

		}

		Action = null;
		x = (Integer) null;
	}

	public void startTransaction() {
		startTransactionTime = ReportManager.currentTimeInMillis();
		ReportManager.ReportEvent(TestStatus.PASS, "Transaction Start Time", "Transaction Start Time Initiated");
		startTransactionStep = stepCount;
		System.out.println(startTransactionStep);
	}

	public void endTransaction() {
		endTransactionStep = stepCount;

		endTransactionTime = ReportManager.currentTimeInMillis();
		long transactionTime = endTransactionTime - startTransactionTime;

		if (transactionTime > 8000) {
			if ((endTransactionStep - startTransactionStep == 1)) {
				ReportManager.ReportEvent(TestStatus.WARNING, "Transaction End Time",
						"Transaction End Time is Initiated. Total transaction time for the step " + (stepCount) + " is "
								+ ReportManager.textExecutionTime(transactionTime));
			} else {
				ReportManager.ReportEvent(TestStatus.PASS, "Transaction End Time",
						"Transaction End Time is Initiated. Total transaction time from the Step "
								+ startTransactionStep + " till the Step " + endTransactionStep + " is "
								+ ReportManager.textExecutionTime(transactionTime));
			}
		} else {
			if ((endTransactionStep - startTransactionStep == 1)) {
				ReportManager.ReportEvent(TestStatus.PASS, "Transaction End Time",
						"Transaction End Time is Initiated. Total transaction time for the step " + (stepCount) + " is "
								+ ReportManager.textExecutionTime(transactionTime));
			} else {
				ReportManager.ReportEvent(TestStatus.PASS, "Transaction End Time",
						"Transaction End Time is Initiated. Total transaction time from the Step "
								+ startTransactionStep + " till the Step " + endTransactionStep + " is "
								+ ReportManager.textExecutionTime(transactionTime));
			}
		}

		startTransactionTime = 0;
		endTransactionTime = 0;
		transactionTime = 0;
		startTransactionStep = 0;
		endTransactionStep = 0;

	}

	public void mouseOver(String TestObject) throws IOException {

		String[] sTemp = splitTestObject(TestObject);

		try {
			DriverActions.moveToElement(checkElement(sTemp[1], sTemp[2])).build().perform();

			// ReportManager.ReportEvent(TestStatus.PASS,"Move the mouse to the dropdown
			// label '" + sTemp[0] + "'","Moved mouse to the dropdown label '" + sTemp[0] +
			// "'");
			ReportManager.ReportEvent(TestStatus.PASS, "Move The Mouse Over - '" + sTemp[0] + "'",
					"Moved The Mouse Over - '" + sTemp[0] + "'");
		} catch (Exception e) {

			checkException(e, "Move The Mouse Over - '" + sTemp[0] + "'");
		}
		sTemp = null;
	}

	public void sendKey(String TestObject, String Key) throws IOException {

		String[] sTemp = splitTestObject(TestObject);

		try {
			DriverActions.sendKeys("Keys." + Key);

			ReportManager.ReportEvent(TestStatus.PASS, "Performed The Keyboard Key Stroke Operation - ", Key);
		} catch (Exception e)

		{

			checkException(e, "Unable to Performed the Keyboard Key Stroke operation - ");
		}
		sTemp = null;
	}

	/*
	 * public void highlightElement(String TestObject) throws IOException { String[]
	 * sTemp = splitTestObject(TestObject);
	 * selenium.highlight(sTemp[1]+"="+sTemp[2]);
	 * selenium.highlight(sTemp[1]+"="+sTemp[2]); sTemp = null; }
	 */

	public void refreshPage() throws InterruptedException {
		driver.navigate().refresh();

	}
	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To get text out of an object RETURN VALUE: None INPUT(s):
	 * Driver and Xpath of object AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	public String getTextOnTheScreen(String testObject) throws IOException {
		boolean flag = false;
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		// flag= checkElementExist(sTemp[1], sTemp[2]);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		if (we.isDisplayed()) {
			ReportManager.ReportEvent(TestStatus.PASS, "Get Text From The ScreenObject - '" + sTemp[1] + "'",
					"Got The Text From The Screen - ' " + sTemp[1] + "' - Successfull".toUpperCase());
		} else {
			ReportManager.ReportEvent(TestStatus.WARNING, "Get Text From The ScreenObject - '" + sTemp[1] + "'",
					"Unable to get The Text From The Screen - ' " + sTemp[1] + "' - UnSuccessfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.WARNING, "","Exiting Script
			// Execution".toUpperCase());
			// Assert.fail("Exiting Script Execution");
		}

		return we.getText();

	}

	// sanjeev
	public String getDisplayedTextOnTheScreenE(String testObject) throws IOException {
		boolean flag = false;
		if (testObject.equalsIgnoreCase("btn")) {
			testObject = "Button";
		} else if (testObject.equalsIgnoreCase("lnk")) {
			testObject = "Link";
		} else if (testObject.equalsIgnoreCase("img")) {
			{
				testObject = "Image";
			}
		}

		String[] sTemp = splitTestObject(testObject);
		// flag= checkElementExist(sTemp[1], sTemp[2]);
		WebElement we = checkElementExplicitWait(sTemp[1], sTemp[2]);

		return we.getText();

	}

	public String getText(WebDriver wd, String wdxPath) {

		String sTemp = null;
		try {

			sTemp = wd.findElement(By.xpath(wdxPath)).getText().trim();
			// ReportManager.ReportEvent(TestStatus.PASS, "Get Text From Object : "+
			// wdxPath, "Get Text From Object : " + wdxPath+ " -
			// Successfull".toUpperCase());
			ReportManager.ReportEvent(TestStatus.PASS, "Get Text From The ScreenObject - '" + wdxPath + "'",
					"Got The Text From The Screen - ' " + wdxPath + "' - Successfull".toUpperCase());
		} catch (Exception e) {
			String vException = e.getMessage().toString();
			// System.out.println("vException - " + vException);
			if (vException.indexOf("(") != -1) {

				// vException = vException.substring(11,
				// vException.lastIndexOf("]")+1);
				vException = vException.substring(0, vException.indexOf("(") - 1);
				System.out.println("Final Message - " + vException);
				// ReportManager.ReportEvent(TestStatus.WARNING, "Get Text From Object : "+
				// wdxPath, vException.toUpperCase());
				ReportManager.ReportEvent(TestStatus.WARNING, "Get Text From The ScreenObject - '" + wdxPath + "'",
						"Unable to get The Text From The Screen - ' " + wdxPath + "' - UnSuccessfull".toUpperCase());
			}

		}
		return sTemp;
	}

//Sanjeev
	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To get attribue out of an object RETURN VALUE: None
	 * INPUT(s): Driver and Xpath of object and the Attribute Value AUTHOR:Sanjeev
	 * Ranjan Kumar
	 ***************************************************************************************/
	public String getAttribute(WebDriver wd, String wdxPath, String wdAttribute) {

		// return wd.findElement(By.xpath(wdxPath)).getAttribute(wdAttribute);

		String sTemp = null;
		try {

			sTemp = wd.findElement(By.xpath(wdxPath)).getAttribute(wdAttribute).trim();
			ReportManager.ReportEvent(TestStatus.PASS, "Get - " + wdAttribute + " From Object : " + wdxPath,
					"Get - " + wdAttribute + " From Object : " + wdxPath + " - Successfull".toUpperCase());

		} catch (Exception e) {
			String vException = e.getMessage().toString();
			// System.out.println("vException - " + vException);
			if (vException.indexOf("(") != -1) {

				// vException = vException.substring(11,
				// vException.lastIndexOf("]")+1);
				vException = vException.substring(0, vException.indexOf("(") - 1);
				System.out.println("Final Message - " + vException);
				ReportManager.ReportEvent(TestStatus.FAIL, "Get - " + wdAttribute + " From Object : " + wdxPath,
						vException.toUpperCase());
				ReportManager.ReportEvent(TestStatus.FAIL, "", "Exiting Script Execution".toUpperCase());
				// Assert.fail("Exiting Script Execution");

			}
		}
		return sTemp;

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To wait for a certain amount of time RETURN VALUE: None
	 * INPUT(s): Driver and Xpath of object and the Attribute Value AUTHOR: Sanjeev
	 * Ranjan Kumar
	 ***************************************************************************************/
	public static void waitTime(long wdWait) {

		try {
			Thread.sleep(wdWait);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

	}
	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To wait for a certain amount of time RETURN VALUE: None
	 * INPUT(s): AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	public static void waitTime() {

		try {
			System.out.println("wait time-start");
			Thread.sleep(GlobalVariables.waitTime);
			System.out.println("wait time -ended");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To wait for a certain amount of time RETURN VALUE: None
	 * INPUT(s): AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/

	public void waitTimeLong() {

		try {
			System.out.println("wait time-start");
			Thread.sleep(GlobalVariables.waitLongTime);
			System.out.println("wait time -ended");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void newwaitTime() {

		try {
			System.out.println("wait time-start");
			Thread.sleep(GlobalVariables.newwaitTime);
			System.out.println("wait time -ended");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To compare text from the application and the expected
	 * RETURN VALUE: None INPUT(s): object and the Attribute Value AUTHOR: Sanjeev
	 * Ranjan Kumar CREATION DATE: Feb 15 2017
	 ***************************************************************************************/
	public void compareText(String testObject, String wdText) throws IOException {

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		String str = we.getText();
		System.out.println(str + "****************************************");
		if (str.equalsIgnoreCase(wdText)) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - Successfull".toUpperCase());

		} else {
			ReportManager.ReportEvent(TestStatus.WARNING, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - UnSuccessfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.FAIL, "Verified " + testObject + " - '"
			// + sTemp[0].toUpperCase() + "'", "Verified " + testObject + " - '"+ sTemp[0] +
			// "' - Not Successfull".toUpperCase());

		}
	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To compare partial text from the application and the
	 * expected RETURN VALUE: None INPUT(s): object and the Attribute Value AUTHOR:
	 * Sanjeev Ranjan Kumar CREATION DATE: Jan-21'17
	 ***************************************************************************************/
	public void comparePartialText(String testObject, String wdText) throws IOException {

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		String str = we.getText();
		System.out.println(str + "****************************************");
		if (str.contains(wdText)) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - Successfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.PASS, "Verify" + testObject + " - '" +
			// sTemp[0].toUpperCase() + "'", "Verify " + testObject + " - '"+ sTemp[0] + "'
			// - Successfull".toUpperCase());

		} else {
			ReportManager.ReportEvent(TestStatus.WARNING, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - UnSuccessfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.FAIL, "Verified " + testObject + " - '"
			// + sTemp[0].toUpperCase() + "'", "Verified " + testObject + " - '"+ sTemp[0] +
			// "' - Not Successfull".toUpperCase());

		}
	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To compare text from the application by truncating the
	 * spaces in between with the expected value RETURN VALUE: None INPUT(s): object
	 * and the Attribute Value AUTHOR: Sanjeev Ranjan Kumar CREATION DATE: Feb-04'17
	 ***************************************************************************************/
	public void compareNICAndHBA(String testObject, String wdText) throws IOException {

		String[] sTemp = splitTestObject(testObject);
		WebElement we = checkElement(sTemp[1], sTemp[2]);

		String str = we.getText();
		System.out.println(str + "****************************************");
		str = str = str.replaceAll("\\s", "").trim();
		if (str.equals(wdText)) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - Successfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.PASS, "Verify" + testObject + " - '" +
			// sTemp[0].toUpperCase() + "'", "Verify " + testObject + " - '"+ sTemp[0] + "'
			// - Successfull".toUpperCase());

		} else {

			ReportManager.ReportEvent(TestStatus.WARNING, "Verify - '" + wdText.toUpperCase() + "'",
					"Verified - '" + str + "' - UnSuccessfull".toUpperCase());
			// ReportManager.ReportEvent(TestStatus.FAIL, "Verified " + testObject + " - '"
			// + sTemp[0].toUpperCase() + "'", "Verified " + testObject + " - '"+ sTemp[0] +
			// "' - Not Successfull".toUpperCase());

		}
	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To compare attribute from the application and the
	 * expected RETURN VALUE: None INPUT(s): Driver and Xpath of object and the
	 * Attribute Value AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public String compareAttribute(WebDriver wd, String wdxPath, String wdAttribute, String wdText) {

		String wdTextOut = wd.findElement(By.xpath(wdxPath)).getAttribute(wdAttribute);
		if (wdTextOut.equals(wdText)) {
			return "Pass";
		} else {
			return "Fail";
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Switch to second window and close RETURN VALUE: None
	 * INPUT(s): driver and window title AUTHOR:Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void SwitchWindowAndClose(WebDriver wd, String windowTitle)

	{
		// save the current window handle.
		String parentWindowHandle = wd.getWindowHandle();

		WebDriver popup = null;

		@SuppressWarnings({ "rawtypes", "unused" })
		Set set = wd.getWindowHandles();
		@SuppressWarnings("rawtypes")
		Set windowIterator = wd.getWindowHandles();
		@SuppressWarnings("rawtypes")
		Iterator it = windowIterator.iterator();

		while (it.hasNext()) {

			String windowHandle = (String) it.next();
			popup = wd.switchTo().window(windowHandle);

			// System.out.println("TITLE OF PARENT WINDOW BROWSER IS" +
			// parentWindowHandle);
			// System.out.println("TITLE OF CHILD WINDOW BROWSER IS" +
			// windowHandle);
			// System.out.println("TITLE OF THIS BROWSER IS" +
			// popup.getTitle());

			if (popup.getTitle().equals(windowTitle)) {

				// "ANNOUNCEMENT"
				wd.switchTo().window(windowHandle);
				wd.close();
				wd.switchTo().window(parentWindowHandle);
				break;

			}

		}
	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Switch to Popup and close it RETURN VALUE: None INPUT(s):
	 * driver AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void PopupHandle(WebDriver wd)

	{
		String parentWindowHandle = wd.getWindowHandle();

		Alert alert = wd.switchTo().alert();
		// System.out.println(alert.getText());
		alert.accept();
		wd.switchTo().window(parentWindowHandle);

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Qcentral and OEC Application - Wait for the progress to
	 * disappear RETURN VALUE: None INPUT(s): Driver AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void WaitForProgressBar(WebDriver wd, String sObject, int iTimeout) throws Exception {
		for (int second = 0;; second++) {
			if (second >= iTimeout)
				fail("Timeout - Progress Bar Still Exists After " + iTimeout + " seconds");
			try {
				if (!wd.findElement(By.id(sObject)).isDisplayed())
					break;
			} catch (Exception e) {
			}

		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE:wait for object visibility RETURN VALUE: None INPUT(s):
	 * driver and window title AUTHOR: Sanjeev Ranjan Kumar
	 ***************************************************************************************/
	public void WaitForObject(WebDriver wd, String oBjString, String xPath, int vTimeout) throws Exception {

		for (int second = 0; second <= vTimeout; second++) {

			String sTemp = null;

			if (wd.findElement(By.xpath(xPath)).getText() != null) {
				System.out.println("Inside getText()");
				sTemp = wd.findElement(By.xpath(xPath)).getText().trim();
				System.out.println("Text Is - " + sTemp);
				oBjString.equals(sTemp);
				// System.out.println("Done waiting");
				break;
			} else if (wd.findElement(By.xpath(xPath)).getAttribute("id") != null) {
				System.out.println("Inside getAttribute(\"id\")");
				sTemp = wd.findElement(By.xpath(xPath)).getAttribute("id").trim();
				System.out.println("ID Is - " + sTemp);
				oBjString.equals(sTemp);
				break;
			} else if (wd.findElement(By.xpath(xPath)).getAttribute("name") != null) {
				System.out.println("Inside getAttribute(\"name\")");
				sTemp = wd.findElement(By.xpath(xPath)).getAttribute("name").trim();
				System.out.println("Name Is - " + sTemp);
				oBjString.equals(sTemp);
				break;
			} else if (wd.findElement(By.xpath(xPath)).getAttribute("value") != null) {
				System.out.println("Inside getAttribute(\"value\")");
				sTemp = wd.findElement(By.xpath(xPath)).getAttribute("value").trim();
				System.out.println("Value Is - " + sTemp);
				oBjString.equals(sTemp);
				break;
			} else {
				fail("Object did not appear after - " + vTimeout + "Seconds");

			}
			Thread.sleep(1000);
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: Wait for the object to appear RETURN VALUE: None
	 * INPUT(s): Driver, Object Text and Xpath of object AUTHOR: Sanjeev Ranjan
	 * Kumar
	 ***************************************************************************************/
	public void WaitForObject_Attribute(WebDriver wd, String oBjValue, String xPath, String vAttValue)
			throws Exception {
		for (int second = 0;; second++) {
			if (second >= 180)
				fail("timeout - Object did not appear after 3 Minutes");

			try {
				String sTemp = null;
				if (oBjValue.equals(wd.findElement(By.xpath(xPath)).getAttribute(vAttValue)))
					sTemp = wd.findElement(By.xpath(xPath)).getAttribute(vAttValue);
				System.out.println(vAttValue + " - value is - " + sTemp);
				;
				break;
			} catch (Exception e) {
			}
			Thread.sleep(1000);
		}
	}

	/*
	 * *****************************************************************************
	 * *********** PURPOSE: To Take Screen Print(Screenshot) RETURN VALUE: None
	 * INPUT(s): Driver and Path AUTHOR: Sanjeev Ranjan Kumar
	 ****************************************************************************************
	 */
	public static void takeScreenShot(WebDriver wd, String vPath) throws IOException {

		try {
			File TakeScreenPrint = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(TakeScreenPrint, new File(vPath));
			// Thread.sleep(2000);
		} catch (Exception e) {
			String vException = e.getMessage().toString();
			System.out.println("vException Message ------------------------------------------ " + vException);
			ReportManager.ReportEvent(TestStatus.FAIL, "Take Screen Print : ", vException.toUpperCase());
		}

	}

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To Establish a Remote Desktop connection RETURN VALUE:
	 * None INPUT(s): RDP machine IP address,Username and Password AUTHOR: Sanjeev
	 * Ranjan Kumar (sanjeev.r.kumar@in.unisys.com)
	 ***************************************************************************************/

	/*
	 * *****************************************************************************
	 * ********** PURPOSE: To Establish a Remote Desktop connection RETURN VALUE:
	 * None INPUT(s): To set the DesiredCapablities for Grid AUTHOR: Sanjeev Ranjan
	 * Kumar (sanjeev.r.kumar@in.unisys.com) *
	 ***************************************************************************************/
	public static void setCapabilites(String Browser, String ANY, String NodeIPAddress) throws IOException {

		DesiredCapabilities cap = new DesiredCapabilities();
		// DesiredCapabilities cap = DesiredCapabilities.firefox();
		cap.setBrowserName("Browser");
		// cap.setVersion("17.0.10");
		cap.setPlatform(Platform.ANY);

		try {
			driver = new RemoteWebDriver(new URL(NodeIPAddress + "/wd/hub"), cap);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);

			// Report to Notepad, HTMl and Excel
			ReportManager.ReportEvent(TestStatus.PASS, "Establish RemoteWebDriver connection ",
					"Established RemoteWebDriver connection - Successfull".toUpperCase());

		} catch (Exception e) {

			checkException(e, "Established connection with - Not Successfull".toUpperCase());

		}

	}

//	protected static void ReportEvent(TestStatus Status, String Expected, String Actual){
//		
//		switch (Status)
//		{
//		case PASS:{
//			try {
//				writeToNotepadHTML("PASS", Expected, Actual);
//				WriteToExcel("PASS", Expected, Actual);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}	
//			break;
//		}
//		case FAIL:{
//			try {
//				writeToNotepadHTML("FAIL", Expected, Actual.toUpperCase());
//				WriteToExcel("FAIL", Expected, Actual.toUpperCase());
//				failurepopUp(Expected,Actual);
//				writeToNotepadHTML("FAIL", "", "Exiting Script Execution".toUpperCase());
//				WriteToExcel("FAIL", "", "Exiting Script Execution".toUpperCase());
//				Assert.fail();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			break;
//		}
//		case WARNING:{
//			try {
//				writeToNotepadHTML("WARNING", Expected, Actual);
//				WriteToExcel("WARNING", Expected, Actual);
//				} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			break;
//		}
//		
//		}
//	}

	static void checkException(Exception e, String Expected) {

		if (driver.getPageSource().contains("HTTP Status")) {
			//String[] sTemp =  driver.getBodyText().split("type");
			//System.out.println(sTemp[0]);
			try {
				takeScreenShotonFailure("FAIL");
			} catch (Exception e1) {

				e1.printStackTrace();
			}
			//ReportManager.ReportEvent(TestStatus.WARNING, Expected, sTemp[0].trim());
			//sTemp = null;
		}

		else

		{
			String sTemp1 = e.getMessage();
			String[] sTemp2 = sTemp1.split("WARN");
			sTemp2[0] = new String(sTemp2[0].substring(0, (sTemp2[0].length()) - 1));

			try {
				takeScreenShotonFailure("FAIL");
				ReportManager.ReportEvent(TestStatus.WARNING, Expected, sTemp2[0]);
			} catch (Exception e1) {

				e1.printStackTrace();
			}

			sTemp1 = null;
		}
	}

	static void takeScreenShotonFailure(String status) throws Exception {

		String sTemp21 = ReportManager.outputNotePadResultFile;
		System.out.println("outputNotePadFile is " + ReportManager.outputNotePadResultFile);
		sTemp21 = sTemp21.substring(50, 75);
		// System.out.println("sTemp21 is " + sTemp21);

		File folder1 = new File(System.getProperty("user.dir") + "\\Screen_Prints\\");
		if (!folder1.exists()) {
			folder1.mkdir();
		}

		String failurescreen = System.getProperty("user.dir") + "\\Screen_Prints\\" + sTemp21 + "_" + screenShotTime()
				+ "_" + status + ".PNG";
		File TakeScreenPrint = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(TakeScreenPrint, new File(failurescreen));
		Runtime.getRuntime().gc();
	}

	protected static String screenShotTime() {

		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat dateformat = new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss");
		return (dateformat.format(date));

	}
	///// ***************************************************************************************NEW
	///// FUNCTIONS*****************************************

	/*
	 * public void tearDownMethod(ITestResult testResult) throws Exception { long
	 * vScriptStartTime = Calendar.getInstance().getTimeInMillis();
	 * RM.updateReports(updateValue.tEndTime, "", "");
	 * RM.updateReports(updateValue.execTime,formatIntoHHMMSS(Calendar.getInstance()
	 * .getTimeInMillis() - vScriptStartTime).toString(), "");
	 * RM.updateReports(updateValue.execStatus, "",
	 * RM.getFinalStatus(testResult.getStatus()));
	 * RM.updatExcelTestResult(testResult.getStatus());
	 * this.driver.manage().deleteAllCookies(); this.driver.close();
	 * this.driver.quit(); }
	 */

	public static void clickButton(String Locators) {
		WebElement Element = null;
		FluentWait<WebDriver> wait = null;
		wait = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS).pollingEvery(30, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class, StaleElementReferenceException.class);

		try {
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.name(Locators))));
			Element = driver.findElement(By.name(Locators));
			Element.click();
		} catch (Exception e) {
			// S(e, "Check Element Displayed / Enabled");
		}

	}

	// added By Ravi Kumar
	// Date- 6 Dec 2018
	// TCD- CPE-1123
	// Common method for Deleting all Alerts at once
	public static void ClearAtOnce() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(ALERTVIEW)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AlretLink + "'",
				"Clicked On - '" + AlretLink + "' - Successfull");
		// Thread.Sleep(1000);
		driver.findElement(By.xpath(CLRALL)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLEARLINK + "'",
				"Clicked On - '" + CLEARLINK + "' - Successfull");
		String popup_message = driver.findElement(By.xpath(POPUPWIN)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + popup_message + "'",
				"Verified message - '" + popup_message + "' - Successfull");
		driver.findElement(By.xpath(YESBUN)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + YESLINK + "'",
				"Verified message - '" + YESLINK + "' - Successfull");
		String statusString = driver.findElement(By.xpath(ClearStatus)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + statusString + "'",
				"Verified message - '" + statusString + "' - Successfull");
	}

	// added By Ravi Kumar
	// Date- 7 Dec 2018
	// TCD- CPE-1128
	// Common Method for filtering the logs by severity and application category
	public static void filterLogsBySeverity() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(VIEWEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + VEIWEVENTLINK + "'",
				"Clicked On - '" + VEIWEVENTLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGFILT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
				"Clicked On - '" + FILTLINK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown.selectByValue(Application);
		Select dropdown1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		dropdown1.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Application + ", " + Error + "'",
				"Filter event logs by - '" + Application + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn2 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn2.selectByValue(WARNING);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Application + ", " + WARNING + "'",
				"Filter event logs by - '" + Application + ", " + WARNING + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString3 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString3.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn3 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn3.selectByValue(SuccessAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Application + ", " + SuccessAudit + "'",
				"Filter event logs by - '" + Application + ", " + SuccessAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString1 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString1.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn4 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn4.selectByValue(FailureAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Application + ", " + FailureAudit + "'",
				"Filter event logs by - '" + Application + ", " + FailureAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString4 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString4.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		// Infromation
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn5 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn5.selectByValue(Infromation);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Application + ", " + Infromation + "'",
				"Filter event logs by - '" + Application + ", " + Infromation + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString5 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString5.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		}
	}

// added By Ravi Kumar
	// Date- 9 Dec 2018
	// TCD- CPE-1129
	// Common Method for filtering the security logs by severity and Security
	// category
	public static void SecurityLogFilter() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(VIEWEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + VEIWEVENTLINK + "'",
				"Clicked On - '" + VEIWEVENTLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGFILT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
				"Clicked On - '" + FILTLINK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown.selectByValue(Security);
		Select dropdown1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		dropdown1.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Security + ", " + Error + "'",
				"Filter event logs by - '" + Security + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn.selectByValue(SuccessAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Security + ", " + SuccessAudit + "'",
				"Filter event logs by - '" + Security + ", " + SuccessAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString1 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString1.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		// ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTBUNLINK+"'",
		// "Clicked On - '"+ FILTBUNLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn1.selectByValue(FailureAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Security + ", " + FailureAudit + "'",
				"Filter event logs by - '" + Security + ", " + FailureAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString2 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString2.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn2 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn2.selectByValue(WARNING);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Security + ", " + WARNING + "'",
				"Filter event logs by - '" + Security + ", " + WARNING + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString3 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString3.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		}

	}

//To check if link is disabled
	public static boolean isClickable(WebElement addwbprt) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(addwbprt));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

//added By Ravi Kumar
// Date- 13 Dec 2018
// TCD- CPE-1136
// Common Method to view various parameter under cluster under the cluster level
	@SuppressWarnings("unlikely-arg-type")
	public static void AddPartionAndDeploy() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		boolean res = isClickable(driver.findElement(By.xpath(ADDWBPRT)));
		if (res) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + WebDisabled + "'",
					"Verified - '" + WebDisabled + "' - Successfull");
		} else {
			driver.findElement(By.xpath(ADDWBPRT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + VEIWEVENTLINK + "'",
					"Clicked On - '" + VEIWEVENTLINK + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(NXTBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
					"Clicked On - '" + FILTLINK + "' - Successfull");
			Select dropdown = new Select(driver.findElement(By.xpath(SELECTPM)));
			dropdown.selectByValue(PMVALUE);
			driver.findElement(By.xpath(NXTBUN)).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(FINISHBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Create Web Partion - '" + PMVALUE + "'",
					"Created Web Partion - '" + PMVALUE + "' - Successfull");
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(WebpartionName)).click();
		driver.findElement(By.xpath(PerformaceST)).click();
		String performvalid = driver.findElement(By.xpath(PerfrmSTLink)).getText();
		if (performvalid.contains("this Web")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + performvalid + "'",
					"Verified - '" + performvalid + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify - '" + performvalid + "'",
					"Verified - '" + performvalid + "' - Successfull");
		}
	}

	// added By Ravi Kumar
// Date- 18 Dec 2018
// TCD- CPE-1130
// Common Method to VIEW ALL THE SYSTEM LOGS
	public static void systemLogsFilter() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(VIEWEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + VEIWEVENTLINK + "'",
				"Clicked On - '" + VEIWEVENTLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGFILT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
				"Clicked On - '" + FILTLINK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown.selectByValue(system);
		Select dropdown1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		dropdown1.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + system + ", " + Error + "'",
				"Filter event logs by - '" + system + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTBUNLINK + "'",
				"Clicked On - '" + FILTBUNLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String nonevalue = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (nonevalue.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn.selectByValue(WARNING);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + system + ", " + WARNING + "'",
				"Filter event logs by - '" + system + ", " + WARNING + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn1.selectByValue(SuccessAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + system + ", " + SuccessAudit + "'",
				"Filter event logs by - '" + system + ", " + SuccessAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString1 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString1.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn2 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn2.selectByValue(FailureAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + system + ", " + FailureAudit + "'",
				"Filter event logs by - '" + system + ", " + FailureAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString2 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString2.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn3 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn3.selectByValue(Infromation);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + system + ", " + Infromation + "'",
				"Filter event logs by - '" + system + ", " + Infromation + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString3 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString3.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		}

	}

//added By Ravi Kumar
// Date- 18 Dec 2018
// TCD- CPE-1126
// Common Method to VIEW ALL THE Management LOGS

	public static void Managementlogs() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(VIEWEVENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + VEIWEVENTLINK + "'",
				"Clicked On - '" + VEIWEVENTLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGFILT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
				"Clicked On - '" + FILTLINK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown.selectByValue(Management);
		Select dropdown1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		dropdown1.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Management + ", " + Error + "'",
				"Filter event logs by - '" + Management + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTBUNLINK + "'",
				"Clicked On - '" + FILTBUNLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String nonevalue = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (nonevalue.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn.selectByValue(WARNING);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Management + ", " + WARNING + "'",
				"Filter event logs by - '" + Management + ", " + WARNING + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn1.selectByValue(SuccessAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Management + ", " + SuccessAudit + "'",
				"Filter event logs by - '" + Management + ", " + SuccessAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString1 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString1.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn2 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn2.selectByValue(FailureAudit);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Management + ", " + FailureAudit + "'",
				"Filter event logs by - '" + Management + ", " + FailureAudit + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString2 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString2.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select securedropdn3 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn3.selectByValue(Infromation);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter event logs by - '" + Management + ", " + Infromation + "'",
				"Filter event logs by - '" + Management + ", " + Infromation + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString3 = driver.findElement(By.xpath(NONEEVENT)).getText();
		if (noEventString3.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST + "'",
					"Verified - '" + EMPTYLIST + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST + "'",
					"Verified - '" + FULLLIST + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			// driver.findElement(By.xpath(BACK)).click();
		}
	}

	// added By Ravi Kumar
// Date- 27 Dec 2018
// TCD- CPE-1139
// Common Method to Restart Manager and check everything works fine
	@SuppressWarnings("deprecation")
	public static void RestartManager() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(RESTART)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + RestartLink + "'",
				"Clicked On - '" + RestartLink + "' - Successfull");
		Thread.sleep(100);
		String restartpopup = driver.findElement(By.xpath(RestartPopup)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify popup message - '" + restartpopup + "'",
				"Verified popup message - '" + restartpopup + "' - Successfull");
		driver.findElement(By.xpath(RestartYES)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + YESBUTTONCLICK + "'",
				"Clicked on  - '" + YESBUTTONCLICK + "' - Successfull");
		Thread.sleep(150000);
		// driver.manage().timeouts().pageLoadTimeout(150, TimeUnit.SECONDS);
		// driver.navigate().refresh();
		driver.get(driver.getCurrentUrl());
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		// ReportManager.ReportEvent(TestStatus.PASS, "Verify popup message - '" +
		// restartpopup+"'", "Verified popup message - '"+ restartpopup + "' -
		// Successfull");
		// WebDriverWait wait = new WebDriverWait(driver,100);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TASKLINK)));
		String pageload = driver.findElement(By.xpath(TASKLINK)).getText();
		if (pageload.equals("Go to Common Tasks page")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + Restartsuccess + "'",
					"Verified  - '" + Restartsuccess + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify - '" + Restartsuccess + "'",
					"Verified  - '" + Restartsuccess + "' - Successfull");
		}
	}

	// added By Ravi Kumar
// Date- 3rd Jan 2019
// TCD- CPE-1154
// Common Method to Change the theme to default theme

	public static void DefaultTheme() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(PREFERENCE)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PreferenceLink + "'",
				"Clicked On - '" + PreferenceLink + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select themedropdown = new Select(driver.findElement(By.xpath(THEME)));
		themedropdown.selectByValue(Classic);
		Thread.sleep(500);
		Select themedropdown1 = new Select(driver.findElement(By.xpath(THEME)));
		themedropdown1.selectByValue(Default);
		driver.findElement(By.xpath(ApplyBun)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ApplyLink + "'",
				"Clicked On - '" + ApplyLink + "' - Successfull");
		WebElement webdata = driver.findElement(By.xpath(taskStartup));
		String setPagetext = webdata.getAttribute("value");
		if (setPagetext.contains("Tasks")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Current Page set as - '" + setPagetext + "'",
					"Verified Current Page set as - '" + setPagetext + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify Current Page set as - '" + setPagetext + "'",
					"Verified Current Page set as - '" + setPagetext + "' - Successfull");
		}

		// Thread.sleep(500);
		driver.findElement(By.xpath(ResethomePage)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + defaultHomePage + "'",
				"Clicked On - '" + defaultHomePage + "' - Successfull");
		WebElement webdata1 = driver.findElement(By.xpath(taskStartup));
		String currentPagetext = webdata1.getAttribute("value");
		if (currentPagetext.contains("Page")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Current Page set as - '" + currentPagetext + "'",
					"Verified Current Page set as - '" + currentPagetext + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify Current Page set as - '" + currentPagetext + "'",
					"Verified Current Page set as - '" + currentPagetext + "' - Successfull");
		}

	}

	// added By Ravi Kumar
//Date- 7th Jan 2019
//TCD- CPE-1161
//Common Method to to check right frame links
	public static void verifyRightLink() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		// backend
		driver.findElement(By.xpath(BACKENDSERVER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + BACKENDSERVERLINK + "'",
				"Clicked on -'" + BACKENDSERVERLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		String backendserver = driver.findElement(By.xpath(BACKENDPAGE)).getText();
		if (backendserver.contains("New")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + backendserver + "'",
					"Verified text -'" + backendserver + "'- Successfull");
		} else

		{
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + backendserver + "'",
					"Verified text -'" + backendserver + "'- Successfull");
		}
		driver.navigate().back();
		Thread.sleep(1000);
		// web cluster
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(NEWCLUSTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + NEWCLUSTERLINK + "'",
				"Clicked on -'" + NEWCLUSTERLINK + "'- Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		String webclust = driver.findElement(By.xpath(CLUSTERPAGE)).getText();
		if (webclust.contains("New Web Cluster")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + webclust + "'",
					"Verified text -'" + webclust + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + webclust + "'",
					"Verified text -'" + webclust + "'- Successfull");
		}
		driver.navigate().back();
		// deploy application
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(DEPLOYCLUSTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + DEPLOYSERVERLINK + "'",
				"Clicked on -'" + DEPLOYSERVERLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		// Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String deployapp = driver.findElement(By.xpath(STAGEDAPP)).getText();
		if (deployapp.contains("Deployed")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + deployapp + "'",
					"Verified text -'" + deployapp + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + deployapp + "'",
					"Verified text -'" + deployapp + "'- Successfull");
		}
		driver.navigate().back();
		// assign web partion
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(WEBPARTCLSUTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + WEBPARTCLUSTERLINK + "'",
				"Clicked on -'" + WEBPARTCLUSTERLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		String assignweb = driver.findElement(By.xpath(ASSIGNCLUST)).getText();
		if (assignweb.contains("Assign Web")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + assignweb + "'",
					"Verified text -'" + assignweb + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + assignweb + "'",
					"Verified text -'" + assignweb + "'- Successfull");
		}
		driver.navigate().back();
		// bring web partion online
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(BRINGCLUSTONLINE)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + CLUSTERONLINELINK + "'",
				"Clicked on -'" + CLUSTERONLINELINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String bringonline = driver.findElement(By.xpath(MONITORCLUST)).getText();
		if (bringonline.contains("Web")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + bringonline + "'",
					"Verified text -'" + bringonline + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + bringonline + "'",
					"Verified text -'" + bringonline + "'- Successfull");
		}
		driver.navigate().back();
		// ssl for manager partion
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SSLONMNGR)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + SSLMNGLINK + "'",
				"Clicked on -'" + SSLMNGLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String sslmanager = driver.findElement(By.xpath(MANAGECERT)).getText();
		if (sslmanager.contains("certificate")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + sslmanager + "'",
					"Verified text -'" + sslmanager + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + sslmanager + "'",
					"Verified text -'" + sslmanager + "'- Successfull");
		}
		driver.navigate().back();
		// ssl for web cluster
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SSLONWEBCLUST)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + SSLWEBLINK + "'",
				"Clicked on -'" + SSLWEBLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String sslcluster = driver.findElement(By.xpath(MONITORCLUST)).getText();
		if (sslcluster.contains("Web Cluster")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + sslcluster + "'",
					"Verified text -'" + sslcluster + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + sslcluster + "'",
					"Verified text -'" + sslcluster + "'- Successfull");
		}
		driver.navigate().back();
		// setup events and alerts
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(EMAILNOTIFY)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + EMAILLINK + "'",
				"Clicked on -'" + EMAILLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String emailnotify = driver.findElement(By.xpath(CONFIGNOTIFIC)).getText();
		if (emailnotify.contains("notification")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + emailnotify + "'",
					"Verified text -'" + emailnotify + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + emailnotify + "'",
					"Verified text -'" + emailnotify + "'- Successfull");
		}
		driver.navigate().back();
		// manage licenses
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MANAGELICENSE)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + MNGLICENSELINK + "'",
				"Clicked on -'" + MNGLICENSELINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String license = driver.findElement(By.xpath(ManageLicense)).getText();
		if (license.contains("License")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + license + "'",
					"Verified text -'" + license + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + license + "'",
					"Verified text -'" + license + "'- Successfull");
		}
		driver.navigate().back();
		// common tasks
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(GOCOMMONTASKS)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + COMMONTASKLINK + "'",
				"Clicked on -'" + COMMONTASKLINK + "'- Successfull");
		Thread.sleep(2000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		// driver.switchTo().frame(0);
		String commontask = driver.findElement(By.xpath(STARTUPTASK)).getText();
		if (commontask.contains("Startup")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + commontask + "'",
					"Verified text -'" + commontask + "'- Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify text- '" + commontask + "'",
					"Verified text -'" + commontask + "'- Successfull");
		}
		driver.navigate().back();
	}

	// added By Ravi Kumar
	// Date- 9th Jan 2019
	// TCD- CPE-1124
	// Description- view The detailed alert message at once in the manager.
	public static void DisplayMessage() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(ALERTVIEW)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AlretLink + "'",
				"Clicked On - '" + AlretLink + "' - Successfull");
		Thread.sleep(1000);
		driver.findElement(By.xpath(ERRORXPATH)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String description = driver.findElement(By.xpath(DESCRIPTION)).getText();
		if (description.contains("Workstation")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Description - '" + description + "'",
					"Verified Description - '" + description + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify Description - '" + description + "'",
					"Verified Description - '" + description + "' - Successfull");
		}
	}

	// added By Ravi Kumar
	// Date- 9th Jan 2019
	// TCD- CPE-1121
	// Description- Sort the alert messages by column titles in manager.
	public static void SortByColumn() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(ALERTVIEW)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AlretLink + "'",
				"Clicked On - '" + AlretLink + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGFILT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTLINK + "'",
				"Clicked On - '" + FILTLINK + "' - Successfull");
		Select dropdown = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown.selectByValue(system);
		Select dropdown1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		dropdown1.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter alerts by - '" + system + ", " + Error + "'",
				"Filter event logs by - '" + system + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + FILTBUNLINK + "'",
				"Clicked On - '" + FILTBUNLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String nonevalue = driver.findElement(By.xpath(NONEEVENT1)).getText();
		if (nonevalue.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST1 + "'",
					"Verified - '" + EMPTYLIST1 + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST1 + "'",
					"Verified - '" + FULLLIST1 + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select dropdown11 = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown11.selectByValue(Hardware);
		Select securedropdn = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn.selectByValue(WARNING);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter alerts by - '" + Hardware + ", " + WARNING + "'",
				"Filter event logs by - '" + Hardware + ", " + WARNING + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString = driver.findElement(By.xpath(NONEEVENT1)).getText();
		if (noEventString.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST1 + "'",
					"Verified - '" + EMPTYLIST1 + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST1 + "'",
					"Verified - '" + FULLLIST1 + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select dropdown12 = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown12.selectByValue(Application);
		Select securedropdn1 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn1.selectByValue(Infromation);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter alerts by - '" + Application + ", " + Infromation + "'",
				"Filter event logs by - '" + Application + ", " + Infromation + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString1 = driver.findElement(By.xpath(NONEEVENT1)).getText();
		if (noEventString1.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST1 + "'",
					"Verified - '" + EMPTYLIST1 + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST1 + "'",
					"Verified - '" + FULLLIST1 + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select dropdown13 = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown13.selectByValue(Security);
		Select securedropdn2 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn2.selectByValue(Error);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter alerts by - '" + Security + ", " + Error + "'",
				"Filter event logs by - '" + Security + ", " + Error + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString2 = driver.findElement(By.xpath(NONEEVENT1)).getText();
		if (noEventString2.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST1 + "'",
					"Verified - '" + EMPTYLIST1 + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST1 + "'",
					"Verified - '" + FULLLIST1 + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");
			driver.findElement(By.xpath(BACK)).click();
		}
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Select dropdown14 = new Select(driver.findElement(By.xpath(CATDRPDWN)));
		dropdown14.selectByValue(Hardware);
		Select securedropdn3 = new Select(driver.findElement(By.xpath(SEVDRPDWN)));
		securedropdn3.selectByValue(Infromation);
		ReportManager.ReportEvent(TestStatus.PASS, "Filter alerts by - '" + Hardware + ", " + Infromation + "'",
				"Filter event logs by - '" + Hardware + ", " + Infromation + "' - Successfull");
		driver.findElement(By.xpath(FILTERBUN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String noEventString3 = driver.findElement(By.xpath(NONEEVENT1)).getText();
		if (noEventString3.contains("None")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + EMPTYLIST1 + "'",
					"Verified - '" + EMPTYLIST1 + "' - Successfull");
		} else {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + FULLLIST1 + "'",
					"Verified - '" + FULLLIST1 + "' - Successfull");
			driver.findElement(By.xpath(DOWNLOADBUN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DOWNLOADLINK + "'",
					"Clicked On - '" + DOWNLOADLINK + "' - Successfull");

		}
		ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + TestSteps + "'",
				"Verified - '" + TestSteps + "' - Successfull");
	}

	// added By Ravi Kumar
	// Date- 10th Jan 2019
	// TCD- CPE-1120
	// Description- View the alert messages on the manager.
	public static void AlertsInManagerPartion() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(ALERTVIEW)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AlretLink + "'",
				"Clicked On - '" + AlretLink + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String alertscheck = driver.findElement(By.xpath(NONEEVENT1)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify showing alerts count as- '" + alertscheck + "'",
				"Verified showing alert count as - '" + alertscheck + "' - Successfull");
		ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + TestSteps + "'",
				"Verified - '" + TestSteps + "' - Successful");

	}

//added By Ravi Kumar
// Date- 10th Jan 2019
// TCD- CPE-1120
// Description- Apply a new software and Update Packages (Windows Update, ASP.NET Update) update on the Manager.
	public static void synchronizeSystemSoftware() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
				"Clicked On - '" + MNGLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGSYSSOFT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGSYSTEMSOFTLINK + "'",
				"Clicked On - '" + MNGSYSTEMSOFTLINK + "' - Successful");
		Thread.sleep(1000);
		if (driver.findElement(By.xpath(SYNMSG)).isEnabled()) {
			String synmsg = driver.findElement(By.xpath(SYNMSG)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + synmsg + "'",
					"Verified message - '" + synmsg + "' - Successful");
		} else {
			driver.findElement(By.xpath(SYSEVENTLNK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + SYNLINKTEXT + "'",
					"Click on - '" + SYNLINKTEXT + "' - Successful");
		}
		ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + TestSteps + "'",
				"Verified - '" + TestSteps + "' - Successful");
	}

//added By Ravi Kumar
//Date- 10th Jan 2019
//TCD- CPE-1163
//Description- Validates it signs Off.
	public static void SignOff() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SIGNOFF)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + LOGOFF + "'",
				"Clicked on - '" + LOGOFF + "' - Successful");
		Thread.sleep(2000);
		driver.switchTo().alert().accept();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + Alertpopup + "'",
				"Clicked on - '" + Alertpopup + "' - Successful");
	}

//added By Ravi Kumar
//Date- 10th Jan 2019
//TCD- CPE-1163
//Description- Verify that all the wizards and buttons such as ?Back?, ?Forward?, ?Cancel?,
//?OK? are working properly and linking to the correct page..
	public static void WizardsAndButtons() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WEBCLUSTER)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CLUSTERCREATELINK)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		driver.findElement(By.xpath(CLUSTERNXTBTN)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + NEXTLINK + "'",
				"Clicked on - '" + NEXTLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		String nextvalid = driver.findElement(By.xpath(CLUSTERPAGE)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify Text - '" + nextvalid + "'",
				"Verified text - '" + nextvalid + "' - Successful");
		ReportManager.ReportEvent(TestStatus.PASS, "Verify button - '" + NEXTLINK + "'",
				"Verified button - '" + NEXTLINK + "' - Linked properly");
		driver.findElement(By.xpath(ClusterBackBun)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + BACKBUNLINK + "'",
				"Clicked on - '" + BACKBUNLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(5000);
		driver.switchTo().frame(0);
		String backvalid = driver.findElement(By.xpath(CLUSTERPAGE)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify Text - '" + backvalid + "'",
				"Verified text - '" + backvalid + "' - Successful");
		ReportManager.ReportEvent(TestStatus.PASS, "Verify button - '" + BACKBUNLINK + "'",
				"Verified button - '" + BACKBUNLINK + "' - Linked properly");
		driver.findElement(By.xpath(CANCELBUTTON)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + CANCELLINK + "'",
				"Clicked on - '" + CANCELLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String cancelvalid = driver.findElement(By.xpath(CLUSTERCREATELINK)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify link - '" + cancelvalid + "'",
				"Verified link - '" + cancelvalid + "' - Successful");
		ReportManager.ReportEvent(TestStatus.PASS, "Verify button - '" + CANCELLINK + "'",
				"Verified button - '" + CANCELLINK + "' - Linked properly");

	}

//added By Ravi Kumar
//Date- 10th Jan 2019
//TCD- CPE-1151
//Description- Modify the descripition of backend server.

	public static void ModifyDescription() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(BACKENDSERVERleft)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on- '" + BACKENDLINK + "'",
				"Clicked on - '" + BACKENDLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(BACKENDHOST)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + HostNameLink + "'",
				"Clicked on - '" + HostNameLink + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MODIFYDES)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + ModifyLink + "'",
				"Clicked on - '" + ModifyLink + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String Description = driver.findElement(By.xpath(DESCRIPTEDIT)).getText();
		Thread.sleep(1000);
		driver.findElement(By.xpath(DESCRIPTEDIT)).clear();
		driver.findElement(By.xpath(DESCRIPTEDIT)).sendKeys(SampleText);
		driver.findElement(By.xpath(DESOKBUN)).click();
		Thread.sleep(3000);
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + okbunlink + "'",
				"Clicked on - '" + okbunlink + "' - Successful");
		ReportManager.ReportEvent(TestStatus.PASS, "Update description as - '" + SampleText + "'",
				"Updated Description as - '" + SampleText + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MODIFYDES)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(DESCRIPTEDIT)).clear();
		Thread.sleep(1000);
		driver.findElement(By.xpath(DESCRIPTEDIT)).sendKeys(Description);
		driver.findElement(By.xpath(DESOKBUN)).click();
		Thread.sleep(3000);
		ReportManager.ReportEvent(TestStatus.PASS, "Revert to old Description - '" + Description + "'",
				"Reverted to old Description - '" + Description + "' - Successful");
	}

//added By Ravi Kumar
//Date- 11th Jan 2019
//TCD- CPE-1162
//Description- Verify refresh with click option
	public static void Refresh() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(GOCOMMONTASKS)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + COMMONTASKLINK + "'",
				"Clicked on - '" + COMMONTASKLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(BACKUPSYSTEM)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + backupsystemLink + "'",
				"Clicked on - '" + backupsystemLink + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(BACKUPCURRENT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + BackupCurrentLink + "'",
				"Clicked on - '" + BackupCurrentLink + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(BACKUPNAME)).sendKeys(Namedata);
		ReportManager.ReportEvent(TestStatus.PASS, "Enter Backup Name - '" + Namedata + "'",
				"Entered backup name - '" + Namedata + "' - Successful");
		driver.findElement(By.xpath(REFRESHXPATH)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String nameverify = driver.findElement(By.xpath(BACKUPNAME)).getText();
		System.out.println(nameverify);
		if (nameverify.equals("")) {
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Backup Name after refreshing - '" + nameverify + "'",
					"Verified Backup Name after refreshing - '" + nameverify + "' - Successful");
			ReportManager.ReportEvent(TestStatus.PASS, "Verify - '" + refreshdata + "'",
					"Verified - '" + refreshdata + "' - Successful");
		} else {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify - '" + refreshdata + "'",
					"Verified - '" + refreshdata + "' - Successful");
		}
	}

//added By Ravi Kumar
//Date- 11th Jan 2019
//TCD- CPE-1162
//Description- Verify refresh with click option
//Enter incorrect old password.
	public static void OldPassword() throws InterruptedException {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MNGPRT)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + MNGLINK + "'",
				"Clicked on - '" + MNGLINK + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(MNGUSERS)).click();
		String MngUserLink = "Manage users Link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + MngUserLink + "'",
				"Clicked on - '" + MngUserLink + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CREATEUSER)).click();
		String CreateUser = "Define User link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + CreateUser + "'",
				"Clicked on - '" + CreateUser + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		try {
			// validation 1
			driver.findElement(By.xpath(USERNAME1)).sendKeys(USER1);
			ReportManager.ReportEvent(TestStatus.PASS, "Enter only usernmae - '" + USER1 + "'",
					"Entered UserName - '" + USER1 + "' - Successful");
			driver.findElement(By.xpath(ADDUSER)).click();
			String AddButton = "Add Button";
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + AddButton + "'",
					"Clicked on - '" + AddButton + "' - Successful");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			String validate1 = driver.findElement(By.xpath(VALIDATEPASSWD)).getText();
			if (validate1.contains("Req")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify text - '" + validate1 + "'",
						"Verified text - '" + validate1 + "' - Successful");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Verify text - '" + validate1 + "'",
						"Verified text - '" + validate1 + "' - Successful");
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(REFRESHXPATH)).click();
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(USERNAME1)).sendKeys(USER1);
			ReportManager.ReportEvent(TestStatus.PASS, "Enter UserName - '" + USER1 + "'",
					"Entered UserName - '" + USER1 + "' - Successful");
			driver.findElement(By.xpath(USERPASSWD)).sendKeys(PASSWD1);
			ReportManager.ReportEvent(TestStatus.PASS, "Enter password - '" + PASSWD1 + "'",
					"Entered password - '" + PASSWD1 + "' - Successful");
			driver.findElement(By.xpath(CONFIRMPASSWD)).sendKeys(PASSWD2);
			ReportManager.ReportEvent(TestStatus.PASS, "Reenter incorrect password - '" + PASSWD2 + "'",
					"Rentered incorrect password - '" + PASSWD2 + "' - Successful");
			Thread.sleep(1000);
			driver.findElement(By.xpath(ADDUSER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + AddButton + "'",
					"Clicked on - '" + AddButton + "' - Successful");
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// validation 2
			String validate2 = driver.findElement(By.xpath(MISMATCHPASSWD)).getText();
			if (validate2.contains("Passwords")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Text - '" + validate2 + "'",
						"Verified Text - '" + validate2 + "' - Successful");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Verify Text  - '" + validate2 + "'",
						"Verified Text - '" + validate2 + "' - Successful");
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(REFRESHXPATH)).click();
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(USERPASSWD)).sendKeys(PASSWD1);
			ReportManager.ReportEvent(TestStatus.PASS, "Enter password - '" + PASSWD1 + "'",
					"Entered password - '" + PASSWD1 + "' - Successful");
			driver.findElement(By.xpath(CONFIRMPASSWD)).sendKeys(PASSWD1);
			ReportManager.ReportEvent(TestStatus.PASS, "Reenter correct password - '" + PASSWD1 + "'",
					"Reentered correct password - '" + PASSWD1 + "' - Successful");
			Thread.sleep(1000);
			driver.findElement(By.xpath(ADDUSER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + AddButton + "'",
					"Clicked on - '" + AddButton + "' - Successful");
			Thread.sleep(2000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// validation 3
			String validate3 = driver.findElement(By.xpath(WITHOUTUSER)).getText();
			if (validate3.contains("failed")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Alert Message - '" + validate3 + "'",
						"Verified Alert Message - '" + validate3 + "' - Successful");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Verify Alert Message - '" + validate3 + "'",
						"Verified Alert Message - '" + validate3 + "' - Successful");
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(REFRESHXPATH)).click();
			Thread.sleep(2000);
		} catch (Exception ex) {
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify Alert Message - '" + ex + "'",
					"Verified Alert Message - '" + ex + "' - Successful");
		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
//This Method will edit the Description for a Web Partition and validates after save.
	public static void WPDescriptionCheck() {
		try {
			String desp1, desp2;
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WPLINKMSG + "'",
					"Clicked On - '" + WPLINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PM1LINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1LINKMSG + "'",
					"Clicked On - '" + PM1LINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PM1MODIFY)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1MODIFYMSG + "'",
					"Clicked On - '" + PM1MODIFYMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			desp1 = driver.findElement(By.xpath(PM1DESP)).getText();
			System.out.println("The Text was : " + desp1);
			driver.findElement(By.xpath(PM1DESP)).clear();
			driver.findElement(By.xpath(PM1DESP)).sendKeys("Hello World");
			if (driver.findElement(By.xpath(FRONTENDIP)).getText() == "") {
				driver.findElement(By.xpath(FRONTENDIP)).sendKeys("10.62.177.111");
			}
			driver.findElement(By.xpath(PM1OK)).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1OKMSG + "'",
					"Clicked On - '" + PM1OKMSG + "' - Successfull");
			desp2 = driver.findElement(By.xpath(PM1DESPVALUE)).getText();

			if (desp2.contains("Hello World")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Modified Description - '" + desp2 + "'",
						"Verified message - '" + desp2 + "' - Successfull");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Modified Description verification Failed - '" + desp2 + "'",
						"Verified message - '" + desp2 + "' - Failed");
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PM1MODIFY)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1MODIFYMSG + "'",
					"Clicked On - '" + PM1MODIFYMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PM1DESP)).clear();
			driver.findElement(By.xpath(PM1DESP)).sendKeys(desp1);
			driver.findElement(By.xpath(PM1OK)).click();
		} catch (Exception e) {
			String msg = "Modified Description Value";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "' - Failed");

		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
//This Method validates the current Web Partition Status.
	public static void WPStatusCheck() {
		String desp1, desp2;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WPLINKMSG + "'",
					"Clicked On - '" + WPLINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			desp1 = driver.findElement(By.xpath(PMCURRENTSTATEPARTITION)).getText();
			driver.findElement(By.xpath(PM1LINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1LINKMSG + "'",
					"Clicked On - '" + PM1LINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PMSTATUS)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PMSTATUSMSG + "'",
					"Clicked On - '" + PMSTATUSMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			desp2 = driver.findElement(By.xpath(PMCURRENTSTATE)).getText();
			if (desp1.contains(desp2)) {
				System.out.println("The Status Test Passed");
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Modified Description - '" + desp1 + "'",
						"Verified message - '" + desp1 + "' - Successfull");
			}
		} catch (Exception ex) {
			String msg = "PM-1 Status check failed";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
//Method to validate partition specified state

	public static void WPSpecifiedStatusCheck(String Status) {
		String desp1, desp2;
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WPLINKMSG + "'",
					"Clicked On - '" + WPLINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			desp1 = driver.findElement(By.xpath(PMCURRENTSTATEPARTITION)).getText();
			driver.findElement(By.xpath(PM1LINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PM1LINKMSG + "'",
					"Clicked On - '" + PM1LINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(PMSTATUS)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PMSTATUSMSG + "'",
					"Clicked On - '" + PMSTATUSMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			desp2 = driver.findElement(By.xpath(PMCURRENTSTATE)).getText();
			if (desp1.contains(desp2) && desp1.contains(Status) && desp2.contains(Status)) {
				System.out.println("The Status Test Passed");
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Specified Partition Status - '" + Status + "'",
						"Verified Specified Partition Status - '" + desp1 + "' - Successfull");
			} else {
				String msg = "PM-1 Status check failed";
				ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
						"Verified message - '" + msg + "'");
			}
		} catch (Exception ex) {
			String msg = "PM-1 Status check failed";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}

	// Added By Ravi Kumar Modified By Arif
	// Modified Date- 17th Jan 2019
//Method Creates a  Web Cluster and accepts parameters as CLUSTERNAME, CLUSTERIPADDR, CLUSTERSUBMASK and GATEWAYIP
	public static void CreateCluster(String CLUSTERNAME, String CLUSTERIPADDR, String CLUSTERSUBMASK,
			String GATEWAYIP) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(CLUSTERCREATELINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERCREATELINKMSG + "'",
					"Clicked On - '" + CLUSTERCREATELINKMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERNXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERNXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERNXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERNAMETXT)).sendKeys(CLUSTERNAME);
			if(GlobalVariables.WebClusterServerDNSName != null){
				driver.findElement(By.id(WebClusterDNSNameLink)).sendKeys(GlobalVariables.WebClusterServerDNSName);
			}
			driver.findElement(By.xpath(CLUSTERNAMENXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERNAMENXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERNAMENXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERIPADDRTXT)).sendKeys(CLUSTERIPADDR);
			driver.findElement(By.xpath(CLUSTERSUBNETMASKTXT)).sendKeys(CLUSTERSUBMASK);
			driver.findElement(By.xpath(CLUSTERGATEWAYIPTXT)).sendKeys(GATEWAYIP);
			driver.findElement(By.xpath(CLUSTERNETINFONXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERNETINFONXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERNETINFONXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERDESPNXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERDESPNXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERDESPNXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERFINISHBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERFINISHBTNMSG + "'",
					"Clicked On - '" + CLUSTERFINISHBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			String FinishMsg = driver.findElement(By.xpath(CLUSTERCREATEDMSG)).getText();
			if (FinishMsg.contains(GlobalVariables.ClusterCreatedSuccessMessage)) {
				ReportManager.ReportEvent(TestStatus.PASS,
						"Click On - '" + GlobalVariables.ClusterCreatedSuccessMessage + "'",
						"Clicked On - '" + GlobalVariables.ClusterCreatedSuccessMessage + "' - Successfull");
			}
		} catch (Exception ex) {
			String msg = "Failed to create Cluster";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");

		}

	}

	// Added By Ravi Kumar Modified By Arif
	// Modified Date- 17th Jan 2019
//This Method Brings the specified Cluster Online and accepts parameter as clustername
	public static void BringClusterOnline(String CLUSTER) {
		try {
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// driver.findElement(By.name(CLUSTER)).click();
			driver.findElement(By.xpath(CLUSTERNAME)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTER + "'",
					"Clicked On - '" + CLUSTER + "' - Successfull");
			Thread.sleep(3000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(BRINGCLUSTERONLINELINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BRINGCLUSTERONLINELINKMSG + "'",
					"Clicked On - '" + BRINGCLUSTERONLINELINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(BRINGONLINEYESBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BRINGONLINEYESBUTTONMSG + "'",
					"Clicked On - '" + BRINGONLINEYESBUTTONMSG + "' - Successfull");
			Thread.sleep(5000); // Wait for a some seconds for the cluster to be online.
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(CLUSTERCOMMINGONLINE)).getText().contains(CLUSTERCOMMINGONLINEMSG)) {
				ReportManager.ReportEvent(TestStatus.PASS,
						"Verifying Online message- '" + "CLUSTER " + BRINGCLUSTERONLINELINKMSG + "'",
						"Verified Online message- '" + BRINGCLUSTERONLINELINKMSG + "' - Successfull");
			} else {
				String msg = "Failed to Bring Cluster Online";
				ReportManager.ReportEvent(TestStatus.FAILED, "Verify message - '" + msg + "'",
						"Verified message - '" + msg + "'");
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// driver.findElement(By.name(CLUSTER)).click();
			driver.findElement(By.partialLinkText(CLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTER + "'",
					"Clicked On - '" + CLUSTER + "' - Successfull");
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(CHECKCLUSTERSTATUSONLINE)).getText().contains("Online")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Cluster Status - '" + "Online" + "'",
						"Verified Cluster Status- '" + "Online" + "' - Successfull");
			} else {
				String msg = "Failed to Bring Cluster Online";
				ReportManager.ReportEvent(TestStatus.FAILED, "Verify message - '" + msg + "'",
						"Verified message - '" + msg + "'");
			}

		} catch (Exception ex) {
			String msg = "Failed to Bring Cluster Online";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}

	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
// Method To add partitions to web cluster from web partitions property page.
	public static void AddPartitionToCluster(String CLUSTER) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WPLINKMSG + "'",
					"Clicked On - '" + WPLINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(ADDWPTOCLUSTERLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPTOCLUSTERLINKMSG + "'",
					"Clicked On - '" + ADDWPTOCLUSTERLINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			Thread.sleep(5000);
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(ADDWPNXTBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPNXTBUTTONMSG + "'",
					"Clicked On - '" + ADDWPNXTBUTTONMSG + "' - Successfull");
			// Assign and Select the dropdown list element
			Select selectDropdown = new Select(driver.findElement(By.name("UnassignedDeviceList")));
			// Get all the option from dropdown list and assign into List
			List<WebElement> WPlistOptionDropdown = selectDropdown.getOptions();
			// Count the item dropdown list and assign into integer variable
			int dropdownCount = WPlistOptionDropdown.size();
			for (int i = 1; i <= dropdownCount; i++) {
				driver.switchTo().defaultContent();
				driver.switchTo().frame("leftNav");
				driver.findElement(By.xpath(WPLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WPLINKMSG + "'",
						"Clicked On - '" + WPLINKMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(ADDWPTOCLUSTERLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPTOCLUSTERLINKMSG + "'",
						"Clicked On - '" + ADDWPTOCLUSTERLINKMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				Thread.sleep(5000);
				driver.switchTo().frame(0);
				driver.findElement(By.xpath(ADDWPNXTBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPNXTBUTTONMSG + "'",
						"Clicked On - '" + ADDWPNXTBUTTONMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.switchTo().frame(0);
				driver.findElement(By.xpath(ADDWPNXTBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPNXTBUTTONMSG + "'",
						"Clicked On - '" + ADDWPNXTBUTTONMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.switchTo().frame(0);
				String XPATH = CLUSTERDROPDOWNLIST + "/*[@value=" + "'" + CLUSTER + "'" + "]";
				driver.findElement(By.xpath(XPATH)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERDROPDOWNLISTMSG + "'",
						"Clicked On - '" + CLUSTERDROPDOWNLISTMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.switchTo().frame(0);
				driver.findElement(By.xpath(ADDWPNXTBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPNXTBUTTONMSG + "'",
						"Clicked On - '" + ADDWPNXTBUTTONMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.switchTo().frame(0);
				driver.findElement(By.xpath(ADDWPNXTBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDWPNXTBUTTONMSG + "'",
						"Clicked On - '" + ADDWPNXTBUTTONMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.switchTo().frame(0);
				Thread.sleep(1000 * 5);
				String FinishMsg = driver.findElement(By.xpath(STATUSMESSAGE)).getText();
				if (FinishMsg.contains(GlobalVariables.WPAddToClusterSuccessMessage)) {
					ReportManager.ReportEvent(TestStatus.PASS,
							"Add Partition" + "PM-" + i + " to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "'",
							"Added Partition" + "PM-" + i + " to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "' - Successfull");
				} else {
					ReportManager.ReportEvent(TestStatus.FAILED,
							"Add Partition" + "PM-" + i + " to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "'",
							"Added Partition" + "PM-" + i + " to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "' - Failed");
				}
			}
		} catch (Exception ex) {
			String msg = "Failed to Add Web Partition to Cluster";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
// Method To add partitions to web cluster from web cluster property page.
	public static void AddAllPartitionsToCluster(String CLUSTER) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// driver.switchTo().frame(0);
			driver.findElement(By.partialLinkText(CLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTER + "cluster link" + "'",
					"Clicked On - '" + CLUSTER + " cluster link" + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(ADDMEMBERTOCLUSTERLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ADDMEMBERTOCLUSTERLINKMSG + "'",
					"Clicked On - '" + ADDMEMBERTOCLUSTERLINKMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(SELECTALLWPCHECKBOX)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SELECTALLWPCHECKBOXMSG + "'",
					"Clicked On - '" + SELECTALLWPCHECKBOXMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(ASSIGNWPTOCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ASSIGNWPTOCLUSTERMSG + "'",
					"Clicked On - '" + ASSIGNWPTOCLUSTERMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(ASSIGNWPTOCLUSTERYESBUTTON)).isEnabled()) {
				driver.findElement(By.xpath(ASSIGNWPTOCLUSTERYESBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ASSIGNWPTOCLUSTERYESBUTTONMSG + "'",
						"Clicked On - '" + ASSIGNWPTOCLUSTERYESBUTTONMSG + "' - Successfull");
//				List <WebElement> webPartitions = driver.findElements(By.xpath("//input[@type='checkbox']"));
//				for ( WebElement webPartition : webPartitions ) {
//				    if ( !webPartition.isSelected() ) {
//				    	webPartition.click();
//				    }
//				}
				String FinishMsg = driver.findElement(By.xpath(STATUSMESSAGE)).getText();
				if (FinishMsg.contains(GlobalVariables.WPAddToClusterSuccessMessage)) {
					ReportManager.ReportEvent(TestStatus.PASS,
							"Add Partition to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "'",
							"Add Partition to Cluster Success Message: - '"
									+ GlobalVariables.WPAddToClusterSuccessMessage + "' - Successfull");
				} else {
					ReportManager.ReportEvent(TestStatus.FAIL,
							"Add Partition to Cluster Failure Message- '" + FinishMsg + "'",
							"Add Partition to Cluster Failure Message - '" + FinishMsg + "'");
				}
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Failed to assign Web Partition to cluster - '" + CLUSTER + "'",
						"Failed to assign Web Partition to cluster - '" + CLUSTER + "'");
			}
		} catch (Exception ex) {
			String msg = "Failed to Add Web Partition to Cluster";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
	// This Method will be used for enabling replication
	public static void EnableReplication() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(MANAGEPARTITIONLINK)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MANAGEPARTITIONLINKMSG + "'",
				"Clicked On - '" + MANAGEPARTITIONLINKMSG + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String CurrentStatus = driver.findElement(By.xpath(REPLICATIONSTATEVALUE)).getText();
		String CurrentHealth = driver.findElement(By.xpath(REPLICATIONHEALTHVALUE)).getText();
		if (CurrentStatus.contains(GlobalVariables.ReplicationStatusValue)) {
			System.out.println("Skipping Replication as the current state is \'Replicating\'.");
			ReportManager.ReportEvent(TestStatus.PASS, "Skipping Replication as the current state and health is: - '"
					+ GlobalVariables.ReplicationStatusValue + " and " + GlobalVariables.ReplicationHealthValue + "'",
					"Skipping Replication as the current state and health is: - '"
							+ GlobalVariables.ReplicationStatusValue + " and " + GlobalVariables.ReplicationHealthValue
							+ "' - Skipped");
		} else {
			try {
				driver.switchTo().defaultContent();
				driver.switchTo().frame("leftNav");
				driver.findElement(By.xpath(MANAGEPARTITIONLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MANAGEPARTITIONLINKMSG + "'",
						"Clicked On - '" + MANAGEPARTITIONLINKMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(ENABLEREPLICATIONLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ENABLEREPLICATIONLINKMSG + "'",
						"Clicked On - '" + ENABLEREPLICATIONLINKMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(ENABLEREPLICATIONYESBUTTON)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ENABLEREPLICATIONYESBUTTON + "'",
						"Clicked On - '" + ENABLEREPLICATIONYESBUTTONMSG + "' - Successfull");
				Thread.sleep(60000 * 10); // Wait for 1o minutes for replication status to get sync.
				driver.switchTo().defaultContent();
				driver.switchTo().frame("leftNav");
				driver.findElement(By.xpath(MANAGEPARTITIONLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MANAGEPARTITIONLINKMSG + "'",
						"Clicked On - '" + MANAGEPARTITIONLINKMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				CurrentStatus = driver.findElement(By.xpath(REPLICATIONSTATEVALUE)).getText();
				CurrentHealth = driver.findElement(By.xpath(REPLICATIONHEALTHVALUE)).getText();
				if (CurrentStatus.contains(GlobalVariables.ReplicationStatusValue)
						&& CurrentHealth.contains(GlobalVariables.ReplicationHealthValue)) {
					ReportManager.ReportEvent(TestStatus.PASS,
							"Replication Successfull with: - '" + GlobalVariables.ReplicationStatusValue + " and "
									+ GlobalVariables.ReplicationHealthValue + "'",
							"Replication Successfull with: - '" + GlobalVariables.ReplicationStatusValue + " and "
									+ GlobalVariables.ReplicationHealthValue + "' - Successfull");
				} else {
					ReportManager.ReportEvent(TestStatus.FAIL,
							"Enable Replication either status or health value mismatched- '",
							"Enable Replication either status or health value mismatched- '");
				}
			} catch (Exception ex) {
				String msg = "Exception While Enabling Replication";
				ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
						"Verified message - '" + msg + "'");
			}
		}

	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
	// This Method will be used for Deployed Application property validation
	public static void DeployedAppPropertyCheck(String Name, String DeployedAs, String IDEVersion, String Version) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(DEPLOYEDAPPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPLOYEDAPPLINKMSG + "'",
					"Clicked On - '" + DEPLOYEDAPPLINKMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(DEPLOYEDAPPEXISTS)).getText().contains("None")) {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Check Deployed Application - '" + "Deployed Application" + "'",
						"Verified message - '" + "Deployed Application (None)" + "'");
				throw new Exception();
			} else {

				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(DEPLOYEDAPP)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPLOYEDAPPMSG + "'",
						"Clicked On - '" + DEPLOYEDAPPMSG + "' - Successfull");
				Thread.sleep(5 * 1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				if (driver.findElement(By.xpath(DEPLOYEDAPPPROJECTNAMEVALUE)).getText().contains(Name)
						&& driver.findElement(By.xpath(DEPLOYEDAPPNAMEVALUE)).getText().contains(DeployedAs)
						&& driver.findElement(By.xpath(DEPLOYEDAPPIDEVERSIONVALUE)).getText().contains(IDEVersion)
						&& driver.findElement(By.xpath(DEPLOYEDAPPPRODUCTVERSIONVALUE)).getText().contains(Version)) {
					ReportManager.ReportEvent(TestStatus.PASS,
							"Validate Property - '"
									+ driver.findElement(By.xpath(DEPLOYEDAPPPROJECTNAMEVALUE)).getText() + "'",
							"Verified Property - '" + Name + "'");
					ReportManager.ReportEvent(
							TestStatus.PASS, "Validate Property - '"
									+ driver.findElement(By.xpath(DEPLOYEDAPPNAMEVALUE)).getText() + "'",
							"Verified Property - '" + DeployedAs + "'");
					ReportManager.ReportEvent(
							TestStatus.PASS, "Validate Property - '"
									+ driver.findElement(By.xpath(DEPLOYEDAPPIDEVERSIONVALUE)).getText() + "'",
							"Verified Property - '" + IDEVersion + "'");
					ReportManager.ReportEvent(TestStatus.PASS,
							"Validate Property - '"
									+ driver.findElement(By.xpath(DEPLOYEDAPPPRODUCTVERSIONVALUE)).getText() + "'",
							"Verified Property - '" + Version + "'");
				} else {
					ReportManager.ReportEvent(TestStatus.FAIL,
							"Verify Deployed Application Properties  - '" + Name + " " + DeployedAs + " " + IDEVersion
									+ " " + Version + "'",
							"Verified Deployed Application Properties - '" + DEPLOYEDAPPPROJECTNAMEVALUEMSG + " "
									+ DEPLOYEDAPPNAMEVALUEMSG + " " + DEPLOYEDAPPNAMEVALUEMSG + " "
									+ DEPLOYEDAPPNAMEVALUEMSG + "'");
					throw new Exception();
				}
			}
		} catch (Exception ex) {
			String msg = "Exception While Validating Deployed Application Properties";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}

	// Added By Arif
	// Modified Date- 17th Jan 2019
	// This Method will be used for Remove of Application using specified
	// synchronization method
	public static void UnDeployedAppSyncMethod(String SyncMethod) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(DEPLOYEDAPPLINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPLOYEDAPPLINKMSG + "'",
					"Clicked On - '" + DEPLOYEDAPPLINKMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(DEPLOYEDAPPEXISTS)).getText().contains("None")) {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Check Deployed Application - '" + "Deployed Application" + "'",
						"Verified message - '" + "Deployed Application (None)" + "'");
				throw new Exception();
			} else {

				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(SELECTDEPLOYEDAPP)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SELECTDEPLOYEDAPPMSG + "'",
						"Clicked On - '" + SELECTDEPLOYEDAPPMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(UNDEPLOYBTN)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + UNDEPLOYBTNMSG + "'",
						"Clicked On - '" + UNDEPLOYBTNMSG + "' - Successfull");
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				if (SyncMethod.contains("Offline")) {
					driver.findElement(By.xpath(SYNCMETHODOFFILINE)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SYNCMETHODOFFILINEMSG + "'",
							"Clicked On - '" + SYNCMETHODOFFILINEMSG + "' - Successfull");
				} else if (SyncMethod.contains("Online")) {
					driver.findElement(By.xpath(SYNCMETHODONLINE)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SYNCMETHODONLINEMSG + "'",
							"Clicked On - '" + SYNCMETHODONLINEMSG + "' - Successfull");
				} else if (SyncMethod.contains("Serial")) {
					driver.findElement(By.xpath(SYNCMETHODSERIAL)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SYNCMETHODSERIALMSG + "'",
							"Clicked On - '" + SYNCMETHODSERIALMSG + "' - Successfull");
				} else {
					driver.findElement(By.xpath(SYNCMETHODPHASED)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SYNCMETHODPHASEDMSG + "'",
							"Clicked On - '" + SYNCMETHODPHASEDMSG + "' - Successfull");
				}
				driver.switchTo().defaultContent();
				driver.switchTo().frame("content");
				driver.findElement(By.xpath(CONFIRMYESBTN)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CONFIRMYESBTNMSG + "'",
						"Clicked On - '" + CONFIRMYESBTNMSG + "' - Successfull");
				if (driver.findElement(By.xpath(CONFIRMATION)).getText().contains(CONFIRMATIONMSG)) {
					ReportManager.ReportEvent(TestStatus.PASS, "Verify Message - '" + CONFIRMATIONMSG + "'",
							"Verified Message - '" + CONFIRMATIONMSG + "' - Successfull");
				} else {
					ReportManager.ReportEvent(TestStatus.FAIL,
							"Validate UnDeploy Application Message- '" + CONFIRMATIONMSG + "'",
							"Validation Failed of Message - '" + CONFIRMATIONMSG + "'");
					throw new Exception();
				}
			}
		} catch (Exception ex) {
			String msg = "Exception While Removal of Application using specified synchronization method";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
		}
	}
	
	//added By Ravi Kumar
	//Date- 11th Jan 2019
	//TCD- CPE-1162
	//Description-Enter Stop one member of online web cluster.

	public static void StopMember() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		String webpart = "Web Partition Link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + webpart+"'", "Clicked on - '"+ webpart + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		try {
		driver.findElement(By.xpath(PM1XPATH)).click();
		String pmval = "Cluster PM1 link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + pmval+"'", "Clicked on - '"+ pmval + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(TAKEOFFLINE)).click();
		String takeoffline = "Take offline cluster link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + takeoffline+"'", "Clicked on - '"+ takeoffline + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CONFIRMOFFLINE)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String statusmsg = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
		if(statusmsg.contains("will go offline"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Alert Message - '" + statusmsg+"'", "Verified Alert Message - '"+ statusmsg + "' - Successful");	
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Alert Message - '" + statusmsg+"'", "Verified Alert Message - '"+ statusmsg + "' - Successful");
		}
		driver.findElement(By.xpath(VIEWSTATUS)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String devicestatus = driver.findElement(By.xpath(DEVICESTATUS)).getText();
		if(devicestatus.contains("Offline"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify device status - '" + devicestatus+"'", "Verified device status - '"+ devicestatus + "' - Successful");
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify device status - '" + devicestatus+"'", "Verified device status - '"+ devicestatus + "' - Successful");	
		}
		Thread.sleep(1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(DEPLOYEDAPP)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String baseUrl = driver.getCurrentUrl();
		String app1 = driver.findElement(By.xpath(DEPLOYEDAPP1)).getText();
		String newURL = NewPage +app1;
		Thread.sleep(1000);
		driver.get(newURL);
	    Thread.sleep(1000);
	    String newMsg = driver.getCurrentUrl();
	    ReportManager.ReportEvent(TestStatus.PASS, " launch new browser - '" + newMsg+"'", "Launched new Browser - '"+ newMsg + "' - Successful");
	    Thread.sleep(1000);
	    driver.get(baseUrl);
		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(PM1XPATH)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SYNCHRONIZESTATE)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(syncyesbunxpath)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String onlinemsg = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify new web page message - '" + onlinemsg+"'", "Verified new web page message - '"+ onlinemsg + "' - Successful");
		Thread.sleep(3000);
		driver.findElement(By.xpath(VIEWSTATUS)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(7000);
		String devicestatus1 = driver.findElement(By.xpath(DEVICESTATUS)).getText();
		if(devicestatus.contains("Online"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify device status - '" + devicestatus1+"'", "Verified device status - '"+ devicestatus1 + "' - Successful");
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify device status - '" + devicestatus1+"'", "Verified device status - '"+ devicestatus1 + "' - Successful");	
		}
		}
		catch(Exception ex)
		{System.out.println(ex);
		ReportManager.ReportEvent(TestStatus.FAIL, "Exception occurs - '" + ex+"'", "Exception occurred - '"+ ex + "' - Successful");
		}
	       
	}


	// Added By Ravi Kumar
	// date - 21st Jan 2019
	//common method to bring cluster offline
	public static void OfflineCluster() throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		String webpart = "Web Partition Link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + webpart+"'", "Clicked on - '"+ webpart + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(PM1XPATH)).click();
		String pmval = "Cluster PM1 link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + pmval+"'", "Clicked on - '"+ pmval + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(TAKEOFFLINE)).click();
		String takeoffline = "Take offline cluster link";
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + takeoffline+"'", "Clicked on - '"+ takeoffline + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CONFIRMOFFLINE)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String statusmsg = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
		if(statusmsg.contains("will go offline"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Alert Message - '" + statusmsg+"'", "Verified Alert Message - '"+ statusmsg + "' - Successful");	
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify Alert Message - '" + statusmsg+"'", "Verified Alert Message - '"+ statusmsg + "' - Successful");
		}
		driver.findElement(By.xpath(VIEWSTATUS)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String devicestatus = driver.findElement(By.xpath(DEVICESTATUS)).getText();
		if(devicestatus.contains("Offline"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify device status - '" + devicestatus+"'", "Verified device status - '"+ devicestatus + "' - Successful");
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify device status - '" + devicestatus+"'", "Verified device status - '"+ devicestatus + "' - Successful");	
		}
	    
		Thread.sleep(3000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(PM1XPATH)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(SYNCHRONIZESTATE)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(syncyesbunxpath)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String onlinemsg = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify new web page message - '" + onlinemsg+"'", "Verified new web page message - '"+ onlinemsg + "' - Successful");
		Thread.sleep(3000);
		driver.findElement(By.xpath(VIEWSTATUS)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(15000);
		String devicestatus1 = driver.findElement(By.xpath(DEVICESTATUS)).getText();
		if(devicestatus.contains("Online"))
		{
			ReportManager.ReportEvent(TestStatus.PASS, "Verify device status - '" + devicestatus1+"'", "Verified device status - '"+ devicestatus1 + "' - Successful");
		}
		else
		{
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify device status - '" + devicestatus1+"'", "Verified device status - '"+ devicestatus1 + "' - Successful");	
		}
		}

 // Added By Ravi
	// Date - 23rd Jan 2019
	// Common method for Rebuilding the PM
	public static void RebuildPM(String PM_Value) throws InterruptedException
	{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WBPARTION)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + WPLINKMSG+"'", "Clicked on - '"+ WPLINKMSG + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(PM_Value)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + WPLINKMSG+"'", "Clicked on - '"+ WPLINKMSG + "' - Successful");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(REBUILD)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		driver.findElement(By.xpath(CONFIRMYESBTN)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String statusmsg = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
		ReportManager.ReportEvent(TestStatus.PASS, "Verify  rebuild message - '" + statusmsg+"'", "Verified rebuild message - '"+ statusmsg + "' - Successful");
		Thread.sleep(1000);
		driver.findElement(By.xpath(VIEWSTATUS)).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		Thread.sleep(125000);
		String device_status = driver.findElement(By.xpath(DEVICESTATUS)).getText();
	    if(device_status.contains("Ready"))
	    {
	    	ReportManager.ReportEvent(TestStatus.PASS, "Verify device status - '" + device_status+"'", "Verified device status - '"+ device_status + "' - Successful");
	    }
	    else
	    {
	    	ReportManager.ReportEvent(TestStatus.FAIL, "Verify device status - '" + device_status+"'", "Verified device status - '"+ device_status + "' - Successful");
	    }
	}
	/**
	 Method Name : RemoveCluster
	 * RemoveCluster Method Removes offline cluster , performs offline function,Deletes associated webpartitions and Remove cluster.
	 * @author Channabasappa M
	 * @param ClusterName Cluster Need to be removed
	 * @throws InterruptedException
	 */
	public  void RemoveCluster(String ClusterName)
	{
		String statusmessage = null;
		try {
			SelectWebclusterPropertyPage(ClusterName);
			PerformWebClusterOperation("TakeOffline");
			Thread.sleep(6000);
			removeWebPartitonsFromOfflineCluster();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + WEBCLUSTERMSG+"'", "Clicked on - '"+ WEBCLUSTERMSG + "' - Successful");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			String WCLName=WebclusterName+ClusterName+"']";
			WebElement ClusterElement = driver.findElement(By.xpath(WCLName));
			String ClusterId = ClusterElement.getAttribute("id");
			ClusterId = ClusterId.substring(0,ClusterId.length()-4);
			ClusterId=ClusterId+"chkSelection";
			System.out.println("The Id values is "+ClusterId);
			driver.findElement(By.id(ClusterId)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Select cluster - '" + ClusterName+"'", "  Selected Cluster - '"+ ClusterName + "' - Successful");
			driver.findElement(By.xpath(REMOVEBUTTON)).click();
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(CONFIRMYESBTN)).click();
			statusmessage = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Verify status message - '" + statusmessage+"'", "Verified  status message - '"+ statusmessage + "' - Successful");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(REFRESHXPATH)).click();
		}
		catch(Exception ex){
			ReportManager.ReportEvent(TestStatus.PASS, "Verify status message - '" + statusmessage+"'", "Verified  status message - '"+ statusmessage + "' - Successful");

		}
		
	}
	
	//Developedcode as part of Common Methods :

/**
	 * Method Selects the Given Cluster 
	 * @param clusterName
	 * @throws InterruptedException
	 */
	public  void SelectWebclusterPropertyPage(String clusterName) throws InterruptedException {
		System.out.println("In SelectWebclusterPropertyPage");
				
		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			/*int count = driver.findElements(By.tagName("frame")).size();
			System.out.println("count="+count);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");*/
			Thread.sleep(2000);
			driver.findElement(By.xpath(IISLOG)).click();
			driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + GlobalVariables.ClusterName+"'", "Clicked On - '"+ GlobalVariables.ClusterName + "' - Successfull");
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			String WCLName=WebclusterName+clusterName+"']";
			System.out.println("The clusterName Xpath to be clicked is "+WCLName);
			WebElement WebclusterElement = driver.findElement(By.xpath(WCLName));
			String WebclusterName = driver.findElement(By.xpath(WCLName)).getText();
			if(WebclusterName.equalsIgnoreCase(GlobalVariables.ClusterName)){
				ReportManager.ReportEvent(TestStatus.PASS, "Find  - '" + WebclusterName+"'", "Found  - '"+ WebclusterName + "' - Successfull");
			}
			else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Find  - '" + WebclusterName+"'", "Could not Found  - '"+ WebclusterName + "' - Successfull");
			}
			if(WebclusterElement.isEnabled() == true)
				driver.findElement(By.xpath(WCLName)).click();
			String DesiredAppstate = GetDesiredApplicationState();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WebclusterName+"'", "Clicked On - '"+ WebclusterName + "' - Successfull");
			if(GlobalVariables.DesiredAppstate != null){
				if(DesiredAppstate.contains( GlobalVariables.DesiredAppstate)){
					ReportManager.ReportEvent(TestStatus.PASS, "DesiredAppstate - '" + GlobalVariables.DesiredAppstate+"'", "same as Required state - '"+DesiredAppstate);
				}

				else {
					ReportManager.failurepopUp(GlobalVariables.DesiredAppstate, DesiredAppstate);
					ReportManager.ReportEvent(TestStatus.FAIL, "DesiredAppstate - '" + DesiredAppstate+"'", "Is Not same as Requested state "+GlobalVariables.DesiredAppstate);
				}
			}
		    
		} catch (InterruptedException e) {
			 ReportManager.ReportEvent(TestStatus.FAIL, "Navigation to  - '" + CLUSTERLINK+"'", "Unable to Navigate to  "+CLUSTERLINK+ "' - Successfull");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This Method Retrives GetDesiredApplicationState of Webcluster
	 * @return DesiredApplicationState of webcluster as string 
	 * @throws InterruptedException
	 */
	public String GetDesiredApplicationState() throws InterruptedException {
		String DesiredApplicationState = null;
		try {
			DesiredApplicationState = driver.findElement(By.xpath(DesiredWebclusterStatus)).getText();
		    ReportManager.ReportEvent(TestStatus.PASS, "Desired Application State   - '" + DesiredApplicationState+"'", "App state is   "+DesiredApplicationState+ "' - Successfull");
		}
		catch(Exception ex){
			System.out.println("Failed to getDesiredApplicationState due to  "+ex.toString());
		}
		return DesiredApplicationState; 
	}
	
	/**
	 * Method retrives  GetVirtualIPAddress as string 
	 * 
	 * @return GetVirtualIPAddress as string 
	 * @throws InterruptedException
	 */
	public String GetVirtualIPAddress()throws InterruptedException {
	  String VIPAdress=null;
	  try{
		  VIPAdress = driver.findElement(By.xpath(VIPAddress)).getText(); 
		  System.out.println("The VIPAdress  of element is  "+driver.findElement(By.xpath(VIPAddress)).getText());
		  ReportManager.ReportEvent(TestStatus.PASS, "Getting Virtual Ip Address   - '" + VIPAdress+"'", "Found Virtual Ip Address   "+VIPAdress+ "' - Successfull");
		  
	  }
	  catch(Exception ex){
		  System.out.println("Unable to find The VIPAdress  of element due to   "+ex.toString());
	  }
	 return VIPAdress;
	}
	
	/**
	 * MethodName :PerformRestartOperationonWebCluster
	 * PerformRestartOperationonWebCluster Performs Operation on webcluster
	 * @author Channabasappa M
	 * @param Operation Operation Need to Be Performed 
	 * @param OpType  Opration Type
	 * @return Void
	 * @throws InterruptedException
	 */
	public void  PerformRestartOperationonWebCluster(String Operation,String OpType )throws  InterruptedException {

		System.out.println("PerformRestartOperationonWebCluster :::: ");
		try {
			WebElement RestartElement = driver.findElement(By.xpath(WebClusterRestartLink));

			RestartElement.click();
			System.out.println("PerformOperationonWebCluster :::: Performing  Operation  "+Operation);
			ReportManager.ReportEvent(TestStatus.PASS, " Click On  - '" + WebClusterRestartLinkMsg+"'", "Clicked On - '"+ WebClusterRestartLinkMsg + "' on Webcluster   ");
			//Handling Poup up with RadioButton selection
			Thread.sleep(1000);
			
			String locaterID2="//label[contains(text(),";
        	locaterID2= ""+locaterID2+"'"+GlobalVariables.SynchornizationMethod+"')]";
			if((driver.findElement(By.xpath(locaterID2)).isSelected()== false)){
				System.out.println("Inside Selecting Option for  :::: Performing  Operation  "+GlobalVariables.SynchornizationMethod);
				String operationType= OpTypepath+GlobalVariables.SynchornizationMethod+"']";
				WebElement Selecting= driver.findElement(By.xpath(operationType));
				Selecting.click();
				System.out.println("Inside Selecting Option for  :::: Performing  Operation  "+driver.findElement(By.xpath(operationType)).getAttribute("checked"));
				System.out.println("Inside Selected option isr  :::: Performing  Operation  "+driver.findElement(By.xpath(locaterID2)).isSelected());
				
				}
			//Click Yes to Proceed 
			driver.findElement(By.xpath(IISGTYES)).click();
			ReportManager.ReportEvent(TestStatus.PASS, " Click On  - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' on Webcluster   ");
			ReportManager.ReportEvent(TestStatus.PASS, " Perform - '" + RestartElement.getText()+"'", "RestartElement - '"+ Operation + "' on Webcluster   ");
		}

		catch(Exception ex ){
			ReportManager.ReportEvent(TestStatus.FAILED, "To Perform - '" + CHCLULNK+"'", "Operation - '"+ Operation + "' on Webcluster   "+ex.toString());

		}
	}
   
	/**
	 * MethodName :navigateDeployedApps
	 * navigateDeployedApps Navigates to Deployed Apps page
	 *  @author Channabasappa M
	 * @throws IOException
	 * @throws Exception
	 */
	
	public void navigateDeployedApps()throws InterruptedException{
		System.out.println("In navigateDeployedApps Method ");

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		try{
			driver.findElement(By.xpath(DEPAPP)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DEPAPPLNK +
					
					"'",
					"Clicked On - '" + DEPAPPLNK + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
		}
		catch(Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Click On - '" + DEPAPPLNK + "'",
					"Clicked On - '" + DEPAPPLNK + "' - Successfull");
		}
	}
	
	/**
	 * MethodName :AddNewMemberstoCluster
	 * AddNewMemberstoCluster method adds web partitions to web cluster
	 * @author Channabasappa M
	 * this method adds New  Members to Cluster
	 * @throws InterruptedException
	 */
	public static void AddNewMemberstoCluster() throws InterruptedException{
		//Add 4 Partitions to cluster
		System.out.println("In AddNewMemberstoCluster Method ");
		try {
			// Access Add New Members
			WebElement AddnewMwmbersElement = driver.findElement(By.xpath(AddNewMemberstoCluster));
			if(AddnewMwmbersElement != null){
			AddnewMwmbersElement.click();
			}
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AddNewMemberstoClusterMsg + "'",
					"Clicked On - '" + AddNewMemberstoClusterMsg + "' - Successfull");
			//Check  and select 4 Partitions 
			int count =0;
			List <WebElement> webPartitions = driver.findElements(By.xpath(CheckBoxLink));
			webPartitions.remove(0);
			System.out.println("Size o checkbox List "+webPartitions.size());
			if(webPartitions.size()<Integer.parseInt(GlobalVariables.NumberofApllicationsToBeAdded)){
				System.out.println("The number of Partitions is Less than Required Numbers "+webPartitions.size()+"But Required is "+GlobalVariables.NumberofApllicationsToBeAdded);
				ReportManager.ReportEvent(TestStatus.FAIL, "Number of Partitions to be selected  - '" + GlobalVariables.NumberofApllicationsToBeAdded + "'",
					"Actual Number of partitions Present  - '" + webPartitions.size() + "' - Successfull");	
			}
			for ( WebElement el : webPartitions ) {
    		    if ( !el.isSelected() ) {
    		        el.click();
    		        count++;
    		        if(count == Integer.parseInt(GlobalVariables.NumberofApllicationsToBeAdded)){
    		        	System.out.println("No of Applications selected  "+count);
    		        	ReportManager.ReportEvent(TestStatus.PASS, "Number of Partitions to be selected  - '" + GlobalVariables.NumberofApllicationsToBeAdded + "'",
    							"Number of partitions Selected  - '" + webPartitions.size() + "' - Successfull");	
    		        	break;
    		        }
    		    }
    		}
			driver.findElement(By.xpath(ASSIGNWebPTOCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ASSIGNWebPTOCLUSTERMsg + "'",
					"Clicked On - '" + ASSIGNWebPTOCLUSTERMsg + "' - Successfull");
			driver.findElement(By.xpath(ASSIGNWebPTOCLUSTERYESBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ASSIGNWebPTOCLUSTERYESBUTTONMsg + "'",
					"Clicked On - '" + ASSIGNWebPTOCLUSTERYESBUTTONMsg + "' - Successfull");	
			ReportManager.ReportEvent(TestStatus.PASS, "Perform   - '" + AddNewMemberstoClusterMsg + "'",
					"Performed  - '" + AddNewMemberstoClusterMsg + "' - Successfull");	
			
		}
		catch(Exception ex){
			System.out.println("Failed to AddNewMemberstoCluster  "+ex.toString());
			ReportManager.ReportEvent(TestStatus.FAIL, "Failed to  - '" + AddNewMemberstoClusterMsg + "'",
					"Clicked On - '" + AddNewMemberstoClusterMsg + "' - Successfull");	
		}
		
	}
	
	
	/**
	 * MethodName PerformWebClusterOperation
	 * This Method performs operation on webcluster bases on the request
	 * @author Channabasappa M 
	 * @param Operation Need to be performed 
	 * @throws InterruptedException
	 */
	
	public void PerformWebClusterOperation(String Operation)throws InterruptedException  {

		System.out.println("PerformWebClusterOperation   :::: ");
		WebElement Opelement = null;
		try {
			System.out.println("Performing Operation   :::: "+Operation);
			if(Operation.equalsIgnoreCase("BringOnline") ) {
				Opelement = driver.findElement(By.xpath(BringCLusterOnline));
				Opelement.click();

				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BringCLusterOnlineMsg + "'",
						"Clicked On - '" + BringCLusterOnlineMsg + "' - Successfull");	
			}
			if(Operation.equalsIgnoreCase("shutdowncluster")) {
				Opelement = driver.findElement(By.xpath(ShutdownWebcluster));
				Opelement.click();

				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ShutdownWebclusterMsg + "'",
						"Clicked On - '" + ShutdownWebclusterMsg + "' - Successfull");	
			}
			if(Operation.equalsIgnoreCase("TakeOffline")) {
				Opelement = driver.findElement(By.xpath(TAKEOFFLINE));
				Opelement.click();

				String takeoffline = "Take offline cluster link";
				ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + takeoffline+"'", "Clicked on - '"+ takeoffline + "' - Successful");
			}

			//Click Yes to Proceed 
			Thread.sleep(3000);
			driver.findElement(By.xpath(IISGTYES)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Performed  - '" + Operation + "'",
					"Clicked On - '" + Operation + "' - Successfull");	
		}
		catch(Exception ex ){
			System.out.println("Failed to Perform Oeration due to   :::: "+ex.getMessage());
			ReportManager.ReportEvent(TestStatus.FAIL, "Perform  - '" + Operation + "'",
					"Perform - '" + Operation + "' - UnSuccessfull");	
		}
	}
	
	/**
	 * Method Name : SelectSynchronizationMethod
	 * Description : This method is to select the required select synchronization method
	 * @author Channabasappa M
	 * @param SynchorizatonType : string value of synchronization method to be selected
	 * @throws InterruptedException
	 */
	public static void  SelectSynchronizationMethod(String SynchorizatonType)throws InterruptedException {
		System.out.println("Started  SelectSynchronizationMethod "+SynchorizatonType);
		String SelectedSyncType=null;
		try {
			String locaterID2="//label[contains(text(),";
			locaterID2= ""+locaterID2+"'"+GlobalVariables.SynchornizationMethod+"')]";
			if((driver.findElement(By.xpath(locaterID2)).isSelected()== false)){
				System.out.println("Inside Selecting Option for  :::: Performing  Operation  "+GlobalVariables.SynchornizationMethod);
				String operationType= OpTypepath+GlobalVariables.SynchornizationMethod+"']";
				WebElement Selecting= driver.findElement(By.xpath(operationType));
				Selecting.click();
				Thread.sleep(1000);
				if(Selecting.getText().equalsIgnoreCase(GlobalVariables.SynchornizationMethod)){
					SelectedSyncType =  driver.findElement(By.xpath(operationType)).getText();
					ReportManager.ReportEvent(TestStatus.PASS, "select SynchornizationMethod - '"+GlobalVariables.SynchornizationMethod+"'", "Selected SynchornizationMethod - '"+Selecting.getText());
				}
			}
		}
		catch (Exception ex){
			System.out.println("Failed to select  SynchornizationMethod due to   "+ex.getMessage());
			ReportManager.ReportEvent(TestStatus.FAIL, "Failed to select - '"+GlobalVariables.SynchornizationMethod+"'", "Selected is -' "+SelectedSyncType);
			
		}
	}
	
	/**
	 * MethodName :ValidateWebpartitionStatus
	 * ValidateWebpartitionStatus method  Validates web partition status after performing operation 
	 * @author Channabasappa M
	 * This Method validates the status of web partitions 
	 * @throws InterruptedException
	 */
	
	public static void ValidateWebpartitionStatus()throws InterruptedException {
		System.out.println("Started  ValidateWebpartitionStatus");
		
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WebPartitionLink)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WebPartitionLinkMsg + "'",
				"Clicked On - '" + WebPartitionLinkMsg + "' - Successfull");
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		try {
			int startWpcount = 2;
			int statuscountTimes =3;
			DecimalFormat formatter = new DecimalFormat("00");
			String decimalvale = formatter.format(startWpcount);
			String DeviceIdGridLink = "//*[@id='DevicesGrid_ctl";
			String StatePrefix = "_State']";
			String NamePrefix="_Name']";
			List<WebElement> numberofRows = driver.findElements(By.xpath(NumberofwebpartitionsLink));
			for(int i =0;i<=statuscountTimes;i++){
				for( startWpcount = 2;startWpcount<=numberofRows.size()+1;startWpcount++) {
					System.out.println("The status  serching for "+startWpcount);
					System.out.println("TnumberofRows  "+numberofRows.size());
					decimalvale = formatter.format(startWpcount);
					String DeviCeGridFinalLink = DeviceGridPartialLink+decimalvale+StatePrefix;
					WebElement StatusElement = driver.findElement(By.xpath(DeviCeGridFinalLink));
					String Status = StatusElement.getText();
					System.out.println("The status value for "+StatusElement.getText()+ "for"+decimalvale);
					System.out.println("The class value for "+StatusElement.getAttribute("class")+ "for"+decimalvale);
					DeviCeGridFinalLink = DeviceGridPartialLink+decimalvale+NamePrefix;
					WebElement NameElement = driver.findElement(By.xpath(DeviCeGridFinalLink));
					String WebPartitionName = NameElement.getText();				
					System.out.println("PartitionName :::"+WebPartitionName+"Status ::::"+Status);
					//Synchronizing
					//Offline  4 Partitions will go Synchronization mode 
					//Online  All partitions  synchorinigating/online
					//Serial 1 partition /2 partition syncho one by one 
					//Phased  1 and 3  ,2 and 4 Sysncronizing mode 
					if(Status.contains("Synchronizing")){
						ReportManager.ReportEvent(TestStatus.PASS, "Verify WebPartition status 2  - '" + WebPartitionName+"'", "Verifyed WebPartition status - '"+ Status + "' - Successfull");
						
					}

				}
			}
			 ReportManager.ReportEvent(TestStatus.PASS, "Verify WebPartition status for partition   - '" + WebPartitionLinkMsg+"'", "WebPartition status  - '"+ WebPartitionLinkMsg + "' - Successfull");
		}
		catch (Exception ex) {
			
		}
	}
	
	/**
	 * MethodName :launchWebApplication
	 * launchWebApplication Launches WebApplication
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public void launchWebApplication()throws InterruptedException{
		
		System.out.println("In method launchWebApplication :" );
		String Url = null;
		try {
			//Get Virtual Ip
			String Vip =GetVirtualIPAddress();
			//web application url
			Url = "http://"+Vip+"/"+GlobalVariables.DeppAppName;
			//launch(Url);
			openBrowser(Url);
			Thread.sleep(3000);
			Url= "http://"+Vip+"/"+GlobalVariables.DeppAppName;
			driver.findElement(By.xpath(DepAppUserCodeLink)).sendKeys(GlobalVariables.DepAppUserCode);
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DepAppUserCodeLinkMsg+"'", "Clicked On - '"+ DepAppUserCodeLinkMsg + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Enter  Value for  - '" + DepAppUserCodeLinkMsg+"'", "Entered  value - '"+ GlobalVariables.DepAppUserCode + "' - Successfull");
			driver.findElement(By.xpath(DepAppPasswdLink)).sendKeys(GlobalVariables.DepAppPassword);
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DepAppPasswdLinkMsg+"'", "Clicked On - '"+ DepAppPasswdLinkMsg + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Enter Value for  - '" + DepAppUserCodeLinkMsg+"'", "Entered  value - '"+ GlobalVariables.DepAppPassword+ "' - Successfull");
			driver.findElement(By.xpath(DepAppSubmitLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + DepAppSubmitLinkMsg+"'", "Clicked On - '"+ DepAppSubmitLinkMsg + "' - Successfull");
		}
		catch (Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Launch URL   - '" + Url+"'", "Failed to Launch - '"+ Url + "' - UnSuccessful");
			
		}
		
	}
	
	/**
	 * Method Name :navigatetoBackEndServerPage
	 * This Method Navigates to BackEndserverpage 
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void navigatetoBackEndServerPage()throws InterruptedException{
		System.out.println("In Method ::: navigatetoBackEndServerPage");
		int count = driver.findElements(By.tagName("frame")).size();
		System.out.println("count="+count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		Thread.sleep(2000);
		try{
			String ElementName =driver.findElement(By.xpath(BackEndServersLink)).getText();
			driver.findElement(By.xpath(BackEndServersLink)).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServersLinkMsg+"'", "Clicked On - '"+ ElementName + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			Thread.sleep(2000);
		}
		
		
		catch (Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Click On - '" + BackEndServersLinkMsg+"'", "Clicked On - '"+ BackEndServersLinkMsg + "' - UnSuccessfull");

		}
	}
	
	/**
	 * Method Name:defineNewBackEndServer
	 * This Method Creates New dummy Backend Server
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void defineNewBackEndServer()throws InterruptedException{
		System.out.println("In Method ::: defineNewBackEndServer");
		try {
			System.out.println("Started Defining new backend server ");
			driver.findElement(By.xpath(DefineNewBackEndServerLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + DefineNewBackEndServerLinkMsg+"'", "Clicked On  - '"+ DefineNewBackEndServerLinkMsg + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(NextButtonLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			//Enter Name for New backendServer 
			driver.findElement(By.xpath(BackEndServerNameLink)).sendKeys(GlobalVariables.BackEndServerName);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerNameLinkMsg+"'", "Clicked On - '"+ BackEndServerNameLinkMsg + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Enter BackEnd Server Name  - '" + GlobalVariables.BackEndServerName+"'", "Entered  BackEnd Server Name  - '"+ GlobalVariables.BackEndServerName + "' - Successfull");
			driver.findElement(By.xpath(BackEndServerIpFourthOctcetLink)).sendKeys(GlobalVariables.BackEndServerIpFourthOctet);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerIpFourthOctcetLinkMsg+"'", "Clicked On - '"+ BackEndServerIpFourthOctcetLinkMsg + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Enter Fourth octet  - '" + GlobalVariables.BackEndServerIpFourthOctet+"'", "Entered Fourth Octet  - '"+ GlobalVariables.BackEndServerIpFourthOctet + "' - Successfull");
			driver.findElement(By.xpath(BackEndServerDNSNameLink)).sendKeys(GlobalVariables.BackEndServerDNSName);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerDNSNameLinkMsg+"'", "Clicked On - '"+ BackEndServerDNSNameLinkMsg + "' - Successfull");
			String BackEndSreverDNSName = driver.findElement(By.xpath(BackEndServerDNSNameLink)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Enter DNS Name  - '" + GlobalVariables.BackEndServerDNSName+"'", "Entered DNS Name - '"+ BackEndSreverDNSName + "' - Successfull");
			driver.findElement(By.xpath(NextButtonLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
		    driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.findElement(By.xpath(NextButtonLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(NextButtonLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(NextButtonLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(NextButtonLink)).click();
			Thread.sleep(1000);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NextButtonLinkMsg+"'", "Clicked On  - '"+ NextButtonLinkMsg + "' - Successfull");
			//After Finsh check status
			String MessagetoVerify = "successfully created a new Back-End Server Definition";
			String ActualMessage = driver.findElement(By.xpath(BackEndServerStatusLink)).getText();
			if(driver.findElement(By.xpath(BackEndServerStatusLink)).getText().contains("successfully created a new Back-End Server Definition"))
			{
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Msg - '" + MessagetoVerify+"'", "Verified Msg  - '"+ ActualMessage + "' - Successfull");
				ReportManager.ReportEvent(TestStatus.PASS, "Create Backend Server - '" + GlobalVariables.BackEndServerName+"'", "created  backend Server  - '"+ GlobalVariables.BackEndServerName + "' - Successfull");
			}
			//close 
			Thread.sleep(1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CancelButtonLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CancelButtonLinkMsg+"'", "Clicked On  - '"+ CancelButtonLinkMsg + "' - Successfull");
		}
		catch(Exception ex){
			System.out.println("Failed Defining new backend server "+ex.toString());
			ReportManager.ReportEvent(TestStatus.FAIL, "Could not create  - '" + DefineNewBackEndServerLinkMsg+"'", "New backend Server  - '"+ DefineNewBackEndServerLinkMsg + "' - unSuccessfull");
		}
		
	}

	/**
	 * Method Name:defineNewBackEndServer
	 * This Method Creates New dummy Backend Server
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void deleteBackEndServer()throws InterruptedException{
		System.out.println("In Method ::: deleteBackEndServer");
		try {
			//Verify BackEndServer is Primary Server
			
			WebElement ChkBoxBackSerElement = driver.findElement(By.xpath(BackEndServerNametoSelectLink+GlobalVariables.BackEndServerName+"']"));
			String id = ChkBoxBackSerElement.getAttribute("id");
			int BeginIndex = 0;
			id = id.substring(0,(id.length()-4));
			String XpathforPrimaryServerValueId = id+"IsPrimaryString";
			id=id+"chkSelection";
			String XpathforId="//*[@id='"+id+"']";
			String PrimaryServerValue = driver.findElement(By.id(XpathforPrimaryServerValueId)).getText();
			System.out.println("The  primary Server values is "+PrimaryServerValue);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PrimaryBackEndServerLinkMsg+"'", "Clicked On  - '"+ PrimaryBackEndServerLinkMsg + "' - Successfull");
			if(PrimaryServerValue.equalsIgnoreCase("yes")){
				ReportManager.ReportEvent(TestStatus.FAIL, "Verifiy Primary Server   - '" + PrimaryBackEndServerLinkMsg+"'", "verified Primary Server  - '"+ PrimaryServerValue + "' - Can not delete");
			}
			else {
				ReportManager.ReportEvent(TestStatus.PASS, "Verifiy Primary Server   - '" + PrimaryBackEndServerLinkMsg+"'", "verified  Primary Server value   - '"+ PrimaryServerValue + "' - Successfull");
			}
			ChkBoxBackSerElement = driver.findElement(By.xpath(XpathforId));
			ChkBoxBackSerElement.click();
			//Remove selected BackEndServer 
			driver.findElement(By.xpath(BackEndServerRemoveLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerRemoveLinkMsg+"'", "Clicked On  - '"+ BackEndServerRemoveLinkMsg + "' - Successfull");
			Thread.sleep(1000);
			driver.findElement(By.xpath(APPUNDPYYES)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + APPUNDPYLNK+"'", "Clicked On - '"+ APPUNDPYLNK + "' - Successfull");
			Thread.sleep(1000);
			//checkMessage successfully deleted
			String DeletedMessage = driver.findElement(By.xpath(BackEndServerStatusLink)).getText();
			if(driver.findElement(By.xpath(BackEndServerStatusLink)).getText().contains("successfully deleted")){
				ReportManager.ReportEvent(TestStatus.PASS, "Expected Message   - '" + DeletedMessage+"'", " Veryfied msg - '"+ DeletedMessage + "' - Successfull");
				ReportManager.ReportEvent(TestStatus.PASS, "Delete  BackEnd Server  - '" + GlobalVariables.BackEndServerName+"'", " Deleted BackEnd Server - '"+ GlobalVariables.BackEndServerName + "' - Successfull");
			}
		}
		catch(Exception ex){
			System.out.println("Failed to  deleteBackEndServer due to "+ex.toString());
			ReportManager.ReportEvent(TestStatus.FAIL, "Delete BackEnd Server   - '" + GlobalVariables.BackEndServerName+"'", " Failed to delete  - '"+ GlobalVariables.BackEndServerName + "' - Successfull");
		}
	}
	
	/**
	 * MethodName :modifyBackEndServer
	 * This Method Modifies the Backend server (Name and IpAddrress )
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void modifyBackEndServer()throws InterruptedException{
		System.out.println("In Method modifyBackEndServer ");
		String backEndserverName  = null;
		String backEndserverIpAddress  = null;
		String msg ="Modified BackENdserver";
		try {
			//Select required Backend Server 
			driver.findElement(By.xpath(BackEndServerNametoSelectLink+GlobalVariables.BackEndServerName+"']")).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerNametoSelectLinkMsg+"'", "Clicked On - '"+ BackEndServerNametoSelectLinkMsg + "' - Successfull");
			//capture Ip Address Modify click
			backEndserverName = driver.findElement(By.xpath(BackEndServerNamemodifciationLink)).getText();
			backEndserverIpAddress = driver.findElement(By.xpath(BackEndServerIpAddressLink)).getText();
			driver.findElement(By.xpath(BackEndServerModifyLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerModifyLinkMsg+"'", "Clicked On - '"+ BackEndServerModifyLinkMsg + "' - Successfull");
			WebElement BackendServerName =driver.findElement(By.xpath(BackEndServerNamemodifciationLink));
			BackendServerName.clear();
			BackendServerName.sendKeys(GlobalVariables.BackEndServerModifiedName);
			ReportManager.ReportEvent(TestStatus.PASS, "Initial Backend ServerName - '" + backEndserverName+"'", "Modified Name  - '"+ GlobalVariables.BackEndServerModifiedName + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerNamemodifciationLinkMsg+"'", "Clicked On - '"+ BackEndServerNamemodifciationLinkMsg + "' - Successfull");
			WebElement BackendServerThirdOctet =driver.findElement(By.xpath(BackEndServerthirdOctetmodifciationLink));
			BackendServerThirdOctet.clear();
			BackendServerThirdOctet.sendKeys(GlobalVariables.BackEndServerIpThirdOctet);
			ReportManager.ReportEvent(TestStatus.PASS, "Initial Ip  Address - '" + backEndserverIpAddress+"'", "Third octet value Entered   - '"+ GlobalVariables.BackEndServerIpThirdOctet + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerthirdOctetmodifciationLinkMsg+"'", "Clicked On - '"+ BackEndServerthirdOctetmodifciationLinkMsg + "' - Successfull");
			WebElement BackendServerFourthOctet =driver.findElement(By.xpath(BackEndServerFourthOctetmodifciationLink));
			BackendServerFourthOctet.clear();
			BackendServerFourthOctet.sendKeys(GlobalVariables.BackEndServerIpFourthOctet);
			ReportManager.ReportEvent(TestStatus.PASS, "Initial Ip  Address - '" + backEndserverIpAddress+"'", "fourth octet value Entered   - '"+ GlobalVariables.BackEndServerIpThirdOctet + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerFourthOctetmodifciationLinkMsg+"'", "Clicked On - '"+ BackEndServerFourthOctetmodifciationLinkMsg + "' - Successfull");
			driver.findElement(By.xpath(BackEndServerModifyOKLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerModifyOKLinkMsg+"'", "Clicked On - '"+ BackEndServerModifyOKLinkMsg + "' - Successfull");
			Thread.sleep(52000);
			//verify modified changes 
			String changedBackEndserverName = driver.findElement(By.xpath(BackEndServerNamemodifciationLink)).getText();
			String changedBackEndserverIpAddress = driver.findElement(By.xpath(BackEndServerIpAddressLink)).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Changed Backend ServerName - '" + changedBackEndserverName+"'", "Changed BackEndServer IpAddress - '"+ changedBackEndserverIpAddress + "' - Successfull");
			if(backEndserverName.equalsIgnoreCase(changedBackEndserverName)&&(changedBackEndserverIpAddress.equalsIgnoreCase(changedBackEndserverIpAddress))){
				ReportManager.ReportEvent(TestStatus.FAIL, "Modifying   - '" + msg+"'", "name and Ip Address - '"+ msg + "' -UnSuccessfull");
			}
			
			ReportManager.ReportEvent(TestStatus.PASS, "Change Name from  - '" + backEndserverName+"'", "Changed Name to  - '"+ changedBackEndserverName    + "' - Successfull"); 
			ReportManager.ReportEvent(TestStatus.PASS, "Change Ip Address  from  - '" + backEndserverIpAddress+"'", "Changed IpAddress to  - '"+ changedBackEndserverIpAddress   + "' - Successfull"); 
		}
		catch(Exception ex){
			System.out.println("Failed to Modify BackEndServer due to "+ex.toString());
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg+"'", "Verified message - '"+ msg    + "' - Successfull"); 
		}
	}
	
	/**
	 * Methodname :modifyBackEndServerandVerifyMessage
	 * modifyBackEndServerandVerifyMessage Method Modifies the Attribute and Capture Error Message 
	 * @author Channabasappa M
	 * @param ModifyingAttribute (IpAddress and Name )
	 * @throws InterruptedException
	 */
	public static void modifyBackEndServerandVerifyMessage(String ModifyingAttribute)throws InterruptedException{

		System.out.println("In Method modifyBackEndServerandVerifyMessage ");
		try {
			if(driver.findElement(By.xpath(BackEndServerNametoSelectLink+GlobalVariables.BackEndServerName+"']"))==null)
			{
				ReportManager.ReportEvent(TestStatus.FAIL, "Click BackEndServerName  - '" + GlobalVariables.BackEndServerName+"'", "Could Not find BackEnd Server - '"+ GlobalVariables.BackEndServerName + "' - UnSuccessfull");
			}
			driver.findElement(By.xpath(BackEndServerNametoSelectLink+GlobalVariables.BackEndServerName+"']")).click();
			String ServenrName = driver.findElement(By.xpath(BackEndServerNametoSelectLink+GlobalVariables.BackEndServerName+"']")).getText();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + GlobalVariables.BackEndServerName+"'", "Clicked On - '"+ ServenrName + "' - Successfull");
			driver.findElement(By.xpath(BackEndServerModifyLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerModifyLinkMsg+"'", "Clicked On - '"+ BackEndServerModifyLinkMsg + "' - Successfull");
			//for Name change Verification 
			if(ModifyingAttribute.equalsIgnoreCase("AlphaNumericNameVerification")){
				WebElement BackendServerName =driver.findElement(By.xpath(BackEndServerNamemodifciationLink));
				BackendServerName.clear();
				BackendServerName.sendKeys(GlobalVariables.BackEndServerModifiedName);
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerNamemodifciationLinkMsg+"'", "Clicked On - '"+ BackEndServerNamemodifciationLinkMsg + "' - Successfull");
				ReportManager.ReportEvent(TestStatus.PASS, "Enter Value  - '" +GlobalVariables.BackEndServerModifiedName+"'", "Enterd Value - '"+ GlobalVariables.BackEndServerModifiedName + "' - Successfull");
				driver.findElement(By.xpath(BackEndServerModifyOKLink)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerModifyOKLinkMsg+"'", "Clicked On - '"+ BackEndServerModifyOKLinkMsg + "' - Successfull");
				//check for error message 
				String ReturnMessage = driver.findElement(By.xpath(BackEndServerAlphaNumericNameVerificatioLink)).getText();
				if(ReturnMessage.contains(BackEndServerAlphaNumericNameVerificatioLinkMsg)){
					ReportManager.ReportEvent(TestStatus.PASS, " Expected - '" + ReturnMessage+"'", "Message - '"+ ReturnMessage + "' - Successfull");
				}
				else{
					ReportManager.ReportEvent(TestStatus.FAIL, " Expected - '" + BackEndServerAlphaNumericNameVerificatioLinkMsg+"'", "Message - '"+ BackEndServerAlphaNumericNameVerificatioLinkMsg + "' - UnSuccessfull");
				}
			}
				//for Ip Address Change message 
				if(ModifyingAttribute.equalsIgnoreCase("InValidIpAddress")){
					System.out.println("In Method modifyBackEndServerandVerifyMessage  for IpAddress ");
					WebElement BackendServerFourthOctet =driver.findElement(By.xpath(BackEndServerFourthOctetmodifciationLink));
					BackendServerFourthOctet.clear();
					BackendServerFourthOctet.sendKeys(GlobalVariables.BackEndServerIpFourthOctet);
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerFourthOctetmodifciationLinkMsg+"'", "Clicked On - '"+ BackEndServerFourthOctetmodifciationLinkMsg + "' - Successfull");
					ReportManager.ReportEvent(TestStatus.PASS, "Enter Value  - '" +GlobalVariables.BackEndServerIpFourthOctet+"'", "Enterd Value - '"+ GlobalVariables.BackEndServerIpFourthOctet + "' - Successfull");
					driver.findElement(By.xpath(BackEndServerModifyOKLink)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BackEndServerModifyOKLinkMsg+"'", "Clicked On - '"+ BackEndServerModifyOKLinkMsg + "' - Successfull");
					String ReturnMessage = driver.findElement(By.xpath(BackEndServerInvalidIpAddressStatusLink)).getText();
					if(ReturnMessage.contains(BackEndServerInvalidIpAddressStatusLinkMsg)){
						ReportManager.ReportEvent(TestStatus.PASS, " Expected - '" + ReturnMessage+"'", "Message - '"+ ReturnMessage + "' - Successfull");
				}
					else{
						ReportManager.ReportEvent(TestStatus.FAIL, " Expected - '" + BackEndServerInvalidIpAddressStatusLinkMsg+"'", "Message - '"+ BackEndServerInvalidIpAddressStatusLinkMsg + "' - UnSuccessfull");
						
					}
			}
		}
		catch(Exception ex){
			System.out.println("Validating Error Messages Failed due to "+ex.toString());
			ReportManager.ReportEvent(TestStatus.FAIL, "Returned Expected - '" + BackEndServerInvalidIpAddressStatusLinkMsg+"'", "Message - '"+ BackEndServerInvalidIpAddressStatusLinkMsg + "' - UnSuccessfull");

		}

	}
	
	/**
	 *MethodName : navigateToPreferences
	 * navigateToPreferences : Navigates to Preferences Page
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void navigateToPreferences()throws InterruptedException{
		System.out.println("In Method ::: navigateToPreferences");
		int count = driver.findElements(By.tagName("frame")).size();
		System.out.println("count="+count);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		Thread.sleep(1000);
		try{
			driver.findElement(By.xpath(PreferencesLink)).click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PreferencesLinkMsg+"'", "Clicked On - '"+ PreferencesLinkMsg + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			Thread.sleep(1000);
		}
		catch (Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Click On - '" + PreferencesLinkMsg+"'", "Clicked On - '"+ PreferencesLinkMsg + "' - UnSuccessfull");

		}
		
	}
	
	/**
	 * MethdName :SelectClassisTheme
	 * SelectClassisTheme Method selects ClassicTheme and then Revert to Default Theme 
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	
	public static void SelectClassisTheme()throws InterruptedException{
		System.out.println("In Method ::: SelectClassisTheme");
		try{
			//select Classic Theme from Dropdwon 
			WebElement ClassiscTheme = driver.findElement(By.xpath("//select[@id='ThemesList']/option[contains(text(),'Classic')]"));
			ClassiscTheme.click();		
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PreferencesClassicMsg+"'", "Clicked On - '"+ PreferencesClassicMsg + "' - Successfull");
			driver.findElement(By.xpath(PreferencesApplyThemeLink)).click();;
			System.out.println("themeSelect is 2 "+driver.findElement(By.xpath(PreferencesApplyThemeLink)).getText());
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PreferencesApplyThemeLinkMsg+"'", "Clicked On - '"+ PreferencesApplyThemeLinkMsg + "' - Successfull");
			ReportManager.ReportEvent(TestStatus.PASS, "Apply - '" + PreferencesClassicMsg+"'", "Applied - '"+ PreferencesClassicMsg + "' - Successfull");
			//Again change it to Default Theme
			Thread.sleep(25000);
			System.out.println("Started to Revert to Default");
			WebElement DefaultTheme = driver.findElement(By.xpath(PreferencesDefault));
			System.out.println("Started to Revert to Default 1 ::: "+DefaultTheme.getText());
			System.out.println("Started to Revert to Default 1 ::: "+DefaultTheme.getTagName());
			DefaultTheme.click();		
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + PreferencesDefaultMsg+"'", "Clicked On - '"+ PreferencesDefaultMsg + "' - Successfull");
			driver.findElement(By.xpath(PreferencesApplyThemeLink)).click();;
			ReportManager.ReportEvent(TestStatus.PASS, "Apply - '" + PreferencesDefaultMsg+"'", "Applied - '"+ PreferencesDefaultMsg + "' - Successfull");
		}
		catch(Exception ex){
			
		}
	}
	
	/**
	 * MethodName :verifyDNSname
	 * verifyDNSname Method Verifies DNS name of Created Cluster 
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public static void verifyDNSname()throws InterruptedException{
		System.out.println("In Method verifyDNSname::::" );
		String commonName =  null;
		try {
			driver.findElement(By.id("ManageCertificateLink_LinkText")).click();
			//driver.findElement(By.id(WebClusterManageCertificatesLink)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WebClusterManageCertificatesLinkMsg+"'", "Clicked On - '"+ WebClusterManageCertificatesLinkMsg + "' - Successfull");
			driver.findElement(By.id("InstallSSCertLink_LinkText")).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WebClusterInstallSelfSignedCerificateLinkMsg+"'", "Clicked On - '"+ WebClusterInstallSelfSignedCerificateLinkMsg + "' - Successfull");
			//Get Common Name 
			commonName =driver.findElement(By.id("txtCN")).getAttribute("value");
			if(GlobalVariables.WebClusterServerDNSName.equalsIgnoreCase(commonName)){
			ReportManager.ReportEvent(TestStatus.PASS, "Verify DNSName  - '" + GlobalVariables.WebClusterServerDNSName+"'", "Veryfied DNS Name - '"+ commonName + "' - Successfull");
			}
		}
		catch(Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Verifying DNSName  - '" + GlobalVariables.WebClusterServerDNSName+"'", "Verified DNS Name - '"+ commonName + "' - UnSuccessfull");
			System.out.println("The excpetion is due to "+ex.toString());
			
		}
	}
	
	// Added By Arif
	// Modified Date- 25th Jan 2019
	// This Method will be used to check if the cluster is in the specified state
	public static void ClusterSpecifiedStatusCheck(String CLUSTER, String STATE) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// driver.findElement(By.name(CLUSTER)).click();
			driver.findElement(By.partialLinkText(CLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTER + "'",
					"Clicked On - '" + CLUSTER + "' - Successfull");
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(CHECKCLUSTERSTATUSONLINE)).getText().contains(STATE)) {
				ReportManager.ReportEvent(TestStatus.PASS, "Verify Cluster Status - '" + STATE + "'",
						"Verified Cluster Status- '" + STATE + "' - Successfull");
			} else {
				String msg = "Failed to verify cluster state:";
				ReportManager.ReportEvent(TestStatus.FAILED, "Verify message - '" + msg + STATE + "'",
						"Verified message - '" + msg + STATE + "'");
			}
		} catch (Exception ex) {
			String msg = "Failed to verify cluster state:";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + STATE + "'",
					"Verified message - '" + STATE + "'");
		}
	}

	// Added By Arif
	// Modified Date- 29th Jan 2019
	// This Method will be used to install certificate for cluster
	public static void InstallCertificatesForCluster(String CLUSTER) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");

			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			// driver.findElement(By.name(CLUSTER)).click();
			driver.findElement(By.partialLinkText(CLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTER + "'",
					"Clicked On - '" + CLUSTER + "' - Successfull");
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(MANAGECERTIFICATELINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MANAGECERTIFICATELINKMSG + "'",
					"Clicked On - '" + MANAGECERTIFICATELINKMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(INSTALLSELFSIGNEDCERTIFICATELINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + INSTALLSELFSIGNEDCERTIFICATEMSG + "'",
					"Clicked On - '" + INSTALLSELFSIGNEDCERTIFICATEMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(INSTALLSELFSIGNEDCERTIFICATELINK)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + INSTALLSELFSIGNEDCERTIFICATEMSG + "'",
					"Clicked On - '" + INSTALLSELFSIGNEDCERTIFICATEMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(COMMONNAMETXT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + COMMONNAMEMSG + "'",
					"Clicked On - '" + COMMONNAMEMSG + "' - Successfull");
			driver.findElement(By.xpath(ORGANIZATIONTXT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ORGANIZATIONMSG + "'",
					"Clicked On - '" + ORGANIZATIONMSG + "' - Successfull");
			driver.findElement(By.xpath(LOCALITYTXT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + LOCALITYMSG + "'",
					"Clicked On - '" + LOCALITYMSG + "' - Successfull");
			driver.findElement(By.xpath(STATETXT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + STATEMSG + "'",
					"Clicked On - '" + STATEMSG + "' - Successfull");
			driver.findElement(By.xpath(COUNTRYTXT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + COUNTRYMSG + "'",
					"Clicked On - '" + COUNTRYMSG + "' - Successfull");
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(OKBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + OKBUTTONMSG + "'",
					"Clicked On - '" + OKBUTTONMSG + "' - Successfull");

		} catch (Exception ex) {
			String msg = "Failed to verify cluster state:";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + "aaa" + "'",
					"Verified message - '" + "aaa" + "'");
		}
	}

	public static void CheckButtonWork() {
		try {
			Thread.sleep(1000 * 2);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(STARTUP)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + STARTUPTASKMSG + "'",
					"Clicked On - '" + STARTUPTASKMSG + "' - Successfull");
			Thread.sleep(1000 * 2);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(REFRESHBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + REFRESHBUTTONMSG + "'",
					"Clicked On - '" + REFRESHBUTTONMSG + "' - Successfull");
			Thread.sleep(1000 * 2);
			if (driver.findElement(By.xpath(STARTUPMESSAGE)).getText().contains("Getting Started")) {
				ReportManager.ReportEvent(TestStatus.PASS,
						"Verify message after refresh button click - '" + STARTUPMESSAGETXT + "'",
						"Verified message after refresh button click - '" + STARTUPMESSAGETXT + "' - Successfull");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Failed To verfiy Refresh Button Click - '" + STARTUPMESSAGETXT + "'",
						"Verified message - '" + STARTUPMESSAGETXT + "'");
				fail();
			}
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(WEBCLUSTER)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
					"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
			Thread.sleep(1000 * 2);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(BACKBUTTON)).click();
			ReportManager.ReportEvent(TestStatus.PASS,
					"Click On - '" + BACKBUTTONMSG + "'" + " on WebCluster Page and see if gets back to Startup Tasks ",
					"Clicked On - '" + BACKBUTTONMSG + "'"
							+ " on WebCluster Page and see if gets back to Startup Tasks " + "  - Successfull");
			Thread.sleep(1000 * 2);
			if (driver.findElement(By.xpath(STARTUPMESSAGE)).getText().contains("Getting Started")) {
				ReportManager.ReportEvent(TestStatus.PASS,
						"Verify message after cancel button click - '" + STARTUPMESSAGETXT + "'",
						"Verified message after cancel button click - '" + STARTUPMESSAGETXT + "' - Successfull");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Failed To verfiy calcel button click - '" + STARTUPMESSAGETXT + "'",
						"Verified message after cancel button click - '" + STARTUPMESSAGETXT + "'");
				fail();
			}
		} catch (Exception ex) {
			String msg = "Failed to verify Refresh/Cancel Button works:";
			ReportManager.ReportEvent(TestStatus.FAIL, msg, msg);
			fail();
		}
	}

	// Added By Arif
	// Modified Date- 01th Jan 2019
	// Method is used to cancel Web Cluster creation message, accepts parameters as
	// CLUSTERNAME, CLUSTERIPADDR, CLUSTERSUBMASK and GATEWAYIP
	public static void ClusterCancel(String CLUSTERNAME, String CLUSTERIPADDR, String CLUSTERSUBMASK,
			String GATEWAYIP) {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(STARTUP)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + STARTUPMESSAGETXT + "'",
					"Clicked On - '" + STARTUPMESSAGETXT + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(CREATECLUSTERSTARTUP)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CREATECLUSTERSTARTUPMSG + "'",
					"Clicked On - '" + CREATECLUSTERSTARTUPMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERNXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERNXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERNXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERNAMETXT)).sendKeys(CLUSTERNAME);
			driver.findElement(By.xpath(CLUSTERNAMENXTBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERNAMENXTBTNMSG + "'",
					"Clicked On - '" + CLUSTERNAMENXTBTNMSG + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CLUSTERIPADDRTXT)).sendKeys(CLUSTERIPADDR);
			driver.findElement(By.xpath(CLUSTERSUBNETMASKTXT)).sendKeys(CLUSTERSUBMASK);
			driver.findElement(By.xpath(CLUSTERGATEWAYIPTXT)).sendKeys(GATEWAYIP);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.switchTo().frame(0);
			driver.findElement(By.xpath(CANCELCLUSTERCREATIONBTN)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CANCELCLUSTERCREATIONBTNMSG + "'",
					"Clicked On - '" + CANCELCLUSTERCREATIONBTNMSG + "' - Successfull");

		} catch (Exception ex) {
			String msg = "Failed to create Cluster";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
			fail();
		}

	}

	// Added By Arif
	// Modified Date- 01th Jan 2019
	// Method Created to chesck Web Cluster cancel button works
	public static void CheckAfterCancel(String MESSAGE) {
		try {
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			if (driver.findElement(By.xpath(STARTUPMESSAGE)).getText().contains(MESSAGE)) {
				ReportManager.ReportEvent(TestStatus.PASS,
						"Verify message after refresh button click - '" + STARTUPMESSAGETXT + "'",
						"Verified message after refresh button click - '" + STARTUPMESSAGETXT + "' - Successfull");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL,
						"Failed To verfiy Refresh Button Click - '" + STARTUPMESSAGETXT + "'",
						"Verified message - '" + STARTUPMESSAGETXT + "'");
				fail();
			}
		} catch (Exception ex) {
			String msg = "Failed to validate message:";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + STARTUPMESSAGETXT + "'",
					"Verified message - '" + STARTUPMESSAGETXT + " -Failed" + "'");
			fail();
		}

	}

	// Added By Arif
	// Modified Date- 01th Jan 2019
	// Method is used to validate help links
	public static void ValidateHelpLinks(String TEXT, String ELEMENT) {

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			if (ELEMENT.contains("Startup")) {
				driver.findElement(By.xpath(STARTUP)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + STARTUPMESSAGETXT + "'",
						"Clicked On - '" + STARTUPMESSAGETXT + "' - Successfull");
				Thread.sleep(5 * 1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("fraBanner");
				// driver.switchTo().frame(0);
				driver.findElement(By.xpath(BUTTONHELPLINK)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BUTTONHELPLINKMSG + "'",
						"Clicked On - '" + BUTTONHELPLINKMSG + "' - Successfull");
			} else {
				driver.findElement(By.xpath(WEBCLUSTER)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
						"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
				Thread.sleep(5 * 1000);
				driver.switchTo().defaultContent();
				driver.switchTo().frame("fraBanner");
				// driver.switchTo().frame(0);
				driver.findElement(By.xpath(BUTTONHELP)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BUTTONHELPMSG + "'",
						"Clicked On - '" + BUTTONHELPMSG + "' - Successfull");
			}

			Thread.sleep(5 * 1000);
			Set<String> handles = driver.getWindowHandles();
			Iterator<String> it = handles.iterator();
			String parent = it.next();
			String child = it.next();
			if (driver.switchTo().window(child).getCurrentUrl().contains(TEXT) && ELEMENT.contains("Startup")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + "Startup Tasks help link" + "'",
						"Clicked On - '" + "Startup Tasks help link" + "' - Successfull");
			} else if (driver.switchTo().window(child).getCurrentUrl().contains(TEXT) && ELEMENT.contains("Cluster")) {
				ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + "Web Clusters button help link" + "'",
						"Clicked On - '" + "Web Clusters button help link" + "' - Successfull");
			} else {
				ReportManager.ReportEvent(TestStatus.FAIL, "Click On - '" + "help" + "'",
						"Clicked On - '" + "help" + "' - Failed");
			}
			driver.close();
			driver.switchTo().window(parent);
		} catch (Exception ex) {
			String msg = "Failed to validate Help Link";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + msg + "'",
					"Verified message - '" + msg + "'");
			fail();
		}

	}

	// Added By Ravi
	 // Common Method to delete the Web Cluster
	public static void DeleteWebCluster(String ClusterName) throws InterruptedException
	{
		driver.switchTo().defaultContent();
		driver.switchTo().frame("leftNav");
		driver.findElement(By.xpath(WEBCLUSTER)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERMSG + "'",
				"Clicked On - '" + WEBCLUSTERMSG + "' - Successfull");
		Thread.sleep(2 * 1000);
		driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
		String clusterXpath = ".//*[@id=\"GroupsGrid_ctl03_chkSelection\"]";
		String RemoveXPath = ".//*[@id=\"ButtonRemove\"]";
		String YesButton = ".//*[@id=\"ConfirmPanel1_YesButton\"]";
		String StatusMessage = ".//*[@id=\"StatusPanel1_StatusLink\"]";
		driver.findElement(By.xpath(clusterXpath)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "select checkbox of cluster - '" + ClusterName + "'",
				"Selected checkbox of - '" + ClusterName + "' - Successfull");
		Thread.sleep(1000);
		driver.findElement(By.xpath(RemoveXPath)).click();
		ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + "Remove button" + "'",
				"Clicked on - '" + "Remove button" + "' - Successfull");
		driver.findElement(By.xpath(YesButton)).click();
		Thread.sleep(2000);
		String message = driver.findElement(By.xpath(StatusMessage)).getText();
		Thread.sleep(3000);
		ReportManager.ReportEvent(TestStatus.PASS, "Verify message - '" + message + "'",
				"verify message - '" + message + "' - Successfull");	
	}
	
	// Added By Arif
	// Modified Date- 01th Jan 2019
	// Method is used to validate help links
	public static void ClearAnAlert() {
		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame("leftNav");
			driver.findElement(By.xpath(MNGPRT)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + MNGLINK + "'",
					"Clicked On - '" + MNGLINK + "' - Successfull");
			Thread.sleep(2 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			driver.findElement(By.xpath(ALERTVIEW)).click();
			ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AlretLink + "'",
					"Clicked On - '" + AlretLink + "' - Successfull");
			Thread.sleep(5 * 1000);
			driver.switchTo().defaultContent();
			driver.switchTo().frame("content");
			List<WebElement> CheckBoxes = driver.findElements(By.className("column-datagrid-checkbox"));
			int numberOfBoxes = CheckBoxes.size();
			String CHECKBOXXPATH = "";
			for (int i = 3; i <= numberOfBoxes + 3; i++) {
				if (i < 10) {
					CHECKBOXXPATH = "//*[@id=\"AlertsGrid_ctl" + "0" + i + "_chkSelection\"]";
				} else {
					CHECKBOXXPATH = "//*[@id=\"AlertsGrid_ctl" + i + "_chkSelection\"]";
				}
				if (driver.findElement(By.xpath(CHECKBOXXPATH)).isEnabled()) {
					driver.findElement(By.xpath(CHECKBOXXPATH)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + "Select Alert" + "'",
							"Clicked On - '" + "Select Alert" + "' - Successfull");
					Thread.sleep(2 * 1000);
					driver.switchTo().defaultContent();
					driver.switchTo().frame("content");
					driver.findElement(By.xpath(CLEARBUTTON)).click();
					ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLEARBUTTONMSG + "'",
							"Clicked On - '" + CLEARBUTTONMSG + "' - Successfull");
					Thread.sleep(10 * 1000);
					if (driver.findElement(By.xpath(ALERT)).getText().contains("cleared")) {
						ReportManager.ReportEvent(TestStatus.PASS, "Verify message:" + ALERTMSG + "'",
								"Verified Message: - '" + ALERTMSG + "' - Successfull");
						break;
					} else {
						ReportManager.ReportEvent(TestStatus.FAIL, "No Alerts to delete" + "'",
								"No Alerts to delete" + "' - FAIL");
						fail();
					}
				} else if (driver.findElement(By.xpath(CHECKBOXXPATH)).isEnabled() == false) {
					continue;
				} else {
					ReportManager.ReportEvent(TestStatus.FAIL, "No Alerts to delete" + "'",
							"No Alerts to delete" + "' - FAIL");
					fail();
				}
			}

		} catch (Exception ex) {
			String msg = "Failed delete a alert";
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify message - '" + "Delete Alert" + "'",
					"Verified message - '" + "Failed to delete Alert" + "'");
			fail();
		}
		

	}
	/**
	 * MethodName :removeWebPartitonsFromOfflineCluster
	 * removeWebPartitonsFromOfflineCluster Removed offline cluster
	 * @author Channabasappa M
	 * @throws InterruptedException
	 */
	public void removeWebPartitonsFromOfflineCluster()throws InterruptedException {
		String statusmessage = null;
		try {
			int numberofMembers =Integer.parseInt(driver.findElement(By.id(WebClusterMembers)).getText());
			if(numberofMembers ==0)
			{
				ReportManager.ReportEvent(TestStatus.PASS, "Remove partitions from cluster- '" + GlobalVariables.ClusterName+"'", "Unable to  to remove the number of partitions are - '"+numberofMembers+ "' - Successfull");
			}
			else {
				driver.findElement(By.id(WebClusterManageMembers)).click();
				ReportManager.ReportEvent(TestStatus.PASS, "Click on - '" + WebClusterManageMembersMsg+"'", "Clicked on - '"+ WebClusterManageMembersMsg + "' - Successful");
				//Select members of cluster to remove DevicesGrid_ctl02_Group
				int count =0;
				List <WebElement> webPartitions = driver.findElements(By.xpath(CheckBoxLink));
				int i =2;
				int countmembers=0;
				System.out.println("Size of checkbox List "+webPartitions.size());
				for ( WebElement el : webPartitions ) {
					String clusterNameId="DevicesGrid_ctl0"+i+"_Group";                   //DevicesGrid_ctl02_Group
					String memberOf = driver.findElement(By.id(clusterNameId)).getText();
					if(memberOf.equalsIgnoreCase(GlobalVariables.ClusterName)){
						if ( !el.isSelected() ) {
							el.click();
							countmembers++;
						}
					}
					i++;
				}
				ReportManager.ReportEvent(TestStatus.PASS, "Select Partitions to remove from  clluster- '" + GlobalVariables.ClusterName+"'", "Selected Partitions to remove from- '"+ GlobalVariables.ClusterName + "' - Successfull");
				driver.findElement(By.xpath(REMOVEBUTTON)).click();
				driver.findElement(By.xpath(CONFIRMYESBTN)).click();
				statusmessage = driver.findElement(By.xpath(STATUSMESSAGE11)).getText();
				ReportManager.ReportEvent(TestStatus.PASS, "Verify status message - '" + statusmessage+"'", "Verified  status message - '"+ statusmessage + "' - Successful");
				driver.findElement(By.xpath(REFRESHXPATH)).click();
			}
		}
		catch(Exception ex){
			ReportManager.ReportEvent(TestStatus.FAIL, "Verify status message - '" + statusmessage+"'", "Verified  status message - '"+ statusmessage + "' - Successful");
		}
		
	}
	/**
	 * Method Name :TestCluster_Create
	 * TestCluster_Create  will create a web cluster named Test
	 * @Author Adarsh 
	 */
	public static void TestCluster_Create() throws InterruptedException
    {
       
        driver.switchTo().defaultContent();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Accessing Cluster Link
        
        driver.findElement(By.xpath(CLUSTERCREATE)).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CLUSTERCREATELINK+"'", "Clicked On - '"+ CLUSTERCREATELINK + "' - Successful");
      
        //code for switching frame
        WebElement WC_frame = driver.findElement(By.xpath(WEBCLUSTERFRAME));
        driver.switchTo().frame(WC_frame);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + WEBCLUSTERFRAMELink+"'", "Clicked On - '"+ WEBCLUSTERFRAMELink + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
       // Accessing NEXTButton
        
        driver.findElement(By.xpath(NEXTBUTTON)).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK+"'", "Clicked On - '"+ NEXTLINK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Thread.sleep(5000);
         
         //Providing cluster name
         
        driver.findElement(By.xpath(NAMETEXTBOX)).sendKeys(WEBCLUSTERTEST);
        ReportManager.ReportEvent(TestStatus.PASS, "Enter cluster name as - '" + WEBCLUSTERTEST+"'", "Entered - '"+ WEBCLUSTERTEST + "' - Successful");
        driver.findElement(By.xpath(NEXTBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK+"'", "Clicked On - '"+ NEXTLINK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        ReportManager.ReportEvent(TestStatus.PASS, WEBCLUSTERTEST +"'",WEBCLUSTERTEST + "' - Successfully");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Thread.sleep(5000);
        driver.findElement(By.xpath(IPTEXTBOX)).sendKeys(IPADDRESS);
        ReportManager.ReportEvent(TestStatus.PASS, "Enter IP Address as - '" + IPADDRESS+"'", "Entered - '"+ IPADDRESS + "' - Successful");
        driver.findElement(By.xpath(SUBNETTEXTBOX)).sendKeys(SUBNET);
        ReportManager.ReportEvent(TestStatus.PASS, "Enter Subnet as - '" + SUBNET+"'", "Entered - '"+ SUBNET + "' - Successful");
        
        driver.findElement(By.xpath(GATEIPTEXTBOX)).sendKeys(GATEIP);
        ReportManager.ReportEvent(TestStatus.PASS, "Enter GATIP as - '" + GATEIP+"'", "Entered - '"+ GATEIP + "' - Successful");
        driver.findElement(By.xpath(NEXTBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK+"'", "Clicked On - '"+ NEXTLINK + "' - Successful");
        
        
        
        driver.findElement(By.xpath(NEXTBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK+"'", "Clicked On - '"+ NEXTLINK + "' - Successful");
        driver.findElement(By.xpath(NEXTBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + NEXTLINK+"'", "Clicked On - '"+ NEXTLINK + "' - Successful");
        System.out.println("webcluster successfully created");
        String check= driver.findElement(By.xpath(STATUS)).getText();
       // Message.replaceAll("\\s+","");
        System.out.println("Messgae"+check);
        if(check.endsWith("successfully created."))
            {
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                ReportManager.ReportEvent(TestStatus.PASS, STATUSLink+"'", STATUSLink + "' - Successfully");
                System.out.println("Message Displayed is correct :"+ check);
            }
            else
            {
               ReportManager.ReportEvent(TestStatus.FAIL, STATUSLink+"'", STATUSLink + "' - Successfully");
               System.out.println("Message Dispalyed is incorrect:" + check);
            }
        driver.findElement(By.xpath(CANCELBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + CANCELBUTTONLink+"'", "Clicked On - '"+ CANCELBUTTONLink + "' - Successful");
        //Providing Duplicate cluster name
        
       
        
        
      
        //code for getting invalid text
        
        //String Message= driver.findElement(By.xpath(INVALIDIP)).getText();
        //if(Message.equalsIgnoreCase(INVALID))
        //{
        
          //  driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
          //  ReportManager.ReportEvent(TestStatus.PASS, INVALIDMESSAGE+"'", INVALIDMESSAGE + "' - Successfully");
          //  System.out.println("Message Displayed is correct :"+ Message);
        //}
        //else
        //{
           //ReportManager.ReportEvent(TestStatus.FAIL, INVALIDMESSAGE+"'", INVALIDMESSAGE + "' - Successfully");
           //System.out.println("Message Dispalyed is incorrect:" + Message);
        //}
        
    }
	/**
	 * Method Name :BringOnline
	 * BringOnline  will bring patition Online
	 * @Author Adarsh 
	 */
	public static void BringOnline() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ModifyFirstclusterlink+"'", "Clicked On - '"+ ModifyFirstclusterlink + "' - Successful");
        driver.findElement(By.xpath(BringOnlineLink)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + BRINGONlINE+"'", "Clicked On - '"+ BRINGONlINE + "' - Successful");
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.findElement(By.xpath(ViewStatusLink)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ViewStatus+"'", "Clicked On - '"+ ViewStatus + "' - Successful");
    }
	/**
	 * Method Name :BringOffline
	 * BringOffline  will bring cluster Oflline
	 * @Author Adarsh 
	 */
	public static void BringOffline() throws InterruptedException
    {
		//Accessing Cluster Link
		
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ModifyFirstclusterlink+"'", "Clicked On - '"+ ModifyFirstclusterlink + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(TakeOffline)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + TakeOfflinelink+"'", "Clicked On - '"+ TakeOfflinelink + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(ViewStatusLink)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ViewStatus+"'", "Clicked On - '"+ ViewStatus + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
	/**
	 * Method Name :BringOffline1
	 * BringOffline1  will bring patition Oflline
	 * @Author Adarsh 
	 */
	public static void BringOffline1() throws InterruptedException
    {
		//Accessing Cluster Link
		
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ModifyFirstclusterlink+"'", "Clicked On - '"+ ModifyFirstclusterlink + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(TakeOffline1)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + TakeOfflinelink+"'", "Clicked On - '"+ TakeOfflinelink + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(ViewStatusLink)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ViewStatus+"'", "Clicked On - '"+ ViewStatus + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
	/**
	 * Method Name :RemoveWC
	 * RemoveWC  will remove web cluster
	 * @Author Adarsh 
	 */
	public static void RemoveWC() throws InterruptedException
    {
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
		driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        //Accessing Cluster Link
        
        //driver.findElement(By.xpath(CLUSTERCREATE)).click();
        driver.findElement(By.xpath(SelectCluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "'" + SelectClusterClick+"'", "Clicked On - '"+ SelectClusterClick + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(REMOVEBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + REMOVEBUTTONCLICK+"'", "Clicked On - '"+ REMOVEBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);        
    }
	/**
	 * Method Name :AddWP
	 * AddWP  will Add web partition
	 * @Author Adarsh 
	 */
	public static void AddWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        //Add partition to the web cluster
        driver.findElement(By.xpath(AddMembers)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AddMembersLink+"'", "Clicked On - '"+ AddMembersLink + "' - Successful");
        driver.findElement(By.xpath(SelectMember)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SelectMemberLink+"'", "Clicked On - '"+ SelectMemberLink + "' - Successful");
        driver.findElement(By.xpath(AssignPM)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + AssignPMLink+"'", "Clicked On - '"+ AssignPMLink + "' - Successful");
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);        
    }
	/**
	 * Method Name :ShutWP
	 * ShutWP  will Shutdownthe web partition
	 * @Author Adarsh 
	 */
	public static void ShutWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);     
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        //Add partition to the web cluster
        driver.findElement(By.xpath(ManageMembers)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ManageMembersLink+"'", "Clicked On - '"+ ManageMembersLink + "' - Successful");
        driver.findElement(By.xpath(SelectMember)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SelectMemberLink+"'", "Clicked On - '"+ SelectMemberLink + "' - Successful");
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath(".//*[@id=\"ShutdownLink_LinkText\"]")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
        
    }
	/**
	 * Method Name :RebuildWP1
	 * RebuildWP1  will Rebuild the web partition
	 * @Author Adarsh 
	 */
	public static void RebuildWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);     
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        //Add partition to the web cluster
        driver.findElement(By.xpath(ManageMembers)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ManageMembersLink+"'", "Clicked On - '"+ ManageMembersLink + "' - Successful");
        driver.findElement(By.xpath(SelectMember)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SelectMemberLink+"'", "Clicked On - '"+ SelectMemberLink + "' - Successful");
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath("//*[@id=\"RebuildLink_LinkText\"]")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
        
    }
	/**
	 * Method Name :RebuildWP1
	 * RebuildWP1  will Rebuild the web partition
	 * @Author Adarsh 
	 */
	public static void RebuildWP1() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WP)).click(); 
        driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);     
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        
        driver.findElement(By.xpath(SelectMember)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SelectMemberLink+"'", "Clicked On - '"+ SelectMemberLink + "' - Successful");
        //driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        //driver.findElement(By.xpath("//*[@id=\"RebuildLink_LinkText\"]")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
        
    }
	/**
	 * Method Name :RemWP
	 * RemWP  will remove the web partition
	 * @Author Adarsh 
	 */
	public static void RemWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        //Add partition to the web cluster
        driver.findElement(By.xpath(ManageMembers)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ManageMembersLink+"'", "Clicked On - '"+ ManageMembersLink + "' - Successful");
        driver.findElement(By.xpath(SelectMember)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + SelectMemberLink+"'", "Clicked On - '"+ SelectMemberLink + "' - Successful");
        driver.findElement(By.xpath(REMOVEBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + REMOVEBUTTONCLICK+"'", "Clicked On - '"+ REMOVEBUTTONCLICK + "' - Successful");
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
                
    }
	/**
	 * Method Name :RestartWP
	 * RestartWP  will restart the web partition
	 * @Author Adarsh 
	 */
	public static void RestartWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        //Add partition to the web cluster
        driver.findElement(By.xpath(ManageMembers)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + ManageMembersLink+"'", "Clicked On - '"+ ManageMembersLink + "' - Successful");
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath("//*[@id=\"RestartLink_LinkText\"]")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(55, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(ViewStatusLink)).click();
        
      
                
    }
	/**
	 * Method Name :RemWCWP
	 * RemWCWP  will remove the webcluster having partition
	 * @Author Adarsh 
	 */
	public static void RemWCWP() throws InterruptedException
    {
		//Accessing Cluster Link
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(SelectCluster)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "'" + SelectClusterClick+"'", "Clicked On - '"+ SelectClusterClick + "' - Successful");
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(REMOVEBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + REMOVEBUTTONCLICK+"'", "Clicked On - '"+ REMOVEBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(YESBUTTON)).click();
        ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + YESBUTTONCLICK+"'", "Clicked On - '"+ YESBUTTONCLICK + "' - Successful");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
                
    }
	/**
	 * Method Name :stopWP
	 * stopWP  will make the Webpartition offlne
	 * @Author Adarsh 
	 */
	public static void stopWP() throws InterruptedException
    {
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        //ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        driver.findElement(By.xpath(ManageMembers)).click();
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath(".//*[@id='TakeOfflineWaitLink_LinkText']")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        //ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        driver.findElement(By.xpath(ManageMembers)).click();
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_chkSelection']")).click();
        driver.findElement(By.xpath(REMOVEBUTTON)).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        
                
    } 
	/**
	 * Method Name :OfflineWP
	 * OfflineWP  will make the Webpartition offline
	 * @Author Adarsh 
	 */
	public static void OfflineWP() throws InterruptedException
    {
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        //ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        driver.findElement(By.xpath(ManageMembers)).click();
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath(".//*[@id='TakeOfflineWaitLink_LinkText']")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        
		
    } 
	/**
	 * Method Name :DrainstopWP
	 * DrainstopWP  will Drainstop the Webpartition
	 * @Author Adarsh 
	 */
	
	public static void DrainstopWP() throws InterruptedException
    {
		driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        //ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        driver.findElement(By.xpath(ManageMembers)).click();
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_Name']")).click();
        driver.findElement(By.xpath(".//*[@id='TakeOfflineNowLink_LinkText']")).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
        driver.switchTo().defaultContent();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		driver.switchTo().frame("leftNav"); 
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
        //Thread.sleep(5000); 
        driver.findElement(By.xpath(WC)).click(); 
             
        driver.switchTo().defaultContent();
        driver.switchTo().frame("content");
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(Firstcluster)).click();
        //ReportManager.ReportEvent(TestStatus.PASS, "Click On - '" + Firstclusterlink+"'", "Clicked On - '"+ Firstclusterlink + "' - Successful");
        
        
        driver.findElement(By.xpath(ModifyFirstcluster)).click();
        driver.findElement(By.xpath(ManageMembers)).click();
        
        driver.findElement(By.xpath(".//*[@id='DevicesGrid_ctl02_chkSelection']")).click();
        driver.findElement(By.xpath(REMOVEBUTTON)).click();
        driver.findElement(By.xpath(YESBUTTON)).click();
                 
    }  
	
}