package com.unisys.eportal.ClusterNPartitionOfflineOnline;

import static com.unisys.eportal.manager.LoginPageData.ePortalUrl;
import static org.junit.Assert.fail;

import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.unisys.eportal.manager.Common_Methods;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ClusterNPartitionOfflineOnline {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;

	public String Chrome;

	@Given("^User is on ePortal home Page$")
	public void Open_WebPartition() throws Throwable {
		System.out.println("Opening Browser");
		System.setProperty("webdriver.chrome.driver",
				"C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

//code for reporting 

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(),
				ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;
		GlobalVariables.RM.createHTMLResultTemplate("BRING OFFLINE ONLINE",
				ReportManager.getDateFormat(ReportManager.vDatetype2));
		RM.updateReports(updateValue.bName, Chrome, "");
		RM.writeTestCaseNameToExcel("BRING OFFLINE ONLINE", Chrome);

//Code for reporting
		CM = new Common_Methods(this.driver);
		CM.openBrowser(ePortalUrl);
		// Runtime.getRuntime().exec("C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\autoit\\cr.exe");
		System.out.println("Now in Home Page");

	}

	@When("^User creates cluster and adds partition$")
	public void User_creates_cluster_and_adds_partition() throws Throwable {
		Thread.sleep(1000 * 2);
		CM.CreateCluster("WC-Script", "10.62.177.138", "255.255.255.192", "10.62.177.190");
		Thread.sleep(1000 * 5);
		CM.ClusterSpecifiedStatusCheck("WC-Script", "Offline");
		Thread.sleep(1000 * 1);
		CM.AddAllPartitionsToCluster("WC-Script");
		Thread.sleep(1000 * 10);
		CM.WPSpecifiedStatusCheck("Ready/Offline");
		Thread.sleep(1000 * 2);
	}

	@Then("^Verify Offline and Online Status of Cluster and Partition$")
	public void Verify_Offline_and_Online_Status_of_Cluster_and_Partition() throws Throwable {
		System.out.println("Now Bring Cluster Online");
		Thread.sleep(1000 * 2);
		CM.BringClusterOnline("WC-Script");
		Thread.sleep(1000 * 5);
		CM.ClusterSpecifiedStatusCheck("WC-Script", "Online");
		Thread.sleep(1000 * 10);
		CM.WPSpecifiedStatusCheck("Ready/Online");
		// Execution of reporting

		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,
				CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis() - vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "", String.valueOf(RM.getFinalResult()));

//Excel Reporting
		ReportManager.ReportSummaryEvent(TestStatus.PASS, GlobalVariables.PartitionName);
		// RM.writeToSummary(String.valueOf(RM.getFinalResult()),GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());
		driver.close();
	}

}