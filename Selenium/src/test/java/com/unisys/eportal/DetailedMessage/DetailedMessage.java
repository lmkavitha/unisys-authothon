package com.unisys.eportal.DetailedMessage;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.unisys.eportal.manager.Common_Methods;
import static com.unisys.eportal.manager.LoginPageData.*;
import com.unisys.eportal.manager.Initialize;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.updateValue;
import com.unisys.eportal.manager.Initialize.TestStatus;
public class DetailedMessage {

	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;
	public String Chrome;
	
	//added By Ravi Kumar
	// Date- 9th Jan 2019
	// TCD- CPE-1124
	// Description- view The detailed alert message at once in the manager.	

@Given("^New User is on ePortal Manager Home Page to see display Alert messages$")
public void I_want_to_open_eportal_Manager_To_see_alert_message() throws Throwable {
	System.out.println("Opening Browser");
	System.setProperty("webdriver.chrome.driver", "D:\\Automation\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.manage().window().maximize();
	
	
//code for reporting start	
     System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
	 GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
	 RM = GlobalVariables.RM;
	 GlobalVariables.PartitionName = "ePortal_Display_Detailed_Message"; 
	 GlobalVariables.SummaryName = "ePortal_Summary"; 
	 GlobalVariables.RM.createHTMLSummaryTemplate(GlobalVariables.SummaryName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
	 RM.updateReports(updateValue.bName, Chrome, "");
	 RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);	
//Code for Opening the browser
	CM = new Common_Methods(this.driver);
	 CM.openBrowser(ePortalUrl);
	System.out.println("Browser Launched successfully");
  
}


@When("^User Navigates to manger partion to see alert messages$")
public void User_Navigate_to_view_alert_messages() throws Throwable {
	 Thread.sleep(10000);
	CM.DisplayMessage();
	 Thread.sleep(10000);
	 System.out.println("Test case steps executed successfully");
}
	 
@Then("^Display the detailed message of alert$")
public void Display_detailed_message_of_alerts() throws Throwable {
    // Express the Regexp above with the code you wish you had
    
    
//code for reporting  end	  
		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "",String.valueOf(RM.getFinalResult()));
		//ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		//Excel Reporting
		ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());	
		driver.close();
		System.out.println("Browser closed successfully");
//code for repoting end	
}
}
