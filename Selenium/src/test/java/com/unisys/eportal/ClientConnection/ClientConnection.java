package com.unisys.eportal.ClientConnection;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.unisys.eportal.manager.Common_Methods;
import static com.unisys.eportal.manager.LoginPageData.*;
import com.unisys.eportal.manager.Initialize;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.updateValue;
import com.unisys.eportal.manager.Initialize.TestStatus;



public class ClientConnection {

	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;

	public String Chrome;



	@Given("^User is on ePortal Manager Home Page$")
	public void I_want_to_open_eportal_Manager() throws Throwable {
		System.out.println("Opening Browser");
		System.setProperty("webdriver.chrome.driver", "C:\\ePortalAutomation\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();


		//code for reporting 

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(), ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;
		GlobalVariables.PartitionName = "ePortal_ClientConnection";
		GlobalVariables.DesiredAppstate ="Online";
		GlobalVariables.DeppAppName = "webapplication1";
		GlobalVariables.ClusterName = "WC137";
		GlobalVariables.DepAppUserCode = "EPORTAL";
		GlobalVariables.DepAppPassword="EPORTAL";
		// GlobalVariables.RM.createHTMLSummaryTemplate(GlobalVariables.SummaryName, ReportManager.getDateFormat(ReportManager.vDatetype2));

		GlobalVariables.RM.createHTMLResultTemplate(GlobalVariables.PartitionName, ReportManager.getDateFormat(ReportManager.vDatetype2));
		RM.updateReports(updateValue.bName, Chrome, "");
		RM.writeTestCaseNameToExcel(GlobalVariables.PartitionName, Chrome);

		//Code for reporting
		CM = new Common_Methods(this.driver);
		CM.openBrowser(ePortalUrl);
		//Runtime.getRuntime().exec("C:\\ePortalAutomation\\Automation_v.2.0\\autoit\\cr(1)");
		System.out.println("Now in Home Page");

	}


	@When("^User Navigate to web cluster property page and fetch Virtual Ip$")
	public void User_Navigate_to_web_cluster_property_page_and_fetch_Virtual_Ip() throws Throwable {
		CM.SelectWebclusterPropertyPage(GlobalVariables.ClusterName);
		Thread.sleep(4000);

	}

	@Then("^Access the application with IPaddress and application name$")
	public void Access_the_application_with_IPaddress_and_application_name() throws Throwable {

		Thread.sleep(4000);
		CM.launchWebApplication();	 
		//Execution of repoting 

		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()	- vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "",String.valueOf(RM.getFinalResult()));

		//Excel Reporting
		ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());

		//Execution of repoting
		driver.close();


	}


}