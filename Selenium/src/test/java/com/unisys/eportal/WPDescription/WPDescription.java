package com.unisys.eportal.WPDescription;

import static com.unisys.eportal.manager.LoginPageData.ePortalUrl;

import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.unisys.eportal.manager.Common_Methods;
import com.unisys.eportal.manager.GlobalVariables;
import com.unisys.eportal.manager.ReportManager;
import com.unisys.eportal.manager.Initialize.TestStatus;
import com.unisys.eportal.manager.Initialize.updateValue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class WPDescription {
	public static WebDriver driver;
	private static Common_Methods CM = null;
	private static ReportManager RM = null;
	public long vScriptStartTime = 0;

	public String Chrome;

	@Given("^User is on ePortal home Page initially$")
	public void Open_WebPartition() throws Throwable {
		System.out.println("Opening Browser");
		System.setProperty("webdriver.chrome.driver",
				"C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\ChromeDriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

//code for reporting 

		System.out.println("Before Class :" + Runtime.getRuntime().freeMemory());
		GlobalVariables.RM = new ReportManager(getClass().getSimpleName(),
				ReportManager.getDateFormat(ReportManager.vDatetype8));
		RM = GlobalVariables.RM;
		GlobalVariables.RM.createHTMLResultTemplate("DESCRIPTION OF WEB PARTITION",
				ReportManager.getDateFormat(ReportManager.vDatetype2));
		RM.updateReports(updateValue.bName, Chrome, "");
		RM.writeTestCaseNameToExcel("DESCRIPTION OF WEB PARTITION", Chrome);

//Code for reporting
		CM = new Common_Methods(this.driver);
		CM.openBrowser(ePortalUrl);
		// Runtime.getRuntime().exec("C:\\Eportal_jar_files\\Automation_v.2.0\\Automation_v.2.0\\autoit\\cr.exe");
		System.out.println("Now in Home Page");

	}

	@When("^User navigates to Web Partition page next$")
	public void User_Navigates_To_Web_Partition_Page_next() throws Throwable {
		Thread.sleep(10000);
		CM.WPDescriptionCheck();
		Thread.sleep(10000);

	}

	@Then("^Successfully modified Web Partition description$")
	public void Successfully_modified_Web_Partition_description() throws Throwable {

//Execution of repoting 

		RM.updateReports(updateValue.tEndTime, "", "");
		RM.updateReports(updateValue.execTime,
				CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis() - vScriptStartTime).toString(), "");
		RM.updateReports(updateValue.execStatus, "", String.valueOf(RM.getFinalResult()));

		// RM.SummaryupdateReports(updateValue.tEndTime, "", "");
		// RM.SummaryupdateReports(updateValue.execTime,CM.formatIntoHHMMSS(Calendar.getInstance().getTimeInMillis()
		// - vScriptStartTime).toString(), "");
		// RM.SummaryupdateReports(updateValue.execStatus,
		// "",String.valueOf(RM.getFinalResult()));

//Excel Reporting
		// ReportManager.ReportSummaryEvent(TestStatus.PASS,GlobalVariables.PartitionName);
		// RM.writeToSummary(String.valueOf(RM.getFinalResult()),GlobalVariables.PartitionName);
		RM.updatExcelTestResult(RM.getFinalResult());
		driver.close();

//Execution of repoting 	

	}

}
