package come.unisys.eportal.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.VerifyDNSNameIsReflected",
        features = "classpath:cucumber/VerifyDNSNameIsReflected.feature"
)
public class VerifyDNSNameIsReflectedRunner {

}
