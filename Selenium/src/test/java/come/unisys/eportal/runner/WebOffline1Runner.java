package come.unisys.eportal.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.WebOffline1",
        features = "classpath:cucumber/WebOffline1.feature"
)

public class WebOffline1Runner {

}
