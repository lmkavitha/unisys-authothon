package come.unisys.eportal.runner;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
//added By Ravi Kumar
//Date- 8th Jan 2019
//TCD- CPE-1164
// Description- Bring the specified Cluster Online and accept parameter as clustername
@RunWith(Cucumber.class)
@CucumberOptions(
		glue = "com.unisys.eportal.WebClusterOnline",
        features = "classpath:cucumber/WebClusterOnline.feature"
)
public class WebClusterOnlineRunner {

}
