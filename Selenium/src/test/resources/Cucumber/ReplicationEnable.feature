#Author: mohammed.kankudti@unisys.com
Feature: Manager Partition Replication

  Scenario: Check for replication enable status
    Given User is on ePortal home Page
    When User Navigates to Magager Partition Page and Enable Replication
    Then Check Replication Status in Properties
