Feature: View all systems logs. 
 
Scenario: Successfully Select system logs and download system log file 
    Given New User is on ePortal Manager to view system logs
    When User Navigates to filter logs by system category and severity
    Then View the system logs and download the system log file