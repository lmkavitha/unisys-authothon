package testbase;

import com.jayway.jsonpath.JsonPath;
import data.APIList;
import data.GenericData;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class CucumberRunnerBase {

//    protected RequestResponseHandler requestResponseHandler;
    /**
     * Before suite method initializes the login API token and the session ID
     * for other requests to be executed
     */
    @BeforeSuite
    public void beforeSuite(){
        RestAssured.baseURI = GenericData.URL;
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given();

    }

    /**
     *Before class has to be started to be used when you want to segregate different features and run their scenarios seperately
     * Use Before Class to run and set up something before every feature
     */
    @BeforeClass
    public void beforeClass() {
        // Called before every class
    }

    @AfterSuite
    public void afterSuite() {
    }
}
