package data;

public class APIList {
    public static final String DIAGNOSTIC_LIST="/diagnostic";
    public static final String PARTITION_LIST="/partition";
    public static String SET_RETENTION_PERIOD= "/settings/retention";
    public static String GET_RETENTION_PERIOD="/settings/retention";
    public static String GENERATE_DIAGNOSTIC="/diagnostic";
    public static String DELETE_DIAGNOSTIC="/diagnostic";
    public static String EVENTS_LIST="/events";
    public static String CLEAR_EVENTS="/events/clear";
    public static String APPINFO_LIST="/appinfo";
    public static String CA_ENROLLMENT="/users";
}
