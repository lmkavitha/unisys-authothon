package responseValidation;

import data.GenericData;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsCollectionContaining;
import org.testng.Assert;
import requestResponseHandler.MainRequestResponse;

import java.lang.reflect.GenericArrayType;
import java.util.ArrayList;

import static org.hamcrest.Matchers.not;

public class PartitionsResponse extends MainRequestResponse {

    /**
     * Constructor Function - DO NOT MODIFY
     * @param response
     * @param responseSpecBuilder
     */
    public PartitionsResponse(Response response, ResponseSpecBuilder responseSpecBuilder){
        super(responseSpecBuilder,response);
    }


    public void validatePartitionList(String partition1, String partition2) {
        GenericData.PARTITIONS = response.jsonPath().get("data.PartitionsList.Name");
        MatcherAssert.assertThat(GenericData.PARTITIONS, IsCollectionContaining.hasItem(partition1));
        MatcherAssert.assertThat(GenericData.PARTITIONS, IsCollectionContaining.hasItem(partition2));
    }
}
