package responseValidation;

import data.GenericData;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsCollectionContaining;
import org.testng.Assert;
import requestResponseHandler.MainRequestResponse;

import java.util.ArrayList;

public class DiagnosticsResponse extends MainRequestResponse {

    /**
     * Constructor Function - DO NOT MODIFY
     * @param response
     * @param responseSpecBuilder
     */
    public DiagnosticsResponse(Response response, ResponseSpecBuilder responseSpecBuilder){
        super(responseSpecBuilder,response);
    }


    public void validateDiagnosticListSchema() {
        SchemaValidator.validateSchema("diagnostics/diagnostics-list.json");
    }

    public void checkTheSetRetentionPeriod(String s) {
        Assert.assertEquals(response.jsonPath().get("data.DiagnosticRetentionLimit"),s);
    }

    public void validateRetentionPeriodSchema() {
        SchemaValidator.validateSchema("diagnostics/diagnostic-retention-period.json");
    }

    public void validateGenerateDiagnostics() {
        SchemaValidator.validateSchema("diagnostics/generate-diagnostic-response.json");
        Assert.assertEquals(response.statusCode(),200);
        Assert.assertNotNull(response.jsonPath().get("data.Name"));
    }

    public void validateDeletedDiagnostic() {
        ArrayList<String> items = response.jsonPath().get("data.DiagnosticsList.Name");
        for(int i=0;i<items.size();i++){
            Assert.assertNotEquals(GenericData.SELECTED_DIAGNOSTIC,items.get(i));
        }
    }
}
