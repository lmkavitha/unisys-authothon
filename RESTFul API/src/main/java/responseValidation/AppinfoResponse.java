package responseValidation;

import data.GenericData;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsCollectionContaining;
import org.testng.Assert;
import requestResponseHandler.MainRequestResponse;

public class AppinfoResponse extends MainRequestResponse{
    public AppinfoResponse(Response response, ResponseSpecBuilder responseSpecBuilder){
        super(responseSpecBuilder,response);
    }

    public void checkTheUserName(String s) {
        Assert.assertEquals(response.jsonPath().get("data.UserName"),s);
    }

    public void checkTheDiagnosticPath(String d) {
        Assert.assertEquals(response.jsonPath().get("data.DiagnosticPath"),d);
    }

    public void checkTheApplicationVersion(String a) {
        Assert.assertEquals(response.jsonPath().get("data.ApplicationVersion"),a);
    }

    public void checkTheBrowserLanguage(String b) {
        Assert.assertEquals(response.jsonPath().get("data.BrowserLanguage"),b);
    }

    public void getToken() {
        response.getBody().prettyPrint();
    }

    public void passBody() {
        requestSpecBuilder.setBody("{\n" +
                "\t\"username\":\"sanket-test\",\n" +
                "\t\"orgName\":\"nutreco\",\n" +
                "\t\"attributes\":[{\"name\":\"org1Admin\",\"value\":\"true\",\"ecert\":true},{\"name\":\"email\",\"value\":\"abc@xyz.com\"}]\n" +
                "} ");
    }
}
