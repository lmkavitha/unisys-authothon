package responseValidation;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import requestResponseHandler.MainRequestResponse;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

/**
 * Simple class that does the schema validations
 */
public class SchemaValidator extends MainRequestResponse {
    public SchemaValidator(Response response, ResponseSpecBuilder responseSpecBuilder){
        super(responseSpecBuilder,response);
    }
    public static void validateSchema(String schema){
        response.then().body(matchesJsonSchemaInClasspath(schema));
    }
}
