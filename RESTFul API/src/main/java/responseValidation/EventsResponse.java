package responseValidation;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.Assert;
import requestResponseHandler.MainRequestResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

public class EventsResponse extends MainRequestResponse {
    public EventsResponse(Response response, ResponseSpecBuilder responseSpecBuilder){
        super(responseSpecBuilder,response);
    }


    public void validateEventsListSchema() {
        SchemaValidator.validateSchema("events/events-list.json");
    }

    public void validateEventsCleared() {
        int value = response.jsonPath().get("data.TotalEventCount");
        assertThat(value,lessThanOrEqualTo(2));
    }
}
