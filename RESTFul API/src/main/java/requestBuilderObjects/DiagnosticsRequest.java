package requestBuilderObjects;

import data.GenericData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import requestResponseHandler.MainRequestResponse;

import java.util.ArrayList;

public class DiagnosticsRequest extends MainRequestResponse {


    /**
     * Constructor for Login requests.
     * This sets up access to functions within the class.
     * Sets requestSpecBuilder and requestSpecification objects.
     */
    public DiagnosticsRequest(RequestSpecBuilder requestSpecBuilder, RequestSpecification requestSpecification) {
        super(requestSpecBuilder,requestSpecification);
    }


    public void setRetentionPeriod() {
        requestSpecBuilder.addFormParam("DiagnosticRetentionLimit",350);
    }

    public void generateDiagnostic(String partition) {
        requestSpecBuilder.addFormParam("PartitionName",partition);
        requestSpecBuilder.addFormParam("Comment","test");
    }

    public void selectDiagnostic() {
        ArrayList<String> items = response.jsonPath().get("data.DiagnosticsList.Name");
        if (items.size()==0){
            GenericData.SELECTED_DIAGNOSTIC ="";
        }
        else {
            GenericData.SELECTED_DIAGNOSTIC = items.get(0);
        }

    }

    public void deleteSelectedDiagnostic() {
        requestSpecBuilder.addFormParam("diaglist",GenericData.SELECTED_DIAGNOSTIC);
    }
}
