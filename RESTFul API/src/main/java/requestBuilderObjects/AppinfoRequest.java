package requestBuilderObjects;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import requestResponseHandler.MainRequestResponse;

public class AppinfoRequest extends MainRequestResponse {

    public AppinfoRequest(RequestSpecBuilder requestSpecBuilder, RequestSpecification requestSpecification) {
        super(requestSpecBuilder,requestSpecification);
    }
}
