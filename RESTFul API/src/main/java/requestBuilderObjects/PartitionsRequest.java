package requestBuilderObjects;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import requestResponseHandler.MainRequestResponse;

public class PartitionsRequest extends MainRequestResponse {


    /**
     * Constructor for Login requests.
     * This sets up access to functions within the class.
     * Sets requestSpecBuilder and requestSpecification objects.
     */
    public PartitionsRequest(RequestSpecBuilder requestSpecBuilder, RequestSpecification requestSpecification) {
        super(requestSpecBuilder,requestSpecification);
    }

    /**
     * Builds the request to login to the STM application
     * @param username
     * @param password
     */
    public void buildLoginRequest(String username,String password){
        requestSpecBuilder.setContentType("application/json");
        requestSpecBuilder.setBody("{\"userName\":\"" + username + "\"," + "\"userPwd\":\""+password+"\"}");
    }

}
