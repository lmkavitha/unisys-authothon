package requestBuilderObjects;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import requestResponseHandler.MainRequestResponse;

public class EventsRequest extends MainRequestResponse {
    public EventsRequest(RequestSpecBuilder requestSpecBuilder, RequestSpecification requestSpecification) {
        super(requestSpecBuilder,requestSpecification);
    }
}
