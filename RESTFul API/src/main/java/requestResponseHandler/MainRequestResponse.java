package requestResponseHandler;

import data.GenericData;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import testbase.CucumberRunnerBase;

import static io.restassured.RestAssured.given;

public class MainRequestResponse {
    /**
     * Below are the variables listed that are used by all the response and request pages
     * Also contains Tracker object instance to track tapes in the STM
     */
    protected static RequestSpecBuilder requestSpecBuilder;
    protected static RequestSpecification requestSpecification;
    protected static ResponseSpecBuilder responseSpecBuilder;
    protected static Response response;


    /**
     * Constructor Function - DO NOT MODIFY
     * @param responseSpecBuilder
     * @param response
     */
    public MainRequestResponse(ResponseSpecBuilder responseSpecBuilder, Response response){
        this.responseSpecBuilder = responseSpecBuilder;
    }

    /**
     * Constructor Function - DO NOT MODIFY
     * @param requestSpecBuilder
     * @param requestSpecification
     */
    public MainRequestResponse(RequestSpecBuilder requestSpecBuilder, RequestSpecification requestSpecification){
        this.requestSpecification = requestSpecification;
        this.requestSpecBuilder = requestSpecBuilder;
    }

    /**
     * Constructor Function - DO NOT MODIFY
     * @param response
     * @param responseSpecBuilder
     * @param requestSpecification
     * @param requestSpecBuilder
     */
    public MainRequestResponse(Response response,ResponseSpecBuilder responseSpecBuilder,RequestSpecification requestSpecification,RequestSpecBuilder requestSpecBuilder )  {
        this.requestSpecification = requestSpecification;
        this.requestSpecBuilder = requestSpecBuilder;
    }

    /**
     * Builds from requestSpecBuilder Object to request object to be make a GET/POST/PUT request
     */
    protected void buildRequest(){
//        requestSpecBuilder.log(LogDetail.ALL);            //Uncomment if you want to log all the requests being made into the console

        requestSpecification = requestSpecBuilder.build();
        requestSpecification = given().spec(requestSpecification.auth().ntlm("Administrator","Administer4Me","",""));
    }

    /**
     * Makes a POST request
     * @param api
     */
    public void makePostRequest(String api){
        buildRequest();
        response = requestSpecification.post(api);
    }

    /**
     * Makes a GET request
     * @param api
     */
    public void makeGetRequest(String api){
        buildRequest();
        response = requestSpecification.get(api);
//        System.out.println(response.getBody().prettyPrint());
    }

    public void makeDeleteRequest(String api){
        buildRequest();
        response=requestSpecification.delete(api);
    }

    public void buildBaseRequest(String type) {
        if (type.equals("json")){
            requestSpecBuilder.setContentType("application/json");
        }
        else if (type.equals("x-www-form-urlencoded")){
            requestSpecBuilder.setContentType("application/x-www-form-urlencoded");
        }
    }
}
