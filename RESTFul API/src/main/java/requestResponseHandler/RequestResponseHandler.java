package requestResponseHandler;

import data.GenericData;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import requestBuilderObjects.AppinfoRequest;
import requestBuilderObjects.AppinfoRequest;
import requestBuilderObjects.DiagnosticsRequest;
import requestBuilderObjects.EventsRequest;
import requestBuilderObjects.PartitionsRequest;
import responseValidation.*;

public class RequestResponseHandler {

    /**
     * Below listed are all the 'feature' objects that are instantiated in the getInstance function
     */
    private PartitionsRequest partitionsRequest;
    private PartitionsResponse partitionsResponse;

    private DiagnosticsRequest diagnosticsRequest;
    private DiagnosticsResponse diagnosticsResponse;



    private EventsRequest eventsRequest;
    private EventsResponse eventsResponse;
    private AppinfoRequest appinfoRequest;
    private AppinfoResponse appinfoResponse;

    private SchemaValidator schemaValidator;


    RequestSpecBuilder requestSpecBuilder;
    RequestSpecification requestSpecification;
    Response response;
    ResponseSpecBuilder responseSpecBuilder;

    /**
     * Creates an instance of all the 'feature' objects
     * @return
     * @throws Exception
     */
    public RequestResponseHandler getInstance() throws Exception {

        //instantiate request spec builder and request specification
        requestSpecBuilder = new RequestSpecBuilder();
        requestSpecification = RestAssured.given();
        responseSpecBuilder = new ResponseSpecBuilder();

        //Instantiate the requesthandler that will have access to all requestBuilderObjects
        RequestResponseHandler requestResponseHandler = new RequestResponseHandler();

        //Instantiate Rest Assured
        RestAssured.baseURI = GenericData.URL;
        RestAssured.useRelaxedHTTPSValidation();

        //Set the requestResponseHandler to contain all the requestBuilderObjects
        requestResponseHandler.setRequestSpecBuilder(requestSpecBuilder);
        requestResponseHandler.setRequestSpecification(requestSpecification);
        requestResponseHandler.setResponse(response);
        requestResponseHandler.setResponseSpecBuilder(responseSpecBuilder);

        //Below statements initialize request and response objects for those particular features
        requestResponseHandler.setPartitionsRequest(new PartitionsRequest(getRequestSpecBuilder(),getRequestSpecification()));
        requestResponseHandler.setPartitionsResponse(new PartitionsResponse(getResponse(),getResponseSpecBuilder()));

        requestResponseHandler.setDiagnosticsRequest(new DiagnosticsRequest(getRequestSpecBuilder(),getRequestSpecification()));
        requestResponseHandler.setDiagnosticsResponse(new DiagnosticsResponse(getResponse(),getResponseSpecBuilder()));

        requestResponseHandler.setEventsRequest(new EventsRequest(getRequestSpecBuilder(),getRequestSpecification()));
        requestResponseHandler.setEventsResponse(new EventsResponse(getResponse(),getResponseSpecBuilder()));



        requestResponseHandler.setAppinfoRequest(new AppinfoRequest(getRequestSpecBuilder(),getRequestSpecification()));
        requestResponseHandler.setAppinfoResponse(new AppinfoResponse(getResponse(),getResponseSpecBuilder()));
        //Returns the instantiated requestResponse handler for all the Step defs files to use
        return requestResponseHandler;
    }

    private void setDiagnosticsRequest(DiagnosticsRequest diagnosticsRequest) {
        this.diagnosticsRequest = diagnosticsRequest;
    }
    private void setDiagnosticsResponse(DiagnosticsResponse diagnosticsResponse){
        this.diagnosticsResponse = diagnosticsResponse;
    }

    private void setEventsRequest(EventsRequest eventsRequest) {
        this.eventsRequest = eventsRequest;
    }
    private void setEventsResponse(EventsResponse eventsResponse) {
        this.eventsResponse = eventsResponse;
    }

    private void setAppinfoRequest(AppinfoRequest appinfoRequest) {
        this.appinfoRequest = appinfoRequest;
    }
    private void setAppinfoResponse(AppinfoResponse appinfoResponse) {
        this.appinfoResponse = appinfoResponse;
    }
        /**
         * Sets the responseSpecBuilder Object
         * @param responseSpecBuilder
         */
        private void setResponseSpecBuilder (ResponseSpecBuilder responseSpecBuilder){
            this.responseSpecBuilder = responseSpecBuilder;
        }

        /**
         * Gets the responseSpecBuilder Object
         * @return
         */
        public ResponseSpecBuilder getResponseSpecBuilder () {
            return this.responseSpecBuilder;
        }


        /**
         * Gets the set response object
         * @return Response
         */
        public Response getResponse () {
            return this.response;
        }

        /**
         * Sets the response object for an entire lifecycle of a scenario
         * @param response
         */
        private void setResponse (Response response){
            this.response = response;
        }


        /**
         * Sets the RequestSpecBuilder Object
         * @param request
         */
        private void setRequestSpecBuilder (RequestSpecBuilder request){
            this.requestSpecBuilder = request;
        }

        /**
         * Gets the RequestSpecBuilder Object
         * @return RequestSpecBuilder
         */
        public RequestSpecBuilder getRequestSpecBuilder () {
            return requestSpecBuilder;
        }

        /**
         * Gets the RequestSpecification Object
         * @return RequestSpecification
         */
        public RequestSpecification getRequestSpecification () {
            return requestSpecification;
        }

        /**
         * Sets the RequestSpecification Object
         * @param requestSpecification
         */
        private void setRequestSpecification (RequestSpecification requestSpecification){
            this.requestSpecification = requestSpecification;
        }

        /**
         * Sets the PartitionsRequest Object
         * @param partitionsRequest
         */
        private void setPartitionsRequest (PartitionsRequest partitionsRequest){
            this.partitionsRequest = partitionsRequest;
        }
        /**
         * Sets the PartitionsResponse object
         * @param partitionsResponse
         */
        private void setPartitionsResponse (PartitionsResponse partitionsResponse){
            this.partitionsResponse = partitionsResponse;
        }

        /**
         * Gets the PartitionsResponse Object
         * @return PartitionsResponse
         */
        public PartitionsResponse partitionsResponse () {
            return this.partitionsResponse;
        }
        /**
         * Gets the partitionsRequest 'feature' Object
         * @return PartitionsRequest
         */
        public PartitionsRequest partitionsRequest () {
            return this.partitionsRequest;
        }

        public DiagnosticsRequest diagnosticsRequest () {
            return this.diagnosticsRequest;
        }
        public DiagnosticsResponse diagnosticsResponse () {
            return this.diagnosticsResponse;
        }

        public EventsRequest eventsRequest () {
            return this.eventsRequest;
        }
        public EventsResponse eventsResponse () {
            return this.eventsResponse;
        }

        public AppinfoRequest appinfoRequest () {
        return this.appinfoRequest;
    }
        public AppinfoResponse appinfoResponse () {
        return this.appinfoResponse;
    }
        /**
         * Sets the schemValidator object
         * @param schemaValidator
         */
        private void setSchemaValidator (SchemaValidator schemaValidator){
            this.schemaValidator = schemaValidator;
        }

        /**
         * Gets the SchemaValidator Object
         * @return SchemaValidator
         */
        public SchemaValidator schemaValidator () {
            return this.schemaValidator;
        }


    }

