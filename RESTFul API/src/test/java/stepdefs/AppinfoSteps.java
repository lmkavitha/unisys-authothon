package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.APIList;
import testbase.CucumberRunner;

public class AppinfoSteps extends CucumberRunner{
    public AppinfoSteps() throws Exception{}

    @Given("I have the API to get the information on DCS")
    public void prepare_appinfo_api(){
        requestResponseHandler.appinfoRequest().buildBaseRequest("json");
    }

    @When("I make the request to get the information on DCS")
    public void iMakeTheRequestForAppInfo() throws Throwable {
        requestResponseHandler.appinfoRequest().makeGetRequest(APIList.APPINFO_LIST);
    }

    @Then("I should get the username as \"([^\"]*)\" and diagnosticspath as \"([^\"]*)\" and applicationversion as \"([^\"]*)\" and Browserlanguage as \"([^\"]*)\"")
    public void validateTheResponseForAppInfo(String uname, String diagPath, String appVersion, String browserLang) throws Throwable {
        requestResponseHandler.appinfoResponse().checkTheUserName(uname);
        requestResponseHandler.appinfoResponse().checkTheDiagnosticPath(diagPath);
        requestResponseHandler.appinfoResponse().checkTheApplicationVersion(appVersion);
        requestResponseHandler.appinfoResponse().checkTheBrowserLanguage(browserLang);
    }

    @Given("^I have the API to make a call to the CA$")
    public void iHaveTheAPIToMakeACallToTheCA() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        requestResponseHandler.appinfoRequest().buildBaseRequest("json");
        requestResponseHandler.appinfoResponse().passBody();

    }

    @When("^I make a call to the CA$")
    public void iMakeACallToTheCA() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        requestResponseHandler.appinfoRequest().makePostRequest(APIList.CA_ENROLLMENT);

    }

    @Then("^I should get back the token$")
    public void iShouldGetBackTheToken() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        requestResponseHandler.appinfoResponse().getToken();

    }
}
