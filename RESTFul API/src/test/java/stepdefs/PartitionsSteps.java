package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.APIList;
import data.GenericData;
import org.testng.Assert;
import testbase.CucumberRunner;

public class PartitionsSteps extends CucumberRunner {
    public PartitionsSteps() throws Exception {
    }

    @Given("I have the API for list of partitions")
    public void prepare_partition_list_api(){
        requestResponseHandler.partitionsRequest().buildBaseRequest("json");
    }
    @When("^I make the request for list of partitions$")
    public void iMakeTheRequestForListOfPartitions() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        requestResponseHandler.partitionsResponse().makeGetRequest(APIList.PARTITION_LIST);
    }
    @Then("I should get \"([^\"]*)\" and \"([^\"]*)\" as the partitions")
    public void fill_username_password(String partition1,String partition2){
        requestResponseHandler.partitionsResponse().validatePartitionList(partition1,partition2);
    }

}