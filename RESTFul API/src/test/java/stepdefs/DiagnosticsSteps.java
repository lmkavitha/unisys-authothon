package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.APIList;
import data.GenericData;
import requestBuilderObjects.DiagnosticsRequest;
import testbase.CucumberRunner;

public class DiagnosticsSteps extends CucumberRunner {
    public DiagnosticsSteps() throws Exception{}

    @Given("I have the API for list of diagnostics")
    public void prepare_partition_list_api(){
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("json");
    }

    @When("^I make the request for list of diagnostics$")
    public void iMakeTheRequestForListOfDiagnostics() throws Throwable {
        requestResponseHandler.diagnosticsRequest().makeGetRequest(APIList.DIAGNOSTIC_LIST);
    }

    @Then("^Validate the schema for the list of diagnostics$")
    public void validateTheSchemaForTheListOfDiagnostics() throws Throwable {
        requestResponseHandler.diagnosticsResponse().validateDiagnosticListSchema();
    }

    @Given("^I have the \"([^\"]*)\" API for retention period of diagnostics$")
    public void iHaveTheAPIForRetentionPeriodOfDiagnostics(String contentType) throws Throwable {
        if (contentType.equals("POST")){
            requestResponseHandler.diagnosticsRequest().buildBaseRequest("x-www-form-urlencoded");
        }
        else if (contentType.equals("GET")){
            requestResponseHandler.diagnosticsRequest().buildBaseRequest("json");
        }
    }

    @When("^I make the request to set the diagnostic retention period$")
    public void iMakeTheRequestToSetTheDiagnosticRetentionPeriod() throws Throwable {
        requestResponseHandler.diagnosticsRequest().setRetentionPeriod();
        requestResponseHandler.diagnosticsRequest().makePostRequest(APIList.SET_RETENTION_PERIOD);
    }

    @And("^I make the request to get the diagnostic retention period$")
    public void iMakeTheRequestToGetTheDiagnosticRetentionPeriod() throws Throwable {
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("json");
        requestResponseHandler.diagnosticsRequest().makeGetRequest(APIList.GET_RETENTION_PERIOD);
    }

    @Then("^Validate the set retention period$")
    public void validateTheSetRetentionPeriod() throws Throwable {
        requestResponseHandler.diagnosticsResponse().checkTheSetRetentionPeriod("350");
    }

    @Then("^Validate the schema for the diagnostic retention period$")
    public void validateTheSchemaForTheDiagnosticRetentionPeriod() throws Throwable {
        requestResponseHandler.diagnosticsResponse().validateRetentionPeriodSchema();
    }

    @Given("^i have the API to generate a diagnostic$")
    public void iHaveTheAPIToGenerateADiagnostic() throws Throwable {
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("x-www-form-urlencoded");
    }

    @When("^I make the request to generate the diagnostic for \"([^\"]*)\"$")
    public void iMakeTheRequestToGenerateTheDiagnosticFor(String partition) throws Throwable {
    requestResponseHandler.diagnosticsRequest().generateDiagnostic(partition);
        requestResponseHandler.diagnosticsRequest().makePostRequest(APIList.GENERATE_DIAGNOSTIC);
    }

    @Then("^Validate if the diagnostic is generated$")
    public void validateIfTheDiagnosticIsGenerated() throws Throwable {
requestResponseHandler.diagnosticsResponse().validateGenerateDiagnostics();
    }


    @Given("^i have the API to delete a diagnostic$")
    public void iHaveTheAPIToDeleteADiagnostic() throws Throwable {
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("x-www-form-urlencoded");
        requestResponseHandler.diagnosticsRequest().makeGetRequest(APIList.DIAGNOSTIC_LIST);
        requestResponseHandler.diagnosticsRequest().selectDiagnostic();
    }

    @When("^I make the request to delete a diagnostic$")
    public void iMakeTheRequestToDeleteADiagnostic() throws Throwable {
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("x-www-form-urlencoded");
        requestResponseHandler.diagnosticsRequest().deleteSelectedDiagnostic();
        requestResponseHandler.diagnosticsRequest().makeDeleteRequest(APIList.DELETE_DIAGNOSTIC);
    }

    @Then("^Validate if the diagnostic is deleted$")
    public void validateIfTheDiagnosticIsDeleted() throws Throwable {
        requestResponseHandler.diagnosticsRequest().buildBaseRequest("x-www-form-urlencoded");
        requestResponseHandler.diagnosticsRequest().makeGetRequest(APIList.DIAGNOSTIC_LIST);
        requestResponseHandler.diagnosticsResponse().validateDeletedDiagnostic();
    }
}
