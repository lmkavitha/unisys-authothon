package stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.APIList;
import testbase.CucumberRunner;

public class EventsSteps extends CucumberRunner{
    public EventsSteps() throws Exception{}

    @Given("I have the API for listing of the events")
    public void prepare_events_list_api(){
        requestResponseHandler.eventsRequest().buildBaseRequest("json");
    }

    @When("I make the request for listing of the events")
    public void iMakeTheRequestForListOfEvents() throws Throwable {
        requestResponseHandler.eventsRequest().makeGetRequest(APIList.EVENTS_LIST);
    }

    @Then("Validate the schema for the list of events")
    public void validateTheSchemaForTheListOfEvents() throws Throwable {
        requestResponseHandler.eventsResponse().validateEventsListSchema();
    }

    @Given("^I have the API to clear all events$")
    public void iHaveTheAPIToClearAllEvents() throws Throwable {
        requestResponseHandler.eventsRequest().buildBaseRequest("json");
    }

    @When("^I make the request to clear all events$")
    public void iMakeTheRequestToClearAllEvents() throws Throwable {
        requestResponseHandler.eventsRequest().makeDeleteRequest(APIList.CLEAR_EVENTS);
    }

    @Then("^Validate if all events are cleared$")
    public void validateIfAllEventsAreCleared() throws Throwable {
        requestResponseHandler.eventsRequest().makeGetRequest(APIList.EVENTS_LIST);
        requestResponseHandler.eventsResponse().validateEventsCleared();
    }
}
