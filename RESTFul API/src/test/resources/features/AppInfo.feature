Feature: AppInfo

  Scenario: To get the information about DCS application
    Given I have the API to get the information on DCS
    When  I make the request to get the information on DCS
    Then  I should get the username as "Administrator" and diagnosticspath as "C:\DiagCollection" and applicationversion as "2.0.6.0" and Browserlanguage as "en-US"
