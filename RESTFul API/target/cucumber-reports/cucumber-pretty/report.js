$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/AppInfo.feature");
formatter.feature({
  "name": "AppInfo",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "To get the information about DCS application",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I have the API to get the information on DCS",
  "keyword": "Given "
});
formatter.match({
  "location": "AppinfoSteps.prepare_appinfo_api()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I make the request to get the information on DCS",
  "keyword": "When "
});
formatter.match({
  "location": "AppinfoSteps.iMakeTheRequestForAppInfo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I should get the username as \"Administrator\" and diagnosticspath as \"C:\\DiagCollection\" and applicationversion as \"2.0.6.0\" and Browserlanguage as \"en-US\"",
  "keyword": "Then "
});
formatter.match({
  "location": "AppinfoSteps.validateTheResponseForAppInfo(String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
});