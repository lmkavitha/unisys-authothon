package webtest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.SendKeysAction;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.TapOptions;

public class Emulatorstuffs {
	static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest test;
	static AppiumDriver<MobileElement> driver;
	public static String ResultLocation=System.getProperty("user.dir")+"\\test-output\\TestResults\\";

	@BeforeTest
	public void BeforeTest() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "UniAndroid");
		caps.setCapability("udid", "emulator-5554"); //Give Device ID of your mobile phone
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "9");
		caps.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");
//		caps.setCapability("appPackage", "com.google.android.apps.nexuslauncher");
		
//		caps.setCapability("appActivity", "com.google.android.apps.nexuslauncher.NexusLauncherActivity");
		
		try {
			html = new ExtentHtmlReporter(ResultLocation+"TestAutomation.html");
			System.out.println("Path of user DIR"+ResultLocation+"TestAutomation.html");
			extent=new ExtentReports();
			extent.attachReporter(html);
			test=extent.createTest("Test Autothon Script","Description");
			
			driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);			
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			System.out.println("application statred");
			test.log(Status.INFO, "Message: application statred.");

		} catch (MalformedURLException e) {
			test.log(Status.INFO, "Message: "+ e.getMessage());
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void AutoTest() {		
		System.out.println("auto script started.");
		driver.get("https://www.youtube.com");
//		driver.get("https://m.youtube.com/#searching");
		waits(10000);
		System.out.println("youtube opened");
		test.log(Status.INFO, "Message: youtube opened.");
		String val="step-in forum";
		driver.findElement(By.cssSelector("#header-bar > header > div > button > c3-icon > svg")).click();
		System.out.println(val + "entered");
		test.log(Status.INFO, "Message: "+val + "entered");
		
		driver.findElement(By.cssSelector("#header-bar > header > ytm-searchbox > form > div > input")).sendKeys(val);		
		test.log(Status.INFO, "Message: input entered.");
		System.out.println("Clicked 1");
		driver.findElement(By.cssSelector("#header-bar > header > ytm-searchbox > form > div > input")).sendKeys(Keys.ENTER);
		test.log(Status.INFO, "Message: clicked entered.");
		System.out.println("Clicked enter.");
		driver.findElement(By.xpath("//*[@id=\"app\"]/div[3]/ytm-search/ytm-section-list-renderer/lazy-list/ytm-item-section-renderer/lazy-list/ytm-compact-channel-renderer[1]/div/div/a/h4")).click();
		System.out.println("Clicked step-in forum link");
		test.log(Status.INFO, "Message: Clicked step-in forum link.");
		driver.findElement(By.cssSelector("#app > div:nth-child(3) > ytm-browse > ytm-single-column-browse-results-renderer > div.scbrr-tabs.cbox > a:nth-child(2)")).click();	
		System.out.println("Clicked video tab");
		test.log(Status.INFO, "Message: Clicked video tab");
		waits(3000);
		
		//5,6
		String Val2="Docker\\u2013Grid (A On demand and Scalable dockerized selenium grid architecture)";
//		driver.findElement(By.cssSelector("#header-bar > header > div > button > c3-icon > svg")).click();
		
		
		
		try {
			File file1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(file1,new File("C:/temp/Screenshot.jpg"));
			test.log(Status.INFO, "Message: screenshot taken");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.findElement(By.cssSelector("#app > div:nth-child(3) > ytm-browse > ytm-single-column-browse-results-renderer > div:nth-child(2) > div:nth-child(2) > ytm-section-list-renderer > lazy-list > ytm-item-section-renderer > lazy-list > ytm-compact-video-renderer:nth-child(1) > div > div > a > h4")).click();
		test.log(Status.INFO, "Message: clciked on video");
		
		
		
		
		System.out.println("Test One Completed.");
		test.log(Status.INFO, "Message: video played.");
	}

	


	public void waits(int num) {
		try {
			Thread.sleep(num);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		
		public static void APIGetCall() throws IOException 
		{		
//		Response video1 = BasicApiFunctions.GetVideo().get();
//		String responstext = video1.getBody().asString();
//			String responstext="";
//		BasicApiFunctions.printResponse(video1);
//		Config.setProperty("videoname", responstext);
//		
//		File file = new File(projDir+"\\src\\main\\resources\\configuration\\Config.properties");
//		FileOutputStream fos =  new  FileOutputStream(file);
//		Config.store(fos, "videoname");
		}
	
	
	@AfterTest
	public void cleanup() {
		
		extent.flush();
		driver.quit();
	}
}
