package webtest;

import java.net.URL;


import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseCls {
		static AppiumDriver<MobileElement> driver;
//	static AndroidDriver<MobileElement> driver;

	@BeforeTest
	public void SetUp()
	{
		try {
			DesiredCapabilities caps=new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
			caps.setCapability(MobileCapabilityType.VERSION, "7");
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "Galaxy On8");
			caps.setCapability(MobileCapabilityType.UDID, "192.168.100.2:2222");
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 7200);
			//		caps.setCapability(MobileCapabilityType.APP, "ANDROID");
			caps.setCapability(MobileCapabilityType.BROWSER_NAME, "chrome");	

			URL url=new URL("http://0.0.0.0:4723/wd/hub");			
			driver = new AndroidDriver<MobileElement>(url, caps);
			//			driver = new AppiumDriver<MobileElement>(url, caps);
			System.out.println("application started");

		} catch (Exception e) {
			System.out.println("error cause : "+e.getCause());
			e.printStackTrace();
		}

	}

	@Test
	public void TestOne() {
		System.out.println("Chrome opened.");
	}


	@AfterTest
	public void TearDown()
	{
		driver.quit();
	}
}
