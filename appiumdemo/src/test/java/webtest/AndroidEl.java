package webtest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

public class AndroidEl {

	static AppiumDriver<MobileElement> driver;
	
	@BeforeTest
	public void beforetest() throws MalformedURLException {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Galaxy On8");
		caps.setCapability("udid", "192.168.100.2:2222");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "7.0");
		caps.setCapability("appPackage", "com.android.vending");
		caps.setCapability("appActivity", "com.android.vending.AssetBrowserActivity");
		
		URL url=new URL("http://127.0.0.1:4723/wd/hub");
		
		driver=new AppiumDriver<MobileElement>(url,caps);
		System.out.println("application opened");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void Test1() {
		System.out.println("paly store");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		System.out.println("objects loaded");
		driver.findElementById("com.android.vending:id/search_box_idle_text").sendKeys("Score");
		
	}
	
	
	
	
	@AfterTest
	public void shutdown() {
		driver.quit();
		System.out.println("app shut down.");
	}
}
